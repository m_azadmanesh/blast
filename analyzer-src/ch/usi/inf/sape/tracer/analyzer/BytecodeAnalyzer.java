package ch.usi.inf.sape.tracer.analyzer;

import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeSessionManager;

public interface BytecodeAnalyzer {
	
	public void analyze(BytecodeSessionManager session);
}
