package ch.usi.inf.sape.tracer.analyzer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.rmi.Naming;
import java.util.List;
import java.util.Stack;

import ch.usi.inf.sape.tracer.analyzer.abstractops.SourceLineOperation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.ANEWARRAY_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.ARRAYLENGTH_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.ATHROW_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BIPUSH_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BasicBlockEntry_PseudoOperation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.Binary_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent.Condition;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeSessionManager;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.CHECKCAST_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.CatchBlock_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_1_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_2_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_X1_1_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_X1_2_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_X2_1_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_X2_2_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_X2_3_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_X2_4_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP_X1_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP_X2_1_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP_X2_2_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.Division_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.GETFIELD_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.GETSTATIC_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.GOTO_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.IF_ACMP_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.IF_ICMP_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.IF_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.IINC_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.INSTANCEOF_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.INVOKEARGS_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.INVOKERETURNING_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.LOOKUPSWITCH_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.MONITORENTER_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.MONITOREXIT_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.MULTIANEWARRAY_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.NEWARRAY_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.NEW_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.PUTFIELD_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.PUTSTATIC_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.Rem_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.SIPUSH_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.SWAP_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.TABLESWITCH_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.Unary_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XALOAD_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XASTORE_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XCONST_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XLDC_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XLOAD_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XPOP_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XRETURN_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XSTORE_Operation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

/**
 * The Analyzer is called from the instrumentation produced by the Transformer.
 * 
 * This Analyzer has one public static method for each bytecode instruction.
 * It has two methods for the INVOKE* instructions: 
 * INVOKE*_calling is called before the INVOKE instruction,
 * INVOKE*_returning is called after.
 * It also has a methodEntry() method, called at the entry to every method.
 * This way, each method call is represented by four calls to the analyzer:
 * - INVOKE*_calling()
 * - methodEntry() 
 * - *RETURN()
 * - INVOKE_returning()
 * This allows to tie together the flow of arguments from caller to callee,
 * and the flow of the return value from the callee back to the caller.
 */
public class Analyzer {
	
	public static final boolean DO_ABSTRACTION = true; 
	public static final boolean REMOTELY_AVAILABLE = false;
	public static final boolean DO_LOG = false;
	private static Stack<Integer> bbs = new Stack<>();
	/**
	 * This field is used in control dependency analysis to distinguish 
	 * among different inovcations.
	 */
	private static int nextInvocationId = -1;
	
	static{
		//This is initialized to -1 because nobody invokes the main method
		bbs.push(nextInvocationId --);
	}
	
	/**
	 * @param name
	 * @param maxLocalArraySize
	 * @param localDetails
	 * @param maxStack
	 * @param argsCount
	 * @param values
	 * @param isStatic
	 * @param sourceFile
	 */
	public static void methodEntry(final String name, 
									final int maxLocalArraySize, 
									final String localDetails, 
									final int maxStack, 
									final int argsCount, 
									final Object[] values, 
									final boolean isStatic,
									final String sourceFile) {
		log("Method entry: "+name, true);
		
		ThreadCallStack tcs = ThreadCallStack.getCurrentThreadCallStack();
		
		/* If an already thrown exception is caught in an untraced method, 
		 * nobody sets the thrown flag to false and this can be misleading 
		 * in case the untraced method calls another traced method again. 
		 * So we explicitly set this flag to false at this point to remove 
		 * the possibility of such a case.
		 */
		tcs.setExceptionCaught();		
		
		INVOKEARGS_Operation op;
		if (tcs.isCallerInstrumented(name, argsCount, isStatic)){
			Activation currentMethodFrame = tcs.peekTopMostFrame();
			currentMethodFrame.setOwnerClass(Utils.findClassName(name));   		// set to the runtime type of the object
			op = currentMethodFrame.getProvider();
		}else{
			op = new INVOKEARGS_Operation(-1, name, argsCount, isStatic, false,"INVOKEUNKNOWN");
			tcs.addOperation(op);
		}
		
		tcs.peekTopMostFrame().setSourceFile(sourceFile);
		
		op.def(values, name, maxLocalArraySize, localDetails, maxStack, argsCount, isStatic);
		log(op.toString());
	}
	
	public static void ILOAD(int local, int instruction) {
		XLOAD_Operation op = new XLOAD_Operation(OperationCodes.ILOAD, local, MemoryLocationInfo.Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LLOAD(int local, int instruction) {
		XLOAD_Operation op = new XLOAD_Operation(OperationCodes.LLOAD, local, MemoryLocationInfo.Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void FLOAD(int local, int instruction) {
		XLOAD_Operation op = new XLOAD_Operation(OperationCodes.FLOAD, local, MemoryLocationInfo.Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void DLOAD(int local, int instruction) {
		XLOAD_Operation op = new XLOAD_Operation(OperationCodes.DLOAD, local, MemoryLocationInfo.Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ALOAD(int local, int instruction) {
		XLOAD_Operation op = new XLOAD_Operation(OperationCodes.ALOAD, local, MemoryLocationInfo.Type.REFERENCE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ISTORE(int local, int instruction, int bcIndex) {
		XSTORE_Operation op = new XSTORE_Operation(OperationCodes.ISTORE, local, MemoryLocationInfo.Type.INT, instruction, bcIndex);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LSTORE(int local, int instruction, int bcIndex) {
		XSTORE_Operation op = new XSTORE_Operation(OperationCodes.LSTORE, local, MemoryLocationInfo.Type.LONG, instruction, bcIndex);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}	
	
	public static void FSTORE(int local, int instruction, int bcIndex) {
		XSTORE_Operation op = new XSTORE_Operation(OperationCodes.FSTORE, local, MemoryLocationInfo.Type.FLOAT, instruction, bcIndex);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void DSTORE(int local, int instruction, int bcIndex) {
		XSTORE_Operation op = new XSTORE_Operation(OperationCodes.DSTORE, local, MemoryLocationInfo.Type.DOUBLE, instruction, bcIndex);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void ASTORE(int local, int instruction, int bcIndex) {
		XSTORE_Operation op = new XSTORE_Operation(OperationCodes.ASTORE, local, MemoryLocationInfo.Type.REFERENCE, instruction, bcIndex);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ACONST_NULL(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.ACONST_NULL, Value.NULL, MemoryLocationInfo.Type.REFERENCE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ICONST_M1(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.ICONST_M1, new Value(-1), MemoryLocationInfo.Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ICONST_0(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.ICONST_0, new Value(0), MemoryLocationInfo.Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ICONST_1(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.ICONST_1, new Value(1), MemoryLocationInfo.Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ICONST_2(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.ICONST_2, new Value(2), MemoryLocationInfo.Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ICONST_3(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.ICONST_3, new Value(3), MemoryLocationInfo.Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ICONST_4(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.ICONST_4, new Value(4), MemoryLocationInfo.Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ICONST_5(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.ICONST_5, new Value(5), MemoryLocationInfo.Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LCONST_0(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.LCONST_0, new Value(0L), MemoryLocationInfo.Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LCONST_1(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.LCONST_1, new Value(1L), MemoryLocationInfo.Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void FCONST_0(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.FCONST_0, new Value(0F), MemoryLocationInfo.Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void FCONST_1(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.FCONST_1, new Value(1F), MemoryLocationInfo.Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void FCONST_2(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.FCONST_2, new Value(2F), MemoryLocationInfo.Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void DCONST_0(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.DCONST_0, new Value(0.0), MemoryLocationInfo.Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void DCONST_1(int instruction) {
		XCONST_Operation op = new XCONST_Operation(OperationCodes.DCONST_1, new Value(1.0), MemoryLocationInfo.Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IALOAD(int[] array, int index, int instruction) {
		Value valueObject = null;
		boolean accessVerified = array != null && index >=0 && index < array.length;  
		if (accessVerified){    //emulate the behavior of runtime verifier before accessing an array element
			valueObject = new Value(array[index]);
		}
		XALOAD_Operation op = new XALOAD_Operation(OperationCodes.IALOAD, array, valueObject, index, accessVerified, Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LALOAD(long[] array, int index, int instruction) {
		Value valueObject = null;
		boolean accessVerified = array != null && index >=0 && index < array.length;  
		if (accessVerified){    //emulate the behavior of runtime verifier before accessing an array element
			valueObject = new Value(array[index]);
		}
		XALOAD_Operation op = new XALOAD_Operation(OperationCodes.LALOAD, array, valueObject, index, accessVerified, Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void FALOAD(float[] array, int index, int instruction) {
		Value valueObject = null;
		boolean accessVerified = array != null && index >=0 && index < array.length;  
		if (accessVerified){    //emulate the behavior of runtime verifier before accessing an array element
			valueObject = new Value(array[index]);
		}
		XALOAD_Operation op = new XALOAD_Operation(OperationCodes.FALOAD, array, valueObject, index, accessVerified, Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void DALOAD(double[] array, int index, int instruction) {
		Value valueObject = null;
		boolean accessVerified = array != null && index >=0 && index < array.length;  
		if (accessVerified){    //emulate the behavior of runtime verifier before accessing an array element
			valueObject = new Value(array[index]);
		}
		XALOAD_Operation op = new XALOAD_Operation(OperationCodes.DALOAD, array, valueObject, index, accessVerified, Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void AALOAD(Object[] array, int index, int instruction) {
		Value valueObject = null;
		boolean accessVerified = array != null && index >=0 && index < array.length;  
		if (accessVerified){    //emulate the behavior of runtime verifier before accessing an array element
			valueObject = Utils.getValueForObject(array[index]);
		}
		XALOAD_Operation op = new XALOAD_Operation(OperationCodes.AALOAD, array, valueObject, index, accessVerified, Type.REFERENCE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void BALOAD(Object obj, int index, int instruction) {
		// BALOAD is used for both byte[] and boolean[]; 
		Value valueObject = null;
		boolean accessVerified = obj != null && index >=0 && index < Array.getLength(obj);
		XALOAD_Operation op;
		if (obj instanceof byte[]){
			byte[] array = (byte[]) obj;
			if (accessVerified){    //emulate the behavior of runtime verifier before accessing an array element
				valueObject = new Value(array[index]);
			}
			op = new XALOAD_Operation(OperationCodes.BALOAD, obj, valueObject, index, accessVerified, Type.BYTE, instruction);
		}else{
			boolean[] array = (boolean[]) obj;
			if (accessVerified){    //emulate the behavior of runtime verifier before accessing an array element
				valueObject = new Value(array[index]);
			}
			op = new XALOAD_Operation(OperationCodes.BALOAD, obj, valueObject, index, accessVerified, Type.BOOLEAN, instruction);
		}
		
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);	
	}
	
	public static void CALOAD(char[] array, int index, int instruction) {
		Value valueObject = null;
		boolean accessVerified = array != null && index >=0 && index < array.length;  
		if (accessVerified){    //emulate the behavior of runtime verifier before accessing an array element
			valueObject = new Value(array[index]);
		}
		XALOAD_Operation op = new XALOAD_Operation(OperationCodes.CALOAD, array, valueObject, index, accessVerified, Type.CHAR, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void SALOAD(short[] array, int index, int instruction) {
		Value valueObject = null;
		boolean accessVerified = array != null && index >=0 && index < array.length;  
		if (accessVerified){    //emulate the behavior of runtime verifier before accessing an array element
			valueObject = new Value(array[index]);
		}
		XALOAD_Operation op = new XALOAD_Operation(OperationCodes.SALOAD, array, valueObject, index, accessVerified, Type.SHORT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IASTORE(int[] array, int index, int instruction) {
		boolean accessVerified;
		if (array != null && index < array.length && index >=0)
			accessVerified = true;
		else
			accessVerified = false;
		
		XASTORE_Operation op = new XASTORE_Operation(OperationCodes.IASTORE, array, index, accessVerified, Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LASTORE(long[] array, int index, int instruction) {
		boolean accessVerified;
		if (array != null && index < array.length && index >=0)
			accessVerified = true;
		else
			accessVerified = false;
		
		XASTORE_Operation op = new XASTORE_Operation(OperationCodes.LASTORE, array, index, accessVerified, Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void FASTORE(float[] array, int index, int instruction) { 
		boolean accessVerified;
		if (array != null && index < array.length && index >=0)
			accessVerified = true;
		else
			accessVerified = false;
		
		XASTORE_Operation op = new XASTORE_Operation(OperationCodes.FASTORE, array, index, accessVerified, Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void DASTORE(double[] array, int index, int instruction) { 
		boolean accessVerified;
		if (array != null && index < array.length && index >=0)
			accessVerified = true;
		else
			accessVerified = false;
		
		XASTORE_Operation op = new XASTORE_Operation(OperationCodes.DASTORE, array, index, accessVerified, Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void AASTORE(Object[] array, int index, int instruction) { 
		boolean accessVerified;
		if (array != null && index < array.length && index >=0)
			accessVerified = true;
		else
			accessVerified = false;
		
		XASTORE_Operation op = new XASTORE_Operation(OperationCodes.AASTORE, array, index, accessVerified, Type.REFERENCE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void BASTORE(Object array, int index, int instruction) {
		//bastore could be used for both booleans and bytes
		
		boolean accessVerified;
		if (array != null && index < Array.getLength(array) && index >=0)
			accessVerified = true;
		else
			accessVerified = false;
		
		if (array instanceof byte[]){
			XASTORE_Operation op = new XASTORE_Operation(OperationCodes.BASTORE, array, index, accessVerified, Type.BYTE, instruction);
			ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
		}
		else{
			XASTORE_Operation op = new XASTORE_Operation(OperationCodes.BASTORE, array, index, accessVerified, Type.BOOLEAN, instruction);
			ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
		}
	}
	
	public static void CASTORE(char[] array, int index, int instruction) {
		boolean accessVerified;
		if (array != null && index < array.length && index >=0)
			accessVerified = true;
		else
			accessVerified = false;
		
		XASTORE_Operation op = new XASTORE_Operation(OperationCodes.CASTORE, array, index, accessVerified, Type.CHAR, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void SASTORE(short[] array, int index, int instruction) {
		boolean accessVerified;
		if (array != null && index < array.length && index >=0)
			accessVerified = true;
		else
			accessVerified = false;
		
		XASTORE_Operation op = new XASTORE_Operation(OperationCodes.SASTORE, array, index, accessVerified, Type.SHORT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}


	public static void POP(int instruction) {
		XPOP_Operation op = new XPOP_Operation(OperationCodes.POP, 1, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void POP2(int instruction) {
		XPOP_Operation op = new XPOP_Operation(OperationCodes.POP2, 2, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void DUP(int instruction) {
		DUP_Operation op = new DUP_Operation(OperationCodes.DUP, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	/**
	 * Form: value2, value1 -> value1, value2, value1
	 * value1 and value2 are values of category1 computational type
	 * 
	 * @param value1
	 * @param value2
	 * @param instruction
	 */
	public static void DUP_X1(int instruction) {  	
		DUP_X1_Operation op = new DUP_X1_Operation(OperationCodes.DUP_X1, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	/**
	 * Form 1: value3, value2, value1 -> value1, value3, value2, value1    (all values are of category 1 computational type)				  
	 * 
	 * @param value1
	 * @param value2
	 * @param instruction
	 */
	public static void DUP_X2_1(int instruction) {    
		DUP_X2_1_Operation op = new DUP_X2_1_Operation(OperationCodes.DUP_X2, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	
	/**
	 * Form 2: value2, value1  ->  value1, value2, value1				  (value1 is a value of a category 1 computational type and value2 is a value of a category 2 computational type)
	 *  
	 * @param instruction
	 */
	public static void DUP_X2_2(int instruction) {    
		DUP_X2_2_Operation op = new DUP_X2_2_Operation(OperationCodes.DUP_X2, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	/**
	 * Form1: value2, value1   ->   value2, value1, value2, value1       where both value1 and value2 are values of a category 1 computational type               
	 * 
	 * @param value2
	 * @param value1
	 * @param instruction
	 */
	public static void DUP2_1(int instruction) {
		DUP2_1_Operation op = new DUP2_1_Operation(OperationCodes.DUP2, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	/**
	 * Form2: value  -> value, value        where value is a value of a category 2 computational type               
	 * 
	 * @param value2
	 * @param value1
	 * @param instruction
	 */
	public static void DUP2_2(int instruction) {
		DUP2_2_Operation op = new DUP2_2_Operation(OperationCodes.DUP2, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	
	/**
	 * Form1: value3, value2, value1  ->  value2, value1, value3, value2, value1          
	 * where value1, value2, and value3 are all values of a category 1 computational type          
	 * 
	 * @param instruction
	 */
	public static void DUP2_X1_1(int instruction) {
		DUP2_X1_1_Operation op = new DUP2_X1_1_Operation(OperationCodes.DUP2_X1, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	/**
	 * Form2: value2, value1    ->    value1, value2, value1     	
	 * where value1 is a value of a category 2 computational type and value2 is a value of a category 1 computational type
	 * 
	 * @param instruction
	 */
	public static void DUP2_X1_2(int instruction) {
		DUP2_X1_2_Operation op = new DUP2_X1_2_Operation(OperationCodes.DUP2_X1, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	
	/**
	 * Form1: value4, value3, value2, value1  ->   value2, value1, value4, value3, value2, value1      
	 * where value1, value2, value3, and value4 are all values of a category 1 computational type
	 * 
	 * @param instruction
	 */
	public static void DUP2_X2_1(int instruction) {
		DUP2_X2_1_Operation op = new DUP2_X2_1_Operation(OperationCodes.DUP2_X2, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	
	/**
	 * Form2: value3, value2, value1   ->    value1, value3, value2, value1    	
	 * where value1 is a value of a category 2 computational type and value2 and value3 are both values of a category 1 computational type
	 * 
	 * @param instruction
	 */
	public static void DUP2_X2_2(int instruction) {
		DUP2_X2_2_Operation op = new DUP2_X2_2_Operation(OperationCodes.DUP2_X2, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	/**
	 * Form3: value3, value2, value1   ->    value2, value1, value3, value2, value1       
	 * where value1 and value2 are both values of a category 1 computational type and value3 is a value of a category 2 computational type
	 * 
	 * @param instruction
	 */
	public static void DUP2_X2_3(int instruction) {
		DUP2_X2_3_Operation op = new DUP2_X2_3_Operation(OperationCodes.DUP2_X2, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	/**
	 * Form4: value2, value1    ->    value1, value2, value1		
	 * where value1 and value2 are both values of a category 2 computational type
	 * 
	 * @param instruction
	 */
	public static void DUP2_X2_4(int instruction) {
		DUP2_X2_4_Operation op = new DUP2_X2_4_Operation(OperationCodes.DUP2_X2, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	/**
	 * Form:  value2, value1  ->  value1, value2				
	 * value1 and value2 are both values of a category 1 computational type
	 * 
	 * @param instruction
	 */
	public static void SWAP(int instruction) {
		SWAP_Operation op = new SWAP_Operation(OperationCodes.SWAP, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IADD(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.IADD, "IADD", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LADD(long value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.LADD, "LADD", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void FADD(float value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.FADD, "FADD", new Value(value), Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);

	}
	
	public static void DADD(double value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.DADD, "DADD", new Value(value), Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ISUB(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.ISUB, "ISUB", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LSUB(long value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.LSUB, "LSUB", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void FSUB(float value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.FSUB, "FSUB", new Value(value), Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void DSUB(double value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.DSUB, "DSUB", new Value(value), Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IMUL(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.IMUL, "IMUL", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LMUL(long value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.LMUL, "LMUL", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void FMUL(float value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.FMUL, "FMUL", new Value(value), Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void DMUL(double value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.DMUL, "DMUL", new Value(value), Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IDIV(int instruction) {		//we hook before executing this instruction, because it may throw exception. So no value is provided!
		Division_Operation op = new Division_Operation(OperationCodes.IDIV, "IDIV", Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void LDIV(int instruction) {		//we hook before executing this instruction, because it may throw exception. So no value is provided!
		Division_Operation op = new Division_Operation(OperationCodes.LDIV, "LDIV", Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void FDIV(float value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.FDIV, "FDIV", new Value(value), Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void DDIV(double value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.DDIV, "DDIV", new Value(value), Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IREM(int instruction) {		//we hook before executing this instruction, because it may throw exception. So no value is provided!
		Rem_Operation op = new Rem_Operation(OperationCodes.IREM, "IREM", Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LREM(int instruction) {		//we hook before executing this instruction, because it may throw exception. So no value is provided!
		Rem_Operation op = new Rem_Operation(OperationCodes.LREM, "LREM", Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void FREM(float value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.FREM, "FREM", new Value(value), Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void DREM(double value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.DREM, "DREM", new Value(value), Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void INEG(int value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.INEG, "INEG", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LNEG(long value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.LNEG, "LNEG", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void FNEG(float value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.FNEG, "FNEG", new Value(value), Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void DNEG(double value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.DNEG, "DNEG", new Value(value), Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ISHL(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.ISHL, "ISHL", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LSHL(long value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.LSHL, "LSHL", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ISHR(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.ISHR, "ISHR", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LSHR(long value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.LSHR, "LSHR", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IUSHR(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.IUSHR, "IUSHR", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LUSHR(long value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.LUSHR, "LUSHR", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IAND(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.IAND, "IAND", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void LAND(long value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.LAND, "LAND", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IOR(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.IOR, "IOR", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void LOR(long value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.LOR, "LOR", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IXOR(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.IXOR, "IXOR", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LXOR(long value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.LXOR, "LXOR", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void I2L(long value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.I2L, "I2L", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void I2F(float value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.I2F, "I2F", new Value(value), Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void I2D(double value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.I2D, "I2D", new Value(value), Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void L2I(int value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.L2I, "L2I", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void L2F(float value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.L2F, "L2F", new Value(value), Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void L2D(double value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.L2D, "L2D", new Value(value), Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void F2I(int value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.F2I, "F2I", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void F2L(long value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.F2L, "F2L", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void F2D(double value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.F2D, "F2D", new Value(value), Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void D2I(int value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.D2I, "D2I", new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void D2L(long value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.D2L, "D2L", new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void D2F(float value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.D2F, "D2F", new Value(value), Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void I2B(byte value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.I2B, "I2B", new Value(value), Type.BYTE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void I2C(char value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.I2C, "I2C", new Value(value), Type.CHAR, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void I2S(short value, int instruction) {
		Unary_Operation op = new Unary_Operation(OperationCodes.I2S, "I2S", new Value(value), Type.SHORT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LCMP(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.LCMP, "LCMP" , new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void FCMPL(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.FCMPL, "FCMPL" , new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void FCMPG(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.FCMPG, "FCMPG" , new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void DCMPL(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.DCMPL, "DCMPL" , new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void DCMPG(int value, int instruction) {
		Binary_Operation op = new Binary_Operation(OperationCodes.DCMPG, "DCMPG" , new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	
	public static void IRETURN(int instruction) {
		XRETURN_Operation op = new XRETURN_Operation(OperationCodes.IRETURN, Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LRETURN(int instruction) {
		XRETURN_Operation op = new XRETURN_Operation(OperationCodes.LRETURN, Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void FRETURN(int instruction) {
		XRETURN_Operation op = new XRETURN_Operation(OperationCodes.FRETURN, Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void DRETURN(int instruction) {
		XRETURN_Operation op = new XRETURN_Operation(OperationCodes.DRETURN, Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ARETURN(int instruction) {
		XRETURN_Operation op = new XRETURN_Operation(OperationCodes.ARETURN, Type.REFERENCE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void RETURN(int instruction) {
		XRETURN_Operation op = new XRETURN_Operation(OperationCodes.RETURN, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void ARRAYLENGTH(Object array, int instruction) {
		ARRAYLENGTH_Operation op = new ARRAYLENGTH_Operation(OperationCodes.ARRAYLENGTH, array, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void ATHROW(Object exception, int instruction) {
		ATHROW_Operation op = new ATHROW_Operation(OperationCodes.ATHROW, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void MONITORENTER(int instruction) {
		MONITORENTER_Operation op = new MONITORENTER_Operation(OperationCodes.MONITORENTER, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void MONITOREXIT(int instruction) {
		MONITOREXIT_Operation op = new MONITOREXIT_Operation(OperationCodes.MONITOREXIT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void NEWARRAY_pre(int size, int instruction) {
		NEWARRAY_Operation op = new NEWARRAY_Operation(OperationCodes.NEWARRAY, size, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op, false);
	}
	
	public static void NEWARRAY_post(boolean[] array) {
		NEWARRAY_Operation op = (NEWARRAY_Operation) ThreadCallStack.getCurrentThreadCallStack().getBytecodeOperationBeforeLast(0);
		op.def(array, Type.BOOLEAN);
		
		log(op.toString(), DO_LOG);
	}

	public static void NEWARRAY_post(char[] array) {
		NEWARRAY_Operation op = (NEWARRAY_Operation) ThreadCallStack.getCurrentThreadCallStack().getBytecodeOperationBeforeLast(0);
		op.def(array, Type.CHAR);
		
		log(op.toString(), DO_LOG);
	}

	public static void NEWARRAY_post(float[] array) {
		NEWARRAY_Operation op = (NEWARRAY_Operation) ThreadCallStack.getCurrentThreadCallStack().getBytecodeOperationBeforeLast(0);
		op.def(array, Type.FLOAT);
		
		log(op.toString(), DO_LOG);
	}

	public static void NEWARRAY_post(double[] array) {
		NEWARRAY_Operation op = (NEWARRAY_Operation) ThreadCallStack.getCurrentThreadCallStack().getBytecodeOperationBeforeLast(0);
		op.def(array, Type.DOUBLE);
		
		log(op.toString(), DO_LOG);
	}
	
	public static void NEWARRAY_post(byte[] array) {
		NEWARRAY_Operation op = (NEWARRAY_Operation) ThreadCallStack.getCurrentThreadCallStack().getBytecodeOperationBeforeLast(0);
		op.def(array, Type.BYTE);
		
		log(op.toString(), DO_LOG);
	}
	
	public static void NEWARRAY_post(short[] array) {
		NEWARRAY_Operation op = (NEWARRAY_Operation) ThreadCallStack.getCurrentThreadCallStack().getBytecodeOperationBeforeLast(0);
		op.def(array, Type.SHORT);
		
		log(op.toString(), DO_LOG);
	}
	
	public static void NEWARRAY_post(int[] array) {
		NEWARRAY_Operation op = (NEWARRAY_Operation) ThreadCallStack.getCurrentThreadCallStack().getBytecodeOperationBeforeLast(0);
		op.def(array, Type.INT);
		
		log(op.toString(), DO_LOG);
	}

	public static void NEWARRAY_post(long[] array) {
		NEWARRAY_Operation op = (NEWARRAY_Operation) ThreadCallStack.getCurrentThreadCallStack().getBytecodeOperationBeforeLast(0);
		op.def(array, Type.LONG);
		
		log(op.toString(), DO_LOG);
	}

	public static void BIPUSH(int value, int instruction) { // we know it's a byte, should we use the type byte
		BIPUSH_Operation op = new BIPUSH_Operation(OperationCodes.BIPUSH, new Value(value), instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void SIPUSH(int value, int instruction) { // we know it's a short, should we use type short
		SIPUSH_Operation op = new SIPUSH_Operation(OperationCodes.SIPUSH, new Value(value), instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IFEQ(int offset, int instruction) { 
		IF_Operation op = new IF_Operation(OperationCodes.IFEQ, Condition.EQ, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IFNE(int offset, int instruction) { 
		IF_Operation op = new IF_Operation(OperationCodes.IFNE, Condition.NE, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IFLT(int offset, int instruction) { 
		IF_Operation op = new IF_Operation(OperationCodes.IFLT, Condition.LT, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IFGE(int offset, int instruction) { 
		IF_Operation op = new IF_Operation(OperationCodes.IFGE, Condition.GE, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IFGT(int offset, int instruction) { 
		IF_Operation op = new IF_Operation(OperationCodes.IFGT, Condition.GT, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IFLE(int target, int instruction) { 
		IF_Operation op = new IF_Operation(OperationCodes.IFLE, Condition.LE, target, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IF_ICMPEQ(int offset, int instruction) {
		IF_ICMP_Operation op = new IF_ICMP_Operation(OperationCodes.IF_ICMPEQ, Condition.EQ, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IF_ICMPNE(int offset, int instruction) { 
		IF_ICMP_Operation op = new IF_ICMP_Operation(OperationCodes.IF_ICMPNE, Condition.NE, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IF_ICMPLT(int offset, int instruction) { 
		IF_ICMP_Operation op = new IF_ICMP_Operation(OperationCodes.IF_ICMPLT, Condition.LT, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IF_ICMPGE(int offset, int instruction) { 
		IF_ICMP_Operation op = new IF_ICMP_Operation(OperationCodes.IF_ICMPGE, Condition.GE, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IF_ICMPGT(int offset, int instruction) { 
		IF_ICMP_Operation op = new IF_ICMP_Operation(OperationCodes.IF_ICMPGT, Condition.GT, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IF_ICMPLE(int offset, int instruction) { 
		IF_ICMP_Operation op = new IF_ICMP_Operation(OperationCodes.IF_ICMPLE, Condition.LE, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IF_ACMPEQ(int offset, int instruction) { 
		IF_ACMP_Operation op = new IF_ACMP_Operation(OperationCodes.IF_ACMPEQ, Condition.EQ, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IF_ACMPNE(int offset, int instruction) { 
		IF_ACMP_Operation op = new IF_ACMP_Operation(OperationCodes.IF_ACMPNE, Condition.NE, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void GOTO(int offset, int instruction) { 
		GOTO_Operation op = new GOTO_Operation(OperationCodes.GOTO, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IFNULL(int offset, int instruction) { 
		IF_Operation op = new IF_Operation(OperationCodes.IFNULL, Condition.NULL, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void IFNONNULL(int offset, int instruction) { 
		IF_Operation op = new IF_Operation(OperationCodes.IFNONNULL, Condition.NONNULL, offset, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	
	public static void LDC_pre(int instruction) {
		XLDC_Operation op = new XLDC_Operation(OperationCodes.LDC, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LDC(Object reference) {
		XLDC_Operation op = (XLDC_Operation) ThreadCallStack.getCurrentThreadCallStack().getBytecodeOperationBeforeLast(0);
		op.def(Utils.getValueForObject(reference));
	}
	
	public static void LDC(int value, int instruction) {
		XLDC_Operation op = new XLDC_Operation(OperationCodes.LDC, new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LDC(long value, int instruction) {
		XLDC_Operation op = new XLDC_Operation(OperationCodes.LDC, new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LDC(float value, int instruction) {
		XLDC_Operation op = new XLDC_Operation(OperationCodes.LDC, new Value(value), Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LDC(double value, int instruction) {
		XLDC_Operation op = new XLDC_Operation(OperationCodes.LDC, new Value(value), Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void LDC(String value, int instruction) {
		XLDC_Operation op = new XLDC_Operation(OperationCodes.LDC, Utils.getValueForObject(value), Type.REFERENCE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void IINC(int newValue, int localIndex, int incr,  int instruction) {
		IINC_Operation op = new IINC_Operation(OperationCodes.IINC, new Value(newValue), localIndex, incr, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void NEW(Object value, String desc, int instruction) {
		NEW_Operation op = new NEW_Operation(OperationCodes.NEW, value, desc, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void ANEWARRAY_pre(int size, int instruction) {
		ANEWARRAY_Operation op = new ANEWARRAY_Operation(OperationCodes.ANEWARRAY, size, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op, false);
	}

	public static void ANEWARRAY_post(Object[] array) {
		ANEWARRAY_Operation op = (ANEWARRAY_Operation)ThreadCallStack.getCurrentThreadCallStack().getBytecodeOperationBeforeLast(0);
		op.def(array);
		
		log(op.toString(), DO_LOG);
	}

	public static void CHECKCAST(int instruction) {
		CHECKCAST_Operation op = new CHECKCAST_Operation(OperationCodes.CHECKCAST, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void INSTANCEOF(int value, int instruction) {
		INSTANCEOF_Operation op = new INSTANCEOF_Operation(OperationCodes.INSTANCEOF, new Value(value), instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	//For getstatic operations, we hook after the instruction, because this instruction uses only one memory location (static field location) 
	//and this location is available only if there is not any problem regarding class/field resolution. If there is any resolution problem, we
	// cannot use reflection to find the read value before the actual execution of the bytecode. In other terms, no exception can happen as a 
	// result of runtime information passed to this instruction.
	
	public static void GETSTATIC_boolean(boolean value, Class klass, String fieldName, int instruction) {
		GETSTATIC_Operation op = new GETSTATIC_Operation(OperationCodes.GETSTATIC, klass, fieldName, new Value(value), Type.BOOLEAN, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void GETSTATIC_byte(byte value, Class klass, String fieldName, int instruction) {
		GETSTATIC_Operation op = new GETSTATIC_Operation(OperationCodes.GETSTATIC, klass, fieldName, new Value(value), Type.BYTE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void GETSTATIC_char(char value, Class klass, String fieldName, int instruction) {
		GETSTATIC_Operation op = new GETSTATIC_Operation(OperationCodes.GETSTATIC, klass, fieldName, new Value(value), Type.CHAR, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void GETSTATIC_double(double value, Class klass, String fieldName, int instruction) {
		GETSTATIC_Operation op = new GETSTATIC_Operation(OperationCodes.GETSTATIC, klass, fieldName, new Value(value), Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void GETSTATIC_float(float value, Class klass, String fieldName, int instruction) {
		GETSTATIC_Operation op = new GETSTATIC_Operation(OperationCodes.GETSTATIC, klass, fieldName, new Value(value), Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void GETSTATIC_int(int value, Class klass, String fieldName, int instruction) {
		GETSTATIC_Operation op = new GETSTATIC_Operation(OperationCodes.GETSTATIC, klass, fieldName, new Value(value), Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void GETSTATIC_long(long value, Class klass, String fieldName, int instruction) {
		GETSTATIC_Operation op = new GETSTATIC_Operation(OperationCodes.GETSTATIC, klass, fieldName, new Value(value), Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void GETSTATIC_short(short value, Class klass, String fieldName, int instruction) {
		GETSTATIC_Operation op = new GETSTATIC_Operation(OperationCodes.GETSTATIC, klass, fieldName, new Value(value), Type.SHORT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void GETSTATIC_object(Object value, Class klass, String fieldName, int instruction) {
		GETSTATIC_Operation op = new GETSTATIC_Operation(OperationCodes.GETSTATIC, klass, fieldName, Utils.getValueForObject(value), Type.REFERENCE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void PUTSTATIC(boolean value, Class klass, String fieldName, int instruction) {
		PUTSTATIC_Operation op = new PUTSTATIC_Operation(OperationCodes.PUTSTATIC, klass, fieldName, new Value(value), instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void PUTSTATIC(byte value, Class klass, String fieldName, int instruction) {
		PUTSTATIC_Operation op = new PUTSTATIC_Operation(OperationCodes.PUTSTATIC, klass, fieldName, new Value(value), instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void PUTSTATIC(char value, Class klass, String fieldName, int instruction) {
		PUTSTATIC_Operation op = new PUTSTATIC_Operation(OperationCodes.PUTSTATIC, klass, fieldName, new Value(value), instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void PUTSTATIC(double value, Class klass, String fieldName, int instruction) {
		PUTSTATIC_Operation op = new PUTSTATIC_Operation(OperationCodes.PUTSTATIC, klass, fieldName, new Value(value), instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void PUTSTATIC(float value, Class klass, String fieldName, int instruction) {
		PUTSTATIC_Operation op = new PUTSTATIC_Operation(OperationCodes.PUTSTATIC, klass, fieldName, new Value(value), instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void PUTSTATIC(int value, Class klass,  String fieldName, int instruction) {
		PUTSTATIC_Operation op = new PUTSTATIC_Operation(OperationCodes.PUTSTATIC, klass, fieldName, new Value(value), instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void PUTSTATIC(long value, Class klass, String fieldName, int instruction) {
		PUTSTATIC_Operation op = new PUTSTATIC_Operation(OperationCodes.PUTSTATIC, klass, fieldName, new Value(value), instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void PUTSTATIC(short value, Class klass, String fieldName, int instruction) {
		PUTSTATIC_Operation op = new PUTSTATIC_Operation(OperationCodes.PUTSTATIC, klass, fieldName, new Value(value), instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void PUTSTATIC(Object value, Class klass,  String fieldName, int instruction) {
		PUTSTATIC_Operation op = new PUTSTATIC_Operation(OperationCodes.PUTSTATIC, klass, fieldName, Utils.getValueForObject(value), instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	private static Field findFieldFromHierarchy(Class klass, String fieldName){
		if (klass == null)
			return null;
		try {
//			System.out.println("Class :\t"+klass.getName()+"\tfield name :\t"+fieldName);
			return klass.getDeclaredField(fieldName);
		} catch (SecurityException | NoSuchFieldException e) {
			return findFieldFromHierarchy(klass.getSuperclass(), fieldName);
		}
		
	}
	
	public static void GETFIELD_boolean(Object object, String fieldName, int instruction) {
		GETFIELD_Operation op;
		if (object != null){
			Value valueObject;
			try {
				boolean value = false;
				final Field field= findFieldFromHierarchy(object.getClass(), fieldName);
				boolean isAccessible = field.isAccessible();
				field.setAccessible(true);
				value = field.getBoolean(object);
				field.setAccessible(isAccessible);
				valueObject = new Value(value);
			} catch (IllegalArgumentException | IllegalAccessException | NullPointerException e) {
				e.printStackTrace();
				valueObject = Value.UNDEFINED;
			}
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, object, fieldName, valueObject, Type.BOOLEAN, instruction);
		}else{
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, instruction);
		}
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void GETFIELD_byte(Object object, String fieldName, int instruction) {
		GETFIELD_Operation op;
		if (object != null){
			Value valueObject;
			try {
				byte value;
				final Field field= findFieldFromHierarchy(object.getClass(), fieldName);
				boolean isAccessible = field.isAccessible();
				field.setAccessible(true);
				value = field.getByte(object);
				field.setAccessible(isAccessible);
				valueObject = new Value(value);
			} catch (IllegalArgumentException | IllegalAccessException | NullPointerException e) {
				e.printStackTrace();
				valueObject = Value.UNDEFINED;
			}
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, object, fieldName, valueObject, Type.BYTE, instruction);
		}else{
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, instruction);
		}
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void GETFIELD_char(Object object, String fieldName, int instruction) {
		GETFIELD_Operation op;
		if (object != null){
			Value valueObject;
			try {
				char value;
				final Field field= findFieldFromHierarchy(object.getClass(), fieldName);
				boolean isAccessible = field.isAccessible();
				field.setAccessible(true);
				value = field.getChar(object);
				field.setAccessible(isAccessible);
				valueObject = new Value(value);
			} catch (IllegalArgumentException | IllegalAccessException | NullPointerException e) {
				e.printStackTrace();
				valueObject = Value.UNDEFINED;
			}
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, object, fieldName, valueObject, Type.CHAR, instruction);
		}else{
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, instruction);
		}
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void GETFIELD_double(Object object, String fieldName, int instruction) {
		GETFIELD_Operation op;
		if (object != null){
			Value valueObject;
			try {
				double value;
				final Field field= findFieldFromHierarchy(object.getClass(), fieldName);
				boolean isAccessible = field.isAccessible();
				field.setAccessible(true);
				value = field.getDouble(object);
				field.setAccessible(isAccessible);
				valueObject = new Value(value);
			} catch (IllegalArgumentException | IllegalAccessException | NullPointerException e) {
				e.printStackTrace();
				valueObject = Value.UNDEFINED;
			}
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, object, fieldName, valueObject, Type.DOUBLE, instruction);
		}else{
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, instruction);
		}
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void GETFIELD_float(Object object, String fieldName, int instruction) {
		GETFIELD_Operation op;
		if (object != null){
			Value valueObject;
			try {
				float value;
				final Field field= findFieldFromHierarchy(object.getClass(), fieldName);
				boolean isAccessible = field.isAccessible();
				field.setAccessible(true);
				value = field.getFloat(object);
				field.setAccessible(isAccessible);
				valueObject = new Value(value);
			} catch (IllegalArgumentException | IllegalAccessException | NullPointerException e) {
				e.printStackTrace();
				valueObject = Value.UNDEFINED;
			}
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, object, fieldName, valueObject, Type.FLOAT, instruction);
		}else{
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, instruction);
		}
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void GETFIELD_int(Object object, String fieldName, int instruction) {
		GETFIELD_Operation op;
		if (object != null){
			Value valueObject;
			try {
				int value;
				final Field field= findFieldFromHierarchy(object.getClass(), fieldName);
				boolean isAccessible = field.isAccessible();
				field.setAccessible(true);
				value = field.getInt(object);
				field.setAccessible(isAccessible);
				valueObject = new Value(value);
			} catch (IllegalArgumentException | IllegalAccessException | NullPointerException e) {
				e.printStackTrace();
				valueObject = Value.UNDEFINED;
			}
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, object, fieldName, valueObject, Type.INT, instruction);
		}else{
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, instruction);
		}
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void GETFIELD_long(Object object, String fieldName, int instruction) {
		GETFIELD_Operation op;
		if (object != null){
			Value valueObject;
			try {
				long value;
				final Field field= findFieldFromHierarchy(object.getClass(), fieldName);
				boolean isAccessible = field.isAccessible();
				field.setAccessible(true);
				value = field.getLong(object);
				field.setAccessible(isAccessible);
				valueObject = new Value(value);
			} catch (IllegalArgumentException | IllegalAccessException | NullPointerException e) {
				e.printStackTrace();
				valueObject = Value.UNDEFINED;
			}
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, object, fieldName, valueObject, Type.LONG, instruction);
		}else{
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, instruction);
		}
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void GETFIELD_short(Object object, String fieldName, int instruction) {
		GETFIELD_Operation op;
		if (object != null){
			Value valueObject;
			try {
				short value;
				final Field field= findFieldFromHierarchy(object.getClass(), fieldName);
				boolean isAccessible = field.isAccessible();
				field.setAccessible(true);
				value = field.getShort(object);
				field.setAccessible(isAccessible);
				valueObject = new Value(value);
			} catch (IllegalArgumentException | IllegalAccessException | NullPointerException e) {
				e.printStackTrace();
				valueObject = Value.UNDEFINED;
			}
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, object, fieldName, valueObject, Type.SHORT, instruction);
		}else{
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, instruction);
		}
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void GETFIELD_object(Object object, String fieldName, int instruction) {
		GETFIELD_Operation op;
		if (object != null){
			Value valueObject;
			try {
				Object value;
				final Field field= findFieldFromHierarchy(object.getClass(), fieldName);
				boolean isAccessible = field.isAccessible();
				field.setAccessible(true);
				value = field.get(object);
				field.setAccessible(isAccessible);
				valueObject = Utils.getValueForObject(object);
			} catch (IllegalArgumentException | IllegalAccessException | NullPointerException e) {
				e.printStackTrace();
				valueObject = Value.UNDEFINED;
			}
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, object, fieldName, valueObject, Type.REFERENCE, instruction);
		}else{
			op = new GETFIELD_Operation(OperationCodes.GETFIELD, instruction);
		}
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void PUTFIELD(Object object, String fieldName, int instruction) {
		PUTFIELD_Operation op = new PUTFIELD_Operation(OperationCodes.PUTFIELD, object, fieldName, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void INVOKE_calling(String opName, String nameDescClass, int argsCount, int instruction) {
//		INVOKECALLING_Operation op = new INVOKECALLING_Operation(opName, nameDescClass, argsCount, instruction);
//		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
		INVOKEARGS_Operation invokeArgsOp = new INVOKEARGS_Operation(instruction, 
				nameDescClass, 
				argsCount, 
				opName.equals("INVOKESTATIC") ? true : false,
				true,
				opName);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(invokeArgsOp);
	}
	
	public static void INVOKE_returning(boolean returnValue, String opName, String classNameDesc, int instruction) {
		INVOKERETURNING_Operation op = INVOKERETURNING_Operation.create(OperationCodes.INVOKERETURN, new Value(returnValue), opName, classNameDesc, Type.BOOLEAN, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void INVOKE_returning(byte returnValue, String opName, String classNameDesc, int instruction) {
		INVOKERETURNING_Operation op = INVOKERETURNING_Operation.create(OperationCodes.INVOKERETURN, new Value(returnValue), opName, classNameDesc, Type.BYTE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void INVOKE_returning(char returnValue, String opName, String classNameDesc, int instruction) {
		INVOKERETURNING_Operation op = INVOKERETURNING_Operation.create(OperationCodes.INVOKERETURN, new Value(returnValue), opName, classNameDesc, Type.CHAR, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void INVOKE_returning(double returnValue, String opName, String classNameDesc, int instruction) {
		INVOKERETURNING_Operation op = INVOKERETURNING_Operation.create(OperationCodes.INVOKERETURN, new Value(returnValue), opName, classNameDesc, Type.DOUBLE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void INVOKE_returning(float returnValue, String opName, String classNameDesc, int instruction) {
		INVOKERETURNING_Operation op = INVOKERETURNING_Operation.create(OperationCodes.INVOKERETURN, new Value(returnValue), opName, classNameDesc, Type.FLOAT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void INVOKE_returning(int returnValue, String opName, String classNameDesc, int instruction) {
		INVOKERETURNING_Operation op = INVOKERETURNING_Operation.create(OperationCodes.INVOKERETURN, new Value(returnValue), opName, classNameDesc, Type.INT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void INVOKE_returning(long returnValue, String opName, String classNameDesc, int instruction) {
		INVOKERETURNING_Operation op = INVOKERETURNING_Operation.create(OperationCodes.INVOKERETURN, new Value(returnValue), opName, classNameDesc, Type.LONG, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void INVOKE_returning(short returnValue, String opName, String classNameDesc, int instruction) {
		INVOKERETURNING_Operation op = INVOKERETURNING_Operation.create(OperationCodes.INVOKERETURN, new Value(returnValue), opName, classNameDesc, Type.SHORT, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void INVOKE_returning(Object returnValue, String opName, String classNameDesc, int instruction) {
		INVOKERETURNING_Operation op = INVOKERETURNING_Operation.create(OperationCodes.INVOKERETURN, Utils.getValueForObject(returnValue), opName, classNameDesc, Type.REFERENCE, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void INVOKE_returning(String opName, String classNameDesc, int instruction) {
		INVOKERETURNING_Operation op = INVOKERETURNING_Operation.create(OperationCodes.INVOKERETURN, opName, classNameDesc, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}

	public static void MULTIANEWARRAY_pre(int dimensions, int instruction, String desc){
		MULTIANEWARRAY_Operation op = new MULTIANEWARRAY_Operation(OperationCodes.MULTIANEWARRAY, dimensions, instruction, Utils.findMultiArrayElementType(desc));
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op, false);
	}
	
	public static void MULTIANEWARRAY_post(Object[] array, int dimensions){
		MULTIANEWARRAY_Operation op = (MULTIANEWARRAY_Operation) ThreadCallStack.getCurrentThreadCallStack().getBytecodeOperationBeforeLast(0);
		op.def(array, dimensions);
		
		log(op.toString(), DO_LOG);
	}
	
	public static void LOOKUPSWITCH_INSN(int instruction){
		LOOKUPSWITCH_Operation op = new LOOKUPSWITCH_Operation(OperationCodes.LOOKUPSWITCH, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void TABLESWITCH_INSN(int instruction){
		TABLESWITCH_Operation op = new TABLESWITCH_Operation(OperationCodes.TABLESWITCH, instruction);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
	
	public static void onExit(){
		ThreadCallStack curr = ThreadCallStack.getCurrentThreadCallStack();
		if (!curr.isCallStackEmpty()){
			return;
		}
		
		log("Application Exit!", true);
		if (DO_ABSTRACTION){
			List<BytecodeEvent> ops = ThreadCallStack.getCurrentThreadCallStack().getBytecodeOperations();
			BytecodeSessionManager session = BytecodeSessionManager.getInstance(ops);
			List<ThreadCallStack> tcss = ThreadCallStack.getInstances();
			
			session.setExceptionThrowingInstruction(ops.get(ops.size() - 1));
//			System.out.println("Find control slice for bytecode "  + ops.get(ops.size() - 1));
//			RetrievalSession ret = session.findBackwardSliceForBytecode(ops.size() - 1);
//			List<BytecodeOperation> ret = session.findController(ops.get(ops.size() - 1));
//			for (Operation op : ret) {
//				System.out.println(op);
//			}
			
//			log("Find slice for bytecode "+ ops.get(ops.size() - 1), true);
//			try {
//				PrintWriter p = new PrintWriter(new FileWriter("/Users/reza/Documents/workspace2/defects4j-tester/slice.txt"));
//				List<SlicedOperation> ret2 = session.findBackwardSliceForBytecode(ops.get(ops.size() - 1));
//				for (Operation op : ret2) {
//					p.println(op);
//				}
//				p.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
		
//			for (Operation abstractOperation : session.getAbstractionSessionManager().getFullOperationsListSession().getOperations()) {
//				log(abstractOperation.toString(), true);
//			}
			
//			while(true){
//				try{
//					Activation frame = curr.popFrame();
//					System.out.println("Remaining:\t"+frame);
//				}catch(EmptyStackException e){
//					System.out.println("Finished Remianing");
//					break;
//				}
//			}
//			int opIndex = 1;
//			RetrievalSession s = session.getAbstractionSessionManager().findBackwardSliceByAbsOpIndex(opIndex);
//			System.out.println("Find backward slice for the op Index\t" + opIndex);
//			for (Operation abstractOperation : s.getOperations()) {
//				log(abstractOperation.toString(), true);
//			}
//			log("End of Abstract operations!", true);
//			if (REMOTELY_AVAILABLE)
//				generateHtmlView(session);
		}
		
	}
	
//	public static void generateHtmlView(BytecodeSessionManager session){
//		
//		try
//        {
//            HTMLTableViewRMI obj = new HTMLTableViewRMIImpl(session);
//            // Bind this object instance to the name "HelloServer"
//            log("\nWaiting for incoming RMI requests from the web application ...", true);
//            Naming.rebind("AnalysisServer", obj);
//        }
//        catch (Exception e)
//        {
//            System.out.println("Analysis Server err: " + e.getMessage());
//            e.printStackTrace();
//        }
//	}
	
	/////////////////////////////////////////////////////////////
	
	public static void handleException(Throwable exception) {
		log("Pop frame on exception!", true);
		
//		System.out.println("<UNCAUGHT EXCEPTION>");
//		exception.printStackTrace(System.out);
		
		//exception.printStackTrace();
		ThreadCallStack tcs = ThreadCallStack.getCurrentThreadCallStack();
		Activation currFrame = tcs.peekTopMostFrame();
		if (!currFrame.isInitialized()){
			currFrame = tcs.popFrame();
			if (!tcs.isExceptionThrown()){		//This means an exception was thrown and uncaught in an untraced part of the code
				tcs.setUncaughtExceptionInUninstrumented();
			}
		}
		
		tcs.setExceptionThrown();
		
		tcs.popFrame();
		
		if (tcs.isCallStackEmpty()){
			onExit();    
		}
//		tcs.killInvoke();
//		final Activation callerFrame = tcs.peekTopMostFrame();
//		if (currFrame.getMethodName().equals("main") && currFrame.getDesc().equals("([Ljava/lang/String;)V")){
//			System.out.println("EXCEPTION HANDLER: Generating backward trace for the operands of the instruction throwing the exception: "+exception+"\n");
//			StackTraceElement[] elements = exception.getStackTrace();
//			for (StackTraceElement stackTraceElement : elements) {
//				System.out.println("EXCEPTION HANDLER:\t" + stackTraceElement);
//			}
//			BytecodeOperation op = tcs.getLastBytecodeOperation();
//			log (op.toString(), true);
//			for (int i = 0; i<op.uses.length; i++){
//				if (op.uses[i] != Binding.UNDEFINED_BINDING)
//					logException(op.uses[i].getProvider());
//			}
			
//			Trace.traceOrigin(trace.getTarget(), traces.size()-1);
//			ExcelSpreadSheetView.update(traces);
//			Trace.createAST();
//			Trace.createStatementAST();
//			System.out.println("EXCEPTION HANDLER:\t"+trace);

//			try {
//				DotGraph.drawExpGraph();
//				DotGraph.drawGraph();
//				DotGraph.drawFowardTraceGraph();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
	}
	
	private static void logException(BytecodeEvent op){
		log(op.toString(), true);
		for (int i = 0; i<op.uses.length; i++){
			if (op.uses[i] != Binding.UNDEFINED_BINDING || op.uses != Binding.NO_USE_BINDINGS)
				logException(op.uses[i].getProvider());
		}
	}
	
	
	/////////////////////////////////////////////////////////////
	public static void log(String message) {
		log(message, DO_LOG);
	}

	public static void log(String message, boolean write){
		if (write)
			System.out.println("TRACE:\t"+message);
	}
	
	public static void CATCH_BLOCK(Throwable e){
		log("Ooops! Exception "+ e +" was thrown and caught!", false);
		
		//At this point, we are sure that there has been a BasicBlockEntryOperation executed,
		//so, we don't need to check if the topmost activation belongs to the current method.
		//It must belong to this method, because it is already fixed by BasicBlockEntryOp.
		BytecodeEvent op = new CatchBlock_Operation(OperationCodes.CATCH_BLOCK, -1, Utils.getValueForObject(e));
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
 	}
	
	public static void enter(int bb, String name, int[] doms){
		BasicBlockEntry_PseudoOperation op = new BasicBlockEntry_PseudoOperation(OperationCodes.BASIC_BLOCK_ENTRY, SourceLineOperation.PSEUDO_VAR_OPERATION_LINE_NO, bb, name, doms);
		ThreadCallStack.getCurrentThreadCallStack().addOperation(op);
	}
		

}
