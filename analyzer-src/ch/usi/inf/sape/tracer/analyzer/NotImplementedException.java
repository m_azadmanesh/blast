package ch.usi.inf.sape.tracer.analyzer;

public class NotImplementedException extends RuntimeException{

	@Override
	public String getMessage() {
		return "The logic for the currently invoked method is not implemented yet!";
	}
	
}
