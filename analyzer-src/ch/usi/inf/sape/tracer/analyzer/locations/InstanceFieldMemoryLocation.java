package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

/**
 * This models the location containing a particular field of an object.   
 *  
 * @author reza
 *
 */
public class InstanceFieldMemoryLocation extends MemoryLocation{

	private final InstanceMetaData instance;
	
	private final String name;
	
	private final Type type;
	
	/**
	 * used for presentation purposes.
	 */
	private final int offset;
	
	public InstanceFieldMemoryLocation(InstanceMetaData instance, String name, Type type) {
		this.instance = instance;
		this.name = name;
		this.type = type;
		this.offset = instance.getAndIncrementNextAvailableFieldOffset();
	}
	
	@Override
	public String getName(){
		return this.name;
	}
	
	@Override
	public String toString(){
		return this.name;
	}
	
	public Type getType(){
		return type;
	}

	@Override
	public int getPresentationOffset() {
		return offset;
	}

	@Override
	public MemorySpaceType getSpaceType() {
		return MemorySpaceType.HEAP_SPACE;
	}

	@Override
	public InstanceMetaData getContainer() {
		return instance;
	}

	@Override
	public boolean isStackSlot() {
		return false;
	}

	@Override
	public LocationType getLocationType() {
		return LocationType.INSTANCE_FIELD;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
