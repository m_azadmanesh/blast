package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;

/**
 * An artificial location that represents the place-holder for the return value of an invocation.
 * A return value memory location is uniqely identified through the stack frame to which it belongs.
 * 
 * @author reza
 *
 */
public final class ReturnValueMemoryLocation extends StackMemoryLocation{
	
	public ReturnValueMemoryLocation(Activation frame) {
		super(frame);
	}

	@Override
	public String toString() {
		return "(RV:frame#"+frame.getOwnerClass()+"."+frame.getMethodName()+frame.getDesc()+")";
	}

	/**
	 * @return the frame
	 */
	public Activation getFrame() {
		return frame;
	}

	@Override
	public int getPresentationOffset() {
		return frame.getMaxStackHeight() + frame.getLocalCount();
	}

	@Override
	public MemorySpaceType getSpaceType() {
		return MemorySpaceType.STACK_SPACE;
	}

	@Override
	public Container getContainer() {
		return frame;
	}

	@Override
	public boolean isStackSlot() {
		return false;
	}

	@Override
	public String getName() {
		return "RV";
	}

	@Override
	public LocationType getLocationType() {
		return LocationType.RETURN_VALUE;
	}
	
	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
