package ch.usi.inf.sape.tracer.analyzer.locations;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.usi.inf.sape.tracer.analyzer.Utils;
import ch.usi.inf.sape.tracer.analyzer.WeakIdentityHashMap;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

public class HeapSpace {
	
	/**
	 * The weak mapping from a Java object to the corresponding ObjectMemoryLocation instance
	 */
	private static WeakIdentityHashMap<Object, ObjectMetaData> objMap = new WeakIdentityHashMap<Object, ObjectMetaData>();

	/**
	 * Given that we use a weak identity hash map for keeping track of the 
	 * object to object location  mappings, we need a set to keep track of 
	 * all the object locations created during runtime
	 */
	private static final Set<InstanceMetaData> tracedInstanceMemoryLocations = new HashSet<>();
	
	private static final Set<ArrayMetaData> tracedArrayMemoryLocations = new HashSet<>();
	
	private static final Set<InstanceMetaData> untracedInstanceMemoryLocations = new HashSet<>();
	
	private static final Set<ArrayMetaData> untracedArrayMemoryLocations = new HashSet<>();
	
	/**
	 * Keeps track of the next id that can be use for an object location. 
	 */
	private static int nextInstanceId;
	
	private static int nextArrayId;
	

	public static InstanceMetaData getOrDefineInstanceLocationMetaData(Object obj){
		InstanceMetaData objLoc = (InstanceMetaData) objMap.get(obj);
		if (objLoc == null){
			objLoc = defineInstanceLocationMetaData(obj, false, BytecodeEvent.UNTRACED_BYTECODE_OPERATION);
		}
		return objLoc;
	}
	
	/**
	 * Finds or creates an ArrayMemoryLocationMetaData instance. If not found, an instance
	 * will be created and considered as an untraced instance creation.
	 * 
	 * This method never returns null.
	 * 
	 * @param obj should not be null
	 * @return
	 */
	public static ArrayMetaData getOrDefineArrayLocationMetaData(Object obj, Type type){
		ArrayMetaData objLoc = (ArrayMetaData) objMap.get(obj);
		if (objLoc == null){
			objLoc = defineArrayLocationMetaData(obj, type, false, BytecodeEvent.UNTRACED_BYTECODE_OPERATION);
		}
		return objLoc;
	}
	
	/**
	 * 
	 * @param obj
	 * @param isTraced if the creation of this object is already traced. If not, we set the default field values to NA!
	 * @return
	 */
	public static InstanceMetaData defineInstanceLocationMetaData(Object obj, boolean isTraced, BytecodeEvent provider){
		InstanceMetaData instanceLocation = null;
		try{
			Class klass = obj.getClass();  
			instanceLocation = new InstanceMetaData(GlobalSpace.getOrDefineClassLocationMetaData(klass), nextInstanceId ++, provider);
			objMap.put(obj, instanceLocation);
			createInstanceBarrier(instanceLocation, isTraced);
			List<Field> fields = getAllFieldsInHierarchy(klass);
			for (Field field:fields){
				if(!Modifier.isStatic(field.getModifiers())){
					InstanceFieldMemoryLocation fieldLocation = new InstanceFieldMemoryLocation(instanceLocation, field.getName(), Utils.findTypeFromFullTypeName(field.getType().getName()));
					instanceLocation.initializeField(fieldLocation, isTraced, provider);
				}
			}
		}catch(Throwable t){  //We should never cause any interrupt in the normal program execution. In case the object is null, we postpone the exception to the point where the program really sees that value.
			System.out.println("Exception ******");
			t.printStackTrace(System.out);
			System.exit(0);
		}  
		
		return instanceLocation;
	}
	
	/**
	 * 
	 * @param obj
	 * @param type
	 * @param isTraced	if the creation of this array is already traced. If not, we set the default element values to NA!
	 * @return
	 */
	public static ArrayMetaData defineArrayLocationMetaData(Object obj, Type type, boolean isTraced, BytecodeEvent provider){
		ArrayMetaData arrayLocation = null;
		try{
			int arraySize = Array.getLength(obj);
			arrayLocation = new ArrayMetaData(GlobalSpace.getOrDefineClassLocationMetaData(obj.getClass()), nextArrayId ++, arraySize, type, provider);
			objMap.put(obj, arrayLocation);
			createArrayBarrier(arrayLocation, isTraced);
			for (int i = 0; i < arraySize; i++) {
				ArrayElementMemoryLocation element = new ArrayElementMemoryLocation(arrayLocation, i);
				arrayLocation.initializeArrayElement(element, i, isTraced, provider);
			}
			
		}catch(Throwable t){
			t.printStackTrace();	//We should never cause any interrupt in the normal program execution. In case the object is null, we postpone the exception to the point where the program really sees that value.
		}
		
		return arrayLocation;
	}
	
	/**
	 * This method is used just from MultiANewArray bytecode class to create subarrays.
	 * The difference with the normal array initialization is that the default value
	 * for subarrays is not null, but a pointer to the subarray in lower dimension.
	 * 
	 * @param obj
	 * @param type
	 * @param isTraced
	 * @param provider
	 * @return
	 */
	public static ArrayMetaData defineMultiArrayLocationMetaData(Object obj, Type type, boolean isTraced, BytecodeEvent provider){
		ArrayMetaData arrayLocation = null;
		try{
			int arraySize = Array.getLength(obj);
			arrayLocation = new ArrayMetaData(GlobalSpace.getOrDefineClassLocationMetaData(obj.getClass()), nextArrayId++, arraySize, type, provider);
			objMap.put(obj, arrayLocation);
			createArrayBarrier(arrayLocation, isTraced);
		}catch (Throwable t){
			t.printStackTrace();
		}
		
		return arrayLocation;
	}
	
//	public static ArrayMemoryLocation defineArrayLocationMetaData(Object obj){
//		
//	}
	
	private static List<Field> getAllFieldsInHierarchy(Class klass){
		List<Field> fields = new ArrayList<>(); 
		if (klass == null || klass == Object.class)
			return fields;
		
		fields.addAll(getAllFieldsInHierarchy(klass.getSuperclass()));
		fields.addAll(Arrays.asList(klass.getDeclaredFields()));
		return fields;
	}
	
	private static void createInstanceBarrier(InstanceMetaData objLocation, boolean traced){
		if (traced)
			tracedInstanceMemoryLocations.add(objLocation);
		else
			untracedInstanceMemoryLocations.add(objLocation);
	}
	
	private static void createArrayBarrier(ArrayMetaData arrayLocation, boolean traced){
		if (traced)
			tracedArrayMemoryLocations.add(arrayLocation);
		else 
			untracedArrayMemoryLocations.add(arrayLocation);
	}
	
	public static Set<InstanceMetaData> getAllocationTracedInstances(){
		return tracedInstanceMemoryLocations;
	}
	
	public static Set<ArrayMetaData> getAllocationTracedArrays(){
		return tracedArrayMemoryLocations;
	}
	
	public static Set<InstanceMetaData> getAllocationNotTracedInstances(){
		return untracedInstanceMemoryLocations;
	}
	
	public static Set<ArrayMetaData> getAllocationNotTracedArrays(){
		return untracedArrayMemoryLocations;
	}
	
	public static boolean contains(Object obj){
		return objMap.containsKey(obj);
	}
	
	public static ObjectMetaData getObjectLocationMetaData(Object obj){
		return objMap.get(obj);
	}
}
