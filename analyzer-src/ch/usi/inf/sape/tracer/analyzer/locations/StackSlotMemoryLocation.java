package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

/**
 * A stack slot on the operand stack. A particular stack slot index can be bound
 * to different values of different types. So, for the case of simplicity, we generate
 * a new location for each binding to a particular index.
 * 
 * @author reza
 *
 */
public final class StackSlotMemoryLocation extends StackMemoryLocation{

	private final int indices[];
	
	/**
	 * There could be different types for a particular stack slot index.
	 */
	private final Type type;
	
	/**
	 * This is used in abstraction phase. It points to a temporary memory location instance in case this slot is represented by any during abstraction phase.
	 */
	private Binding<TemporaryMemoryLocation> temporaryBinding;  
	
	public StackSlotMemoryLocation(final int index, final Activation frame, final Type type) {
		super(frame);
		this.indices = new int[type.getCategory()];
		for (int i = 0; i < indices.length; i++) {
			this.indices[i] = index + i;
		}
		
		this.type = type;
	}

	@Override
	public String toString() {
		String result=  "(Stack#["+indices[0];
		if (indices.length == 2)
			result += "," + indices[1];
		
		result += "]("+type.getDescriptor()+")"+","+frame.getOwnerClass()+"."+frame.getMethodName()+frame.getDesc()+")";
		return result;
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	@Override
	public boolean isStackSlot() {
		return true;
	}

	/**
	 * Stack is the first part of an activation to be presented 
	 */
	@Override
	public int getPresentationOffset() {
		return indices[0];
	}

	@Override
	public MemorySpaceType getSpaceType() {
		return MemorySpaceType.STACK_SPACE;
	}

	@Override
	public Container getContainer() {
		return frame;
	}

	/**
	 * TODO: the name should be extended to include the Category 2 types
	 */
	@Override
	public String getName() {
		return "S#"+indices[0];
	}
	

	public boolean hasTemporaryRepresentative(){
		return temporaryBinding != null;
	}

	/**
	 * @return the temporaryMemoryLocation
	 */
	public Binding<TemporaryMemoryLocation> getTemporaryBinding() {
		return temporaryBinding;
	}

	/**
	 * @param temporaryMemoryLocation the temporaryMemoryLocation to set
	 */
	public void setTemporaryBinding(Binding<TemporaryMemoryLocation> temporaryBinding) {
		this.temporaryBinding = temporaryBinding;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof StackSlotMemoryLocation))
			return false;
		
		StackSlotMemoryLocation slot = (StackSlotMemoryLocation)obj;
		if (slot.frame.equals(this.frame) && (slot.indices[0] == this.indices[0]))
			return true;
		
		return false;
	}

	@Override
	public LocationType getLocationType() {
		return LocationType.STACK_SLOT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
