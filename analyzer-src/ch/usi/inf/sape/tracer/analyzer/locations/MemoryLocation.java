package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.TraceElement;

/**
 * A MemoryLocation is a container for a value. Different values can be bound to a memory location.
 * Each binding is represented by an instance of the Definition class.
 * This class and its subclasses are immutable.
 *
 * @author reza
 *
 */

public abstract class MemoryLocation implements TraceElement{
	
	public static enum MemorySpaceType {HEAP_SPACE, STACK_SPACE, GLOBAL_SPACE, THREAD_LOCAL_SPACE};  
	
	public static enum LocationType{STACK_SLOT, LOCAL_VAR, RETURN_VALUE, ARRAY_ELEMENT, ARRAY_LENGTH, INSTANCE_FIELD, CLASS_FIELD, PSEUDO_VAR, TEMPORARY_VAR, UNDEFINED};
	
	/**
	 * Each memory location is contained in a larger construct, i.e.:
	 * Each stack slot is contained in an Activation,
	 * Each local var is contained in an Activation,
	 * Each ret val is contained in an Activation,
	 * Each instance field is contained in an ObjectMemoryLocation,
	 * Each array element is contained in an ObjectMemoryLocation,
	 * Each static field is contained in a ClassMemroyLocation. 
	 * @return
	 */
	public abstract Container getContainer();
	
	/**
	 * if this memory location is a stack slot
	 * @return
	 */
	public abstract boolean isStackSlot();
	
	/**
	 * The offset of this memory location component within its container. Used for presentation purposes
	 * @return
	 */
	public abstract int getPresentationOffset();
	
	/**
	 * To which region of memory including heap, stack, or globals does this location belong?
	 * @return
	 */
	public abstract MemorySpaceType getSpaceType();
	
	/**
	 * The symbolic name of this location
	 * @return
	 */
	public abstract String getName();
	
	public abstract LocationType getLocationType();
}
