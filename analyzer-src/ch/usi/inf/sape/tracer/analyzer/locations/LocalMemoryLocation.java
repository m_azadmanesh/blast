package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

/**
 * A local  variable among the local variables of a particular stack frame. 
 * Given that a local variable designated by an index could refer to two 
 * different local variables at different execution points, we need to keep 
 * track of the start and end indices in the code where the local is defined.
 * 
 * 
 * @author reza
 *
 */
public final class LocalMemoryLocation extends StackMemoryLocation{

	private final int index;
	
	/**
	 * Index in the code array where this local variable location starts to have a value (inclusive)
	 */
	private final int start;
	
	/**
	 * Index in the code array where this local variable location ends to have a value (exclusive)
	 */
	private final int end;
	
	/**
	 * this is used for visualization purposes when we need to uniquely refer to this local in a tabular presentaion
	 */
	private final int offset;
	
	/**
	 * The variable name
	 */
	private final String name;
	
	private final Type type;

	public final static String NAME_NOT_AVAILABLE = "_";
	
	public LocalMemoryLocation(final String name, final int index, final Activation frame, final int start, final int end, final Type type) {
		super(frame);
		this.index = index;
		this.start = start;
		this.end = end;
		this.name = name;
		this.type = type;
		this.offset = frame.getAndIncrementNextLocalColumnId();
	}

	public boolean isAlive(int insnIndex){
		if (this.end ==  -1)
			return true;		//this is the case when the method to which this local belongs is not instrumented and we don't have any info about it. we just save the args info.
		
		if (insnIndex < this.end && insnIndex >= this.start-1)
			return true;
		else
			return false;
	}


	@Override
	public String toString() {
		return "("+this.name+","+frame.getOwnerClass()+"."+frame.getMethodName()+frame.getDesc()+")";
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public Type getType(){
		return this.type;
	}

	/**
	 * 
	 * @return the offset among local variables defined within this activation
	 */
	public int getOffset(){
		return this.offset;
	}

	/**
	 * @return the frame
	 */
	public Activation getFrame() {
		return frame;
	}
	
	/**
	 * Locals offset = Stack Max size + offset of the local
	 */
	public int getPresentationOffset(){
		return this.frame.getMaxStackHeight() + this.offset;
	}

	@Override
	public MemorySpaceType getSpaceType() {
		return MemorySpaceType.STACK_SPACE;
	}


	@Override
	public Container getContainer() {
		return frame;
	}


	@Override
	public boolean isStackSlot() {
		return false;
	}


	@Override
	public LocationType getLocationType() {
		return LocationType.LOCAL_VAR;
	}
	
	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
