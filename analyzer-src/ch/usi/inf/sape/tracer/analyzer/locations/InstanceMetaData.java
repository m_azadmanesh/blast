package ch.usi.inf.sape.tracer.analyzer.locations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.PUTFIELD_Operation;

public class InstanceMetaData extends ObjectMetaData{
	
	/**
	 * keeps track of the last binding for each field based on the name
	 */
	private final Map<String, Binding<InstanceFieldMemoryLocation>> lastBindings = new HashMap<>();
	
	/**
	 * used for presentation purposes.
	 */
	private int nextAvailableFieldOffset;
	
	/**
	 * An instance of this class corresponds to the execution of a "new" bytecode. No fields get initialized (no binding), so
	 * one has to call the initialize() method right after creating an instance of this class. The body of the initialize method
	 * could have been embedded into this constructor, but in order to avoid passing an uninitialized "this" reference to some
	 * other method, we seperated these methods.
	 * 
	 * @param fieldsCount
	 */
	public InstanceMetaData(ClassMetaData klass, int id, BytecodeEvent provider) {
		super(klass, id, provider);
	}
	
	/**
	 * All fields are initialized to their default value upon the creation of an object. This method corresponds
	 * to the internal behavior of JVM when there is no constructor or some fields have missing explicit initializations.
	 * We could have done this within the body of the constructor, but in that case, we would have to pass uninitialized 
	 * "this" variable to the constructor of the InstanceFieldMemoryLocation that is risky.
	 * 
	 * @param fieldNames
	 * @param fieldTypes
	 */
	public void initializeField(InstanceFieldMemoryLocation fieldLoc, boolean isTraced, BytecodeEvent provider){
		Binding<InstanceFieldMemoryLocation> binding;
		if (isTraced){
			System.out.println("Default value is "+ fieldLoc.getType().getDefaultValue());
			binding = new Binding<>(fieldLoc, fieldLoc.getType().getDefaultValue(), provider);
		}else{
			binding = new Binding<>(fieldLoc, Value.NOT_AVAILABLE, BytecodeEvent.UNTRACED_BYTECODE_OPERATION);
		}
		
		this.lastBindings.put(fieldLoc.getName(), binding);
	}
	
	
	@SuppressWarnings("unchecked")
	public Binding<InstanceFieldMemoryLocation>[] getInstanceFieldBindings(){
		return (Binding<InstanceFieldMemoryLocation>[]) this.lastBindings.values().toArray(new Binding[0]);
	}
	
	public Binding<InstanceFieldMemoryLocation> getInstanceFieldBinding(String fieldName){
		assert this.lastBindings.get(fieldName) != null;
		return this.lastBindings.get(fieldName);
	}
	
	public Binding<InstanceFieldMemoryLocation> defInstanceFieldBinding(String fieldName, Value value, PUTFIELD_Operation provider){
		Binding<InstanceFieldMemoryLocation> preBinding = this.lastBindings.get(fieldName); 
		Binding<InstanceFieldMemoryLocation> newBinding = new Binding<InstanceFieldMemoryLocation>(preBinding.getMemoryLocation(), value, provider);
		this.lastBindings.put(fieldName, newBinding);
		return newBinding;
	}

	
	@Override
	public ObjectType getObjectType() {
		return ObjectType.INSTANCE;
	}

	@Override
	public int getElementCount() {
		return lastBindings.keySet().size();
	}
	
	public int getAndIncrementNextAvailableFieldOffset(){
		return this.nextAvailableFieldOffset ++;
	}

	@Override
	public List<? extends MemoryLocation> getElements() {
		List<InstanceFieldMemoryLocation> fields = new ArrayList<>();
		Set<String> keys = lastBindings.keySet();
		for(String key : keys){
			fields.add(lastBindings.get(key).getMemoryLocation());
		}
		
		Collections.sort(fields, new Comparator<InstanceFieldMemoryLocation>() {

			@Override
			public int compare(InstanceFieldMemoryLocation o1, InstanceFieldMemoryLocation o2) {
				if (o1.getPresentationOffset() < o2.getPresentationOffset())
					return -1;
				else if (o1.getPresentationOffset() == o2.getPresentationOffset())
					return 0;
				
				return 1;
			}
		});
		
		return fields;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}
	
}
