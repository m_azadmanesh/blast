package ch.usi.inf.sape.tracer.analyzer.locations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.PUTSTATIC_Operation;

/**
 * This class models the method area of the JVM where static information are kept.
 * Each instance of this class corresponds to the metadata stored in the method area
 * for a class in Java. A class in method area contains the static information about
 * that class. 
 * 
 * @author reza
 *
 */
public class ClassMetaData extends Container {

	/**
	 * keeps track of the last binding for each field based on the name
	 */
	private final Map<String, Binding<ClassFieldMemoryLocation>> lastBindings = new HashMap<>();
	
	/**
	 * The class name of the object to which this memory location refers
	 */
	protected final String className;

	/**
	 * This is used for presentation purposes. It indicates the starting offset of this class. 
	 */
	private int offset = -1;
	
	/**
	 * The loader used for loading this class 
	 */
	private final InstanceMetaData classLoader;
	
//	private AbstractContainer currentAbstractPresentation;
	
	/**
	 * An instance of this class is created upon the first access to a static field of this class. 
	 * We cannot already say at which point the class object is created (loaded). One has to call 
	 * the initialize() method right after creating an instance of this class. The body of the 
	 * initialize method could have been embedded into this constructor, but in order to avoid 
	 * passing an uninitialized "this" reference to some other method, we seperated these methods.
	 * 
	 * @param className
	 * @param classLoaderName
	 */
	public ClassMetaData(String className, InstanceMetaData classLoader) {
		super();
		this.className = className;
		this.classLoader = classLoader;
	}
	

	public void initializeField(ClassFieldMemoryLocation fieldLoc, boolean isTraced){
		Binding<ClassFieldMemoryLocation> binding;
		if (isTraced)
			binding = new Binding<>(fieldLoc, fieldLoc.getType().getDefaultValue(), BytecodeEvent.UNTRACED_BYTECODE_OPERATION);
		else
			binding = new Binding<>(fieldLoc, Value.NOT_AVAILABLE, BytecodeEvent.UNTRACED_BYTECODE_OPERATION);
		
		this.lastBindings.put(fieldLoc.getName(), binding);
	}

	@SuppressWarnings("unchecked")
	public Binding<ClassFieldMemoryLocation>[] getClassFieldBindings(){
		return (Binding<ClassFieldMemoryLocation>[]) this.lastBindings.values().toArray(new Binding[0]);
	}
	
	public Binding<ClassFieldMemoryLocation> getClassFieldBinding(String fieldName){
		assert this.lastBindings.get(fieldName) != null;
		return this.lastBindings.get(fieldName);
	}

	public Binding<ClassFieldMemoryLocation> defClassFieldBinding(String fieldName, Value value, PUTSTATIC_Operation provider){
		Binding<ClassFieldMemoryLocation> preBinding = this.lastBindings.get(fieldName); 
		Binding<ClassFieldMemoryLocation> newBinding = new Binding<ClassFieldMemoryLocation>(preBinding.getMemoryLocation(), value, provider);
		this.lastBindings.put(fieldName, newBinding);
		return newBinding;
	}
	
	@Override
	public String toString() {
		return "<"+this.className+"@"+this.classLoader+">"; 
	}

	@Override
	public int getOffset() {
		return this.offset;
	}


	@Override
	public int getElementCount() {
		return 0;
	}


	@Override
	public void setOffset(int offset) {
		this.offset = offset;
	}


	@Override
	public List<? extends MemoryLocation> getElements() {
		List<ClassFieldMemoryLocation> fields = new ArrayList<>();
		Set<String> keys = lastBindings.keySet();
		for(String key : keys){
			fields.add(lastBindings.get(key).getMemoryLocation());
		}
		
		Collections.sort(fields, new Comparator<ClassFieldMemoryLocation>() {

			@Override
			public int compare(ClassFieldMemoryLocation o1, ClassFieldMemoryLocation o2) {
				if (o1.getPresentationOffset() < o2.getPresentationOffset())
					return -1;
				else if (o1.getPresentationOffset() == o2.getPresentationOffset())
					return 0;
				
				return 1;
			}
		});
		
		return fields;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}


	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}


	/**
	 * @return the classLoaderName
	 */
	public String getClassLoaderName() {
		return classLoader == null ? "NULL" : classLoader.toString();
	}
	
	public InstanceMetaData getClassLoaderMetaData(){
		return this.classLoader;
	}

}
