package ch.usi.inf.sape.tracer.analyzer.locations;

import java.io.Serializable;
import java.util.List;

import ch.usi.inf.sape.tracer.analyzer.TraceElement;
import ch.usi.inf.sape.tracer.analyzer.abstractops.RetrievalSession;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.SmallContainer;

public abstract class Container implements Serializable, TraceElement {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4790856574561174216L;

	protected RetrievalSession currentRetrievalSession;
	
	/**
	 * This means a summarized container: it could include both abstraction as well as slicing.
	 */
	protected SmallContainer currentAbstractPresentation;
	
	public abstract List<? extends MemoryLocation> getElements();
	
	public abstract int getElementCount();
	
	public abstract int getOffset();
	
	public abstract void setOffset(int offset);
	
	/**
	 *  used only for activation records and its slices 
	 */
	public int getStartTime(){
		return -1;
	}
	
	/**
	 *  used only for activation records and its slices 
	 */
	public int getEndTime(){
		return -1;
	}

	public SmallContainer getCurrentAbstractPresentation(RetrievalSession session) {
		if (this.currentRetrievalSession != session){
			this.currentRetrievalSession = session;
			this.currentAbstractPresentation = currentRetrievalSession.createSmallContainer(this);
		}
		
		return this.currentAbstractPresentation;
	}

	
}
