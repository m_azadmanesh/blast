package ch.usi.inf.sape.tracer.analyzer.locations;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.usi.inf.sape.tracer.analyzer.Utils;
import ch.usi.inf.sape.tracer.analyzer.WeakIdentityHashMap;

public class GlobalSpace {
	
	/**
	 * The weak mapping from a Java object to the corresponding ObjectMemoryLocation instance
	 */
	private static WeakIdentityHashMap<Object, ClassMetaData> classMap = new WeakIdentityHashMap<>();

	private static int nextClassId;
	
	public static ClassMetaData getOrDefineClassLocationMetaData(Class klass){
		ClassMetaData classLoc = (ClassMetaData) classMap.get(klass);
		if (classLoc == null){
			classLoc = defineClassLocationMetaData(klass, false);
		}
		
		return classLoc;
	}
	
	public static ClassMetaData defineClassLocationMetaData(Class klass, boolean isTraced){
		ClassMetaData classLocation = null;
		try{
			ClassLoader cl = klass.getClassLoader();
			classLocation = new ClassMetaData(klass.getName(), cl == null ? null: HeapSpace.getOrDefineInstanceLocationMetaData(cl));
			classMap.put(klass, classLocation);
			Field[] fields = findAllFieldsInHierarchy(klass);
			for (int i = 0; i < fields.length; i++) {
				if(Modifier.isStatic(fields[i].getModifiers())){
					ClassFieldMemoryLocation fieldLocation = new ClassFieldMemoryLocation(classLocation, fields[i].getName(), Utils.findTypeFromFullTypeName(fields[i].getType().getName()));
					classLocation.initializeField(fieldLocation, isTraced);
				}
			}
		}catch(Throwable t){  //We should never cause any interrupt in the normal program execution. In case the object is null, we postpone the exception to the point where the program really sees that value. 
			t.printStackTrace();
		}  
		
		return classLocation;
	}

	/**
	 * The method class.getDeclaredFields() just returns the fields of the current class. 
	 * This method is used to traverse the inheritence hierarchy and finds all the fields this
	 * can owns or inherits.
	 * 
	 * A super class can have a field with the same name as a field in the subclass. In this
	 * case the subclass's fields overrides the superclass's. 
	 * 
	 * @param klass
	 * @return
	 */
	private static Field[] findAllFieldsInHierarchy(Class klass){
		Set<FieldInHierarchy> set = new HashSet<>();
		try{
			while (klass != null){
				for (Field field : klass.getDeclaredFields()){
					FieldInHierarchy fieldInHierarchy = new FieldInHierarchy(field);
					set.add(fieldInHierarchy);
				}
				klass = klass.getSuperclass();
			}
		}catch(Throwable t){	//The reflection api may throw ClassNotFoundExcpetion while resolving the field dependencies. 
			t.printStackTrace(System.out);
		}

		Field[] fields = new Field[set.size()];
		int i = 0;
		for (FieldInHierarchy fieldInHierarchy : set){
			fields[i] = fieldInHierarchy.field;
			i++;
		}
		return fields;
	}
	
	static class FieldInHierarchy {
		Field field;
		
		public FieldInHierarchy(Field field){
			this.field = field;
		}

		@Override
		public boolean equals(Object arg0) {
			if (arg0 == null || !(arg0 instanceof FieldInHierarchy))
				return false;
			
			FieldInHierarchy fieldInHierarchy = (FieldInHierarchy) arg0;
			if (fieldInHierarchy.field.getName().equals(this.field.getName()))
				return true;
			
			return false;
		}

		/**
		 * This causes the equals method be invoked each time a new instance of
		 * this class is to be added to a set.
		 */
		@Override
		public int hashCode() {
			return 1;
		}
		
		

	}
}
