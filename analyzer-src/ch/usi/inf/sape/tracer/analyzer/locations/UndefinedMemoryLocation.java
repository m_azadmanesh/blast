package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;

public class UndefinedMemoryLocation extends MemoryLocation{

	@Override
	public int getPresentationOffset() {
		return 0;
	}

	@Override
	public MemorySpaceType getSpaceType() {
		return null;
	}

	@Override
	public Container getContainer() {
		return null;
	}

	@Override
	public boolean isStackSlot() {
		return false;
	}

	@Override
	public String getName() {
		return "UNDEFINED";
	}

	@Override
	public String toString(){
		return "UNDEFINED";
	}

	@Override
	public LocationType getLocationType() {
		return LocationType.UNDEFINED;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
