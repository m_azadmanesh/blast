package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;

public interface MemoryLocationInfo {
	
	public static enum Type {
			BOOLEAN("boolean", 'Z', 1), 
			CHAR("char", 'C', 1), 
			BYTE("byte", 'B', 1), 
			SHORT("short", 'S', 1), 
			INT("int", 'I', 1), 
			FLOAT("float", 'F', 1), 
			LONG("long", 'J', 2), 
			DOUBLE("double", 'D', 2), 
			REFERENCE("Object", 'A' , 1),
			VOID("void", 'V',  0),
			UNAVAILABLE("unavailable", '\0', 0),
			UNDEFINED("undefined", '\0', 0);
			
		private final int category;
		
		private final char descriptor;
		
		private final String name;
		
		private Value defaultValue;
		
		Type(String name, char desc, int category){
			this.category = category;
			this.descriptor = desc;
			this.name = name;
		}
		
		/**
		 * @return the category to which this type belongs. The category id is also indicating the length of a value of that type
		 */
		public int getCategory(){
			return this.category;
		}
		
		/**
		 * @return the type descriptor as specified in JVM spec
		 */
		public char getDescriptor(){
			return this.descriptor;
		}
		
		/**
		 * Looks stupid to use a switch where we have dynamic dispatch,
		 * but, indeed, we cannot use dynamic dispatch here, because we would
		 * need a field to keep track of the defaultValue of type Value which 
		 * would be initialized while initializing each static instance of the 
		 * enum Type. Given that each value has a type, that reference would be 
		 * set to null and there is no way to get over it. So, instead of doing
		 * the initialization of the default value at the same time with the
		 * initialization of enum constants, we postpone it to this step when
		 * the value is actually requested.
		 * 
 		 * @return the default Value for this type as specified in JVM spec
		 */
		public Value getDefaultValue() {
			if (defaultValue != null)
				return defaultValue;
			
			switch(this){
			case BOOLEAN:
				defaultValue = new Value(false);
				break;
			case CHAR:
				defaultValue = new Value('\u0000');
				break;
			case BYTE:
				defaultValue = new Value((byte)0);
				break;
			case SHORT:
				defaultValue = new Value((short)0);
				break;
			case INT:
				defaultValue = new Value(0);
				break;
			case FLOAT:
				defaultValue = new Value(0f);
				break;
			case LONG:
				defaultValue = new Value(0l);
				break;
			case DOUBLE:
				defaultValue = new Value(0d);
				break;
			case REFERENCE:
				defaultValue = Value.NULL;
				break;
			default:
				throw new IllegalStateException("Unsupported Type ...!");
			}
			
			return defaultValue;
		}

		public static Type getTypeForDescriptor(String descriptor){
			switch(descriptor){
			case "Z":
				return BOOLEAN; 
			case "C":
				return CHAR;
			case "B":
				return BYTE;
			case "S":
				return SHORT;
			case "I":
				return INT;
			case "F":
				return FLOAT;
			case "J":
				return LONG;
			case "D":
				return DOUBLE;
			case "V":
				return VOID;
			default:
			{
				if (descriptor.startsWith("L") || descriptor.startsWith("[")) 
					return REFERENCE;
				else
					throw new RuntimeException("Wrong type descriptor!");
			}
			}
		}
		
		public String getName(){
			return this.name;
		}
	}
	
	
	public static MemoryLocation UNDEFINED_MEMORY_LOCATION = new UndefinedMemoryLocation();
}

