package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;

/**
 * This class is used in abstraction phase. Given that during abstraction, we don't
 * have the notion of stack slot memory locations, we use instances of this class to
 * present the flow of information for some special cases.
 * 
 *  E.g.:
 *  A a = new A();
 *  this translates into:
 *  new LA;
 *  dup
 *  invokespecial "<init>"
 *  
 *  we use this presentation for such case:
 *  temp = new A
 *  temp.A();
 *  
 *  There are other more complicated scenarios where we need to introduce temporary 
 *  values like when there is different derivations of dup instruction or the combination
 *  of iload plus dup.
 *  
 * @author reza
 *
 */
public class TemporaryMemoryLocation extends MemoryLocation{

	/**
	 * The index of this temporary in the current activation
	 */
	private final int index;
	
	/**
	 * The container in which this temporary is supposed to reside. 
	 */
	private final Container container;
	
	public TemporaryMemoryLocation(final int index, final Container container) {
		this.index = index;
		this.container = container;
	}

	@Override
	public String toString() {
		return "temp_" + this.index;
	}

	@Override
	public boolean isStackSlot() {
		return false;
	}

	@Override
	public int getPresentationOffset() {
		return index;
	}

	@Override
	public MemorySpaceType getSpaceType() {
		return MemorySpaceType.STACK_SPACE;
	}

	@Override
	public Container getContainer() {
		return container;
	}

	@Override
	public String getName() {
		return "temp_"+index;
	}

	@Override
	public LocationType getLocationType() {
		return LocationType.TEMPORARY_VAR;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
