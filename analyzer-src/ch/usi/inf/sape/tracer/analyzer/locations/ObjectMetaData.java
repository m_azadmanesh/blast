package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent;

/**
 * An object location is a placeholder for the meta information about the Java object it corresponds to.
 * For each Java object, there will be an ObjectMemoryLocation instance and a weak identity hash map
 * keeps track of this association, i.e., the Java object is used as the key and the corresponding 
 * ObjectMemoryLocation instance is the value of the mapping.
 * 
 * @author reza
 *
 */
public abstract class ObjectMetaData extends Container{

	/**
	 *  An ObjectMemoryLocation instance can represent either a Class object, an array, or a normal Java object. 
	 *    
	 */
	public static enum ObjectType {INSTANCE, ARRAY};
	
	/**
	 * The meta data of the class object to which this memory location refers
	 */
	protected final ClassMetaData klass;
	
	/**
	 * We use an integer for uniquely identifying objects. This is used only for presentation purposes. 
	 */
	protected final int id;
	
	/**
	 * This is used for presentation purposes. It indicates the offset of this particular object within the heap space.
	 */
	protected int offset = -1;
	
	protected final BytecodeEvent provider;
		
//	protected AbstractContainer currentAbstractPresentation;
	
	public ObjectMetaData(ClassMetaData klass, int id, BytecodeEvent provider) {
		this.klass = klass;
		this.id = id;
		this.provider = provider;
	}

	public abstract ObjectType getObjectType();
	
	@Override
	public String toString() {
		return klass.getClassName() + "@" + id;
	}


	/**
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 * @param offset the offset to set
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getClassName(){
		return this.klass.getClassName();
	}
	
	public int getId(){
		return this.id;
	}

	/**
	 * @return the provider
	 */
	public BytecodeEvent getProvider() {
		return provider;
	}
	
	public ClassMetaData getClassMetaData(){
		return this.klass;
	}
}
