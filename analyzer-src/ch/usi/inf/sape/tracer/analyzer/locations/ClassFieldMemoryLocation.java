package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

public class ClassFieldMemoryLocation extends MemoryLocation{
	
	/**
	 * class location owning this field
	 */
	private final ClassMetaData klass;
	
	/**
	 * The name of this field
	 */
	private final String name;
	
	/**
	 * The type of this field
	 */
	private final Type type;
	
	public ClassFieldMemoryLocation(ClassMetaData klass, String name, Type type) {
		this.klass = klass;
		this.name = name;
		this.type = type;
	}
	
	@Override
	public String getName(){
		return this.name;
	}
	
	@Override
	public String toString(){
		return klass.toString() + "." + this.name;
	}
	
	public Type getType(){
		return type;
	}

	@Override
	public int getPresentationOffset() {
		return 0;
	}

	@Override
	public MemorySpaceType getSpaceType() {
		return MemorySpaceType.GLOBAL_SPACE;
	}

	@Override
	public Container getContainer() {
		return klass;
	}

	@Override
	public boolean isStackSlot() {
		return false;
	}

	@Override
	public LocationType getLocationType() {
		return LocationType.CLASS_FIELD;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
