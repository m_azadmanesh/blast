package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;

public class ArrayLengthMemoryLocation extends MemoryLocation{
	
	private final ArrayMetaData array;

	public ArrayLengthMemoryLocation(ArrayMetaData array) {
		this.array = array;
	}

	@Override
	public String toString() {
		return array+".length";
	}

	@Override
	public int getPresentationOffset() {
		return array.getElementCount() - 1;
	}

	@Override
	public MemorySpaceType getSpaceType() {
		return MemorySpaceType.HEAP_SPACE;
	}

	@Override
	public ArrayMetaData getContainer() {
		return array;
	}

	@Override
	public boolean isStackSlot() {
		return false;
	}

	@Override
	public String getName() {
		return this.toString();
	}

	@Override
	public LocationType getLocationType() {
		return LocationType.ARRAY_LENGTH;
	}
	
	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
