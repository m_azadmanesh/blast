package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

/*TODO This class is used to avoid from keeping redundant 
 * data about the local variables. Currently it is not used, 
 * but should be integrated to the framework later. */
public final class LocalMetaData {

	private final int index;
	
	/**
	 * Index in the code array where this local variable location starts to have a value (inclusive)
	 */
	private final int start;
	
	/**
	 * Index in the code array where this local variable location ends to have a value (exclusive)
	 */
	private final int end;
	
	/**
	 * this is used for visualization purposes when we need to uniquely refer to this local in a tabular presentaion
	 */
	private final int offset;
	
	/**
	 * The variable name
	 */
	private final String name;
	
	private final Type type;

	public LocalMetaData(int index, int start, int end, String name, Type type, int offset){
		this.index = index;
		this.start = start;
		this.end = end;
		this.offset = offset;
		this.name = name;
		this.type = type;
	}

	public boolean isAlive(int insnIndex){
		if (this.end ==  -1)
			return true;		//this is the case when the method to which this local belongs is not instrumented and we don't have any info about it. we just save the args info.
		
		if (insnIndex < this.end && insnIndex >= this.start-1)
			return true;
		else
			return false;
	}


	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public Type getType(){
		return this.type;
	}

	/**
	 * 
	 * @return the offset among local variables defined within this activation
	 */
	public int getOffset(){
		return this.offset;
	}

}
