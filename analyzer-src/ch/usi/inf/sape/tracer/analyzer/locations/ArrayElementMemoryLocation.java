package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;

public class ArrayElementMemoryLocation extends MemoryLocation{
	
	private final ArrayMetaData array;
	
	private final int index;

	public ArrayElementMemoryLocation(ArrayMetaData array, int index) {
		this.array = array;
		this.index = index;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	@Override
	public String toString() {
		return array+"[" + this.index + "]";
	}

	@Override
	public int getPresentationOffset() {
		return index;
	}

	@Override
	public MemorySpaceType getSpaceType() {
		return MemorySpaceType.HEAP_SPACE;
	}

	@Override
	public ArrayMetaData getContainer() {
		return array;
	}

	@Override
	public boolean isStackSlot() {
		return false;
	}

	@Override
	public String getName() {
		return this.toString();
	}

	@Override
	public LocationType getLocationType() {
		return LocationType.ARRAY_ELEMENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
