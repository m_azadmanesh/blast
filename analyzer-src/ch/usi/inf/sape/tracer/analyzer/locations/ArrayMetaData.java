package ch.usi.inf.sape.tracer.analyzer.locations;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Utils;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.MULTIANEWARRAY_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XASTORE_Operation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

public class ArrayMetaData extends ObjectMetaData{

	private final Type elementType;
	
	private final Binding<ArrayElementMemoryLocation>[] elementBindings;
	
	private final Binding<ArrayLengthMemoryLocation> lengthBinding;
	
	public ArrayMetaData(ClassMetaData klass, int id, int size, Type elementType, BytecodeEvent provider) {
		super(klass, id, provider);
		this.elementType = elementType;
		this.elementBindings = new Binding[size];
		this.lengthBinding = new Binding<>(new ArrayLengthMemoryLocation(this), new Value(size), provider);
	}

	/**
	 * This constructor must only be used for an array that has subarrays as its elements
	 * @param id
	 * @param className
	 * @param size
	 * @param elementType
	 * @param provider
	 */
	public ArrayMetaData(int id, ClassMetaData klass, int size, Type elementType, BytecodeEvent provider) {
		super(klass, id, provider);
		this.elementType = elementType;
		this.elementBindings = new Binding[size];
		this.lengthBinding = new Binding<>(new ArrayLengthMemoryLocation(this), new Value(size), provider);
	}

	
	@Override
	public ObjectType getObjectType() {
		return ObjectType.ARRAY;
	}
	
	public void initializeArrayElement(ArrayElementMemoryLocation location, int index, boolean isTraced, BytecodeEvent provider){
		if (isTraced)
			this.elementBindings[index] = new Binding<>(location, elementType.getDefaultValue(), provider);
		else
			this.elementBindings[index] = new Binding<>(location, Value.NOT_AVAILABLE, BytecodeEvent.UNTRACED_BYTECODE_OPERATION);
	}
	
	public Binding<ArrayElementMemoryLocation>[] getElementBindings(){
		return (Binding<ArrayElementMemoryLocation>[]) this.elementBindings;
	}
	
	/**
	 * @return the lengthBidning
	 */
	public Binding<ArrayLengthMemoryLocation> useLengthBinding() {
		return lengthBinding;
	}

	public Binding<ArrayElementMemoryLocation> useElementBinding(int index){
		return elementBindings[index];
	}
	
	public Binding<ArrayElementMemoryLocation> defElementBinding(int index, Value value, XASTORE_Operation provider){
		ArrayElementMemoryLocation elementLocation = elementBindings[index].getMemoryLocation();
		assert elementLocation != null;
		Binding<ArrayElementMemoryLocation> binding = new Binding<ArrayElementMemoryLocation>(elementLocation, value, provider);
		this.elementBindings[index] = binding;
		return binding;
	}
	
	public Binding<ArrayElementMemoryLocation> defElementBinding(ArrayMetaData array, int index, Object subArray, MULTIANEWARRAY_Operation provider){
		elementBindings[index] = new Binding(new ArrayElementMemoryLocation(array, index), Utils.getValueForObject(subArray), provider);
		return elementBindings[index];
	}

	@Override
	public int getElementCount() {
		return elementBindings.length + 1;
	}
	
	@Override
	public List<MemoryLocation> getElements() {
		List<MemoryLocation> elementss = new ArrayList<>();
		for (Binding<ArrayElementMemoryLocation> binding : elementBindings) {
			elementss.add(binding.getMemoryLocation());
		}
		elementss.add(lengthBinding.getMemoryLocation());
		return elementss;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
