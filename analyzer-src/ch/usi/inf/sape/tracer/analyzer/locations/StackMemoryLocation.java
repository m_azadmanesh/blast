package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.Activation;

public abstract class StackMemoryLocation extends MemoryLocation{
	
	/**
	 * The activation to which this stack slot or local variable or retrun value location belongs
	 */
	protected final Activation frame;

	public StackMemoryLocation(Activation frame) {
		this.frame = frame;
	}
	
	public Activation getFrame(){
		return this.frame;
	}
}
