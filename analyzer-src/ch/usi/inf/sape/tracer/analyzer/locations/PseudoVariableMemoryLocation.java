package ch.usi.inf.sape.tracer.analyzer.locations;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;

public class PseudoVariableMemoryLocation extends MemoryLocation {
	
	private final int pseudoVariableId;
	private final Activation stackFrame;
	
	public PseudoVariableMemoryLocation(Activation frame, int pseudoVar) {
		this.stackFrame = frame;
		this.pseudoVariableId = pseudoVar;
	}

	@Override
	public Container getContainer() {
		return this.stackFrame;
	}

	@Override
	public boolean isStackSlot() {
		return false;
	}

	@Override
	public int getPresentationOffset() {
		return -1;		//Filtered from being visualized
	}

	@Override
	public MemorySpaceType getSpaceType() {
		return MemorySpaceType.STACK_SPACE;
	}

	@Override
	public String getName() {
		return "P"+pseudoVariableId+"<"+stackFrame+">";
	}

	public int getPseudoVariableId(){
		return this.pseudoVariableId;
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public LocationType getLocationType() {
		return LocationType.PSEUDO_VAR;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
