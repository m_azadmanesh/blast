package ch.usi.inf.sape.tracer.analyzer;

public interface TraceElement {

	public void accept(TraceElementVisitor visitor) throws VisitorException;
}
