package ch.usi.inf.sape.tracer.analyzer;

import ch.usi.inf.sape.tracer.analyzer.locations.HeapSpace;
import ch.usi.inf.sape.tracer.analyzer.locations.ObjectMetaData;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

public class Value implements TraceElement{
	
	/**
	 * A single representation of null
	 */
	public static Value NULL = new Value("NULL");
	
	/**
	 * This is the case for a value coming from an uninstrumented method to an instrumented method
	 */
	public static Value NOT_AVAILABLE = new Value("NA");
	
	/**
	 *	used for the void cases 
	 */
	public static Value NO_VALUE = new Value();
	
	/**
	 * used for the value of the pre-operations where we don't have the actual value yet and the value 
	 * will be either computed on the next step, or an exception will be thrown.
	 */
	public static Value UNDEFINED = new Value("UNDEFINED");

	/**
	 * Cache some of value objects that are used very frequently
	 */
	public static final Value CONST_M1 = new Value(-1);
	public static final Value CONST_0 = new Value(0);
	public static final Value CONST_1 = new Value(1);
	public static final Value CONST_2 = new Value(2);
	public static final Value CONST_3 = new Value(3);
	public static final Value CONST_4 = new Value(4);
	public static final Value CONST_5 = new Value(5);
	
	public static Value FALSE = new Value(false);
	
	public static Value TRUE = new Value(true);
	
	public static Value CHAR_NULL = new Value('\u0000');
	
	protected final String value;
	
	protected final Type type;
	
	public Value(int val){
		this.value = String.valueOf(val);
		this.type = Type.INT;
	}
	
	public Value (char val){
		this.value = String.valueOf(val);
		this.type = Type.CHAR;
	}
	
	public Value(float val){
		this.value = String.valueOf(val);
		this.type = Type.FLOAT;
	}

	public Value(double val){
		this.value = String.valueOf(val);
		this.type = Type.DOUBLE;
	}

	public Value(long val){
		this.value = String.valueOf(val);
		this.type = Type.LONG;
	}
	
	public Value(boolean val){
		this.value = String.valueOf(val);
		this.type = Type.BOOLEAN;
	}
	
	/**
	 * This should be used only for initializing
	 * special values like NO_VALUE.
	 * 
	 */
	private Value(){
		this.value = "";
		this.type = Type.VOID;
	}
	
	protected Value(String metaStringValue, Type type){
		this.type = type;
		this.value = metaStringValue;
	}
	
	/**
	 * This is NOT a constructor for making String values.
	 * That is why it is private.
	 * 
	 * This constructor has to be used only for special purposes
	 * like creating exceptional values such as UNDEFINED, ...
	 * For constructing a string value, we always must use
	 * the StringValue class.
	 * 
	 * @param value
	 */
	private Value(String value){
		this.value = value;
		this.type = Type.UNDEFINED;
	}
	
	@Override
	public String toString() {
		return value;
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		if (type == null)
			return type.UNDEFINED;
		
		return type;
	}

	public String getValueString(){
		return value;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Value other = (Value) obj;
		if (type != other.type)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}
	
}
