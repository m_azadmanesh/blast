package ch.usi.inf.sape.tracer.analyzer;

import ch.usi.inf.sape.tracer.analyzer.bytecodeops.NonSlicedBytecodeEvent;

public interface NonSlicedAbstractOperation {

	public NonSlicedBytecodeEvent[] getBytecodes();
}
