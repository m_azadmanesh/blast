package ch.usi.inf.sape.tracer.analyzer;

import java.io.Serializable;
import java.util.Comparator;

import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

/**
 * Every execution step (event) in the course of the execution is considered as an operation.
 * The events can be defined in different levels. At the lowest level, we have bytecodes. The
 * other level is abstract operations that are close to program statements.
 * 
 * We divide operations also according to their usage: for slicing or for tracing. When used 
 * for slicing, some of the def locations are filtered because those locations might not have
 * been used for computing the value at slice criterion. In contrast, an operation used during
 * tracing includes all the use and defs, no matter if they get used whatsoever.
 * 
 * @author reza
 *
 */
public interface Event extends Serializable, TraceElement{
	
	/**
	 * @return The set of bindings used by this event
	 */
	public abstract Binding<? extends MemoryLocation>[] getUses();
	
	/**
	 * @return The set of bindings defined by this event. These bindings 
	 * are read only and can be used by other events, but never changed.
	 */
	public abstract Binding<? extends MemoryLocation>[] getDefs();
	
	/**
	 * Given a binding, this method finds the bindings that contributed to
	 * defining it through this event. Depending on the semantics of each
	 * bytecode, either all or part or none of the use set of the event 
	 * could have contributed to the given binding. 
	 * 
	 * @param def
	 */
	public abstract Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def);
	
	/**
	 * @return The lexical name of the bytecode represented by this event
	 */
	public abstract String getOperationName();
	
	/**
	 * @return the operationIndex
	 */
	public int getOperationIndex();
	
	/**
	 * @return The line no. in source code to which this event belongs.
	 */
	public int getSourceCodeLineNumber();
	
	/**
	 * We identify each bytecode using a unique operation code. 
	 * 
	 * @return The operation code corresponding to the bytecode of this event. 
	 */
	public short getOperationCode();
	
	/**
	 * @return The activation record which was active when this event was executed.
	 */
	public Activation getActivationRecord();
	
	/**
	 * Each bytecode is contained in a class file. Several class files can
	 * belong to a single source file. This method returns the name of the
	 * source file to which this event was stemmed from. 
	 * 
	 * The format of the name is as follows:
	 * Full address of the package + / + <file name>.java
	 * 
	 * E.g.:
	 * java/lang/Object.java
	 */
	public String getSourceFileNameWithFullAddress();

	/**
	 * 
	 * @return the name of the source file without any package-related info
	 */
	public String getSourceFileName();
	
	/**
	 * We use this method for serialization purposes. Each event class
	 * has a code that uniquely identifies it.
	 *  
	 * @return The code of the event class.
	 */
	public byte getEventClassCode();
}
