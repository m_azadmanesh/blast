package ch.usi.inf.sape.tracer.analyzer;

import ch.usi.inf.sape.tracer.analyzer.locations.ObjectMetaData;

public class ReferenceTo {
	
	private final ObjectMetaData referred;

	public ReferenceTo(ObjectMetaData referred) {
		this.referred = referred;
	}

	/**
	 * @return the referred
	 */
	public ObjectMetaData getReferred() {
		return referred;
	}

	@Override
	public String toString() {
		return referred.toString();
	}

	
}
