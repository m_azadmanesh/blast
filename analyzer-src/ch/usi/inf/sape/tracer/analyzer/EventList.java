package ch.usi.inf.sape.tracer.analyzer;

import java.util.ArrayList;
import java.util.List;

public class EventList<E extends Event> {
	private final List<E> events;
	
	public EventList(final List<E> events){
		this.events = new ArrayList<E>(events);
	}
	
	public E last(){
		if (events.size() == 0)
			return null;
		
		return events.get(events.size() - 1);
	}

}
