package ch.usi.inf.sape.tracer.analyzer;

import java.util.Set;

import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

/**
 * An instance of this class provides a snapshot of the state of the stack right before
 * executing a bytecode which leads into an abstract operation.
 * 
 * @author reza
 *
 */
public class StackSnapshot {
	private final Binding<StackSlotMemoryLocation>[] slots;
	
	public StackSnapshot(Binding<StackSlotMemoryLocation>[] slots) {
		this.slots = slots;
	}
	
	public static final StackSnapshot createSnapshot(Binding<StackSlotMemoryLocation>[] slots){
		StackSnapshot snapshot = new StackSnapshot(slots);
		return snapshot;
	}
	
	public boolean contains(Binding binding){
		boolean contains = false;
		for (Binding<StackSlotMemoryLocation> slot : slots) {
			if (slot.equals(binding))		//could be replaced with identity comparison, though for refactoring purposes we still use the equality function
				contains = true;
		}
		return contains;
	}
	
	@Override
	public String toString(){
		StringBuffer buffer = new StringBuffer();
		for (Binding slot : slots) {
			buffer.append(slot.toString()).append(",\t");
		}
		return buffer.toString();
		
	}
}
