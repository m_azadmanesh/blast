package ch.usi.inf.sape.tracer.analyzer;


import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

/**
 * A hash map that uses identity for key comparisons and does not prevent its
 * key from being collected.
 *
 * @author Hiroshi Yamauchi
 */
public class WeakIdentityHashMap<K, V> implements Map<K, V> {

  private final WeakHashMap<ReferenceWrapper<K>, V> _map;

  public WeakIdentityHashMap() {
    _map = new WeakHashMap<ReferenceWrapper<K>, V>();
  }

  private ReferenceWrapper<K> wrapK(final K key) {
    return new ReferenceWrapper<K>(key);
  }

  private static ReferenceWrapper<Object> wrap(final Object key) {
    return new ReferenceWrapper<Object>(key);
  }

  @Override
  public void clear() {
    _map.clear();
  }

  @Override
  public boolean containsKey(final Object key) {
    return _map.containsKey(wrap(key));
  }

  @Override
  public boolean containsValue(final Object value) {
    return _map.containsValue(value);
  }

  @Override
  public Set<Map.Entry<K, V>> entrySet() {
    throw new UnsupportedOperationException();
  }

  @Override
  public V get(final Object key) {
    return _map.get(wrap(key));
  }

  @Override
  public boolean isEmpty() {
    return _map.isEmpty();
  }

  @Override
  public Set<K> keySet() {
    throw new UnsupportedOperationException();
  }

  @Override
  public V put(final K key, final V value) {
    _map.put(wrapK(key), value);
    return value;
  }

  @Override
  public void putAll(final Map<? extends K, ? extends V> m) {
    putAll2(m);
  }

  protected <L extends K, W extends V> void putAll2(final Map<L, W> m) {
    final Set<Map.Entry<L, W>> entries = m.entrySet();
    for (final Map.Entry<L, W> e : entries) {
      put(e.getKey(), e.getValue());
    }
  }

  @Override
  public V remove(final Object key) {
    return _map.remove(wrap(key));
  }

  @Override
  public int size() {
    return _map.size();
  }

  @Override
  public Collection<V> values() {
    return _map.values();
  }

  private static class ReferenceWrapper<L> {

    protected final L _ref;

    protected ReferenceWrapper(final L ref) {
      _ref = ref;
    }

    @Override
    public boolean equals(final Object x) {
      if (!(x instanceof ReferenceWrapper)) {
        return false;
      }
      return _ref == ((ReferenceWrapper<?>)x)._ref;
    }

    @Override
    public int hashCode() {
      return System.identityHashCode(_ref);
    }
  }
  
	public static void main(String[] args) {
		WeakIdentityHashMap<Object, Integer> map = new WeakIdentityHashMap<Object, Integer>();
		Object o1 = new Object();
		Object o2 = new Object();
		map.put(o1, 3);
		map.put(o2, 4);
		System.out.println(map.get(o2));
	}

}
