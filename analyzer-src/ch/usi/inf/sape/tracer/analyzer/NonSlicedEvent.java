package ch.usi.inf.sape.tracer.analyzer;

import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

public abstract class NonSlicedEvent implements Event{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2277842982695184020L;

	protected final int operationIndex;
	
	protected final short opCode;
	
	protected Binding<? extends MemoryLocation>[] uses;
	
	protected Binding<? extends MemoryLocation>[] defs;
	
	protected final int sourceLineNo;
	
	protected final Activation activationRecord;
	
	public NonSlicedEvent(short opCode, 
			int sourceLineNo, 
			int operationIndex, 
			Activation activationRecord) {
		this(opCode, sourceLineNo, operationIndex, activationRecord, null, null);
	}
	
	public NonSlicedEvent(short opCode, int sourceLineNo, 
			int operationIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs){ 
		this.operationIndex = operationIndex;
		this.opCode = opCode;
		this.uses = uses;
		this.defs = defs;
		this.sourceLineNo = sourceLineNo;
		this.activationRecord = activationRecord;
	}


	/**
	 * Temporary constructor for compatibility purposes
	 * TODO: remove this constructor and fix the code in AbstractOperation
	 */
	public NonSlicedEvent(int opCode, int sourceLine, int operationIndex){
		this.opCode = (short) opCode;
		this.sourceLineNo = sourceLine;
		this.operationIndex = operationIndex;
		this.activationRecord = null;
	}
	

	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		return this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public int getOperationIndex() {
		return operationIndex;
	}

	@Override
	public int getSourceCodeLineNumber() {
		return sourceLineNo;
	}

	@Override
	public short getOperationCode() {
		return opCode;
	}

	@Override
	public Activation getActivationRecord() {
		return this.activationRecord;
	}
	
	@Override
	public String getSourceFileNameWithFullAddress() {
		return getActivationRecord().getSourceFileWithFullAdress(); 
	}

	@Override
	public String getSourceFileName() {
		return getActivationRecord().getSourceFile();
	}
	
	public abstract SlicedEvent getSlicedOperationForSession(final int sessionId);
}
