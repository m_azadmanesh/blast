package ch.usi.inf.sape.tracer.analyzer.cdg;

public interface Traverse<V>{

	public void traverse(V root);
}
