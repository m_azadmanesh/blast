package ch.usi.inf.sape.tracer.analyzer.cdg;

import java.lang.reflect.Array;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class GraphMatrix<V,E> implements Graph<V,E>{

	protected final int size;
	protected final Edge<V,E>[][] matrix;
	protected final Map<V, GraphMatrixVertex<V>> map;
	protected final List<Integer> freeList;
	protected V root;
	
	/**
	 * An implementation of Graph based on adjacency matrix.
	 * 
	 * @param size
	 */
	@SuppressWarnings("unchecked")
	protected GraphMatrix(int size){
		this.size = size;
		this.matrix = (Edge<V,E>[][])Array.newInstance(Edge.class, size, size);
		this.map = new Hashtable<>(size);
		this.freeList = new LinkedList<>();
		for (int i = size - 1; i >= 0; i--) {
			freeList.add(i);
		}
	}

	@Override
	public void addVertex(V label, Label<V> appNode) {
		if (map.containsKey(label))
			return;
		
		int row = freeList.remove(0);
		
		map.put(label, new GraphMatrixVertex<V>(appNode, row));
	}

	@Override
	public V removeVertex(V label) {
		GraphMatrixVertex<V> vertex = map.remove(label);
		if (vertex == null)
			return null;
		
		int index = vertex.getIndex();
		for (int i = 0; i < matrix.length; i++) {
			matrix[index][i] = null;
			matrix[i][index] = null;
		}
		
		freeList.add(index);
		return vertex.getLabel();
	}


	@Override
	public Iterator<V> iterator() {
		return map.keySet().iterator();
	}

	@Override
	public Edge<V, E> getEdge(V label1, V label2) {
		return matrix[map.get(label1).getIndex()][map.get(label2).getIndex()];
	}

	@Override
	public boolean containsVertex(V label) {
		return map.get(label) != null ? true : false;
	}

	@Override
	public boolean containsEdge(V label1, V label2) {
		Edge<V,E> e = getEdge(label1, label2);
		return e != null ? true : false;
	}

	@Override
	public boolean visitEdge(Edge<V, E> e) {
		return e.visit();
	}

	@Override
	public boolean isVisitedVertex(V label) {
		return map.get(label).isVisited();
	}

	@Override
	public boolean isVisitedEdge(Edge<V, E> e) {
		return e.isVisited();
	}

	@Override
	public void reset() {
		Iterator<V> iterator = map.keySet().iterator();
		while(iterator.hasNext()){
			V label = iterator.next();
			GraphMatrixVertex<V> vtx = map.get(label);
			vtx.reset();
			int row = vtx.getIndex();
			for (int i = 0; i < matrix.length; i++) {
				Edge<V,E> edge = matrix[row][i];
				if (edge != null)
					edge.reset();
			}
		}
	}

	@Override
	public int size() {
		return matrix.length;
	}

	@Override
	public Set<V> getSuccessors(V label){
		GraphMatrixVertex<V> vtx = map.get(label);
		
		Set<V> neighbors = new HashSet<>();
		for (int i = 0; i < matrix.length; i++) {
			Edge<V,E> edge = matrix[vtx.getIndex()][i];
			if (edge != null){
				if (edge.origin().equals(vtx.getLabel()))
					neighbors.add(edge.target());
				else
					neighbors.add(edge.origin());
			}
		}
		
		return neighbors;
	}
	
	@Override
	public Set<V> getPredecessors(V label){
		int col = map.get(label).getIndex();
		Set<V> predecessors = new HashSet<>();
		for (int i = 0; i < matrix.length; i++) {
			Edge<V,E> edge = matrix[i][col];
			if (edge != null){
				if (edge.target().equals(label))
					predecessors.add(edge.origin());
				else
					predecessors.add(edge.target());
			}
		}
		
		return predecessors;
	}

	
	@Override
	public String toString() {
		String res = "";
		res += "Graph G = {\n";
		res += "V = ";
		Set<V> vertices = getVertices();
		res+="[";
		for (V v : vertices) {
			res+= (getVertex(v).getVertexContents().getID() + ", ");
		}
		res+= "]\n";
		res += "E = ";
		Set<Edge<V,E>> edges= edges();
		res += String.valueOf(edges);
		res += "\n";
		res += "}\n";
		res += "Root is " + this.root;
		return res;
	}

	@Override
	public void setRoot(V label) {
		if (map.get(label)== null)
			throw new IllegalStateException("The root set to an uninitialized node!");
		
		this.root = label;
	}

	@Override
	public V getRoot() {
		if (root == null)
			throw new IllegalStateException("The root node not initialized yet!");
		
		return this.root;
	}

	@Override
	public Vertex<V> getVertex(V label) {
		return map.get(label);
	}

	@Override
	public Set<V> getVertices() {
		return map.keySet();
	}

}
