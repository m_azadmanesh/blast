package ch.usi.inf.sape.tracer.analyzer.cdg;

public interface Visitor {

	public void visit(Orderable v);
}
