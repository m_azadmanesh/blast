package ch.usi.inf.sape.tracer.analyzer.cdg;

public interface View<V, E> {
	
	public void update();
	
}
