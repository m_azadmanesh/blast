package ch.usi.inf.sape.tracer.analyzer.cdg;

public interface Orderable {
	
	public int getPreorder();
	
	public void setPreorder(final int preorder);

}
