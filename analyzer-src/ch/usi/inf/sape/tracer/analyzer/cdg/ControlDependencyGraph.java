package ch.usi.inf.sape.tracer.analyzer.cdg;

public interface ControlDependencyGraph<V,E> {

	public Graph<V,E> findCDG();
	
}
