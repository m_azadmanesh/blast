package ch.usi.inf.sape.tracer.analyzer.cdg;

import org.objectweb.asm.tree.AbstractInsnNode;

public class BasicBlock<V> implements Label<V> {
	
	private final V label;
	private final V start;
	private final V end;
	private final int id;
	
	
	public BasicBlock(final V start, final V end, final V label, final int id){
		this.label = label;
		this.start = start;
		this.end = end;
		this.id = id;
	}

	/**
	 * @return the index
	 */
	@Override
	public V getLabel() {
		return label;
	}

	/**
	 * @return the start
	 */
	public V getStart() {
		return start;
	}

	/**
	 * @return the end
	 */
	public V getEnd() {
		return end;
	}

	@Override
	public int getID() {
		return this.id;
	}

}
