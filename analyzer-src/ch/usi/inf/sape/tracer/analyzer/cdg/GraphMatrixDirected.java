package ch.usi.inf.sape.tracer.analyzer.cdg;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class GraphMatrixDirected<V,E> extends GraphMatrix<V,E> {

	public GraphMatrixDirected(int size){
		super(size);
	}

	@Override
	public boolean isDirected() {
		return true;
	}

	@Override
	public void addEdge(V label1, V label2, E label) {
		GraphMatrixVertex<V> vtx1 = map.get(label1);
		GraphMatrixVertex<V> vtx2 = map.get(label2);
		
		Edge<V,E> edge = new Edge<V,E>(this, vtx1.getLabel(), vtx2.getLabel(), label, true); 
		matrix[vtx1.getIndex()][vtx2.getIndex()] = edge;
	}
	
	@Override
	public E removeEdge(V label1, V label2) {
		final int row = map.get(label1).getIndex();
		final int col = map.get(label2).getIndex();
		
		final Edge<V,E> oldEdge = matrix[row][col];
		matrix[row][col] = null;
		
		if (oldEdge == null)
			return null;
		else
			return oldEdge.getLabel();
	}

	@Override
	public Set<Edge<V, E>> edges() {
		Set<Edge<V,E>> edges = new HashSet<>();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				Edge<V,E> e = matrix[i][j];
				if (e != null)
					edges.add(e);
			}
		}
		return edges;
	}
	
	public Graph<V,E> findDominatorsGraph(){
		Graph<V,E> domGraph = new GraphMatrixDirected<>(this.size());
		Iterator<V> it = iterator();
		while (it.hasNext()){
			V label = it.next();
			domGraph.addVertex(label, getVertex(label).getVertexContents());
		}
		domGraph.setRoot(this.getRoot());
		domGraph.addEdge(domGraph.getRoot(), domGraph.getRoot(), null);
		
		Set<V> vertices = domGraph.getVertices();
		for (V v : vertices) {
			if (v.equals(domGraph.getRoot()))
				continue;
			
			for (V dom : vertices )
				domGraph.addEdge(dom, v, null);
		}
		
		// Dom(n) = {n} union with intersection over Dom(p) for all p in pred(n)
		boolean changed = true;
		while(changed){
			changed = false;
			for (V v : this.getVertices()) {
				Set<V> dom = new HashSet<>();
				dom.add(v);

				Set<V> domPre = new HashSet<>();
				Iterator<V> iterator = getPredecessors(v).iterator();
				if (iterator.hasNext()){

					domPre.addAll(domGraph.getPredecessors(iterator.next()));
					while(iterator.hasNext()){
						domPre.retainAll(domGraph.getPredecessors(iterator.next()));
					}
					dom.addAll(domPre);
				}
				Set<V> old = new HashSet<>(domGraph.getPredecessors(v));
				
				old.removeAll(dom);
				if (old.size() != 0){		//new != dom(old)
					for (V ndom: old){
						domGraph.removeEdge(ndom, v);
					}
					changed = true;
				}
			}
		}
		
		Traverse<V> traverser = new DFS<V,E>(this, new PreorderVisitor());
		traverser.traverse(this.getRoot());
		
		Graph<V,E> idom = new GraphMatrixDirected<V,E>(size());
		for (V v: this.getVertices()){
			Set<V> dominators = domGraph.getPredecessors(v);
			V iDom = findIDom(v, dominators);
			idom.addVertex(v, getVertex(v).getVertexContents());
			
			if (iDom != null){
				idom.addVertex(iDom, getVertex(iDom).getVertexContents());
				idom.addEdge(iDom, v, null);
			}
		}

		idom.setRoot(getRoot());
		
		return idom;
	}
	
	private V findIDom(V node, Set<V> dominators){
		int idomPreorder= -1;
		V idom = null;
		
		for (V dominator : dominators) {
			GraphMatrixVertex<V> vtx = (GraphMatrixVertex<V>) getVertex(dominator);
			
			if (vtx.getPreorder() > idomPreorder && !node.equals(dominator)){
				idom = dominator;
				idomPreorder = vtx.getPreorder();
			}
		}
		return idom;
	}
	
	public static void main(String[] args) {
		GraphMatrix<Integer, String> g = new GraphMatrixDirected<>(5);
		BasicBlock<Integer> bb0 = new BasicBlock<>(0, 1, 0 , 0);
		BasicBlock<Integer> bb1 = new BasicBlock<>(1, 1, 1, 1);
		BasicBlock<Integer> bb2 = new BasicBlock<>(2, 1, 2, 2);
		BasicBlock<Integer> bb3 = new BasicBlock<>(3, 1, 3, 3);
		BasicBlock<Integer> bb4 = new BasicBlock<>(1, 1, 3, 3);
		
		g.addVertex(0, bb0);
		g.addVertex(1, bb1);
		g.addVertex(2, bb2);
		g.addVertex(3, bb3);
		g.addEdge(bb1.getLabel(), bb2.getLabel(), "T");
		g.addEdge(bb2.getLabel(), bb3.getLabel(), "F");
		g.addEdge(bb0.getLabel(), bb3.getLabel(), "");
		System.out.println(g);
		g.addEdge(bb2.getLabel(), bb1.getLabel(), "T");
		System.out.println(g);
		Set<Integer> s = g.getPredecessors(bb4.getLabel());
		StringBuffer buffer = new StringBuffer();
		buffer.append(s);
		System.out.println("predecessors are:");
		System.out.println(buffer.toString());

	}

	@Override
	public Graph<V, E> reverse() {
		Graph<V,E> reverse = new GraphMatrixDirected<>(size());
		for (V v : getVertices()) {
			reverse.addVertex(v, getVertex(v).getVertexContents());
		}
		for (Edge<V,E> edge : edges()) {
			reverse.addEdge(edge.target(), edge.origin(), edge.getLabel());
		}
		return reverse;
	}
	
}
