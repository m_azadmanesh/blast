package ch.usi.inf.sape.tracer.analyzer.cdg;

public class PreorderVisitor implements Visitor {

	private int next = 0;

	@Override
	public void visit(Orderable v) {
		v.setPreorder(next++);
	}
	
}
