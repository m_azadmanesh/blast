package ch.usi.inf.sape.tracer.analyzer.cdg;

public class GraphMatrixVertex<V> implements Vertex<V> , Orderable{
	private final int row;
	private final Label<V> appNode;
	private int preorder = -1;
	private boolean visited;
	
	public GraphMatrixVertex(Label<V> appNode, int row) {
		this.appNode = appNode;
		this.row = row;
	}
	
	public int getIndex(){
		return this.row;
	}

	public V getLabel(){
		return appNode.getLabel();
	}

	@Override
	public int hashCode() {
		return appNode.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return appNode.getLabel().equals(obj);
	}

	@Override
	public boolean visit(Visitor visitor) {
		boolean visited = this.visited;
		if (!visited){
			visitor.visit(this);
			this.visited = true;
		}
		
		return visited;
	}

	@Override
	public boolean isVisited() {
		return this.visited;
	}
	
	public void reset(){
		this.visited = false;
	}

	/**
	 * @return the preorder
	 */
	public int getPreorder() {
		return preorder;
	}

	/**
	 * @param preorder the preorder to set
	 */
	public void setPreorder(int preorder) {
		this.preorder = preorder;
	}

	@Override
	public Label<V> getVertexContents() {
		return appNode;
	}
	
}
