package ch.usi.inf.sape.tracer.analyzer.cdg;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CDGUsingPostDom<V, E> implements ControlDependencyGraph<V, E> {

	private final Graph<V,E> cfg;
	private final Graph<V,E> postDomGraph;
	
	public CDGUsingPostDom(final Graph<V,E> cfg, final Graph<V,E> post) {
		this.cfg = cfg;
		this.postDomGraph = post;
	}
	
	@Override
	public Graph<V, E> findCDG() {
		Set<Edge<V,E>> s = new HashSet<>();
		
		// We should first find those edges (A,B) such that B is not an immediate post dominator of A
		for (Edge<V,E> edge : cfg.edges()){
			if (postDomGraph.getEdge(edge.target(), edge.origin()) == null){
				s.add(edge);
			}
		}
		
		final Graph<V,E> cdg = new GraphMatrixDirected<>(cfg.size());
		
		for (Edge<V, E> edge : s) {
			V a = edge.origin();
			V b = edge.target();
			V lca = findLeastCommonAncestor(postDomGraph, a, b);
			cdg.addVertex(a, cfg.getVertex(a).getVertexContents());
			while (!b.equals(lca)){
				cdg.addVertex(b, cfg.getVertex(b).getVertexContents());
				cdg.addEdge(a, b, null);
				b = postDomGraph.getPredecessors(b).iterator().next();
			}
			
			if (lca.equals(a)){
				cdg.addEdge(a, a, null);
			}
				
			
		}
		if (cdg.getVertices().size() > 0)
			cdg.setRoot(cfg.getRoot());
		return cdg;
	}
	
	public V findLeastCommonAncestor(Graph<V,E> g, V v1, V v2){
		List<V> preV1 = new ArrayList<>();
		preV1.add(v1);
		Set<V> temp = g.getPredecessors(v1);
		while(temp.size() != 0){
			preV1.addAll(temp);
			//There should be exactly one predecessor if there is any 
			temp = g.getPredecessors(temp.iterator().next());
		}
		
		List<V> preV2 = new ArrayList<>();
		preV2.add(v2);
		temp = g.getPredecessors(v2);
		while(temp.size() != 0){
			preV2.addAll(temp);
			//There should be exactly one predecessor if there is any 
			temp = g.getPredecessors(temp.iterator().next());
		}
		
		for (V pre1 : preV1) {
			for (V pre2 : preV2) {
				if (pre1.equals(pre2))
					return pre1;
			}
		}
		
		throw new IllegalStateException("No Common Ancestor!");
	}

}
