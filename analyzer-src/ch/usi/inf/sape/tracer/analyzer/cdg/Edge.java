package ch.usi.inf.sape.tracer.analyzer.cdg;

public class Edge<V,E> {
	
	private final boolean directed;
	private boolean visited;
	private final V origin;
	private final V target;
	private final E label;
	private final Graph<V,E> graph;
	
	public Edge(Graph<V,E> graph, V label1, V label2, E edge, boolean directed){
		this.directed = directed;
		this.origin = label1;
		this.target = label2;
		this.label = edge;
		this.graph = graph;
	}
	
	public V origin(){
		return origin;
	}
	
	public V target(){
		return target;
	}
	
	public E getLabel(){
		return label;
	}
	
	public boolean visit(){
		boolean temp = this.visited;
		this.visited = true;
		return temp;
	}
	
	public boolean isVisited(){
		return this.visited;
	}
	
	public boolean isDirected(){
		return this.directed;
	}
	
	public void reset(){
		visited = false;
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return "("+graph.getVertex(origin).getVertexContents().getID()+"," + graph.getVertex(target).getVertexContents().getID()+")";
	}
	
	
}

