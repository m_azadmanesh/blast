package ch.usi.inf.sape.tracer.analyzer.cdg;

public interface Vertex<V> {

	public V getLabel();
	
	public boolean visit(Visitor visitor);
	
	public boolean isVisited();
	
	public void reset();
	
	public Label<V> getVertexContents();
}
