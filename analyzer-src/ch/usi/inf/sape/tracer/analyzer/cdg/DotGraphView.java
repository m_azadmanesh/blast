package ch.usi.inf.sape.tracer.analyzer.cdg;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

public class DotGraphView<V,E> implements View<V,E>{

	private PrintStream out;
	private Graph<V,E> g;
	
	public DotGraphView(String fileName, Graph<V,E> g) throws IOException{
		out = new PrintStream(new FileOutputStream(fileName));
		this.g = g;
	}
	
	@Override
	public void update() {
		out.println("digraph CFG {");
		
		int i = 0;
		Map<V, String> legalNodeNames = new HashMap<>();
		for (V v : g.getVertices()) {
			String legalNodeName = "bb" + i;
			legalNodeNames.put(v, legalNodeName);
			out.println(legalNodeName + " [label=\"" + g.getVertex(v).getVertexContents().getID() + "\"" + (v.equals(g.getRoot()) ? "style= filled color = green" : "") + "]");
			i++;
		}
		
		for (Edge<V,E> edge : g.edges()) {
			out.print(legalNodeNames.get(edge.origin()) + "->" + legalNodeNames.get(edge.target()));
			if (edge.getLabel() != null && edge.getLabel().equals("EX"))
				out.print(" [style = dotted]");
			out.println();
		}
		out.println("}");
		out.close();
	}

}
