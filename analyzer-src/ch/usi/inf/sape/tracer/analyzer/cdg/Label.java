package ch.usi.inf.sape.tracer.analyzer.cdg;

public interface Label<L> {

	public L getLabel();
	
	public int getID();
}
