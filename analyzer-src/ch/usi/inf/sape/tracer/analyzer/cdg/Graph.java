package ch.usi.inf.sape.tracer.analyzer.cdg;

import java.util.Iterator;
import java.util.Set;

public interface Graph <V,E>{
	
	/**
	 * Adds a vertex with label label to the graph; 
	 * if a vertex with label label already exists, no action.
	 * 
	 * @param label
	 */
	public void addVertex(V label, Label<V> appNode);
	
	/**
	 * An edge between is inserted between existing vertices label1
	 * and label2. The edge can be directed or not according to the
	 * type of the graph.
	 * 
	 * @param label1
	 * @param label2
	 * @param label
	 */
	public void addEdge(V label1, V label2, E label);
	
	/**
	 * Remove the vertex with label label
	 * 
	 * @param label
	 * @return
	 */
	public V removeVertex(V label);
	
	/**
	 * Remove the edge between the vertices label1 and label2 
	 * together with its label.
	 * 
	 * @param label1
	 * @param label2
	 * @return
	 */
	public E removeEdge(V label1, V label2);
	
	/**
	 * Returns actual edge between label1 and label2
	 * @param label1
	 * @param label2
	 * @return
	 */
	public Edge<V,E> getEdge(V label1, V label2);
	
	/**
	 * Returns true if the graph already contains a vertex labeled label.
	 * The equality condition is based on equality of labels.
	 * 
	 * @param label
	 * @return
	 */
	public boolean containsVertex(V label);
	
	/**
	 * Returns true if there is an edge between label1 and label2. In case of
	 * directed graph, if there is an edge from label1 to label2.
	 * 
	 * @param label1
	 * @param label2
	 * @return
	 */
	public boolean containsEdge(V label1, V label2);
	
	/**
	 * Sets visited flag on edge and returns the previous value
	 * @param e
	 * @return
	 */
	public boolean visitEdge(Edge<V,E> e);
	
	/**
	 * Returns visited flag on the labeled vertex
	 * @param label
	 * @return
	 */
	public boolean isVisitedVertex(V label);
	
	/**
	 * Returns visited flag on the labelded edge
	 * @param e
	 * @return
	 */
	public boolean isVisitedEdge(Edge<V,E> e);
	
	/**
	 * Resets visited flags to false 
	 */
	public void reset();
	
	/**
	 * Returns the number of vertices in graph
	 * @return
	 */
	public int size();
	
	/**
	 * Returns an iterator across all the vertices in graph
	 * 
	 * @return
	 */
	public Iterator<V> iterator();
	
	/**
	 * Returns the vertices adjacent to label. For a directed graph,
	 * this method traverses over the outgoing edges and finds the 
	 * corresponding adjacent vertices.
	 * 
	 * @return
	 */
	public Set<V> getSuccessors(V label);
	
	/**
	 * Returns the vertices adjacent to label. For a directed graph,
	 * this method traverses over the incoming edges and finds the 
	 * corresponding adjacent vertices.
	 * 
	 * @return
	 */
	public Set<V> getPredecessors(V label);

	/**
	 * An iterator over all the edges in the graph. 
	 * 
	 * @return
	 */
	public Set<Edge<V,E>> edges();
	
	/**
	 * Returns true for a directed graph, otherwise false
	 * @return
	 */
	public boolean isDirected();
	
	/**
	 * Set the root node of the graph to label
	 * @param label
	 */
	public void setRoot(V label);
	
	/**
	 * 
	 * @return the root node of the graph
	 */
	public V getRoot();
	
	/**
	 * Returns the application-specific node corresponding 
	 * to the given input label
	 * 
	 * @param label
	 * @return
	 */
	public Vertex<V> getVertex(V label);
	
	/**
	 * 
	 * @return The set of all vertices defined for this graph
	 */
	public Set<V> getVertices();
	
	/**
	 * 
	 * @return A graph representing the immediate relationship 
	 * among the nodes of this graph
	 */
	public Graph<V,E> findDominatorsGraph();

	/**
	 * Create a new graph whose edges have a direction opposite to 
	 * the original graph
	 * 
	 * @return
	 */
	public Graph<V,E> reverse();
}
