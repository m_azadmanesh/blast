package ch.usi.inf.sape.tracer.analyzer.cdg;

import java.util.HashSet;
import java.util.Set;

public class GraphMatrixUndirected<V,E> extends GraphMatrix<V,E> {

	public GraphMatrixUndirected(int size){
		super(size);
	}

	@Override
	public boolean isDirected() {
		return false;
	}

	@Override
	public void addEdge(V label1, V label2, E label) {
		GraphMatrixVertex<V> vtx1 = map.get(label1);
		GraphMatrixVertex<V> vtx2 = map.get(label2);
		
		Edge<V,E> edge = new Edge<V,E>(this, vtx1.getLabel(), vtx2.getLabel(), label, false); 
		matrix[vtx1.getIndex()][vtx2.getIndex()] = edge;
		matrix[vtx2.getIndex()][vtx1.getIndex()] = edge;
	}

	@Override
	public E removeEdge(V label1, V label2) {
		final int row = map.get(label1).getIndex();
		final int col = map.get(label2).getIndex();
		
		final Edge<V,E> oldEdge = matrix[row][col];
		matrix[row][col] = null;
		matrix[col][row] = null;
		
		if (oldEdge == null)
			return null;
		else
			return oldEdge.getLabel();
	}

	@Override
	public Set<Edge<V, E>> edges() {
		Set<Edge<V,E>> edges = new HashSet<>();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j <= i; j++) {
				Edge<V,E> e = matrix[i][j];
				if (e != null)
					edges.add(e);
			}
		}
		return edges;
	}

	@Override
	public Graph<V, E> findDominatorsGraph() {
		throw new RuntimeException("Domintorss not supported for an undirected graph.");
	}

	/**
	 * This method basically returns a copy of the original graph
	 * for an undirected graph.
	 * 
	 */
	@Override
	public Graph<V, E> reverse() {
		Graph<V,E> reverse = new GraphMatrixUndirected<>(size());
		for (V v : getVertices()) {
			reverse.addVertex(v, getVertex(v).getVertexContents());
		}
		for (Edge<V,E> edge : edges()) {
			reverse.addEdge(edge.target(), edge.origin(), edge.getLabel());
		}
		return reverse;
	}

}
