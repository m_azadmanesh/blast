package ch.usi.inf.sape.tracer.analyzer.cdg;

public class DFS<V, E> implements Traverse<V> {

	private final Graph<V,E> g;
	private final Visitor visitor;
	
	public DFS(final Graph<V,E> g, final Visitor visitor){
		this.g = g;
		this.visitor = visitor;
	}
	
	@Override
	public void traverse(V root) {
		Vertex<V> v = g.getVertex(root);
		v.visit(visitor);
		for (V succLabel : g.getSuccessors(root)){
			Vertex<V> succ = g.getVertex(succLabel);
			if (!succ.isVisited()){
				traverse(succLabel);
			}
		}
	}

}
