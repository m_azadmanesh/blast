package ch.usi.inf.sape.tracer.analyzer.cdg;

public class ControlFlowGraph<V,E> extends GraphMatrixDirected<V, E> {

	/**
	 * exit node for this control flow graph
	 */
	private V exit;
	
	public ControlFlowGraph(int size) {
		super(size);
	}

	public V getExit(){
		return exit;
	}
	
	public void setExit(V exit){
		this.exit = exit;
	}
}

