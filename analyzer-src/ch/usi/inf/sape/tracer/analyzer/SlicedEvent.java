package ch.usi.inf.sape.tracer.analyzer;

import java.util.HashSet;
import java.util.Set;

import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

/**
 * By definition, a slice only contains the statements that affect the values 
 * computed in the slice criterion. The standard abstract operation may define 
 * many values, but an instance of this class returns only those definitions
 * that are indeed used in the slice. 
 * 
 * @author reza
 *
 */
public abstract class SlicedEvent implements Event {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6924090278101940327L;

	private final Set<Binding<? extends MemoryLocation>> slicedDefs = new HashSet<>();

	private final Set<Binding<? extends MemoryLocation>> slicedUses = new HashSet<>();

	private Set<Binding<? extends MemoryLocation>> foundDataDependencies = new HashSet<>();

	private Set<Binding<? extends MemoryLocation>> foundControlDependencies = new HashSet<>();


	public abstract NonSlicedEvent getNonSlicedEvent();



	/**
	 * The abstraction session in which this sliced operation is valid
	 */
	private final int sessionId;

	public SlicedEvent(int sessionId) {
		this.sessionId = sessionId;
	}
	
	@Override
	public short getOperationCode() {
		return getNonSlicedEvent().getOperationCode();
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		return slicedUses.toArray(new Binding[0]);
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return slicedDefs.toArray(new Binding[0]);
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(
			Binding<? extends MemoryLocation> def) {
		return getNonSlicedEvent().getUsesForDef(def);
	}

	@Override
	public String getOperationName() {
		return getNonSlicedEvent().getOperationName();
	}

	@Override
	public int getOperationIndex() {
		return getNonSlicedEvent().getOperationIndex();
	}

	@Override
	public Activation getActivationRecord() {
		return getNonSlicedEvent().getActivationRecord();
	}

	@Override
	public String getSourceFileNameWithFullAddress() {
		return getNonSlicedEvent().getSourceFileNameWithFullAddress();
	}

	@Override
	public String getSourceFileName() {
		return getNonSlicedEvent().getSourceFileName();
	}

	/**
	 * @return the sessionId
	 */
	public int getSessionId() {
		return sessionId;
	}

	public void addDef(Binding<? extends MemoryLocation> def){
		this.slicedDefs.add(def);
	}

	public void addDefs(Binding<? extends MemoryLocation>[] defs){
		for (int i = 0; i < defs.length; i++) {
			this.slicedDefs.add(defs[i]);
		}
	}

	public void addUse(Binding<? extends MemoryLocation> use){
		this.slicedUses.add(use);
	}

	public void addUses(Binding<? extends MemoryLocation>[] uses){
		for (Binding<? extends MemoryLocation> use : uses) {
			this.slicedUses.add(use);
		}
	}

	public boolean checkAndSetDataDependency(Binding<?> def){
		boolean val = this.foundDataDependencies.contains(def);
		if (!val){
			this.foundDataDependencies.add(def);
		}
		return val;
	}

	public boolean checkAndSetControlDependency(Binding<?> def){
		boolean val = this.foundControlDependencies.contains(def);
		if (!val){
			this.foundControlDependencies.add(def);
		}
		return val;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(getOperationIndex()).append(":").append(getSourceCodeLineNumber()).append(":").append(getOperationName()).append("\t");
		int counter = 0;
		if (getUses().length == 0)
			buffer.append("(No Sources)");

		for (Binding binding : getUses()) {
			buffer.append(binding);
			if (counter != getUses().length -1)
				buffer.append(",\t");
			counter ++;
		}

		buffer.append("\t-->\t");

		counter = 0;
		if (getDefs().length == 0)
			buffer.append("(No Targets)");
		for (Binding binding : getDefs()) {
			buffer.append(binding);
			if (counter != getDefs().length -1)
				buffer.append(",\t");
			counter ++;
		}

		return buffer.toString();
	}

	@Override
	public int getSourceCodeLineNumber() {
		return getNonSlicedEvent().getSourceCodeLineNumber();
	}

	@Override
	public byte getEventClassCode() {
		return getNonSlicedEvent().getEventClassCode();
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		//TODO update the logic for writing the sliced events
		throw new IllegalStateException();
	}
	
}
