package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.PseudoVariableMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

/**
 * Includes bytecodes ifeq, ifne, iflt, ifge, ifgt, ifle
 * @author reza
 *
 */
public class IF_Operation extends BytecodeEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2527919861481926049L;

	private final Condition condition;
	
	private final int offset;
	
	public IF_Operation(short opCode, Condition cond, final int offset, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<PseudoVariableMemoryLocation> pseudoValueDefPre = threadCallStack.peekBasicBlockSimulatorStack(); 
		
		Binding<PseudoVariableMemoryLocation> pseudoValueDefNew = new Binding<>(pseudoValueDefPre.getMemoryLocation(), new Value(pseudoValueDefPre.getMemoryLocation().getPseudoVariableId()), this);
		threadCallStack.getCurrentThreadCallStack().pushOnBasicBlockSimulatorStack(pseudoValueDefNew);
		
		this.uses = new Binding[]{threadCallStack.popOffStack(), pseudoValueDefPre};
		this.defs = new Binding[]{pseudoValueDefNew};
		this.condition = cond;
		this.offset = offset;
	}

	public IF_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, Condition condition,
			int offset) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.condition = condition;
		this.offset = offset;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 1;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		assert this.defs.length == 0;
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(
			Binding<? extends MemoryLocation> def) {
		return Binding.NOT_AVAILABLE_BINDINGS;

	}

	@Override
	public String getOperationName() {
		return "IF" + this.condition.toString()+"<offset:"+this.offset+">";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.IF_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
