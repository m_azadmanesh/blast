package ch.usi.inf.sape.tracer.analyzer.bytecodeops;


import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Utils;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.LocalMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public final class XSTORE_Operation extends BytecodeEvent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7414235766872019978L;
	
	private final Type type;
	
//	private final Binding<LocalMemoryLocation> overwrite;
	
//	private final StackSnapshot stackSnapshot;

	@SuppressWarnings("unchecked")
	public XSTORE_Operation(short opCode, int localIndex, Type type, int srcLineNo, int bcIndex){
		super(opCode, srcLineNo, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
//		this.stackSnapshot = threadCallStack.takeStackSnapshot();
		Binding<StackSlotMemoryLocation> use = threadCallStack.popOffStack();
		assert use != null;
		
		this.uses = new Binding[]{use};
		Binding<LocalMemoryLocation> toBeOverWrittenLocalBinding = threadCallStack.readFromLocal(localIndex);
		if (toBeOverWrittenLocalBinding!= null && toBeOverWrittenLocalBinding.isAreadyOnTopOfStack()){
			toBeOverWrittenLocalBinding.setOverwritten(true);
			toBeOverWrittenLocalBinding.setTemporaryName(Utils.generateTemporaryName(toBeOverWrittenLocalBinding.getMemoryLocation().getName(), threadCallStack.getNextTempIndex()));
//			this.overwrite  = toBeOverWrittenLocalBinding;
		}else{
//			overwrite = null;
		}
		
		this.defs = new Binding[]{threadCallStack.writeIntoLocal(localIndex, bcIndex, use.getValue(), this)};
		this.type = type;
	}
	

	public XSTORE_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord, 
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, 
			Type type) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.type = type;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Binding<StackSlotMemoryLocation>[] getUses() {
		
		assert this.uses.length == 1;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		
		return (Binding<StackSlotMemoryLocation>[])this.uses;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Binding<LocalMemoryLocation>[] getDefs() {
		
		assert this.defs.length == 1;
		assert this.defs[0].getMemoryLocation() instanceof LocalMemoryLocation;
		
		return (Binding<LocalMemoryLocation>[])this.defs;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Binding<StackSlotMemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0])){
			assert this.uses.length == 1;
			assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;

			return (Binding<StackSlotMemoryLocation>[])this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	
//	@Override
//	public AbstractOperation generateAbstractOperation(BytecodeAbstractOpMap[] ops, Binding[] uses, Binding[] defs, int minBCIndex) {
//		return new Store_AbsOperation(ops, uses, defs, minBCIndex);
//	}
	
	@Override
	public String getSymbolicInterpretation(Binding def) {
		String result = "";
		
//		if (this.uses[0].isCommonSubexpression()){
//			result += ((CommonSubExpression)this.uses[0].getProvider()).evaluate(this.uses[0]);
//		}
		
//		result += this.uses[0].evaluate(stackSnapshot);
		
		return  result += ((LocalMemoryLocation)this.defs[0].getMemoryLocation()).getName()+ " = " + this.uses[0].getProvider().getSymbolicInterpretation(this.uses[0]) ;
	}


	@Override
	public String getOperationName() {
		return type.getDescriptor() + "STORE";
	}

	@Override
	public boolean isExpressionStatement() {
		return true;
	}


	@Override
	public byte getEventClassCode() {
		return EventConstants.XSTORE_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}
}
