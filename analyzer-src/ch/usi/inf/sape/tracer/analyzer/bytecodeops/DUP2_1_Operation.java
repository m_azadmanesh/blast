package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.StackSnapshot;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Utils;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

/**
 * Form1: value2, value1   ->   value2, value1, value2, value1       
 * where both value1 and value2 are values of a category 1 computational type
 * 
 * @author reza
 */
public class DUP2_1_Operation extends CommonSubExpression {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1226786352802514640L;

	@SuppressWarnings("unchecked")
	public DUP2_1_Operation(short opCode, int bytecodeIndex) {
		super(opCode, bytecodeIndex, 2);
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> value1Binding =  threadCallStack.popOffStackNoSideEffect();
		Binding<StackSlotMemoryLocation> value2Binding =  threadCallStack.popOffStackNoSideEffect();
		this.uses = new Binding[]{value2Binding, value1Binding};
		this.uses[0].setTemporaryName(Utils.generateTemporaryName("", threadCallStack.getNextTempIndex()));
		this.uses[1].setTemporaryName(Utils.generateTemporaryName("", threadCallStack.getNextTempIndex()));
		
		Value value1 = value1Binding.getValue();
		Type type1 = value1Binding.getMemoryLocation().getType();
		
		Value value2 = value2Binding.getValue();
		Type type2 = value2Binding.getMemoryLocation().getType();
		
		Binding def1 = threadCallStack.pushOnStack(value2, type2, this);
		def1.setAsCommonSubexpression();
		Binding def2 = threadCallStack.pushOnStack(value1, type1, this);
		def2.setAsCommonSubexpression();
		Binding def3 = threadCallStack.pushOnStack(value2, type2, this);
		def3.setAsCommonSubexpression();
		Binding def4 = threadCallStack.pushOnStack(value1, type1, this);
		def4.setAsCommonSubexpression();
		
		this.defs = new Binding[]{def1, def2, def3, def4};
	}

	public DUP2_1_Operation(short opCode, int sourceLineNo,
			int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs, new boolean[0]);
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 2;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getDefs() {
		assert this.defs.length == 4;
		assert this.defs[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.defs[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.defs[2].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.defs[3].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
			if (def.equals(this.defs[0]) || def.equals(this.defs[2])){
				return new Binding[]{this.uses[0]};
				
			}else if (def.equals(this.defs[1]) || def.equals(this.defs[3])){
				return new Binding[]{this.uses[1]};
			}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getOperationName() {
		return "DUP2<Form 1>";
	}

	@Override
	public String evaluate(Binding expression, StackSnapshot stackSnapshot) {
		if (expression.equals(this.defs[0]) || expression.equals(this.defs[2])){
			return evaluate(0, 0, stackSnapshot);
		}if (expression.equals(this.defs[1]) || expression.equals(this.defs[3])){
			return evaluate(1, 1, stackSnapshot);
		}else
			throw new RuntimeException();
	}

	@Override
	public Binding getCopy(Binding def) {
		assert def.isCommonSubexpression() == true;
		if (def.equals(this.defs[0]))
			return this.defs[2];
		if (def.equals(this.defs[1]))
			return this.defs[3];
		if (def.equals(this.defs[2]))
			return this.defs[0];
		if (def.equals(this.defs[3]))
			return this.defs[1];
		
		throw new RuntimeException();
	}

	@Override
	public int getCommonSubexpressionIndexFor(Binding def) {
		if (def.equals(this.defs[0]) || def.equals(this.defs[2])){
			return 0;
		}if (def.equals(this.defs[1]) || def.equals(this.defs[3])){
			return 1;
		}else
			throw new RuntimeException();
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.DUP2_1_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
