package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

/**
 * Before an invokation takes place, an instance of this class is created to simulate 
 * the creation of a new stack frame.
 * 
 * @author reza
 *
 */
public class INVOKECALLING_Operation extends BytecodeEvent implements PEI{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1527016071964421757L;

	private final String invokeType;
	
	/**
	 * The class name, the method name and the descriptor of the method called by this operation all concatenated together 
	 */
	private final String nameDescClass;
	
	public INVOKECALLING_Operation(short opCode, String invokeType, String nameDescClass, int argsCount, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
				
		boolean isStatic = false;
		if (invokeType.equals("INVOKESTATIC"))
			isStatic = true;
		
		//threadCallStack.createAndPushFrame(Utils.findMethodName(nameDescClass), Utils.findDescriptor(nameDescClass), Utils.findClassName(nameDescClass), argsCount, isStatic);
		threadCallStack.setLastInvokationFrame(threadCallStack.isCallerInstrumented(
												nameDescClass, 
												argsCount, 
												invokeType.equals("INVOKESTATIC") ? true : false));
		
		this.invokeType = invokeType;
		this.uses = Binding.NO_USE_BINDINGS;
		this.defs = Binding.NO_DEF_BINDINGS;
		this.nameDescClass = nameDescClass;
	}
	
	public INVOKECALLING_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, String invokeType,
			String nameDescClass) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.invokeType = invokeType;
		this.nameDescClass = nameDescClass;
	}


	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		assert this.uses.length == 0;
		
		return this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		assert this.defs.length == 0;
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(
			Binding<? extends MemoryLocation> def) {
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getOperationName() {
		return this.invokeType + "(Calling:  " + this.nameDescClass+")";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.INVOKECALLING_EVENT;
	};

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return -1;
	}

}
