package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

/**
 * This class is created to treat specially with the IDIV and LDIV operations that can throw
 * runtime exception after being executed.
 * 
 * @author reza
 *
 */
public class Division_Operation extends Binary_Operation implements PEI {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8113502107135194291L;

	private final int pseudoVarDefIndex;
	
	
	public Division_Operation(short opCode, String opName, Type type, int bytecodeIndex) {
		super(opCode, opName, bytecodeIndex);
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> divisorBinding = threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> dividendBinding = threadCallStack.popOffStack();
		this.uses = new Binding[]{dividendBinding, divisorBinding};
		
		if (type == Type.INT){
			this.defs = findDefInteger(dividendBinding.getValue(), divisorBinding.getValue(), threadCallStack);
		}else{		//type == long
			this.defs = findDefLong(dividendBinding.getValue(), divisorBinding.getValue(), threadCallStack);
		}
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}
	
	public Division_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, String opName,
			int pseudoVarDefIndex) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs, opName);
		this.pseudoVarDefIndex = pseudoVarDefIndex;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0]) ||
				def.equals(this.defs[pseudoVarDefIndex])){
			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;

	}

	private Binding<StackSlotMemoryLocation>[] findDefInteger(Value dividendValue, Value divisorValue, ThreadCallStack threadCallStack){
		int divisor = Integer.parseInt(divisorValue.toString());
		int dividend = Integer.parseInt(dividendValue.toString());
		if (divisor == 0){
			return Binding.UNDEFINED_BINDINGS;
		}else{
			return new Binding[]{threadCallStack.pushOnStack(new Value(dividend/divisor), Type.INT, this)};
		}
	}
	
	private Binding<StackSlotMemoryLocation>[] findDefLong(Value dividendValue, Value divisorValue, ThreadCallStack threadCallStack){
		long divisor = Long.parseLong(divisorValue.toString());
		long dividend = Long.parseLong(dividendValue.toString());
		if (divisor == 0)
			return Binding.UNDEFINED_BINDINGS;
		else
			return new Binding[]{threadCallStack.pushOnStack(new Value(dividend/divisor), Type.LONG, this)};
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.DIVISION_EVENT;
	}
	
	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}

}
