package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class XCONST_Operation extends BytecodeEvent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1104248630711490752L;
	
	private final Type type;

	public XCONST_Operation(short opCode, Value value, Type type, int bcIndex){
		super(opCode, bcIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();

		this.uses = Binding.NO_USE_BINDINGS;
		this.defs = new Binding[]{threadCallStack.pushOnStack(value, type, this)};
		this.type = type;
	}
	
	public XCONST_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, Type type) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.type = type;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		assert this.uses.length == 0;
		return this.uses;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Binding<StackSlotMemoryLocation>[] getDefs() {
		
		assert this.defs.length == 1;
		assert this.defs[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		
		return (Binding<StackSlotMemoryLocation>[]) this.defs;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0])){
			assert this.uses.length == 0;
			return (Binding[])this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}
	
	@Override
	public String getSymbolicInterpretation(Binding def) {
		return this.defs[0].getValue().toString();
	}

	@Override
	public String getOperationName() {
		return type.getDescriptor() + "CONST";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.XCONST_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
