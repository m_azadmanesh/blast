package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import java.util.Set;

import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.NonSlicedEvent;
import ch.usi.inf.sape.tracer.analyzer.SlicedBytecodeEventI;

public interface NonSlicedBytecodeEvent {

	public SlicedBytecodeEventI getSlicedOperationForSession(final int sessionId);
	
	public boolean isExpressionStatement();
	
//	public AbstractOperation generateAbstractOperation(BytecodeAbstractOpMap[] ops, Binding[] uses, Binding[] defs, int minBCIndex);
	
	public String getSymbolicInterpretation(Binding def);
	
	public Set<Binding> getNonStackUses();
	
	public Set<Binding> getNonStackDefs();
	
	public int addPseudoVarDefForPEI();
	
	public BytecodeEvent getController();
	
	public void setController(BytecodeEvent controller);

}
