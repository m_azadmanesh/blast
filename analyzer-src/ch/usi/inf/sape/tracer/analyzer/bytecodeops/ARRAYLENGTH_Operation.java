package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.ArrayLengthMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.HeapSpace;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class ARRAYLENGTH_Operation extends BytecodeEvent implements PEI{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3935317907032040501L;
	
	private final int pseudoVarDefIndex; 
	
	public ARRAYLENGTH_Operation(short opCode, Object array, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> stackBinding = threadCallStack.popOffStack();
		
		Binding<ArrayLengthMemoryLocation> lengthBinding;
		
		if (array != null){	
			lengthBinding = HeapSpace.getOrDefineArrayLocationMetaData(array, Type.UNAVAILABLE).useLengthBinding();
			this.defs = new Binding[]{threadCallStack.pushOnStack(lengthBinding.getValue(), Type.INT, this)};
		}else{			//there will be an NPE afterwards, we ignore it here
			lengthBinding = Binding.UNDEFINED_BINDING;
			this.defs = Binding.UNDEFINED_BINDINGS;
		}
		
		this.uses = new Binding[]{stackBinding, lengthBinding};
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}

	public ARRAYLENGTH_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, int pseudoVarDefIndex) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.pseudoVarDefIndex = pseudoVarDefIndex;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		assert this.uses.length == 2;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[1].getMemoryLocation() instanceof ArrayLengthMemoryLocation;
		return this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0]) ||
				def.equals(this.defs[pseudoVarDefIndex])){				//the case for pseudo-variable

			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;

	}

	@Override
	public String getOperationName() {
		return "ARRAYLENGTH";
	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		return this.uses[0].getProvider().getSymbolicInterpretation(this.uses[0]) + ".length";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.ARRAYLENGTH_EVENT;
	}
	
	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}


}
