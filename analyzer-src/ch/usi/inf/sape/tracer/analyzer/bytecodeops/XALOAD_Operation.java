package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.ArrayElementMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.HeapSpace;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class XALOAD_Operation extends BytecodeEvent implements PEI{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5559550739489959108L;
	
	private final Type type;
	
	private final int pseudoVarDefIndex;
	
	public XALOAD_Operation(short opCode, Object array, Value value, int index, boolean verified, Type type, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> indexBinding = threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> arrayRefBinding = threadCallStack.popOffStack();
		this.uses = new Binding[]{indexBinding, arrayRefBinding, Binding.UNDEFINED_BINDING};
		
		if (verified){    //emulate the behavior of runtime verifier before accessing an array element
			Binding<ArrayElementMemoryLocation> elementBinding = HeapSpace.getOrDefineArrayLocationMetaData(array, type).useElementBinding(index);
			elementBinding.addCopyOnStack();
			
			this.uses[2] = elementBinding;
			
			this.defs = new Binding[]{threadCallStack.pushOnStack(value, type, this)};
		}else{
			this.defs = Binding.UNDEFINED_BINDINGS;
		}
		this.type = type;
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}

	public XALOAD_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, Type type,
			int pseudoVarIndex) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.type = type;
		this.pseudoVarDefIndex = pseudoVarIndex;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		assert this.uses.length == 3;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[2].getMemoryLocation() instanceof ArrayElementMemoryLocation;
		return this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;

		if (this.defs[pseudoVarDefIndex].equals(def) ||  //the case when the pseudo var is searched for
				def.equals(this.defs[0]))			//the case when the real element is searched for
			return this.uses;
		
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
//		if (this.uses[2].isOverwritten()){
//			return this.uses[2].getTemporaryName();
//		}
		
		return this.uses[1].getProvider().getSymbolicInterpretation(this.uses[1]) + "[" + this.uses[0].getProvider().getSymbolicInterpretation(this.uses[0]) + "]";
	}

	@Override
	public String getOperationName() {
		if (type == Type.BOOLEAN)
			return "BALOAD";
		
		return this.type.getDescriptor()+"ALOAD";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.XALOAD_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}

}
