package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.StackSnapshot;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Utils;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class DUP_Operation extends CommonSubExpression{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7752676484962225135L;
	
	public DUP_Operation(short opCode, int bcIndex) {
		super(opCode, bcIndex, 1);
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		
		Binding<StackSlotMemoryLocation> use = threadCallStack.popOffStack();
		use.setTemporaryName(Utils.generateTemporaryName("", threadCallStack.getNextTempIndex()));

		this.uses = new Binding[]{use};
		
		Value value = use.getValue();
		Type type = use.getMemoryLocation().getType();
		Binding<StackSlotMemoryLocation> def1 = threadCallStack.pushOnStack(value, type, this);
		def1.setAsCommonSubexpression();
		
		Binding<StackSlotMemoryLocation> def2 = threadCallStack.pushOnStack(value, type, this);
		def2.setAsCommonSubexpression();
		
		this.defs = new Binding[]{def1, def2};
	}
	
	public DUP_Operation(short opCode, int sourceLineNo,
			int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs, new boolean[0]);
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 1;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		
		return (Binding<StackSlotMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getDefs() {
		assert this.defs.length == 2;
		assert this.defs[0].getMemoryLocation() instanceof StackSlotMemoryLocation && this.defs[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		
		return (Binding<StackSlotMemoryLocation>[]) this.defs;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
			if (def.equals(this.defs[0]) || def.equals(this.defs[1])){
				assert this.uses.length == 1;
				assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
				
				return (Binding<StackSlotMemoryLocation>[])this.uses;
			}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		return this.uses[0].getTemporaryName();
	}

	@Override
	public String getOperationName() {
		return "DUP";
	}

	@Override
	public String evaluate(Binding expression, StackSnapshot stackSnapshot){
		return evaluate(0, 0, stackSnapshot);
	}

	@Override
	public Binding getCopy(Binding def) {
		assert def.isCommonSubexpression() == true;
		
		if (def.equals(this.defs[0]))
			return this.defs[1];
		if (def.equals(this.defs[1]))
			return this.defs[0];

		throw new RuntimeException();
	}

	@Override
	public int getCommonSubexpressionIndexFor(Binding def) {
		return 0;
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.DUP_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
