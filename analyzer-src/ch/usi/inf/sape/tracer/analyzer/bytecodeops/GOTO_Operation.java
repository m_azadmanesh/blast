package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

public class GOTO_Operation extends BytecodeEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4839685273741114873L;
	
	private final int offset;
	
	public GOTO_Operation(short opCode, int offset, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		this.uses = Binding.NO_USE_BINDINGS;
		this.defs = Binding.NO_DEF_BINDINGS;
		this.offset = offset;
	}

	public GOTO_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, int offset) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.offset = offset;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		return this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getOperationName() {
		return "GOTO <offset:"+ this.offset +">";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.GOTO_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
