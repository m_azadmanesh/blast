package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.HeapSpace;
import ch.usi.inf.sape.tracer.analyzer.locations.InstanceFieldMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class PUTFIELD_Operation extends BytecodeEvent implements PEI{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4849105118469563197L;
	
	private final int pseudoVarDefIndex;
	
	public PUTFIELD_Operation(short opCode, Object object, String fieldName, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> valueBinding = threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> referenceBinding = threadCallStack.popOffStack();
		Binding<InstanceFieldMemoryLocation> defBinding = HeapSpace.getOrDefineInstanceLocationMetaData(object).defInstanceFieldBinding(fieldName, valueBinding.getValue(), this);
		this.uses = new Binding[]{referenceBinding, valueBinding};
		this.defs = new Binding[]{defBinding};
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}

	/**
	 * used only when we know the getfield will cause an exception to be thrown right after execution
	 * @param bytecodeIndex
	 */
	public PUTFIELD_Operation(short opCode, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> valueBinding = threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> referenceBinding = threadCallStack.popOffStack();
		this.uses = new Binding[]{referenceBinding, valueBinding};
		this.defs = Binding.UNDEFINED_BINDINGS;
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}
	
	public PUTFIELD_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, int pseudoVarDefIndex) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.pseudoVarDefIndex = pseudoVarDefIndex;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		return this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0]) ||
				def.equals(this.defs[pseudoVarDefIndex])){
			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}
	
	@Override
	public boolean isExpressionStatement() {
		return true;
	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		return this.uses[0].getProvider().getSymbolicInterpretation(this.uses[0]) + "." + 
				((InstanceFieldMemoryLocation)this.defs[0].getMemoryLocation()).getName() + " = " +
				this.uses[1].getProvider().getSymbolicInterpretation(this.uses[1]);
	}
	
//	@Override
//	public AbstractOperation generateAbstractOperation(BytecodeAbstractOpMap[] ops, Binding[] uses, Binding[] defs,int minBCIndex) {
//		return new Putfield_AbsOperation(ops, uses, defs, minBCIndex);
//	}

	@Override
	public String getOperationName() {
		return "PUTFIELD";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.PUTFIELD_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}

}
