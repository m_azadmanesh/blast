package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.SlicedBytecodeEventI;
import ch.usi.inf.sape.tracer.analyzer.SlicedEvent;

public class SlicedBytecodeEvent extends SlicedEvent
									implements SlicedBytecodeEventI{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5522432618653318946L;
	
	private final BytecodeEvent nonSlicedOperation;
	
	public SlicedBytecodeEvent(BytecodeEvent nonSlicedOperation, int sessionId) {
		super(sessionId);
		this.nonSlicedOperation = nonSlicedOperation;
	}

	@Override
	public BytecodeEvent getNonSlicedEvent() {
		return nonSlicedOperation;
	}

}
