package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Utils;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.LocalMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

public class IINC_Operation extends BytecodeEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8098429685500354549L;
	
	private final int incr;
	
	public IINC_Operation(short opCode, Value value, int localIndex, int incr, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<LocalMemoryLocation> use = threadCallStack.readFromLocal(localIndex);
		
		if (use.isAreadyOnTopOfStack()){		//if the value to be incremented is already on the stack, it gets overwritten
			use.setOverwritten(true);
			use.setTemporaryName(Utils.generateTemporaryName(use.getMemoryLocation().getName(), threadCallStack.getNextTempIndex()));
		}
		
		this.incr = incr;
		this.uses = new Binding[]{use};
		this.defs = new Binding[]{threadCallStack.writeIntoLocal(localIndex, bytecodeIndex, value, this)};
	}
	
	public IINC_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, int incr) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.incr = incr;
	}

	@Override
	public Binding<LocalMemoryLocation>[] getUses() {
		assert this.uses.length == 1;
		assert this.uses[0].getMemoryLocation() instanceof LocalMemoryLocation;
		return (Binding<LocalMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<LocalMemoryLocation>[] getDefs() {
		assert this.defs.length == 1;
		assert this.defs[0].getMemoryLocation() instanceof LocalMemoryLocation;
		return (Binding<LocalMemoryLocation>[]) this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0])){
			assert this.uses.length == 1;

			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}
	
	@Override
	public boolean isExpressionStatement() {
		return true;
	}
	
//	@Override
//	public AbstractOperation generateAbstractOperation(BytecodeAbstractOpMap[] ops, Binding[] uses, Binding[] defs, int minBCIndex) {
//		return new Inc_AbsOperation(ops, uses, defs, minBCIndex);
//	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		String result = "";
		if (this.uses[0].isOverwritten()){
			result += this.uses[0].getTemporaryName() + " = "+ ((LocalMemoryLocation)this.uses[0].getMemoryLocation()).getName() + "; ";
		}
//		result+= this.uses[0].evaluate();
		return result += ((LocalMemoryLocation)this.uses[0].getMemoryLocation()).getName()+ " = " +((LocalMemoryLocation)this.uses[0].getMemoryLocation()).getName() + " + "+ this.incr;
	}

	@Override
	public String getOperationName() {
		return "IINC";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.IINC_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
