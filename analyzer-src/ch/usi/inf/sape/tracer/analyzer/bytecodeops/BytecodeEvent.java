package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import java.util.HashSet;
import java.util.Set;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.NonSlicedEvent;
import ch.usi.inf.sape.tracer.analyzer.OperationCodes;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.PseudoVariableMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

/**
 * This class represents those operations that are computed during tracing phase and it includes 
 * all the uses and defs of a statement. See SlicedOperation for the other category of operatons
 * that are used during slicing. 
 * 
 * @author reza
 *
 */
public abstract class BytecodeEvent extends NonSlicedEvent
									implements NonSlicedBytecodeEvent {

	public static enum InvokeType{INVOKESPECIAL, INVOKEVIRTUAL, INVOKESTATIC, INVOKEINTERFACE};
	
	public static enum Condition {EQ, NE, LT, GE, GT, LE, NULL, NONNULL};
	
	public static final BytecodeEvent UNTRACED_BYTECODE_OPERATION = new UntracedBytecode_Operation(OperationCodes.UNTRACED, -1);
	
	protected SlicedBytecodeEvent slicedVersion;
	
	private BytecodeEvent controller;
	
	public BytecodeEvent(short opCode, 
			int sourceLineNo, 
			int opIndex, 
			Activation activationRecord, 
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs){
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
	}
	
	/**
	 * This is used just by the SlicedOperation to avoid from assigning
	 * a new operation id to the newly generated operation.
	 * @param opCode
	 * @param opIndex
	 * @param activationRecord TODO
	 * @param bytecodeIndex
	 */
	public BytecodeEvent(short opCode, int sourceLineNo, int opIndex, Activation activationRecord){
		super(opCode, sourceLineNo, opIndex, activationRecord);
	}
	
	
	public BytecodeEvent(short opCode, final int sourceLineNo, Activation activationRecord) {
		super(opCode, sourceLineNo, ThreadCallStack.getCurrentThreadCallStack().getAndIncrementNextOperationIndex(), activationRecord);
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(operationIndex).append(":").append(getSourceFileName()).append(":").append(sourceLineNo).append(":").append(getOperationName()).append("\t");
		int counter = 0;
		if (this.getUses().length == 0)
			buffer.append("(No Sources)");
		
		for (Binding binding : this.getUses()) {
			buffer.append(binding);
			if (counter != this.getUses().length -1)
				buffer.append(",\t");
			counter ++;
		}
		
		buffer.append("\t-->\t");
		
		counter = 0;
		if (this.getDefs().length == 0)
			buffer.append("(No Targets)");
		for (Binding binding : this.getDefs()) {
			buffer.append(binding);
			if (counter != this.getDefs().length -1)
				buffer.append(",\t");
			counter ++;
		}
		
		return buffer.toString();
	}
	
	public boolean isExpressionStatement(){
		return false;
	}
	
//	public AbstractOperation generateAbstractOperation(BytecodeAbstractOpMap[] ops, Binding[] uses, Binding[] defs, int minBCIndex){
//		return AbstractOperation.UNDEFINED_ABSTRACT_OPERATION;
//	}
	
	public String getSymbolicInterpretation(Binding def){
		Binding[] uses = getUsesForDef(def); 
		if (uses == Binding.NOT_AVAILABLE_BINDINGS || this == BytecodeEvent.UNTRACED_BYTECODE_OPERATION)
			return "";
		
		return uses[0].getProvider().getSymbolicInterpretation(uses[0]);
	}
	
	public Set<Binding> getNonStackUses(){
		Set<Binding> nonStackUses = new HashSet<>();
		for (Binding use : this.uses) {
			if (!(use.getMemoryLocation().isStackSlot()))
				nonStackUses.add(use);
			else if (((StackSlotMemoryLocation)use.getMemoryLocation()).hasTemporaryRepresentative()){
				nonStackUses.add(((StackSlotMemoryLocation)use.getMemoryLocation()).getTemporaryBinding());
			}
		}
		return nonStackUses;
	}
	
	public Set<Binding> getNonStackDefs(){
		Set<Binding> nonStackDefs = new HashSet<>();
		for (Binding def : this.defs) {
			if (!(def.getMemoryLocation().isStackSlot()))
				nonStackDefs.add(def);
		}
		return nonStackDefs;
	}
	
	public boolean isDup(){
		return false;
	}

	
	/**
	 * Adds a pseudo variable defintion to the def set of this operation and
	 * returns the index of this definition.
	 * @return
	 */
	public int addPseudoVarDefForPEI(){
		if (defs == null)
			throw new IllegalStateException("Definitions of the operation are not intialized yet!!");
		
		ThreadCallStack tcs = ThreadCallStack.getCurrentThreadCallStack();
		Binding<PseudoVariableMemoryLocation> pseudoVarOldUse = tcs.peekBasicBlockSimulatorStack();
		Binding<PseudoVariableMemoryLocation> pseudoVarDef = tcs.definePseudoVariable(this,
				pseudoVarOldUse.getMemoryLocation(),
				pseudoVarOldUse.getMemoryLocation().getPseudoVariableId());
		
		Binding<? extends MemoryLocation>[] defsWithoutPesudo = this.defs;
		this.defs = new Binding[this.defs.length + 1];
		System.arraycopy(defsWithoutPesudo, 0, this.defs, 0, defsWithoutPesudo.length);
		this.defs[defsWithoutPesudo.length] = pseudoVarDef;
		return defsWithoutPesudo.length;
	}

	/**
	 * @return the controller
	 */
	public BytecodeEvent getController() {
		return controller;
	}

	/**
	 * @param controller the controller to set
	 */
	public void setController(BytecodeEvent controller) {
		this.controller = controller;
	}

	@Override
	public int getSourceCodeLineNumber() {
		return sourceLineNo;
	}

	public int getOperationIndex(){
		return operationIndex;
	}

	public SlicedBytecodeEvent getSlicedOperationForSession(final int sessionId){
		if (slicedVersion == null || slicedVersion.getSessionId() != sessionId){
			this.slicedVersion = new SlicedBytecodeEvent(this, sessionId);
		}
		return this.slicedVersion;
	}


	public short getOperationCode(){
		return this.opCode;
	}


	public boolean isBasicBlockEntry(){
		return false;
	}

	public boolean isPEI(){
		return false;
	}
}
