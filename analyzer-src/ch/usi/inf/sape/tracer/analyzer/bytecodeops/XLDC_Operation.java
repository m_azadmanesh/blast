package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public final class XLDC_Operation extends BytecodeEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3441983630805819012L;
	
	private final Type type;
	
	public XLDC_Operation(short opCode, Value value, Type type, int bcIndex) {
		super(opCode, bcIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		this.uses = Binding.NO_USE_BINDINGS;
		this.defs = new Binding[]{threadCallStack.pushOnStack(value, type, this)};
		this.type = type;
	}

	/**
	 * Used only for pushing a reference (class or method object) on the stack. Given that resolving a class
	 * or method reference may throw exception, we create this pre-operation for the purpose of tracking and
	 * on the next step we define the values using the method def() 
	 * 
	 * @param bcIndex
	 */
	public XLDC_Operation(short opCode, int bcIndex) {
		super(opCode, bcIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		this.uses = new Binding[0];
		this.defs = new Binding[1];
		this.type = Type.REFERENCE;
	}
	
	/**
	 * Used only for pushing a reference (class or method object) on the stack. Given that resolving a class
	 * or method reference may throw exception, we should first create a pre-operation for the purpose of tracking 
	 * and on the next step we define the values using this function
	 * 
	 * @param value
	 */
	public void def(Value value){
		this.defs[0] = ThreadCallStack.getCurrentThreadCallStack().pushOnStack(value, Type.REFERENCE, this);
	}
	
	public XLDC_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, Type type) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.type = type;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		assert this.uses.length == 0;
		return this.uses;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Binding<StackSlotMemoryLocation>[] getDefs() {
		assert this.defs.length == 1;
		assert this.defs[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[])this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(
			Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0])){
			assert this.uses.length == 0;
			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}
	
	@Override
	public String getSymbolicInterpretation(Binding def) {
		return this.defs[0].getValue().toString()+" ";
	}

	@Override
	public String getOperationName() {
		return "LDC("+this.type.getDescriptor()+")";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.XLDC_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
