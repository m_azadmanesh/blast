package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.ClassFieldMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.GlobalSpace;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class PUTSTATIC_Operation extends BytecodeEvent implements PEI{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2433273639338662063L;
	
	private final int pseudoVarDefIndex; 
	
	public PUTSTATIC_Operation(short opCode, Class klass, String fieldName, Value value, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		this.uses = new Binding[]{ThreadCallStack.getCurrentThreadCallStack().popOffStack()};
		this.defs = new Binding[]{GlobalSpace.getOrDefineClassLocationMetaData(klass).defClassFieldBinding(fieldName, value, this)};
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}
	
	public PUTSTATIC_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, int pseudoVarDefIndex) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.pseudoVarDefIndex = pseudoVarDefIndex;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 1;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[])this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0]) ||
				def.equals(this.defs[pseudoVarDefIndex])){
			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}
	
	@Override
	public boolean isExpressionStatement() {
		return true;
	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		return "." + ((ClassFieldMemoryLocation)this.defs[0].getMemoryLocation()).getName() + " = ";
	}
	
//	@Override
//	public AbstractOperation generateAbstractOperation(BytecodeAbstractOpMap[] ops, Binding[] uses, Binding[] defs,int minBCIndex) {
//		return new PutStatic_AbsOperation(ops, uses, defs, minBCIndex);
//	}

	@Override
	public String getOperationName() {
		return "PUTSTATIC";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.PUTSTATIC_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}

}
