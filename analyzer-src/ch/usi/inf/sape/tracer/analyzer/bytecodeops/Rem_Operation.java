package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

/**
 * This class is created to treat specially with the IREM and LREM operations that can throw
 * runtime exception after being executed.
 * 
 * @author reza
 *
 */
public class Rem_Operation extends Binary_Operation implements PEI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6807461075701830772L;
	
	private final int pseudoVarDefIndex;
	
	public Rem_Operation(short opCode, String opName, Type type, int bytecodeIndex) {
		super(opCode, opName, bytecodeIndex);
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> divisorBinding = threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> dividendBinding = threadCallStack.popOffStack();
		this.uses = new Binding[]{dividendBinding, divisorBinding};

		if (type == Type.INT){
			this.defs = findDefInteger(dividendBinding.getValue(), divisorBinding.getValue(), threadCallStack);
		}else{		//type == long
			this.defs = findDefLong(dividendBinding.getValue(), divisorBinding.getValue(), threadCallStack);
		}
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}
	
	public Rem_Operation(short opCode, String opName, Value value, Type type,
			int bytecodeIndex, int pseudoVarDefIndex) {
		super(opCode, opName, value, type, bytecodeIndex);
		this.pseudoVarDefIndex = pseudoVarDefIndex;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0]) ||
				def.equals(this.defs[pseudoVarDefIndex])){

			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;

	}

	private Binding<StackSlotMemoryLocation>[] findDefInteger(Value dividendValue, Value divisorValue, ThreadCallStack threadCallStack){
			int divisor = intValue(divisorValue);
			int dividend = intValue(dividendValue);
			
			if (divisor == 0){
				return Binding.UNDEFINED_BINDINGS;
			}else{
				return new Binding[]{threadCallStack.pushOnStack(new Value(dividend % divisor), Type.INT, this)};
			}
	}

	private Binding<StackSlotMemoryLocation>[] findDefLong(Value dividendValue, Value divisorValue, ThreadCallStack threadCallStack){
		long divisor = longValue(divisorValue);
		long dividend = longValue(dividendValue);
		
		if (divisor == 0)
			return Binding.UNDEFINED_BINDINGS;
		else
			return new Binding[]{threadCallStack.pushOnStack(new Value(dividend % divisor), Type.LONG, this)};
	}
	
	private int intValue(Value value){
		int ret;
		
		switch (value.getType()){
		case INT:
		case SHORT:
		case BYTE:
		case LONG:
			ret = Integer.parseInt(value.toString());
			break;
		case CHAR:
			ret = value.toString().charAt(0);
			break;
		default:
			throw new IllegalStateException("The operand to Rem operation is not correctly typed...");
		}
		
		return ret;
	}
	
	private long longValue(Value value){
		long ret;
		
		switch(value.getType()){
		case INT:
		case SHORT:
		case BYTE:
		case LONG:
			ret = Long.parseLong(value.toString());
			break;
		case CHAR:
			ret = value.toString().charAt(0);
			break;
		default:
			throw new IllegalStateException("The operand to Rem operation is not correctly typed...");
		}
		
		return ret;
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.REM_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}

}
