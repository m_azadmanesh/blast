package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.ArrayElementMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.HeapSpace;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class XASTORE_Operation extends BytecodeEvent implements PEI{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7689516125524490492L;
	
	private final Type type;
	
//	private final StackSnapshot stackSnapshot;
	
//	private final Binding<ArrayElementMemoryLocation> overwrite;
	
	private final int pseudoVarDefIndex;
	
	public XASTORE_Operation(short opCode, Object array, int index, boolean accessVerified, Type type, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
//		this.stackSnapshot = threadCallStack.takeStackSnapshot();
		
		Binding<StackSlotMemoryLocation> valueBinding = threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> indexBinding = threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> arrayRefBinding = threadCallStack.popOffStack();
		
		this.uses = new Binding[]{arrayRefBinding, indexBinding, valueBinding};
		
		if (accessVerified){
			Binding<ArrayElementMemoryLocation> elementBinding = HeapSpace.getOrDefineArrayLocationMetaData(array, type).useElementBinding(index);
			
			if (elementBinding.isAreadyOnTopOfStack()){
				elementBinding.setOverwritten(true);
//				elementBinding.setTemporaryName(Utils.generateTemporaryName(this.uses[0].getProvider().getSymbolicInterpretation(this.uses[0]) +"_" + index, threadCallStack.getNextTempIndex()));
//				this.overwrite = elementBinding;
			}else{
//				this.overwrite = null;
			}
				
			this.defs = new Binding[]{HeapSpace.getOrDefineArrayLocationMetaData(array, type).defElementBinding(index, valueBinding.getValue(), this)};
		}else{
//			this.overwrite = null;
			this.defs = Binding.UNDEFINED_BINDINGS;
		}
		
		this.type = type;
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}

	public XASTORE_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, Type type,
			int pseudoVarDefIndex) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.type = type;
		this.pseudoVarDefIndex = pseudoVarDefIndex;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 3;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[2].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;

		if (def.equals(this.defs[0]) ||							//The case for the written heap location
				def.equals(this.defs[pseudoVarDefIndex])){		//The case for the pseudo var
			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}
	
	@Override
	public boolean isExpressionStatement() {
		return true;
	}

//	@Override
//	public AbstractOperation generateAbstractOperation(BytecodeAbstractOpMap[] ops, Binding[] uses, Binding[] defs, int minBCIndex) {
//		return new XAStore_AbsOperation(ops, uses, defs, minBCIndex);
//	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
//		System.out.println("hello");
		String result = "";
//		if (overwrite != null){
//			result += this.overwrite.getTemporaryName() + " = "+ this.uses[0].getProvider().getSymbolicInterpretation(this.uses[0]) + "[" +this.overwrite.getMemoryLocation().getIndex() +"]" + "; ";
//		}
		
/*		result += this.uses[0].evaluate(stackSnapshot);*/
		
//		if (this.uses[0].getProvider().isDup()){
//			System.out.println("1");
//			result += ((CommonSubExpression)this.uses[0].getProvider()).evaluate(this.uses[0]);
//		}

/*		result += this.uses[1].evaluate(stackSnapshot);
		
		result += this.uses[2].evaluate(stackSnapshot);*/
		
//		if (this.uses[1].getProvider().isDup()){
//			System.out.println("2");
//			result += ((CommonSubExpression)this.uses[1].getProvider()).evaluate(this.uses[1]);
//		}
//		
//		if (this.uses[2].getProvider().isDup()){
//			System.out.println("3");
//			result += ((CommonSubExpression)this.uses[2].getProvider()).evaluate(this.uses[2]);
//		}
		
		return result += this.uses[0].getProvider().getSymbolicInterpretation(this.uses[0]) + "[" + this.uses[1].getProvider().getSymbolicInterpretation(this.uses[1]) + "] = " + this.uses[2].getProvider().getSymbolicInterpretation(this.uses[2]);
	}

	@Override
	public String getOperationName() {
		if (type == Type.BOOLEAN)
			return "BASTORE";
		
		return this.type.getDescriptor()+"ASTORE";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.XASTORE_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}

}
