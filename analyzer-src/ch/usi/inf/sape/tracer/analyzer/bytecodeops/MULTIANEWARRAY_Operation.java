package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Utils;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.ArrayMetaData;
import ch.usi.inf.sape.tracer.analyzer.locations.HeapSpace;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

public class MULTIANEWARRAY_Operation extends BytecodeEvent implements PEI{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6130593151734774878L;
	
	private int pseudoVarDefIndex;
	
	private final Type elementType;
	
	public MULTIANEWARRAY_Operation(short opCode, int dims, int bytecodeIndex, Type elementType) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		this.elementType = elementType;
		
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		this.uses = new Binding[dims];
		for (int i = 0; i < dims; i++) {
			this.uses[i] = threadCallStack.popOffStack();
		}
		this.defs = Binding.UNDEFINED_BINDINGS;
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}
	
	public MULTIANEWARRAY_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, int pseudeVarDefIndex,
			Type elementType) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.pseudoVarDefIndex = pseudeVarDefIndex;
		this.elementType = elementType;
	}

	public void def(Object[] array, int dimensions){
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		List<Binding> subArrayBindings = traceSubArrays(array, dimensions);
		this.defs = new Binding[subArrayBindings.size() + 1];   // 1 for the array ref binding 
		this.defs[0] = threadCallStack.pushOnStack(Utils.getValueForObject(array), Type.REFERENCE, this);	// array ref
		for (int i = 0; i < subArrayBindings.size(); i++) {
			this.defs[i+1] = subArrayBindings.get(i);
		}
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}

	public List<Binding> traceSubArrays(Object obj, int dim){
		List<Binding> bindings = new ArrayList<>();
		ArrayMetaData arrayLocation;
		if (dim == 1){
			arrayLocation = HeapSpace.defineArrayLocationMetaData(obj, elementType, true, this);
		}else{
			arrayLocation = HeapSpace.defineMultiArrayLocationMetaData(obj, Type.REFERENCE, true, this);
		}
		
		bindings.add(arrayLocation.useLengthBinding());
		
		if (dim == 1){
			for (int i = 0; i < Array.getLength(obj); i++) {
				bindings.add(arrayLocation.useElementBinding(i));
			}
			return bindings;
		}
		
		for (int i = 0; i < Array.getLength(obj); i++) {
			Object subArray = Array.get(obj, i);
			bindings.addAll(traceSubArrays(subArray, dim-1));
			arrayLocation.defElementBinding(arrayLocation, i, HeapSpace.getObjectLocationMetaData(subArray), this);
			bindings.add(arrayLocation.useElementBinding(i));
		}
		return bindings;
	}
	
	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		return this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		for (Binding<? extends MemoryLocation> binding : defs) {
			if (binding.equals(def))
				return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;

	}

	@Override
	public String getOperationName() {
		return "MULTIANEWARRAY";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.MULTIANEWARRAY_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}

}
