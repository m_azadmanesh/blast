package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.PseudoVariableMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class IF_ICMP_Operation extends BytecodeEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8096312773989751557L;

	private final Condition condition;
	
	private final int offset;
	
	public IF_ICMP_Operation(short opCode, Condition cond, int offset, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<PseudoVariableMemoryLocation> pseudoValueDefPre = threadCallStack.peekBasicBlockSimulatorStack(); 
		
		Binding<PseudoVariableMemoryLocation> pseudoValueDefNew = new Binding<>(pseudoValueDefPre.getMemoryLocation(), new Value(pseudoValueDefPre.getMemoryLocation().getPseudoVariableId()), this);
		threadCallStack.getCurrentThreadCallStack().pushOnBasicBlockSimulatorStack(pseudoValueDefNew);
		
		Binding<StackSlotMemoryLocation> valueBinding2 = threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> valueBinding1 = threadCallStack.popOffStack();		
		this.uses = new Binding[]{valueBinding1, valueBinding2, pseudoValueDefPre};
		this.defs = new Binding[]{pseudoValueDefNew};
		this.condition = cond;
		this.offset = offset;
	}
	
	public IF_ICMP_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, Condition condition,
			int offset) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.condition = condition;
		this.offset = offset;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 2;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		assert this.defs.length == 0;
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		return Binding.NOT_AVAILABLE_BINDINGS;

	}
	
	@Override
	public boolean isExpressionStatement() {
		return true;
	}
	
	@Override
	public String getSymbolicInterpretation(Binding def) {
		return "IF (" + this.uses[0].getProvider().getSymbolicInterpretation(this.uses[1]) + " " + condition.toString() + " "+ this.uses[1].getProvider().getSymbolicInterpretation(this.uses[1]) +")";
	}

	@Override
	public String getOperationName() {
		return "IF_ICMP" + this.condition.toString()+" <offset:"+this.offset+">";
	}

//	@Override
//	public AbstractOperation generateAbstractOperation(BytecodeAbstractOpMap[] ops, Binding[] uses, Binding[] defs, int minBCIndex) {
//		return new If_AbsOperation(ops, uses, defs, minBCIndex);
//	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.IF_ICMP_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
