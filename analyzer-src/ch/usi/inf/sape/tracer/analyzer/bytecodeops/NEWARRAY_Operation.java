package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.ArrayReferenceValue;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.ArrayElementMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.ArrayMetaData;
import ch.usi.inf.sape.tracer.analyzer.locations.HeapSpace;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class NEWARRAY_Operation extends BytecodeEvent implements PEI{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2200239057884456674L;

	private Type type;
	
	private final int pseudoVarDefIndex;
	
	public NEWARRAY_Operation(short opCode, int size, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		this.uses = new Binding[]{threadCallStack.popOffStack()};    //size binding
		
		// one for the array ref and one for the array length and the rest for the elements
		//defs should be initialized in the next step. We split it into two steps because
		//an exception can happen while we create an array and we need to be able to track
		//the origins of the instruction causing the exception
		if (size >= 0)
			this.defs = new Binding[size + 2];
		else
			this.defs = Binding.UNDEFINED_BINDINGS;
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}
	
	public NEWARRAY_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, Type type,
			int pseudoVarDefIndex) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.type = type;
		this.pseudoVarDefIndex = pseudoVarDefIndex;
	}

	/**
	 * Defines array location including array elements location, and array length location.
	 * Besides, it defines array reference location on top of stack. 
	 * 
	 * @param array
	 * @param type
	 */
	public void def(Object array, Type type){
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		ArrayMetaData arrayLocation = HeapSpace.defineArrayLocationMetaData(array, type, true, this);
		this.defs[0] = threadCallStack.pushOnStack(new ArrayReferenceValue(arrayLocation), Type.REFERENCE, this);	// array ref
		this.defs[1] = arrayLocation.useLengthBinding();							// array length
		Binding<ArrayElementMemoryLocation>[] elementLocations = arrayLocation.getElementBindings();
		System.arraycopy(elementLocations, 0, this.defs, 2, elementLocations.length);	//array elements
		this.type = type;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 1;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;

		for (Binding<? extends MemoryLocation> defBinding: this.defs){
			if (def.equals(defBinding)){
				return this.uses;
			}
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		return "new " + type.getName() + "[" + this.uses[0].getProvider().getSymbolicInterpretation(this.uses[0]) + "]";
 	}

	@Override
	public String getOperationName() {
		return "NEWARRAY";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.NEWARRAY_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}
	
}
