package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class IF_ACMP_Operation extends BytecodeEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8110307736932000209L;
	
	private final Condition condition;
	
	private final int offset;
	
	public IF_ACMP_Operation(short opCode, Condition condition, int offset, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> valueBinding2 = threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> valueBinding1 = threadCallStack.popOffStack();
		this.uses = new Binding[]{valueBinding1, valueBinding2};
		this.defs = Binding.NO_DEF_BINDINGS;
		this.condition = condition;
		this.offset = offset;

	}
	
	public IF_ACMP_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, Condition condition,
			int offset) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.condition = condition;
		this.offset = offset;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 2;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		assert this.defs.length == 0;
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getOperationName() {
		return "IF_ACMP"+this.condition+" <offset:"+this.offset+">";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.IF_ACMP_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
