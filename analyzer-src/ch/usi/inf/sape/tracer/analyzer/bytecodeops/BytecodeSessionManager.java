package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.SlicedEvent;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

public class BytecodeSessionManager {

	/**
	 * Bytecode operations for the current thread.
	 * TODO: extend this model to cover multi-threaded paradigm
	 */
	private final List<BytecodeEvent> bytecodes;
	
//	private AbstractionSessionManager abstractionSessionManager;
	
	private int nextSessionId = 1; 				//sessions start from 1, 0 is reserved for the full bytecode list
	
//	private AbstractSlicedSession fullBytecodeOperationsSession;
	
	private BytecodeEvent exceptionThrowingInstruction;

	private BytecodeSessionManager(List<BytecodeEvent> bcs) {
		this.bytecodes= bcs;
//		fullBytecodeOperationsSession = new AbstractSlicedSession(0, bcs);
	}
	
	/**
	 * The singleton instance
	 */
	private static BytecodeSessionManager instance;
	
	/**
	 * @return the sole instance of this class that is already created. If the instance 
	 * is not created yet, it throws an exception.
	 */
	public static BytecodeSessionManager getInstance(){
		if (instance == null){
			throw new RuntimeException("Abstraction phase not initialized yet!");
		}
		return instance;
	}
	
	/**
	 * The method assumes that no instance of this class already exists and it creates
	 * the sole instance and returns it. If there is already an instance, it throws an
	 * exception
	 * 
	 * @param bcOps
	 * @return
	 */
	public static BytecodeSessionManager getInstance(List<BytecodeEvent> bcOps){
		if (instance != null){
			throw new RuntimeException("An instance is already initialized! Unable to create another!");
		}
		instance = new BytecodeSessionManager(bcOps);
		return instance;
	}

	/**
	 * @return the bcs
	 */
	public BytecodeNonSlicedRetrievalSession getFullBytecodeOperationsSession() {
		return new BytecodeNonSlicedRetrievalSession(this.bytecodes, nextSessionId++);
	}

//	public AbstractionSessionManager getAbstractionSessionManager(){
//		if (this.abstractionSessionManager == null){
//			this.abstractionSessionManager = AbstractionSessionManager.getInstance(this.bytecodes);
//		}
//		
//		return this.abstractionSessionManager;
//	}
//	
//	public BytecodeSlicedRetrievalSession findBackwardSliceForBytecode(int bcIndex){
//		BytecodeEvent criterion = bytecodes.get(bcIndex);
//		int sessionId = nextSessionId ++;
//		return new BytecodeSlicedRetrievalSession(sortAbstractOperations(findBackwardSlice(criterion, sessionId), sessionId), sessionId);
//	}
	
	private List<SlicedBytecodeEvent> sortAbstractOperations(Set<BytecodeEvent> ops, int sessionId){
		List<SlicedBytecodeEvent> backSlice = new ArrayList<>();
		for (BytecodeEvent op : ops) {
			SlicedBytecodeEvent slicedOp = op.getSlicedOperationForSession(sessionId);
			backSlice.add(slicedOp);
		}
		
		Collections.sort(backSlice, new Comparator<SlicedBytecodeEvent>(){
			
			@Override
			public int compare(SlicedBytecodeEvent o1, SlicedBytecodeEvent o2) {
				return o1.getOperationIndex() < o2.getOperationIndex() ? -1 : o1.getOperationIndex() == o2.getOperationIndex() ? 0 : 1;
			}
			
		});
		
		return backSlice;
	}

	private Set<BytecodeEvent> findBackwardSlice(BytecodeEvent criterion, int sessionId){
		Set<BytecodeEvent> backSlice = new HashSet<>();
		SlicedEvent slicedOp = criterion.getSlicedOperationForSession(sessionId);
		slicedOp.addDefs(criterion.getDefs());			//we add the slice criterion itself as part of the backward slice
		backSlice.add(criterion);
		backSlice.addAll(findBackwardSliceWithoutDuplicateAndUnordered(criterion, sessionId));
		return backSlice;
	}
	
	private Set<BytecodeEvent> findBackwardSliceWithoutDuplicateAndUnordered(BytecodeEvent op, int sessionId){
		Set<BytecodeEvent> ops = new HashSet<>();
		if (op == BytecodeEvent.UNTRACED_BYTECODE_OPERATION){
			return ops;
		}
		ops.add(op);
		
		for (Binding<? extends MemoryLocation> binding : op.getUses()) {
			BytecodeEvent provider = binding.getProvider();
//			SlicedOperation slicedOperation = provider.getSlicedOperationForSession(sessionId);
//			slicedOperation.addDef(binding);
			ops.addAll(findBackwardSliceWithoutDuplicateAndUnordered(provider, sessionId));
		}
		
//		BytecodeOperation controller = findControllersForByteocde(op);
//		ops.addAll(findBackwardSliceWithoutDuplicateAndUnordered(controller, sessionId));
		return ops;
	}
	
	public List<BytecodeEvent> findController(BytecodeEvent op){
		List<BytecodeEvent> ops = new ArrayList<>(findControllersForBytecode(op));
		Collections.sort(ops, new Comparator<BytecodeEvent>() {

			@Override
			public int compare(BytecodeEvent o1, BytecodeEvent o2) {
				return o1.getOperationIndex() < o2.getOperationIndex() ? -1 : o1.getOperationIndex() == o2.getOperationIndex() ? 0 : 1;
			}
			
		});
		return ops;
	}

//	public BytecodeSlicedRetrievalSession getBytecodeSliceForAbsOpId(int absOpId){
//		AbstractSlicedRetrievalSession absSlicedSession = abstractionSessionManager.findBackwardSliceByAbsOpIndex(absOpId);
//		
//		List<SlicedOperation> ops = absSlicedSession.getOperations();
//		List<SlicedOperation> bcSlicedOps = new ArrayList<>();
//		int sessionId = nextSessionId++;
//		
//		for (SlicedOperation operation : ops) {
//			BytecodeOperation[] bcOps = operation.getBytecodes();
//			for (BytecodeOperation opMap : bcOps) {
//				bcSlicedOps.add(opMap.getSlicedOperationForSession(sessionId));
//			}
//		}
//		
//		Collections.sort(bcSlicedOps, new Comparator<SlicedOperation>(){
//
//			@Override
//			public int compare(SlicedOperation arg0, SlicedOperation arg1) {
//				return arg0.getOperationIndex() < arg1.getOperationIndex() ? -1 : arg0.getOperationIndex() == arg1.getOperationIndex() ? 0 : 1;
//			}
//			
//		});
//		
//		return new BytecodeSlicedRetrievalSession(bcSlicedOps, sessionId);
//	}
	
	/**
	 * Given a bytecode operation, this method finds the bytecode operation to
	 * which this bytecode is control dependent on, i.e., the result bytecode 
	 * operation defines the pseudo variable which the argument uses.
	 * 
	 * @param op
	 * @return
	 */
	public Set<BytecodeEvent> findControllersForBytecode(BytecodeEvent op){
		if (op.isBasicBlockEntry()){
			throw new IllegalStateException("Trying to find backward slice for a non-bytecode");
		}
		Set<BytecodeEvent> controllers = new HashSet<>();
		BytecodeEvent controller = op;
		do{
			controllers.add(controller);
			controller = findControllerForBytecode(controller);
		}while(controller != BytecodeEvent.UNTRACED_BYTECODE_OPERATION);
		
		return controllers;
	}
	
	public BytecodeEvent findControllerForBytecode(final BytecodeEvent op){
		//try to use the cached value
		if (op.getController() != null)
			return op.getController();
		
		BytecodeEvent controller = op;
		
		//first we go back in the history to find the basic block to which this op belongs
		while (!controller.isBasicBlockEntry()){
			int index = controller.getOperationIndex() - 1;
			if (index < 0){
				controller = BytecodeEvent.UNTRACED_BYTECODE_OPERATION;
				break;
			}
			controller = bytecodes.get(index);
		}
		
		// Now we iterate over the basic block entries until we find a non-basic-block-entry-operation
		// which defines a pseudo variable
		while (controller.isBasicBlockEntry()){
			controller = controller.getUses()[0].getProvider();
			if (controller == BytecodeEvent.UNTRACED_BYTECODE_OPERATION)
				break;
		}
		
		op.setController(controller);
		return controller;
	}

	/**
	 * This is a full backward slice including both data dependency and control dependency.
	 * 
	 * @param op
	 * @return
	 */
	public List<SlicedBytecodeEvent> findBackwardSliceForBytecode(final int criterionIndex, boolean includeData, boolean includeControl){
		return findBackwardSliceForBytecode(bytecodes.get(criterionIndex), includeData, includeControl);
	}

	/**
	 * This is a full backward slice including both data dependency and control dependency.
	 * 
	 * @param op
	 * @return
	 */
	public List<SlicedBytecodeEvent> findBackwardSliceForBytecode(final BytecodeEvent criterion, boolean includeData, boolean includeControl){
		Set<SlicedBytecodeEvent> backwardSliceSet = new HashSet<>();
	
		/*
		 	Perform data flow analysis first. For data flow analysis, for every bytecode, we find
		 	only those origins that have been used for defining our considered target value, i.e.,
		 	we use the getUsesForDef() method.
		 */
		int sessionId = nextSessionId ++;
		SlicedBytecodeEvent slicedOp = criterion.getSlicedOperationForSession(sessionId);
		slicedOp.addUses(criterion.getUses());		// for the original slice criterion, we add all its uses to the use list of the sliced operation
		backwardSliceSet.add(slicedOp);				// add the slice criterion 
		if (includeData){
			for (Binding<? extends MemoryLocation> use : criterion.getUses()){
				backwardSliceSet.addAll(findBackwardSliceForBytecode(use.getProvider(), use, sessionId, includeData, includeControl));
			}
		}
		
		/*
		 	In the second step, we perform control flow analysis  
		 */
		if (includeControl){
			backwardSliceSet.addAll(findBackwardSliceForBytecode(criterion, sessionId, includeData, includeControl));
		}
		
		List<SlicedBytecodeEvent> backwardSliceList = new ArrayList<>(backwardSliceSet);
		Collections.sort(backwardSliceList, new Comparator<SlicedEvent>() {

			@Override
			public int compare(SlicedEvent o1, SlicedEvent o2) {
				return o1.getOperationIndex() < o2.getOperationIndex() ? -1 : o1.getOperationIndex() == o2.getOperationIndex() ? 0 : 1;
			}
		});
		
		return backwardSliceList;
	}
	
	private Set<SlicedBytecodeEvent> findBackwardSliceForBytecode(final BytecodeEvent criterion, 
										Binding<? extends MemoryLocation> def, 
										int sessionId,
										boolean includeData, 
										boolean includeControl){
		Set<SlicedBytecodeEvent> ops = new HashSet<>();
		if (criterion == BytecodeEvent.UNTRACED_BYTECODE_OPERATION){
			return ops;
		}
		//Data flow analysis  
		if (includeData){
			SlicedBytecodeEvent slicedOp = criterion.getSlicedOperationForSession(sessionId);
			if (!slicedOp.checkAndSetDataDependency(def)){
				slicedOp.addDef(def);			
				slicedOp.addUses(criterion.getUsesForDef(def));
				ops.add(slicedOp);

				for (Binding<? extends MemoryLocation> binding : criterion.getUsesForDef(def)) {
					BytecodeEvent provider = binding.getProvider();
					ops.addAll(findBackwardSliceForBytecode(provider, binding, sessionId, includeData, includeControl));
				}
			}
		}

		//Control flow analysis
		if (includeControl){
			final BytecodeEvent controller = findControllerForBytecode(criterion);
			SlicedBytecodeEvent slicedController = controller.getSlicedOperationForSession(sessionId);
			if (!slicedController.checkAndSetControlDependency(def)){
				slicedController.addDefs(controller.getDefs());
				slicedController.addUses(controller.getUses());
				ops.add(slicedController);
				ops.addAll(findBackwardSliceForBytecode(controller, sessionId, includeData, includeControl));
			}
		}
		return ops;
	}
	
	private Set<SlicedBytecodeEvent> findBackwardSliceForBytecode(final BytecodeEvent criterion, 
																int sessionId,
																boolean includeData,
																boolean includeControl){
		Set<SlicedBytecodeEvent> ops = new HashSet<>();
		if (criterion == BytecodeEvent.UNTRACED_BYTECODE_OPERATION){
			return ops;
		}
		
		//Data flow analysis  
		if (includeData){
			SlicedBytecodeEvent slicedOp = criterion.getSlicedOperationForSession(sessionId);
			for (Binding<?> def : criterion.getDefs()){
				if (!slicedOp.checkAndSetDataDependency(def)){
					slicedOp.addDefs(criterion.getDefs());			
					slicedOp.addUses(criterion.getUses());
					ops.add(slicedOp);

					for (Binding<? extends MemoryLocation> binding : criterion.getUses()) {
						BytecodeEvent provider = binding.getProvider();
						ops.addAll(findBackwardSliceForBytecode(provider, binding, sessionId, includeData, includeControl));
					}
				}
			}
		}
		
		
		//Control flow analysis
		if (includeControl){
			final BytecodeEvent controller = findControllerForBytecode(criterion);
			SlicedBytecodeEvent slicedController = controller.getSlicedOperationForSession(sessionId);
			for (Binding<?> def : criterion.getDefs()){
				if (!slicedController.checkAndSetControlDependency(def)){
					slicedController.addDefs(controller.getDefs());
					slicedController.addUses(controller.getUses());
					ops.add(slicedController);
					ops.addAll(findBackwardSliceForBytecode(controller, sessionId, includeData, includeControl));
				}
			}
		}
		return ops;
	}
	
	public void setExceptionThrowingInstruction(BytecodeEvent op){
		this.exceptionThrowingInstruction = op;
	}
	
	public BytecodeEvent getExceptionThrowingInstruction(){
		return this.exceptionThrowingInstruction;
	}
	
	public BytecodeEvent getBytecodeOperation(int index){
		return bytecodes.get(index);
	}
}
