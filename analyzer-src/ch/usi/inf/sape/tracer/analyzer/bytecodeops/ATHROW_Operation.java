package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class ATHROW_Operation extends BytecodeEvent implements PEI{

	/**
	 * 
	 */
	private static final long serialVersionUID = -563167311401346039L;
	
	private final int pseudoVarDefIndex;
	
	public ATHROW_Operation(short opCode, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<?> exceptionBinding = threadCallStack.popOffStack();
		this.uses = new Binding[]{exceptionBinding};
		
		//Upon throwing an exception, the stack is made clear 
		//and the reference to the exception object gets pushed 
		//on the stack.
		threadCallStack.clearTopMostFrame();
		
		this.defs = new Binding[]{threadCallStack.pushOnStack(exceptionBinding.getValue(), Type.REFERENCE, this)};			
		threadCallStack.setThrowInstructionOccured();
		
		pseudoVarDefIndex = addPseudoVarDefForPEI();
	}

	public ATHROW_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, int pseudoVarDefIndex) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.pseudoVarDefIndex = pseudoVarDefIndex;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 1;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[])this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[pseudoVarDefIndex])){
			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getOperationName() {
		return "ATHROW";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.ATHROW_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}

}
