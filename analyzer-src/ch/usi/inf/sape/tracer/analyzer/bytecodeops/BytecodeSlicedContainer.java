package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sape.tracer.analyzer.locations.Container;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

public class BytecodeSlicedContainer extends SmallContainer{
		
		public BytecodeSlicedContainer(Container origin) {
			super(origin);
		}

		@Override
		public int getStartTime() {
			return origin.getStartTime();
		}

		@Override
		public int getEndTime() {
			return origin.getEndTime();
		}
		
	}
