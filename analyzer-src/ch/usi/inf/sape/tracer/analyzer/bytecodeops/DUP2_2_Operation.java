package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.StackSnapshot;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

/**
 * Form2: value  -> value, value        where value is a value of a category 2 computational type
 * 
 * @author reza
 */
public class DUP2_2_Operation extends CommonSubExpression{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2858666258130161111L;

	public DUP2_2_Operation(short opCode, int bytecodeIndex) {
		super(opCode, bytecodeIndex, 1);
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> use = threadCallStack.popOffStack();
		
		this.uses = new Binding[]{use};
		
		Value value = use.getValue();
		Type type = use.getMemoryLocation().getType();
		Binding def1 = threadCallStack.pushOnStack(value, type, this);
//		def1.setCommonSubexpression(true);
		Binding def2 = threadCallStack.pushOnStack(value, type, this);
//		def2.setCommonSubexpression(true);
		this.defs = new Binding[]{def1, def2};
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 1;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.uses;
	}

	public DUP2_2_Operation(short opCode, int sourceLineNo,
			int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs, new boolean[0]);
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getDefs() {
		assert this.defs.length == 2;
		assert this.defs[0].getMemoryLocation() instanceof StackSlotMemoryLocation; 
		assert this.defs[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		
		return (Binding<StackSlotMemoryLocation>[]) this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
			if (def.equals(this.defs[0]) || def.equals(this.defs[1])){
				return new Binding[]{this.uses[0]};
			}	
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getOperationName() {
		return "DUP2<Form 2>";
	}

	@Override
	public String evaluate(Binding expression, StackSnapshot stackSnapshot){
		return evaluate(0, 0, stackSnapshot);
	}

	@Override
	public Binding getCopy(Binding def) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getCommonSubexpressionIndexFor(Binding def) {
		return 0;
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.DUP2_2_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
