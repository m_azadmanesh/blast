package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.HeapSpace;
import ch.usi.inf.sape.tracer.analyzer.locations.InstanceFieldMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class GETFIELD_Operation extends BytecodeEvent implements PEI{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2939444321754734583L;
	
	private final int pseudoVarDefIndex;
	
	public GETFIELD_Operation(short opCode, Object obj, String fieldName, Value value, Type type, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> refBinding = threadCallStack.popOffStack();
		Binding<InstanceFieldMemoryLocation> fieldBinding = HeapSpace.getOrDefineInstanceLocationMetaData(obj).getInstanceFieldBinding(fieldName);
		this.uses = new Binding[]{refBinding, fieldBinding};
		this.defs = new Binding[]{threadCallStack.pushOnStack(value, type, this)};
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}
	
	/**
	 * used only when we know the getfield will cause an exception to be thrown right after execution
	 * @param bytecodeIndex
	 */
	public GETFIELD_Operation(short opCode, int bytecodeIndex){
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> refBinding = threadCallStack.popOffStack();
		Binding<InstanceFieldMemoryLocation> fieldBinding = Binding.UNDEFINED_BINDING;
		this.uses = new Binding[]{refBinding, fieldBinding};
		this.defs = Binding.UNDEFINED_BINDINGS;
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}
	
	public GETFIELD_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, int pseudoVarDefIndex) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.pseudoVarDefIndex = pseudoVarDefIndex;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		assert this.uses.length == 2;
		return this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0]) ||
				def.equals(this.defs[pseudoVarDefIndex])){
			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getOperationName() {
		return "GETFIELD";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.GETFIELD_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}

}
