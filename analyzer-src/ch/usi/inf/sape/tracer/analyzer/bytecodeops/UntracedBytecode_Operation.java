package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

public class UntracedBytecode_Operation extends BytecodeEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8931972991120424734L;
	
	public UntracedBytecode_Operation(short opCode, int bytecodeIndex) {
		super(opCode, bytecodeIndex, -1, null);
	}
	
	@Override
	public Activation getActivationRecord() {
		return Activation.UNTRACED_ACTIVATION;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		return Binding.UNTRACED_BINDINGS;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return Binding.UNTRACED_BINDINGS;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getOperationName() {
		return "UNTRACED";
	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		return "UNTRACED";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.UNTRACED_BC_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
