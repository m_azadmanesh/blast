package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.StackSnapshot;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Utils;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

/**
 * Form 1: value3, value2, value1 -> value1, value3, value2, value1    (all values are of category 1 computational type)
 * 
 * @author reza
 *
 */
public class DUP_X2_1_Operation extends CommonSubExpression{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 140339135420942333L;
	
	public DUP_X2_1_Operation(short opCode, int bytecodeIndex) {
		super(opCode, bytecodeIndex, 1);
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> value1Binding =  threadCallStack.popOffStackNoSideEffect();
		Binding<StackSlotMemoryLocation> value2Binding =  threadCallStack.popOffStackNoSideEffect();
		Binding<StackSlotMemoryLocation> value3Binding =  threadCallStack.popOffStackNoSideEffect();
		value1Binding.setTemporaryName(Utils.generateTemporaryName("", threadCallStack.getNextTempIndex()));
		value2Binding.setTemporaryName(Utils.generateTemporaryName("", threadCallStack.getNextTempIndex()));
		value3Binding.setTemporaryName(Utils.generateTemporaryName("", threadCallStack.getNextTempIndex()));
		
		this.uses = new Binding[]{value3Binding, value2Binding, value1Binding};
		this.uses[2].setTemporaryName(Utils.generateTemporaryName("", threadCallStack.getNextTempIndex()));
		
		Value value1 = value1Binding.getValue();
		Type type1 = value1Binding.getMemoryLocation().getType();
		
		Binding def1 = threadCallStack.pushOnStack(value1, type1, this);
		def1.setAsCommonSubexpression();
		
		Binding def2 = threadCallStack.pushOnStack(value3Binding.getValue(), value3Binding.getMemoryLocation().getType(), this);
		
		Binding def3 = threadCallStack.pushOnStack(value2Binding.getValue(), value2Binding.getMemoryLocation().getType(), this);
		
		Binding def4 = threadCallStack.pushOnStack(value1, type1, this);
		def4.setAsCommonSubexpression();
		
		this.defs = new Binding[]{def1, def2, def3, def4};
	}
	
	public DUP_X2_1_Operation(short opCode, int sourceLineNo,
			int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs, new boolean[0]);
	}


	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 3;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[2].getMemoryLocation() instanceof StackSlotMemoryLocation;
		
		return (Binding<StackSlotMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getDefs() {
		assert this.defs.length == 4;
		assert this.defs[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.defs[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.defs[2].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.defs[3].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(
			Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
			if (def.equals(this.defs[1])){
				return new Binding[]{this.uses[0]};
				
			}else if (def.equals(this.defs[2])){
				return new Binding[]{this.uses[1]};
				
			}else if (def.equals(this.defs[0]) || def.equals(this.defs[3])){
				return new Binding[]{this.uses[2]};
				
			}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}
	
	@Override
	public String getOperationName() {
		return "DUP_X2<Form 1>";
	}
	

	@Override
	public String evaluate(Binding expression, StackSnapshot stackSnapshot) {
		return evaluate(0, 2, stackSnapshot);
	}

	@Override
	public Binding getCopy(Binding def) {
		assert def.isCommonSubexpression() == true;
		
		if (def.equals(this.defs[0]))
			return this.defs[3];
		if (def.equals(this.defs[3]))
			return this.defs[0];
		
		throw new RuntimeException();
	}

	@Override
	public int getCommonSubexpressionIndexFor(Binding def) {
		return 0;
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.DUP_X2_1_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
