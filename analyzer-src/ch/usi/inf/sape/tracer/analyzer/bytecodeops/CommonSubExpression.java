package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.StackSnapshot;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

public abstract class CommonSubExpression extends BytecodeEvent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7431024164561374747L;
	
	private final boolean[] commonSubExpressionEvaluated;
	
	protected CommonSubExpression(short opCode, int bytecodeIndex, int expressionNo) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		this.commonSubExpressionEvaluated = new boolean[expressionNo];
	}

	public CommonSubExpression(short opCode, int sourceLineNo,
			int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs,
			boolean[] evaluated) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.commonSubExpressionEvaluated = evaluated;
	}
	/**
	 * each derivative of a dup instruction can have more than common subexpression, like dup2_x1.
	 * the subexpressionIndex identifies a particular common subexpression (the use set of a dup) 
	 * among the others. It should be noted that while common subexpression mean the use set of a
	 * dup, their indices are not the same as the indices of the use set. That's why we pass the 
	 * source index to this method as well.
	 * 
	 * @param subexpressionIndex
	 * @param sourceIndex
	 * @param stackSnapshot TODO
	 * @return
	 */
	protected String evaluate(int subexpressionIndex, int sourceIndex, StackSnapshot stackSnapshot){
		if (!isEvaluated(subexpressionIndex)){
			this.commonSubExpressionEvaluated[subexpressionIndex] = true;
			String val = this.uses[sourceIndex].getTemporaryName() + " = " ;
			val+= this.uses[sourceIndex].getProvider().getSymbolicInterpretation(this.uses[sourceIndex]) + ";  ";
			return val;
		}else
			return "";
	}
	
	protected boolean isEvaluated(int subexpressionIndex){
		return commonSubExpressionEvaluated[subexpressionIndex];
	}
	
	@Override
	public String getSymbolicInterpretation(Binding def) {
//		System.out.println(def.toString());
		if (def.isCommonSubexpression() && this.commonSubExpressionEvaluated[getCommonSubexpressionIndexFor(def)]){
//			String evalute = evaluate(def, null); 
			return  getUsesForDef(def)[0].getTemporaryName();   //evalute + getUsesForDef(def)[0].getTemporaryName();
		}
		
		return super.getSymbolicInterpretation(def);
	}

	@Override
	public boolean isDup() {
		return true;
	}
	
	public abstract String evaluate(Binding expression, StackSnapshot stackSnapshot);
	
	public abstract Binding getCopy(Binding def);
	
	public abstract int getCommonSubexpressionIndexFor(Binding def);
	
}
