package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.OperationCodes;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Utils;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.PseudoVariableMemoryLocation;

public class INVOKEARGS_Operation extends BytecodeEvent implements PEI{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5699264165538852205L;
	
	private final Activation calleeFrame;
	
	private final Activation callerFrame;
	
	private final boolean callerInstrumented;
	
	public final static UnTracedInvokeArgs_Operation UNTRACED_INVOKEARGS_OPERATION = new UnTracedInvokeArgs_Operation();
	
	/**
	 * This operation is initialized in two steps: 1) right before an invokation takes place,
	 * 2) after entering the invoked method. In the first step, the operation defines only a
	 * pseudo variable for control dependency tracking and in the second step, the actual args
	 * are added. This is because for exception handling, we need to keep track of the last 
	 * operation which was executed before the exception happens. So we need to create this 
	 * operation in the first step before the actual invocation. However, given that we don't 
	 * have the full information about the callee at the call site, we cannot fully initialize 
	 * until the method is entered. For these two steps, there are different cases like calling
	 * an instrumented method from an uninstrumented or calling an uninstrumented method from
	 * an instrumented. All these cases should be taken into account.
	 *  
	 * @param instruction
	 * @param name
	 * @param argsCount
	 * @param isStatic
	 */
	public INVOKEARGS_Operation(int instruction, String name, int argsCount, boolean isStatic, boolean callerInstrumented, String invokeType) {
		super(lookupOperationCode(invokeType), instruction, 
				callerInstrumented ? ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame() : Activation.UNTRACED_ACTIVATION);
		
		//This is the first step of initializing this operation
		ThreadCallStack tcs = ThreadCallStack.getCurrentThreadCallStack();
		this.callerInstrumented = callerInstrumented;
		this.callerFrame = this.callerInstrumented ? tcs.peekTopMostFrame() : Activation.UNTRACED_ACTIVATION;
		
		Activation frame = tcs.addFrameFor(name, argsCount, isStatic, this);
		tcs.setLastInvokationFrame(callerInstrumented);
		
		Binding<PseudoVariableMemoryLocation> pseudoVarOldUse = tcs.peekBasicBlockSimulatorStack();
		Binding<PseudoVariableMemoryLocation> pseudoVarDef = tcs.definePseudoVariable(this, 
				pseudoVarOldUse.getMemoryLocation().getPseudoVariableId());
		
		frame.setInvokationId(pseudoVarDef.getMemoryLocation().getPseudoVariableId());
		
		Binding<? extends MemoryLocation>[] actualArgsUse = tcs.useActualParameters(callerInstrumented);
		
		int useLength = actualArgsUse.length + 1;
		this.uses = new Binding[useLength];
		System.arraycopy(actualArgsUse, 0, this.uses, 0, actualArgsUse.length);
		this.uses[useLength - 1] = pseudoVarOldUse;
		
//		Binding[] actualArgs = tcs.defActualParameters(values, this);
		this.defs = new Binding[1];
//		System.arraycopy(actualArgs	, 0	, this.defs, 0, actualArgs.length);
 		this.defs[0] = pseudoVarDef;
		this.calleeFrame = frame;
	}
	
	/**
	 * This constructor has to be used only by the UnTracableInvokeArgs_Operation. 
	 * It provides a lazy initialization of the dependencies. We use it to remove
	 * the circular dependency during initialization of static UnTraced_* fields.
	 */
	private INVOKEARGS_Operation(){
		super((short)-1,-1,-1,null);
		this.calleeFrame = null;
		this.callerFrame = null;
		this.callerInstrumented = false;
	}
	
	public INVOKEARGS_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, Activation calleeFrame,
			Activation callerFrame, boolean callerInstrumented) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.calleeFrame = calleeFrame;
		this.callerFrame = callerFrame;
		this.callerInstrumented = callerInstrumented;
	}

	/**
	 * This method is called from the Analyzer.methodEntry() to perform the second
	 * step of initializing this operation. It also initializes the callee's frame.
	 */
	public void def(Object[] args, final String name,  int maxLocalArraySize, String localDetails, int maxStack, int argsCount, boolean isStatic) {
		ThreadCallStack tcs = ThreadCallStack.getCurrentThreadCallStack();
		calleeFrame.initialize(name, maxLocalArraySize, localDetails);
		Value[] values;
		if (!this.callerInstrumented){	//In this case, we generate the Value objects based on the actual args passed to the callee on entrance
			values = Utils.generateArgValues(args);
		}else{								//otherwise, we use the Value objects already existing on top of the Caller's stack frame
			values = new Value[argsCount];
			for (int i = 0; i < values.length; i++) {
				values[i] = this.uses[i].getValue();
			}
		}
		
		Binding[] actualArgs = tcs.defActualParameters(values, this);
		Binding<PseudoVariableMemoryLocation> pseudoVar = (Binding<PseudoVariableMemoryLocation>) this.defs[0];
		
		this.defs = new Binding[argsCount + 1];
		System.arraycopy(actualArgs, 0, this.defs, 0, actualArgs.length);
 		this.defs[argsCount] = pseudoVar;
	}
	
	/**
	 * This method is used for the second step of initialization when the callee is not 
	 * instrumented and we need to simulate the invocation for tracking data dependencies.
	 * @param values
	 */
	public void def(Value[] values){
		ThreadCallStack tcs = ThreadCallStack.getCurrentThreadCallStack();
		Binding[] actualArgs = tcs.defActualParameters(values, this);
		
		Binding<PseudoVariableMemoryLocation> pseudoVar = (Binding<PseudoVariableMemoryLocation>) this.defs[0];
		
		this.defs = new Binding[values.length];
		System.arraycopy(actualArgs, 0, this.defs, 0, actualArgs.length);
 		this.defs[values.length - 1] = pseudoVar;
	}
	
	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		return this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;	
		
		for (int i = 0; i < this.defs.length; i++) {
			if (defs[i].equals(def)){
				if (this.uses.length == 1)   //this means either a method with no argument or an uninstrumented method. in both cases, the 0th element is the pseudo var
					return new Binding[]{this.uses[0]};
				else{
					return new Binding[]{uses[i]};
				}
			}
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public boolean isExpressionStatement() {
		return true;
	}

//	@Override
//	public AbstractOperation generateAbstractOperation(BytecodeAbstractOpMap[] ops, Binding[] uses, Binding[] defs, int minBCIndex) {
//		return new Calling_AbsOperation(ops, uses, defs, minBCIndex);
//	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		String symbol = "";
		int start = 0;
		if (!calleeFrame.isStatic() && this.uses.length > 0){   //In the case of calling an instrumented method from an uninstrumented 
															//method, there is the chance that we have an invokevirtual with no uses
			symbol += this.uses[0].getProvider().getSymbolicInterpretation(null) + ".";
			start = 1;
		}
		symbol += calleeFrame.getMethodName() + "(";
		for (int i = start; i < this.uses.length ; i++) {											  
			symbol += uses[i].getProvider().getSymbolicInterpretation(null);
			if (i < this.uses.length - 1)
				symbol += " , ";
		}
		symbol += ")";
		return symbol;
	}

	@Override
	public String getOperationName() {
		return "INVOKE_ARGS";
	}

	private static short lookupOperationCode(String invokeType){
		short opCode = -1;
		switch(invokeType){
		case "INVOKESTATIC":
			opCode = OperationCodes.INVOKESTATIC;
			break;
		case "INVOKEVIRTUAL":
			opCode = OperationCodes.INVOKEVIRTUAL;
			break;
		case "INVOKEINTERFACE":
			opCode = OperationCodes.INVOKEINTERFACE;
			break;
		case "INVOKESPECIAL":
			opCode = OperationCodes.INVOKESPECIAL;
			break;
		case "INVOKEDYNAMIC":
			opCode = OperationCodes.INVOKEDYNAMIC;
			break;
		default:
			opCode = OperationCodes.INVOKEUNKNOWN;
		}
		return opCode;		
	}
	
	public Activation getCalleeFrame(){
		return this.calleeFrame;
	}
	
	public Activation getCallerFrame(){
		return this.callerFrame;
	}
	
	public static class UnTracedInvokeArgs_Operation extends INVOKEARGS_Operation{

		
		@Override
		public Activation getCalleeFrame() {
			return Activation.UNTRACED_ACTIVATION;
		}

		@Override
		public Activation getCallerFrame() {
			return Activation.UNTRACED_ACTIVATION;
		}
		
		@Override
		public Activation getActivationRecord() {
			return Activation.UNTRACED_ACTIVATION;
		}

		@Override
		public Binding<? extends MemoryLocation>[] getUses() {
			return Binding.UNTRACED_BINDINGS;
		}

		@Override
		public Binding<? extends MemoryLocation>[] getDefs() {
			return Binding.UNTRACED_BINDINGS;
		}

		@Override
		public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
			return Binding.NOT_AVAILABLE_BINDINGS;
		}

		@Override
		public String getOperationName() {
			return "UNTRACED_INVOKE_ARGS";
		}

		@Override
		public String getSymbolicInterpretation(Binding def) {
			return "UNTRACED_INVOKE_ARGS";
		}

		@Override
		public void accept(TraceElementVisitor visitor) throws VisitorException {
			visitor.visit(this);
		}

		@Override
		public byte getEventClassCode() {
			return EventConstants.UNTRACED_INVOKEARGS_EVENT;
		}

		@Override
		public boolean isPEI() {
			return false;
		}
		
		
		
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.INVOKEARGS_EVENT;
	}
	
	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		//TODO: fix the value of pseudo var def index
		return -1;
	}

	/**
	 * @return the callerInstrumented
	 */
	public boolean isCallerInstrumented() {
		return callerInstrumented;
	}
}
