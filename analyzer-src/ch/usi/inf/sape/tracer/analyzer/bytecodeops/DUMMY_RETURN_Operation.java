package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.LocalMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.ReturnValueMemoryLocation;

/**
 * An instance of this class is used when there is a lack of instructions that can show the flow of 
 * information (an uninstrumented method) and we use this class to artificially connect the inputs 
 * to the outputs, i.e., to connect arguments to the local variables of the callee and the local
 * variables to the return value location.
 *  
 * @author reza
 *
 */
public class DUMMY_RETURN_Operation extends BytecodeEvent{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -961419203240254524L;
	
	public DUMMY_RETURN_Operation(short opCode, Value returnValue, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		this.uses = threadCallStack.readAllLocals();
		this.defs = new Binding[]{threadCallStack.defReturnValue(returnValue, this)};
	}

	public DUMMY_RETURN_Operation(short opCode, int sourceLineNo,
			int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
	}

	@Override
	public Binding<LocalMemoryLocation>[] getUses() {
		for (int i = 0; i < this.uses.length; i++) {
			assert this.uses[i].getMemoryLocation() instanceof LocalMemoryLocation;
		}
		
		return (Binding<LocalMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<ReturnValueMemoryLocation>[] getDefs() {
		assert this.defs.length == 1;
		assert this.defs[0].getMemoryLocation() instanceof LocalMemoryLocation;
		
		return (Binding<ReturnValueMemoryLocation>[]) this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0])){
			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}
	
	@Override
	public boolean isExpressionStatement() {
		return true;
	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		String result = this.defs[0].getMemoryLocation().toString() + " = ";
		
		for (Binding use : this.uses) {
			result += ((LocalMemoryLocation)use.getMemoryLocation()).getName() + " ";
		}
		
		return result;
	}
	
//	@Override
//	public AbstractOperation generateAbstractOperation(BytecodeAbstractOpMap[] ops, Binding[] uses, Binding[] defs,int minBCIndex) {
//		return new Dummy_AbsOperation(ops, uses, defs, minBCIndex);
//	}


	@Override
	public String getOperationName() {
		return "DUMMY";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.DUMMY_RETURN_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
