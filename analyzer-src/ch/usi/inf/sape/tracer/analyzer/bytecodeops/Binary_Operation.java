package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public class Binary_Operation extends BytecodeEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7038348741726646958L;
	
	private final String opName; 
	
	public Binary_Operation(short opCode, final String opName, Value value, final Type type, int bytecodeIndex) {
		this(opCode, opName, bytecodeIndex);
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> use1 = threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> use2 = threadCallStack.popOffStack();
		this.uses = new Binding[]{use1, use2};
		
		this.defs = new Binding[]{threadCallStack.pushOnStack(value, type, this)};
	}
	
	protected Binary_Operation(final short opCode, final String opName, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		this.opName = opName;
	}
	
	public void def(Value value, Type type){
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		this.defs[0] = threadCallStack.pushOnStack(value, type, this);
	}
	
	public Binary_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, String opName) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.opName = opName;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 2;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[])this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0])){
			assert this.uses.length == 2;
			assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
			assert this.uses[1].getMemoryLocation() instanceof StackSlotMemoryLocation;

			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;

	}
	
	@Override
	public String getSymbolicInterpretation(Binding def) {
		return this.uses[1].getProvider().getSymbolicInterpretation(this.uses[1])+ " + " + this.uses[0].getProvider().getSymbolicInterpretation(this.uses[0]);
	}


	@Override
	public String getOperationName() {
		return this.opName;
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.BINARY_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
