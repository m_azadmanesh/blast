package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

public final class XRETURN_Operation extends BytecodeEvent implements PEI {

	/**
	 * 
	 */
	private static final long serialVersionUID = -131542548557842525L;
	
	private final Type type;
	
	private final int pseudoVarDefIndex;
	
	@SuppressWarnings("unchecked")
	public XRETURN_Operation(short opCode, Type type, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> use = threadCallStack.popOffStack();
		this.uses = new Binding[]{use};
		this.defs = new Binding[]{threadCallStack.defReturnValue(use.getValue(), this)};
		this.type = type;
		threadCallStack.popFrame();
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}
	
	@SuppressWarnings("unchecked")
	public XRETURN_Operation(short opCode, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		this.uses = Binding.NO_USE_BINDINGS;
		this.defs = Binding.NO_DEF_BINDINGS;
		this.type = Type.VOID;

		ThreadCallStack.getCurrentThreadCallStack().popFrame();
		
		pseudoVarDefIndex = addPseudoVarDefForPEI();
	}

	public XRETURN_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, Type type,
			int pseudoVarDefIndex) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.type = type;
		this.pseudoVarDefIndex = pseudoVarDefIndex;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Binding<StackSlotMemoryLocation>[] getUses() {
		if (type != Type.VOID){
			assert this.uses.length == 1;
			assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		}else{
			assert this.uses.length == 0;
		}
		
		return (Binding<StackSlotMemoryLocation>[]) this.uses;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if ((type != Type.VOID && def.equals(this.defs[0]))  ||
				def.equals(this.defs[pseudoVarDefIndex])){
			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}
	
	@Override
	public boolean isExpressionStatement() {
		return true;
	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		if (type == Type.VOID)
			return "return";
		else 
			return "return " + this.uses[0].getProvider().getSymbolicInterpretation(null);
	}
	
//	@Override
//	public AbstractOperation generateAbstractOperation(BytecodeAbstractOpMap[] ops, Binding[] uses, Binding[] defs,int minBCIndex) {
//		return new Return_AbsOperation(ops, uses, defs, minBCIndex);
//	}


	@Override
	public String getOperationName() {
		if (type == Type.VOID)
			return "RETURN";
		else 
			return type.getDescriptor() + "RETURN";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.XRETURN_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}

}
