package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Analyzer;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.OperationCodes;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.ReturnValueMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

/**
 * After an invokation returns to the caller where we need to copy the return value from the callee's return value 
 * location to the caller's operand stack. We'd expect that the stack frame of the callee is already popped off and 
 * this pseudo-instruction has nothing to do with the state of the call stack, though given that we have the chance
 * to call an uninstrumented method from an instrumented method, the logics for the return case should be expanded 
 * so that it has to first figure out if this is the case and if yes, it has to artificially add operations that show
 * the flow of information from the local variabels of the callee to the return value, and from there to the operand
 * stack of the caller. 
 * 
 * @author reza
 *
 */
public class INVOKERETURNING_Operation extends BytecodeEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 168725475661173814L;

	private final String invokeType;
	
	/**
	 * The class name, the method name and the descriptor of the method called by this operation all concatenated together 
	 */
	private final String nameDescClass;
	
	private final Type type;
	
	public static INVOKERETURNING_Operation create(short opCode, Value returnValue, String opName, String classNameDesc, Type type, int bytecodeIndex) {
		if (!isCalleeInstrumented()){
			generateInvokeArgsOp(classNameDesc, bytecodeIndex);
			generateArtificialReturnOp(returnValue, bytecodeIndex);
			ThreadCallStack.getCurrentThreadCallStack().popFrame();
		}
		return new INVOKERETURNING_Operation(opCode, returnValue, opName, classNameDesc, type, bytecodeIndex);
	}
	
	/**
	 * This is the case when the method doesn't have any return value
	 * @param opName
	 * @param classNameDesc
	 * @param bytecodeIndex
	 * @return
	 */
	public static INVOKERETURNING_Operation create(short opCode, String opName, String classNameDesc, int bytecodeIndex) {
		if (!isCalleeInstrumented()){
			generateInvokeArgsOp(classNameDesc, bytecodeIndex);
			ThreadCallStack.getCurrentThreadCallStack().popFrame();
		}

		return new INVOKERETURNING_Operation(opCode, opName, classNameDesc, bytecodeIndex);
	}
	
	private INVOKERETURNING_Operation(short opCode, Value returnValue, String opName, String classNameDesc, Type type, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		threadCallStack.killInvoke();
		this.uses = new Binding[]{threadCallStack.useReturnValue()};
		this.defs = new Binding[]{threadCallStack.pushOnStack(returnValue, type, this)};
		this.invokeType = opName;
		this.nameDescClass = classNameDesc;
		this.type = type;
		
		//We explicitly set this flag here to show that this invokation returned successfully. 
		//This helps us to deal with different situations where an exception is thrown/caught 
		//in an uninstrumented method
		ThreadCallStack.getCurrentThreadCallStack().setExceptionCaught();		

	}
	
	private INVOKERETURNING_Operation(short opCode, String opName, String classNameDesc, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack.getCurrentThreadCallStack().killInvoke();
		this.uses = Binding.NO_USE_BINDINGS;
		this.defs = Binding.NO_DEF_BINDINGS;
		this.invokeType = opName;
		this.nameDescClass = classNameDesc;
		this.type = Type.VOID;
		
		//We explicitly set this flag here to show that this invokation returned successfully. 
		//This helps us to deal with different situations where an exception is thrown/caught 
		//in an uninstrumented method
		ThreadCallStack.getCurrentThreadCallStack().setExceptionCaught();		
	}
	
	public INVOKERETURNING_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, String invokeType,
			String nameDescClass, Type type) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.invokeType = invokeType;
		this.nameDescClass = nameDescClass;
		this.type = type;
	}

	@Override
	public Binding<ReturnValueMemoryLocation>[] getUses() {
		assert this.uses.length == 1;
		assert this.uses[0].getMemoryLocation() instanceof ReturnValueMemoryLocation;
		
		return (Binding<ReturnValueMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getDefs() {
		assert this.defs.length == 1;
		assert this.defs[0].getMemoryLocation()  instanceof StackSlotMemoryLocation;
		
		return (Binding<StackSlotMemoryLocation>[]) this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (type != Type.VOID && def.equals(this.defs[0])){
			return this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getOperationName() {
		return this.invokeType + "(Returning:  "+ this.nameDescClass +")";
	}

	private static boolean isCalleeInstrumented(){
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		if (threadCallStack.peekTopMostFrame().isInitialized()){  //This means that we have called an uninstrumented method which couldn't pop its corresponding frame, otherwise the frame of the current method with a non-null lastInvokationFrame should have been on top of the stack
			return true;
		}
		
		return false;
	}
	
	/**
	 * This method is used only when the callee is not instrumented, but the caller is
	 * instrumented
	 * @param classNameDesc
	 * @param bytecodeIndex
	 */
	private static void generateInvokeArgsOp(String classNameDesc, int bytecodeIndex){
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		
		//This method may be called in two cases: 1) Returning from an uninstrumented method which 
		//didn't call any instrumented method during its execution, 2) returning fromm an uninstrumented
		//method which called some instrumented method during execution. For the latter case, the last
		//operation is not necessarily an Invokeargs op.
		
		INVOKEARGS_Operation argsOp = threadCallStack.peekTopMostFrame().getProvider();   //Given that the caller is available, we can find the values of the args from the operand stack of the caller
		Binding<? extends MemoryLocation>[] argsBindings = argsOp.getUses();
		
		Type[] types = new Type[argsBindings.length - 1];  //The last use of an invokeArgs op is the pseudovar

		for (int i = 0; i < types.length; i++) {
			types[i] = ((StackSlotMemoryLocation) argsBindings[i].getMemoryLocation()).getType();
		}
		
		Activation callee = threadCallStack.initializeCalleeFrame(classNameDesc, types);
		Value[] args = new Value[argsBindings.length];
		for (int i = 0; i < args.length; i++) {
			args[i] = argsBindings[i].getValue();
		}
		argsOp.def(args);
		Analyzer.log(argsOp.toString());
	}
	
	private static void generateArtificialReturnOp(Value returnValue, int bytecodeIndex){
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		DUMMY_RETURN_Operation dummyOp = new DUMMY_RETURN_Operation(OperationCodes.DUMMY_RETURN, returnValue, -1);
		threadCallStack.addOperation(dummyOp);
	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		Activation frame = ((Binding<ReturnValueMemoryLocation>)this.uses[0]).getMemoryLocation().getFrame();
		return frame.getMethodSignature();
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.INVOKERETURNING_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
