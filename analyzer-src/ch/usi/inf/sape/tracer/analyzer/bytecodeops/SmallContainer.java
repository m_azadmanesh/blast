package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sape.tracer.analyzer.NotImplementedException;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.Container;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

public abstract class SmallContainer extends Container{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1024762135033831729L;

	protected List<MemoryLocation> locations = new ArrayList<>();
	
	/**
	 * The bytecode-level non-abstract container
	 */
	protected final Container origin;
	
	private int offset = -1;

	public SmallContainer(Container origin) {
		this.origin = origin;
	}

	@Override
	public List<? extends MemoryLocation> getElements() {
		return this.locations;
	}

	@Override
	public int getElementCount() {
		return locations.size();
	}

	@Override
	public int getOffset() {
		return this.offset;
	}

	@Override
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public void addLocationIfNew(MemoryLocation location){
		if (!locations.contains(location))
			this.locations.add(location);
	}

	public int getPresentationOffset(MemoryLocation memLocation){
		int i = 0;
		
		while(true){
			if (locations.get(i).equals(memLocation))
				break;
			i++;
		}	//there should never be an IndexOutOfBoundsException here
		
		return i;
	}

	@Override
	public String toString() {
		return origin.toString();
	}
	
	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		throw new NotImplementedException();
	}

}
