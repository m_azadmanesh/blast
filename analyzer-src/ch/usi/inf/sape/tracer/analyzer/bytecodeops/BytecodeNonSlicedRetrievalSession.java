package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import java.util.List;

import ch.usi.inf.sape.tracer.analyzer.Event;
import ch.usi.inf.sape.tracer.analyzer.abstractops.RetrievalSession;
import ch.usi.inf.sape.tracer.analyzer.locations.Container;

public class BytecodeNonSlicedRetrievalSession implements RetrievalSession{
	
	private final List<BytecodeEvent> ops;
	
	private final int sessionId;
	
	public BytecodeNonSlicedRetrievalSession(List<BytecodeEvent> ops, int sessionId) {
		this.ops = ops;
		this.sessionId = sessionId;
	}

	/**
	 * For this session, this method determines the number of operations that reside within the border of
	 * the input container. In order to avoid passing through all the operation of a session and checking
	 * for the start time and end time, we use this optimization that upon passing from the end time of the
	 * container once, we don't check for the remaining operations. 
	 * @param container
	 * @return
	 */
	public int getOperationCountForContainer(Container container){
		boolean started = false;
		int count = 0;
		
		for (BytecodeEvent operation : ops) {
			if (!started){
				if (operation.getOperationIndex()>= container.getStartTime() && operation.getOperationIndex() <= container.getEndTime()){
					started = true;
					count ++;
				}
			} else if (operation.getOperationIndex() <= container.getEndTime()){
					count ++;
			} else
					break;
		}
		
		return count;
	}

	/**
	 * @return the sessionId
	 */
	public int getId() {
		return sessionId;
	}

	/**
	 * @return the operations
	 */
	public List<BytecodeEvent> getOperations() {
		return ops;
	}
	
	public int getMaxOperationId(){
		return ops.get(ops.size() - 1).getOperationIndex();
	}
	
	public int getOperationCount(){
		return ops.size();
	}
	
	/**
	 * The return value is inclusive.
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public int getOperationCountBetweenInclusive(int start, int end){
		int count = 0;
		for (BytecodeEvent operation : ops) {
			int opIndex = operation.getOperationIndex();
			if (opIndex >= start && opIndex <= end)
				count ++;
			else if (opIndex >end)
				break;			//just to avoid unnecessary iterations
		}
		return count;
	}
	
	public int getOperationCountBetweenExclusive(int start, int end){
		int count = 0;
		for (Event operation : ops) {
			int opIndex = operation.getOperationIndex();
			if (opIndex > start && opIndex < end)
				count ++;
			else if (opIndex >end)
				break;			//just to avoid unnecessary iterations
		}
		return count;
	}

	/**
	 * should never be called
	 */
	@Override
	public BytecodeSlicedContainer createSmallContainer(Container parent) {
		throw new RuntimeException();
	}

}
