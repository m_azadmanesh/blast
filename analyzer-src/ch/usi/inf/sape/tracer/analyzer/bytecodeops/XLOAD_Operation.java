package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.LocalMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;



public final class XLOAD_Operation extends BytecodeEvent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1844233242363951704L;
	
	private final Type type;
	
	@SuppressWarnings("unchecked")
	public XLOAD_Operation(short opCode, int localIndex, Type type, int srcLineNo){
		super(opCode, srcLineNo, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack tcs = ThreadCallStack.getCurrentThreadCallStack();
		Binding<LocalMemoryLocation> use = tcs.readFromLocal(localIndex);
		
		// A local should always have been defined before being used
		assert use != null;
		use.addCopyOnStack();		//used for resolving complex source code statements
		
		this.uses = new Binding[]{use};
		this.defs = new Binding[]{tcs.pushOnStack(use.getValue(), type, this)};
		this.type = type;
	}

	public XLOAD_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, Type type) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.type = type;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Binding<LocalMemoryLocation>[] getUses() {
		assert this.uses.length == 1;
		assert this.uses[0].getMemoryLocation() instanceof LocalMemoryLocation;
		
		return (Binding<LocalMemoryLocation>[]) this.uses;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Binding<StackSlotMemoryLocation>[] getDefs() {
		assert this.defs.length == 1;
		assert this.defs[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		
		return (Binding<StackSlotMemoryLocation>[]) this.defs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Binding<LocalMemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0])){
			assert this.uses.length == 1;
			assert this.uses[0].getMemoryLocation() instanceof LocalMemoryLocation;

			return (Binding<LocalMemoryLocation>[])this.uses;
		}
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		if (this.uses[0].isOverwritten()){
			return this.uses[0].getTemporaryName();
		}
		
		return ((LocalMemoryLocation)this.uses[0].getMemoryLocation()).getName();
	}


	@Override
	public String getOperationName() {
		return type.getDescriptor() + "LOAD";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.XLOAD_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
