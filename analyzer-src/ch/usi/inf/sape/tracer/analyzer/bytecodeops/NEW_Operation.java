package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.InstanceReferenceValue;
import ch.usi.inf.sape.tracer.analyzer.PEI;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.HeapSpace;
import ch.usi.inf.sape.tracer.analyzer.locations.InstanceFieldMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.InstanceMetaData;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

/**
 * Operation: _  ->  ObjectRef
 * 
 * @author reza
 *
 */
public class NEW_Operation extends BytecodeEvent implements PEI {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -276175632688966881L;

	private final String desc;
	
	private final int pseudoVarDefIndex; 
	
	@SuppressWarnings("unchecked")
	public NEW_Operation(short opCode, Object obj, String desc, int bcIndex){
		super(opCode, bcIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack tcs = ThreadCallStack.getCurrentThreadCallStack();
		
		this.uses = Binding.NO_USE_BINDINGS;
		
		InstanceMetaData instanceLocation = HeapSpace.defineInstanceLocationMetaData(obj, true, this);
		Binding<InstanceFieldMemoryLocation>[] fields = instanceLocation.getInstanceFieldBindings();
		this.defs = new Binding[fields.length + 1];
		this.defs[0] = tcs.pushOnStack(new InstanceReferenceValue(instanceLocation), Type.REFERENCE, this);  // object ref
		System.arraycopy(fields, 0, this.defs, 1, fields.length);					  //instance fields
		this.desc = desc;
		
		this.pseudoVarDefIndex = addPseudoVarDefForPEI();
	}

	public NEW_Operation(short opCode, int sourceLineNo, int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, String desc,
			int pseudoVarDefIndex) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.desc = desc;
		this.pseudoVarDefIndex = pseudoVarDefIndex;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUses() {
		return this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;

		for (Binding<? extends MemoryLocation> binding : defs) {
			if (binding.equals(def))
				return this.uses;
		}		
		
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getSymbolicInterpretation(Binding def) {
		return "new "+ desc+"()";
	}

	@Override
	public String getOperationName() {
		return "NEW";
	}

	@Override
	public boolean isExpressionStatement() {
		return true;
	}

//	@Override
//	public AbstractOperation generateAbstractOperation(BytecodeAbstractOpMap[] ops, Binding[] uses, Binding[] defs,int minBCIndex) {
//		return new New_AbsOperation(ops, uses, defs, minBCIndex);
//	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.NEW_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

	@Override
	public boolean isPEI() {
		return true;
	}

	@Override
	public int getPseudoVarDefIndex() {
		return this.pseudoVarDefIndex;
	}

}
