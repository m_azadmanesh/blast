package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

/**
 * Form 2: value2, value1  ->  value1, value2, value1				  (value1 is a value of a category 1 computational type and value2 is a value of a category 2 computational type)
 * 
 * @author reza
 *
 */
public class DUP_X2_2_Operation extends BytecodeEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2655656765489175451L;
	
	public DUP_X2_2_Operation(short opCode, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> value1Binding =  threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> value2Binding =  threadCallStack.popOffStack();
		this.uses = new Binding[]{value2Binding, value1Binding};
		
		Value value1 = value1Binding.getValue();
		Type type1 = value1Binding.getMemoryLocation().getType();
		this.defs = new Binding[]{threadCallStack.pushOnStack(value1, type1, this), 
									threadCallStack.pushOnStack(value2Binding.getValue(), value2Binding.getMemoryLocation().getType(), this),
									threadCallStack.pushOnStack(value1, type1, this)};
	}

	public DUP_X2_2_Operation(short opCode, int sourceLineNo,
			int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 2;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getDefs() {
		assert this.defs.length == 3;
		assert this.defs[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.defs[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.defs[2].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(
			Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
			if (def.equals(this.defs[0]) || def.equals(this.defs[2])){
				return new Binding[]{this.uses[1]};
				
			}else if (def.equals(this.defs[1])){
				return new Binding[]{this.uses[0]};
				
			}
		
		return Binding.NOT_AVAILABLE_BINDINGS;

	}

	@Override
	public String getOperationName() {
		return "DUP_X2<Form 2>";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.DUP_X2_2_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
