package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.PseudoVariableMemoryLocation;

public class BasicBlockEntry_PseudoOperation extends BytecodeEvent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6948581719688550694L;
	
	private final int bbId;
	
	public BasicBlockEntry_PseudoOperation(short opCode, int bytecodeIndex, int bb, String name, int[] doms){
		super(opCode, bytecodeIndex, findContainer());
		this.bbId = bb;
		ThreadCallStack tcs = ThreadCallStack.getCurrentThreadCallStack();
		
		if (!ThreadCallStack.getCurrentThreadCallStack().isTopMostFrameInitialized()){				//This is the case when we invoke an uninstrumented method
			ThreadCallStack.getCurrentThreadCallStack().popFrame();
		}
		
		// We need to find the basic block from which we enter into this basic block
		Binding<PseudoVariableMemoryLocation> use;
		
		if (tcs.peekBasicBlockSimulatorStack().getMemoryLocation().getContainer() 
				!= tcs.peekTopMostFrame()){						//this means an uncaught exception in another method

			use = tcs.peekBasicBlockSimulatorStack();
			
			setExceptionFlag(tcs);
			
			//pop off the basic block stack simulator to point we reach to a 
			//pseudo variable which belongs to the current topmost stack frame.
			removeExtraPseudoVariables(); 
		}else{
			while (true){
				int top = tcs.peekBasicBlockSimulatorStack().getMemoryLocation().getPseudoVariableId();
				if (canBeEnteredFrom(top, doms)){
					use = tcs.peekBasicBlockSimulatorStack();
					break;
				}

				tcs.popOffBasicBlockSimulatorStack();
			}
		}
		
		this.uses = new Binding[]{use};
		PseudoVariableMemoryLocation location = new PseudoVariableMemoryLocation(tcs.peekTopMostFrame(), bb);
		Binding<PseudoVariableMemoryLocation> binding = new Binding<>(location, 
				new Value(use.getMemoryLocation().getPseudoVariableId()), this);
		tcs.pushOnBasicBlockSimulatorStack(binding);
		
		
		this.defs = new Binding[]{binding};
	}

	public BasicBlockEntry_PseudoOperation(short opCode, int sourceLineNo,
			int opIndex, Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs, int bbId) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
		this.bbId = bbId;
	}

	@Override
	public Binding<PseudoVariableMemoryLocation>[] getUses() {
		return (Binding<PseudoVariableMemoryLocation>[])this.uses;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getDefs() {
		return this.defs;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(
			Binding<? extends MemoryLocation> def) {
		return Binding.NOT_AVAILABLE_BINDINGS;
	}

	@Override
	public String getOperationName() {
		return "BASICBLOCK [entry]";
	}

	@Override
	public boolean isBasicBlockEntry() {
		return true;
	}

	/**
	 * Returns true if the current basic block can be entered from the top basic block
	 * 
	 * @param top
	 * @param dominators
	 * @return
	 */
	private boolean canBeEnteredFrom(int top, int[] dominators){
		for (int dominator : dominators) {
			if (dominator >= 0) {
				if (top == dominator){
					return true;
				}
			} else{
				if (top < 0)
					return true;
			}
		}
		return false;
	}
	
	private void removeExtraPseudoVariables(){
		ThreadCallStack tcs = ThreadCallStack.getCurrentThreadCallStack();
		Activation currentActivation = tcs.peekTopMostFrame();
		while (tcs.peekBasicBlockSimulatorStack().getMemoryLocation().getContainer() != currentActivation){
			tcs.popOffBasicBlockSimulatorStack();
		}
	}
	
	private static Activation findContainer(){
		ThreadCallStack tcs = ThreadCallStack.getCurrentThreadCallStack();
		Activation result;
		if (!tcs.isTopMostFrameInitialized()){				//This is the case when we invoke an uninstrumented method
			result = tcs.getFrame(1);
		}else{
			result = tcs.peekTopMostFrame();
		}

		return result;
	}
	
	private static void setExceptionFlag(ThreadCallStack tcs){
		if (!tcs.isExceptionThrown()){
			tcs.setUncaughtExceptionInUninstrumented();
		}else{
			tcs.setExceptionThrown();
		}
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.BASIC_BLOCK_ENTRY_EVENT;
	}
	
	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
