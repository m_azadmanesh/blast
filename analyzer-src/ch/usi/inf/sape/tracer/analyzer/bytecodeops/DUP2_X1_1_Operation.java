package ch.usi.inf.sape.tracer.analyzer.bytecodeops;

import ch.usi.inf.sape.tracer.analyzer.Activation;
import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.EventConstants;
import ch.usi.inf.sape.tracer.analyzer.ThreadCallStack;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.Utils;
import ch.usi.inf.sape.tracer.analyzer.Value;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;

/**
 * Form1: value3, value2, value1  ->  value2, value1, value3, value2, value1          
 * where value1, value2, and value3 are all values of a category 1 computational type
 *  
 * @author reza
 */
public class DUP2_X1_1_Operation extends BytecodeEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1815618163093135391L;
	
	@SuppressWarnings("unchecked")
	public DUP2_X1_1_Operation(short opCode, int bytecodeIndex) {
		super(opCode, bytecodeIndex, ThreadCallStack.getCurrentThreadCallStack().peekTopMostFrame());
		ThreadCallStack threadCallStack = ThreadCallStack.getCurrentThreadCallStack();
		Binding<StackSlotMemoryLocation> value1Binding =  threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> value2Binding =  threadCallStack.popOffStack();
		Binding<StackSlotMemoryLocation> value3Binding =  threadCallStack.popOffStack();
		value1Binding.setTemporaryName(Utils.generateTemporaryName("", threadCallStack.getNextTempIndex()));
		value2Binding.setTemporaryName(Utils.generateTemporaryName("", threadCallStack.getNextTempIndex()));
		value3Binding.setTemporaryName(Utils.generateTemporaryName("", threadCallStack.getNextTempIndex()));
		
		this.uses = new Binding[]{value3Binding, value2Binding, value1Binding};
		
		Value value1 = value1Binding.getValue();
		Type type1 = value1Binding.getMemoryLocation().getType();
		
		Value value2 = value2Binding.getValue();
		Type type2 = value2Binding.getMemoryLocation().getType();
		
		Binding def1 = threadCallStack.pushOnStack(value2, type2, this);
		def1.setAsCommonSubexpression();
		Binding def2 = threadCallStack.pushOnStack(value1, type1, this);
		def2.setAsCommonSubexpression();
		Binding def3 = threadCallStack.pushOnStack(value3Binding.getValue(), value3Binding.getMemoryLocation().getType(), this);
		def3.setAsCommonSubexpression();
		Binding def4 = threadCallStack.pushOnStack(value2, type2, this);
		def4.setAsCommonSubexpression();
		Binding def5 = threadCallStack.pushOnStack(value1, type1, this);
		def5.setAsCommonSubexpression();
		
		this.defs = new Binding[]{def1, def2, def3, def4, def5};
	}

	public DUP2_X1_1_Operation(short opCode, int sourceLineNo,
			int opIndex,
			Activation activationRecord,
			Binding<? extends MemoryLocation>[] uses,
			Binding<? extends MemoryLocation>[] defs) {
		super(opCode, sourceLineNo, opIndex, activationRecord, uses, defs);
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUses() {
		assert this.uses.length == 3;
		assert this.uses[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.uses[2].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.uses;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getDefs() {
		assert this.defs.length == 5;
		assert this.defs[0].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.defs[1].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.defs[2].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.defs[3].getMemoryLocation() instanceof StackSlotMemoryLocation;
		assert this.defs[4].getMemoryLocation() instanceof StackSlotMemoryLocation;
		return (Binding<StackSlotMemoryLocation>[]) this.defs;
	}

	@Override
	public Binding<StackSlotMemoryLocation>[] getUsesForDef(
			Binding<? extends MemoryLocation> def) {
		if (def == null)
			return Binding.NOT_AVAILABLE_BINDINGS;
		
		if (def.equals(this.defs[0]) || def.equals(this.defs[3])){
				return new Binding[]{this.uses[1]};
				
			}else if (def.equals(this.defs[1]) || def.equals(this.defs[4])){
				return new Binding[]{this.uses[2]};
				
			}else if (def.equals(this.defs[2])){
				return new Binding[]{this.uses[0]};
				
			}
		
		return Binding.NOT_AVAILABLE_BINDINGS;

	}

	@Override
	public String getOperationName() {
		return "DUP2_X1<Form 1>";
	}

	@Override
	public byte getEventClassCode() {
		return EventConstants.DUP2_X1_1_EVENT;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}
