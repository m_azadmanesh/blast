package ch.usi.inf.sape.tracer.analyzer;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import ch.usi.inf.sape.tracer.analyzer.locations.HeapSpace;
import ch.usi.inf.sape.tracer.analyzer.locations.LocalMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

public class Utils {

	public static String findClassName(final String classNameDesc){
		return classNameDesc.substring(0, classNameDesc.indexOf('.'));
	}
	
	public static String findMethodName(final String classNameDesc){
		return classNameDesc.substring(classNameDesc.indexOf('.') + 1, classNameDesc.indexOf('('));
	}
	
	public static String findDescriptor(String classNameDesc){
		return classNameDesc.substring(classNameDesc.indexOf('('));
	}
	
	public static LocalMemoryLocation[][] parseAndCreateLocals(int maxLocalArraySize, String localDetails, Activation frame){
		StringTokenizer tokenizer = new StringTokenizer(localDetails, ",");
		boolean cond = true;
		LocalMemoryLocation [][] localArray = new LocalMemoryLocation[maxLocalArraySize][];
		
		List<LocalMemoryLocation>[] tempListArrayForLocals = new ArrayList[maxLocalArraySize];
		for (int i = 0; i < tempListArrayForLocals.length; i++) {
			tempListArrayForLocals[i] = new ArrayList<LocalMemoryLocation>();
		}
		
		while(tokenizer.hasMoreTokens()){
			String name = tokenizer.nextToken();
			MemoryLocationInfo.Type type = MemoryLocationInfo.Type.getTypeForDescriptor(tokenizer.nextToken());
			int index = Integer.parseInt(tokenizer.nextToken());
			int start = Integer.parseInt(tokenizer.nextToken());
			int end = Integer.parseInt(tokenizer.nextToken());
			LocalMemoryLocation local = new LocalMemoryLocation(name, index, frame, start, end, type); 
			tempListArrayForLocals[index].add(local);
		}
		
		for (int i = 0; i < maxLocalArraySize; i++) {
			localArray[i] = tempListArrayForLocals[i].toArray(new LocalMemoryLocation[0]);
		}
		
		return localArray;
	}
	
	/**
	 * This method gets as input the name of a Java type and returns the 
	 * corresponding Type object that we use internally in our system.
	 * 
	 * The input is the name returned by the Java reflection upon calling
	 * getType().toString() on a Field instance.
	 *  
	 * @param typeName
	 * @return
	 */
	public static Type findTypeFromFullTypeName(String typeName){
		switch(typeName){
		case "byte":
			return Type.BYTE;
		case "char":
			return Type.CHAR;
		case "double":
			return Type.DOUBLE;
		case "float":
			return Type.FLOAT;
		case "int":
			return Type.INT;
		case "long":
			return Type.LONG;
		case "short":
			return Type.SHORT;
		case "boolean":
			return Type.BOOLEAN;
		default:
			return Type.REFERENCE;
		}
	}
	
	/**
	 * 
	 * @param descriptor as returned by getClass().getName()
	 * @return
	 */
	public static Type findArrayElementType(String descriptor){
		if (descriptor.charAt(0) != '[')
			throw new RuntimeException("Not an array type descriptor!");
		
		return Type.getTypeForDescriptor(descriptor.substring(1));
	}
	
	/**
	 * This method is used only for the descriptor of a MULTIANEWARRAY
	 * bytecode.
	 * @param descriptor
	 * @return
	 */
	public static Type findMultiArrayElementType(String descriptor){
		return findArrayElementType(descriptor.substring(descriptor.lastIndexOf("[")));
	}
	
	public static Value[] generateArgValues(Object[] args){
		ThreadCallStack tcs = ThreadCallStack.getCurrentThreadCallStack();
		Type[] argTypes = tcs.getArgTypes();
		Value[] values = new Value[argTypes.length];
		for (int i = 0; i < argTypes.length; i++) {
			switch(argTypes[i]){
			case REFERENCE:
				values[i] = getValueForObject(args[i]);
				break;
			case UNDEFINED:
				values[i] = Value.UNDEFINED;
				break;
			case BOOLEAN:
				values[i] = new Value((boolean)args[i]);
				break;
			case CHAR:
				values[i] = new Value((char)args[i]);
				break;
			case BYTE:
				values[i] = new Value((byte)args[i]);
				break;
			case SHORT:
				values[i] = new Value((short)args[i]);
				break;
			case INT:
				values[i] = new Value((int)args[i]);
				break;
			case FLOAT:
				values[i] = new Value((float)args[i]);
				break;
			case LONG:
				values[i] = new Value((long)args[i]);
				break;
			case DOUBLE:
				values[i] = new Value((double)args[i]);
				break;
			case VOID:
				values[i] = Value.NO_VALUE;
				break;
			case UNAVAILABLE:
				values[i] = Value.NOT_AVAILABLE;
			default:
				throw new IllegalStateException("The type not supported...");
			}
		}
		
		return values;
	}
	
	

	public static Value getValueForObject(Object obj){
		if (obj == null)
			obj = Value.NULL;

		Class klass = obj.getClass();
		if (klass.isArray())
			return new ArrayReferenceValue(HeapSpace.getOrDefineArrayLocationMetaData(obj, Type.UNAVAILABLE));
		
		else if (isLiterallyPresentable(obj))
			return new InstanceReferenceValue(HeapSpace.getOrDefineInstanceLocationMetaData(obj), obj.toString());
		
		else
			return new InstanceReferenceValue(HeapSpace.getOrDefineInstanceLocationMetaData(obj));
	}

	
	public static String generateTemporaryName(String locationName, int tempIndex){
		return "temp_" + locationName + "_" + tempIndex; 
	}

	private static boolean isLiterallyPresentable(Object value){
		if ((value instanceof Integer) ||
				(value instanceof Character) ||
				(value instanceof Float) ||
				(value instanceof Double) ||
				(value instanceof Long) ||
				(value instanceof Boolean) ||
				(value instanceof String)){
			return true;
		}

		return false;
	}
}
