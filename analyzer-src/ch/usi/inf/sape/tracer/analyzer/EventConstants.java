package ch.usi.inf.sape.tracer.analyzer;

public interface EventConstants {
	
	public static final byte ANEWARRAY_EVENT = 0;
	public static final byte ARRAYLENGTH_EVENT = 1;
	public static final byte ATHROW_EVENT = 2;
	public static final byte BASIC_BLOCK_ENTRY_EVENT = 3;
	public static final byte BINARY_EVENT = 4;
	public static final byte BIPUSH_EVENT = 5;
	public static final byte CATCH_BLOCK_EVENT = 6;
	public static final byte CHECKCAST_EVENT = 7;
	public static final byte DIVISION_EVENT = 8;
	public static final byte DUMMY_RETURN_EVENT = 9;
	public static final byte DUP_EVENT = 10;
	public static final byte DUP_X1_EVENT = 11;
	public static final byte DUP_X2_1_EVENT = 12;
	public static final byte DUP_X2_2_EVENT = 13;
	public static final byte DUP2_1_EVENT = 14;
	public static final byte DUP2_2_EVENT = 15;
	public static final byte DUP2_X1_1_EVENT = 16;
	public static final byte DUP2_X1_2_EVENT = 17;
	public static final byte DUP2_X2_1_EVENT = 18;
	public static final byte DUP2_X2_2_EVENT = 19;
	public static final byte DUP2_X2_3_EVENT = 20;
	public static final byte DUP2_X2_4_EVENT = 21;
	public static final byte GETFIELD_EVENT = 22;
	public static final byte GETSTATIC_EVENT = 23;
	public static final byte GOTO_EVENT = 24;
	public static final byte IF_ACMP_EVENT = 25;
	public static final byte IF_ICMP_EVENT = 26;
	public static final byte IF_EVENT = 27;
	public static final byte IINC_EVENT = 28;
	public static final byte INSTANCEOF_EVENT = 29;
	public static final byte INVOKEARGS_EVENT = 30;
	public static final byte INVOKECALLING_EVENT = 31;
	public static final byte INVOKERETURNING_EVENT = 32;
	public static final byte LOOKUPSWITCH_EVENT = 33;
	public static final byte MONITORENTER_EVENT = 34;
	public static final byte MONITOREXIT_EVENT = 35;
	public static final byte MULTIANEWARRAY_EVENT = 36;
	public static final byte NEW_EVENT = 37;
	public static final byte NEWARRAY_EVENT = 38;
	public static final byte PUTFIELD_EVENT = 39;
	public static final byte PUTSTATIC_EVENT = 40;
	public static final byte REM_EVENT = 41;
	public static final byte SIPUSH_EVENT = 42;
	public static final byte SWAP_EVENT = 43;
	public static final byte TABLESWITCH_EVENT = 44;
	public static final byte UNARY_EVENT = 45;
	public static final byte UNTRACED_BC_EVENT = 46;
	public static final byte XALOAD_EVENT = 47;
	public static final byte XASTORE_EVENT = 48;
	public static final byte XCONST_EVENT = 49;
	public static final byte XLDC_EVENT = 50;
	public static final byte XLOAD_EVENT = 51;
	public static final byte XPOP_EVENT = 52;
	public static final byte XRETURN_EVENT = 53;
	public static final byte XSTORE_EVENT = 54;
	public static final byte UNTRACED_INVOKEARGS_EVENT = 55;
	
	
	public static final byte NO_USE_BINDINGS = -1;
	public static final byte NO_DEF_BINDINGS = -2;
	public static final byte UNTRACED_BINDINGS = -3;
	public static final byte UNDEFINED_BINDINGS = -4;
	
}
