package ch.usi.inf.sape.tracer.analyzer;

import ch.usi.inf.sape.tracer.analyzer.locations.ObjectMetaData;

public interface Pointer {

	public ObjectMetaData getReferencedObject();
}
