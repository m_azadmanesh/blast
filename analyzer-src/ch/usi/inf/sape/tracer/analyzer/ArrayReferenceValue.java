package ch.usi.inf.sape.tracer.analyzer;

import ch.usi.inf.sape.tracer.analyzer.locations.ArrayMetaData;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

public class ArrayReferenceValue extends Value implements Pointer{

	private final ArrayMetaData array;

	public ArrayReferenceValue(ArrayMetaData array) {
		super(array.toString(), Type.REFERENCE);
		this.array = array;
	}

	@Override
	public ArrayMetaData getReferencedObject(){
		return array;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}
}
