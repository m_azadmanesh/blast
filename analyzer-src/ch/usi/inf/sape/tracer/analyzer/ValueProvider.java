package ch.usi.inf.sape.tracer.analyzer;

import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent;

/**
 * An instance of this class determines where a particular value (uses of a bytecode operation)
 * came from, i.e., the bytecode operation generating the value and the source index used for it
 * @author reza
 *
 */
public class ValueProvider {
	
	private final BytecodeEvent operation;
	
	private final int useIndex;

	public ValueProvider(BytecodeEvent operation, int useIndex) {
		this.operation = operation;
		this.useIndex = useIndex;
	}

	public ValueProvider(BytecodeEvent operation){
		this(operation, 0);
	}

	/**
	 * @return the operation
	 */
	public BytecodeEvent getOperation() {
		return operation;
	}

	/**
	 * @return the useIndex
	 */
	public int getUseIndex() {
		return useIndex;
	}
	
}
