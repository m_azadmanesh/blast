package ch.usi.inf.sape.tracer.analyzer;

public class VisitorException extends Exception{

	private final Throwable cause;

	public VisitorException(Throwable cause) {
		this.cause = cause;
	}

	@Override
	public synchronized Throwable getCause() {
		return this.cause;
	}
	
}
