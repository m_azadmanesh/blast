package ch.usi.inf.sape.tracer.analyzer;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sape.tracer.analyzer.abstractops.SourceLineOperation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeSessionManager;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.SlicedBytecodeEvent;
import ch.usi.inf.sape.tracer.analyzer.locations.LocalMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation.LocationType;

public class QueryFacade {

	public static boolean isInvokeInstruction(Event op){
		int opCode = op.getOperationCode();
		if ( opCode == OperationCodes.INVOKEINTERFACE ||
				opCode == OperationCodes.INVOKESPECIAL ||
				opCode == OperationCodes.INVOKESTATIC ||
				opCode == OperationCodes.INVOKEVIRTUAL)
			return true;
		
		return false;
	}
	
	
	public static Activation getActivationRecordFor(SlicedBytecodeEvent op, BytecodeSessionManager session){
		return getActivationRecordFor(op.getNonSlicedEvent(), session);
	}
	
	public static Activation getActivationRecordFor(BytecodeEvent op, BytecodeSessionManager session){
		return (Activation) session.findControllerForBytecode(op).getDefs()[0].getMemoryLocation().getContainer();
	}
	
	public static SourceLineOperation getNthOccurenceOfSourceLineNo(int nth, int sourceLineNo, BytecodeSessionManager session){
		List<BytecodeEvent> bcs = session.getFullBytecodeOperationsSession().getOperations();
		List<BytecodeEvent> occurences = new ArrayList<>();
		int index = 0;
		int match = 1;
		while (index < bcs.size()){
			if (bcs.get(index).getSourceCodeLineNumber() != sourceLineNo){
				index ++;
				continue;
			}else if (match == nth){
				do{
					occurences.add(bcs.get(index));
					index++;
				}while(bcs.get(index).getSourceCodeLineNumber() == sourceLineNo || bcs.get(index).getSourceCodeLineNumber() == SourceLineOperation.PSEUDO_VAR_OPERATION_LINE_NO);
				break;
			}else {
				do {
					index ++;
				}while(bcs.get(index).getSourceCodeLineNumber() == sourceLineNo);
				match ++;
			}
		}
		if (occurences.size() == 0){
			throw new IllegalArgumentException("Line number "+ sourceLineNo+ " never occured in this run!");
		}
		
		return new SourceLineOperation(occurences.toArray(new BytecodeEvent[0]), occurences.get(0).getOperationIndex(), sourceLineNo, nth);
	}
	
//	public static String findStackTrace(List<? extends Operation> ops){
//		final List<Activation> frames = new ArrayList<Activation>();
//		final List<Integer> lineNos = new ArrayList<Integer>();
//		
//		for (Operation operation : ops){
//			if (isInvokeInstruction(operation)){
//				frames.add(((INVOKEARGS_Operation)operation.getNonSlicedOperation()).getCalleeFrame());
//				lineNos.add(operation.getSourceCodeLineNumber());
//			}
//		}
//		
//		final StringBuilder builder = new StringBuilder();
//		for (int i = 0; i < frames.size(); i++) {
//			builder.append("\tat ").append(frames.get(i).getOwnerClass().replace('/', '.'))
//							.append(".")
//							.append(frames.get(i).getMethodName())
//							.append(frames.get(i).getDesc())
//							.append("(").append("Line: ").append(lineNos.get(i)).append(")\n");
//		} 
//		
//		return builder.toString();
//	}
	
	public static String stack(Binding<? extends MemoryLocation> l){
		BytecodeEvent bc = l.getProvider();
		Activation container = (Activation)l.getMemoryLocation().getContainer();
		
		final StringBuilder builder = new StringBuilder();
		while(true){
			System.out.println(bc);
			int sourceLineNo = bc.getSourceCodeLineNumber();
			if (container == null || container.getProvider() == null)
				break;
			
			builder.append("\tat ").append(container.getOwnerClass().replace('/', '.'))
			.append(".")
			.append(container.getMethodName())
			.append(container.getDesc())
			.append("(").append("Line: ").append(sourceLineNo).append(")\n");
			
			bc = container.getProvider();
			container = container.getProvider().getCallerFrame();
		}
		return builder.toString();
	}


	public static boolean isReturnInstruction(BytecodeEvent op) {
		// TODO Auto-generated method stub
		return false;
	}


	public static boolean isObjectAllocation(BytecodeEvent op) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	public static EventList<BytecodeEvent> getWriteIntoArrayElementEvents(BytecodeSessionManager session){
		List<BytecodeEvent> writes = new ArrayList<>();
		List<BytecodeEvent> events = session.getFullBytecodeOperationsSession().getOperations();
		for (BytecodeEvent event : events) {
			for (Binding<?> use : event.getDefs()){
				if (use.getMemoryLocation().getLocationType() == LocationType.ARRAY_ELEMENT)
					writes.add(event);
			}
		}
		return new EventList<BytecodeEvent>(writes);
	}
	
}
