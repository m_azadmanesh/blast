package ch.usi.inf.sape.tracer.analyzer.abstractops;

import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.NonSlicedAbstractOperation;
import ch.usi.inf.sape.tracer.analyzer.NonSlicedEvent;
import ch.usi.inf.sape.tracer.analyzer.SlicedEvent;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

public abstract class AbstractOperation extends NonSlicedEvent
									implements NonSlicedAbstractOperation {
	
//	public static final AbstractOperation UNDEFINED_ABSTRACT_OPERATION = new UndefinedAbstractOperation(new BytecodeEvent[0], -1);
	
	protected final BytecodeEvent bytecodes[];
	
//	protected SlicedAbstractOperation slicedVersion;
	
	/**
	 * Each abstract operation is composed of a set of bytecodes. We keep track of the index 
	 * of the bytecode that has the lowest index because we need to use it for finding a 
	 * logical order among the operations. An operation that has a lower minBCIndex should 
	 * precede the ones with higher minBCIndex.
	 */
	protected final int minBCIndex;
	
	private static int AbstractOperationCounter = 0;
	
	protected AbstractOperation(int opCode, BytecodeEvent[] bytecodes, int minBCIndex, int sourceLineNo) {
		super(opCode, sourceLineNo, AbstractOperationCounter ++);
		this.bytecodes = bytecodes;
		this.minBCIndex = minBCIndex;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(this.operationIndex).append(":");
		buffer.append(this.sourceLineNo).append(":");
		buffer.append(getOperationName());
		buffer.append("\t");
		
		int counter = 0;
		if (this.uses.length == 0)
			buffer.append("(No Sources)");

		for (Binding binding : this.uses) {
			buffer.append(binding);
			if (counter != this.uses.length -1)
				buffer.append(",\t");
			counter ++;
		}

		buffer.append("\t-->\t");

		counter = 0;
		if (this.defs.length == 0)
			buffer.append("(No Targets)");
		for (Binding binding : this.defs) {
			buffer.append(binding);
			if (counter != this.defs.length -1)
				buffer.append(",\t");
			counter ++;
		}

		return buffer.toString();
	}

	/**
	 * @return the minBCIndex
	 */
	public int getMinBCIndex() {
		return minBCIndex;
	}

	@Override
	public String getOperationName() {
		return this.bytecodes[bytecodes.length - 1].getSymbolicInterpretation(null);
	}

	public BytecodeEvent[] getBytecodes(){
		return this.bytecodes;
	}
	
	public int getMaxBcIndex(){
		int maxIndex = 0;
		for(BytecodeEvent bytecode: bytecodes){
			if (bytecode.getOperationIndex() > maxIndex)
				maxIndex = bytecode.getOperationIndex();
		}
		
		return maxIndex;
	}

//	@Override
//	public SlicedAbstractOperation getSlicedOperationForSession(int sessionId) {
//		if (slicedVersion == null || slicedVersion.getSessionId() != sessionId){
//			this.slicedVersion = new SlicedAbstractOperation(this, sessionId);
//		}
//		return this.slicedVersion;
//	}
	
}
