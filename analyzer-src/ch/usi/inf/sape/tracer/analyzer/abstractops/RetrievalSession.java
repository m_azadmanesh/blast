package ch.usi.inf.sape.tracer.analyzer.abstractops;

import java.util.List;

import ch.usi.inf.sape.tracer.analyzer.Event;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.SmallContainer;
import ch.usi.inf.sape.tracer.analyzer.locations.Container;

/**
 * A user can ask for either abstraction, de-abstraction, or slicing.
 * Each of these requests are responsed in a retrieval session;
 * 
 * @author reza
 *
 */
public interface RetrievalSession {

	/**
	 * For this session, this method determines the number of operations that reside within the border of
	 * the input container. In order to avoid passing through all the operation of a session and checking
	 * for the start time and end time, we use this optimization that upon passing from the end time of the
	 * container once, we don't check for the remaining operations. 
	 * @param container
	 * @return
	 */
	public int getOperationCountForContainer(Container container);
	
	public int getId();

	/**
	 * @return the operations
	 */
	public List<? extends Event> getOperations();

	
	public int getMaxOperationId();
	
	public int getOperationCount();
	
	/**
	 * The return value is inclusive.
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public int getOperationCountBetweenInclusive(int start, int end);
	
	public int getOperationCountBetweenExclusive(int start, int end);

	public SmallContainer createSmallContainer(Container parent);
}
