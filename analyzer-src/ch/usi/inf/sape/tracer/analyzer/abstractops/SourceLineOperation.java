package ch.usi.inf.sape.tracer.analyzer.abstractops;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import ch.usi.inf.sape.tracer.analyzer.Binding;
import ch.usi.inf.sape.tracer.analyzer.NotImplementedException;
import ch.usi.inf.sape.tracer.analyzer.SlicedEvent;
import ch.usi.inf.sape.tracer.analyzer.TraceElementVisitor;
import ch.usi.inf.sape.tracer.analyzer.VisitorException;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;

public class SourceLineOperation extends AbstractOperation{

	public static final int PSEUDO_VAR_OPERATION_LINE_NO = -2; 
	
	private final int occurence;
	
	public SourceLineOperation(BytecodeEvent[] bytecodes,
			int minBCIndex, int lineNo, int occurence) {
		super(-1, bytecodes, minBCIndex, lineNo);
		this.uses = findUses(bytecodes);
		this.defs = findDefs(bytecodes);
		this.occurence = occurence;
	}

	@Override
	public Binding<? extends MemoryLocation>[] getUsesForDef(
			Binding<? extends MemoryLocation> def) {
		return this.uses;
	}

	@Override
	public String getOperationName() {
		return "SOURCE*"+ occurence;
	}

	public Binding<?>[] findUses(BytecodeEvent[] ops){
		Set<Binding<?>> uses = new HashSet<>();
		for (BytecodeEvent bytecodeOperation : ops) {
			uses.addAll(Arrays.asList(bytecodeOperation.getUses()));
		}
		return uses.toArray(new Binding<?>[0]);
	}
	
	public Binding<?>[] findDefs(BytecodeEvent[] ops){
		Set<Binding<?>> defs = new HashSet<>();
		for (BytecodeEvent bytecodeOperation : ops) {
			defs.addAll(Arrays.asList(bytecodeOperation.getDefs()));
		}
		return defs.toArray(new Binding<?>[0]);
	}

	@Override
	public byte getEventClassCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		throw new NotImplementedException();
	}
	
	@Override
	public SlicedEvent getSlicedOperationForSession(int sessionId) {
		throw new NotImplementedException();
	}


}
