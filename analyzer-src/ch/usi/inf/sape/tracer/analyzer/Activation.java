package ch.usi.inf.sape.tracer.analyzer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeEvent;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.INVOKEARGS_Operation;
import ch.usi.inf.sape.tracer.analyzer.locations.Container;
import ch.usi.inf.sape.tracer.analyzer.locations.LocalMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.ReturnValueMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

/**
 * 
 *  
 * @author reza
 *
 */
public class Activation extends Container {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6989237830829974187L;

	private final String methodName;
	
	private final String desc;
	
	private String ownerClass;
	
	public final static Activation UNTRACED_ACTIVATION = new UnTracedActivation();
	
	/**
	 * if a stackframe is initialized, it means it has been in use and it is not a new stack frame
	 * for the current method. This is required to distinguish between a method that has been called
	 * through an instrumented invoke_* or in some other way such as reflection, class initializer, 
	 * or uninstrumented caller.
	 */
	private boolean initialized;
	private int currentAvailabeSlotIndex;
	private int maxStackHeight;		
	private Binding<LocalMemoryLocation>[] localLastBindings;
	private LocalMemoryLocation[][] localLocations;
	private Stack<Binding<StackSlotMemoryLocation>> stackBindings = new Stack<>();
	private ReturnValueMemoryLocation returnValueLocation;
	private Binding<ReturnValueMemoryLocation> returnValueBinding;
	private final int argsCount;
	private int nextLocalColumnId;    //This is used for visualization purposes when we need to assign a unique column within the a sub-table presenting this frame
	private int offset = -1; 			//This is used for visualization purposes. It indicates the offset of the sub-table representing this activation in the whole table
	private final boolean isStatic; 
	private int nextTemporaryId;
	private List<Integer> pseudoVariables;
	private int invokationId; 	//This is used for control flow dependency. Each method invocation is assigned a unique ID. 
	private final INVOKEARGS_Operation provider;
	protected String sourceFile = "UNTRACED";
	protected String sourceFileWithFullAddress = "UNTRACED";
	
	/**
	 * The earliest time (in terms of bytecode index) which this activation record was alive
	 */
	private int startTime = -1;
	
	/**
	 * The latest time (in terms of bytecode index) which this activation record was still alive and died afterwards
	 */
	private int endTime = -1;
	
	/**
	 * We need to keep a reference to the last invokation frame of this activation, because we already assumed that 
	 * after each activation ends (either regularly through a return or an uncaught exception), that activation 
	 * has to remove its frame. This means that we always expect to see the frame corresponding to the current method
	 * after each invokation and we need to have a handle to the callee method so that we can connect the flow of info
	 * from the callee return value to the caller's operand stack. Calling an uninstrumented exception violates this 
	 * assumption, but using this field, we still can figure out if the frame on top of the stack belongs to our current 
	 * method or to a method invokation whose body was not instrumented. 
	 */
	private Activation lastInvokationFrame;
	

	public Activation(String methodName, String desc, String ownerClass, int argsCount, boolean isStatic, INVOKEARGS_Operation provider) {
		this.methodName = methodName;
		this.desc = desc;
		this.ownerClass = ownerClass;
		this.argsCount = argsCount;
		this.isStatic = isStatic;
		this.provider = provider;
	}
	
	/**
	 * The initialization corresponds to setting out the meta locations for the local variables, stack
	 * slots and the retrun value memory locations. This method is used for intializing the activation 
	 * of an instrumented method where we have all the information
	 * 
	 * @param classMethodDescString
	 * @param maxLocalArraySize
	 * @param localDetails
	 */
	public void initialize(String classMethodDescString, int maxLocalArraySize, String localDetails){
		this.localLocations = Utils.parseAndCreateLocals(maxLocalArraySize, localDetails, this);
		this.returnValueLocation = new ReturnValueMemoryLocation(this);
			
		assert maxLocalArraySize == localLocations.length;
		
		this.localLastBindings =  new Binding[localLocations.length];
		this.initialized = true;
	}
	
	/**
	 * This method is used for initializing the activation of an uninstrumented method. It is called from
	 * the INVOKERETURN_operation and CATCH_BLOCK, when we find out that the activation on top of the stack 
	 * belongs to an uninstrumented method and we still need to convey information from caller to callee and 
	 * back to the caller.
	 *  
	 */
	public void initialize(Type[] types, String classMethodDescString){
		int maxLocalArraySize = 0;
		for (Type type : types) {
			maxLocalArraySize+= type.getCategory();
		}
		
		if (maxLocalArraySize == 0){
			this.localLocations = new LocalMemoryLocation[0][];
			this.returnValueLocation = new ReturnValueMemoryLocation(this);
			return;
		}
		
		this.localLocations = new LocalMemoryLocation[maxLocalArraySize][];
		localLocations[0] = new LocalMemoryLocation[1];
		localLocations[0][0] = new LocalMemoryLocation(LocalMemoryLocation.NAME_NOT_AVAILABLE, 0, this, 0, -1, types[0]);
		int counter = 1;
		for (int i=types[0].getCategory(); counter < types.length; i += types[counter].getCategory(), counter++){
			localLocations[i] = new LocalMemoryLocation[1];
			localLocations[i][0] = new LocalMemoryLocation(LocalMemoryLocation.NAME_NOT_AVAILABLE, i, this, 0, -1, types[counter]);
		}

		this.returnValueLocation = new ReturnValueMemoryLocation(this);

		assert maxLocalArraySize == this.localLocations.length;
		
		this.localLastBindings =  new Binding[localLocations.length];
		this.initialized = true;			//not required, because has no effect in this case
	}

	/**
	 * This method is used for the case when the runtime information is different from the static 
	 * information provided by the instrumentation framework. In particular, upon calling a virtual 
	 * method, the target method could be different at runtime from the target diagnosed at load time.
	 * 
	 * @param ownerClass
	 */
	public void setOwnerClass(String ownerClass) {
		this.ownerClass = ownerClass;
	}

	/**
	 * Checking for being initialized happens in method entry to find out if the current method 
	 * has been invoked through an instrumented invoke_* or in some other way such as reflection,
	 * class initializer, uninstrumented caller)
	 * 
	 * @return the initialized
	 */
	public boolean isInitialized() {
		return initialized;
	}

	/**
	 * Sets a new binding for the local variable at the specified index
	 * 
	 * @param localIndex
	 * @param newBinding
	 */
	private void writeIntoLocal(int localIndex, Binding<LocalMemoryLocation> newBinding){
		this.localLastBindings[localIndex] = newBinding;
	}

	/**
	 * Finds the last binding for the local variable specified by the localIndex. This method
	 * should never return null, because it is supposed that each local variable is defined 
	 * (has a binding) before being used.
	 * 
	 * @param localIndex
	 * @return
	 */
	public Binding<LocalMemoryLocation> readFromLocal(int localIndex){
		return localLastBindings[localIndex];
	}
	

	private LocalMemoryLocation getLocalLocation(int localIndex, int insnIndex){
		LocalMemoryLocation[] locations = localLocations[localIndex];
		
		//This could happen for the case of inner classes where there is a reference to the outer class as a local variable but not listed among the official locals
		if (locations == null){
			LocalMemoryLocation location = new LocalMemoryLocation("", localIndex, this, 0, -1, Type.REFERENCE);
			localLocations[localIndex] = new LocalMemoryLocation[]{location};
			return location;
		}
			
		for (int i = 0; i < locations.length; i++) {
			if (locations[i].isAlive(insnIndex))
				return locations[i];
		}
		
		LocalMemoryLocation[] newLocations = new LocalMemoryLocation[locations.length + 1];
		System.arraycopy(locations, 0, newLocations, 0, locations.length);
		LocalMemoryLocation newLocation = new LocalMemoryLocation("_", localIndex, this, insnIndex, -1, Type.UNDEFINED);
		newLocations[locations.length] = newLocation;
		localLocations [localIndex] = newLocations;
		return newLocation;
	}
	
	public Type[] getArgTypes(){
		Type[] argTypes = new Type[argsCount];
		int localIndex = 0;
		for (int i = 0; i < argsCount; i++) {
			argTypes[i] = getLocalLocation(i, 0).getType();
			localIndex = getNextLocalIndex(localIndex);
		}
		return argTypes;
	}
	
	/**
	 * Writing into a local variable at bytecode-level means adding a new binding for either a currently
	 * defined local variable or a completely new local variable. In the former case, the new binding
	 * could also introduce a new local variable whose index overlaps with an already defined but died local var.
	 * We first check if there is already a binding for the given local variable and if the binding has the given
	 * type. If anyone of these two condition doesn't hold, we create a new location object and invalidate the 
	 * former one by overwriting it, otherwise we use the currently existing location. Finally, we change the 
	 * binding of the location to this new definition and overwrite this binding into the local variable array 
	 * of this frame.
	 * 
	 * Distinguishing between an already defined local variable from a local variable whose index is re-used 
	 * is not always possible. Consider the case when a local variable inside a block has the same type as 
	 * the local variable that is coming next out of this block with the same type. Even using the names for 
	 * comparison doesn't work because we can use a name that is already used within a block for defining another
	 * local variable outside after the block. This causes issues in counting the number of memory locations used
	 * but it doesn't have any effect on finding backward slices.

	 * @param localIndex
	 * @param type
	 * @param insnIndex
	 * @param value
	 * @return
	 */
	public Binding<LocalMemoryLocation> writeIntoLocal(int localIndex, int insnIndex, Value value, BytecodeEvent provider){
		LocalMemoryLocation localLocation = getLocalLocation(localIndex, insnIndex);
		Binding<LocalMemoryLocation> newBinding = new Binding<>(localLocation, value, provider);
		writeIntoLocal(localIndex, newBinding);
		return newBinding;
	}
	
	public int getNextLocalIndex(int currLocalIndex){
		while (++currLocalIndex < this.localLocations.length && 
				(this.localLocations[currLocalIndex] == null || this.localLocations[currLocalIndex].length == 0));
	
		return currLocalIndex;
	}	
	
	/**
	 * Pushes a new stack slot location and assigns a binding to that location.
	 *  
	 * Depending on if the value is a category 1 or category 2, the slot index is incremented 
	 * by 1 or 2 respectively.
	 */
	public Binding<StackSlotMemoryLocation> pushOnStack(Value value, Type type, BytecodeEvent provider){
		StackSlotMemoryLocation stackSlot = new StackSlotMemoryLocation(currentAvailabeSlotIndex, this, type);
		Binding<StackSlotMemoryLocation> binding = new Binding<>(stackSlot, value, provider);
		stackBindings.push(binding);
		currentAvailabeSlotIndex += type.getCategory();
		
		if (maxStackHeight < currentAvailabeSlotIndex)
			maxStackHeight = currentAvailabeSlotIndex;
		
		return binding;
	}

	/**
	 * Pop off the last binding for the top-most stack slot in this activation and return it.
	 * The corresponding memory location to this binding is also removed in turn.
	 * 
	 * Depending on if the value is a category 1 or category 2, the slot index is decremented 
	 * by 1 or 2 respectively.
	 */
	public Binding<StackSlotMemoryLocation> popStackSlotBinding(){
		Binding<StackSlotMemoryLocation> binding = stackBindings.pop();
		Binding[] uses = binding.getProvider().getUsesForDef(binding);
		if (uses != null)
			for (Binding use : uses) {
				use.removeCopyFromStack();
			}
		
		currentAvailabeSlotIndex -= binding.getMemoryLocation().getType().getCategory();
		return binding;
	}
	
	public Binding<StackSlotMemoryLocation> popStackSlotBindingNoSideEffect(){
		Binding<StackSlotMemoryLocation> binding = stackBindings.pop();
		
		currentAvailabeSlotIndex -= binding.getMemoryLocation().getType().getCategory();
		return binding;

	}
	
	public Binding<StackSlotMemoryLocation>[] getStackSlotBindings(int parametersCount){
		Binding<StackSlotMemoryLocation>[] slots = new Binding[parametersCount];
		for (int i = 0; i < slots.length; i++) {
			slots[i] = stackBindings.elementAt(stackBindings.size() - parametersCount + i);
		}
		
		return slots;
	}
	
	public Binding<ReturnValueMemoryLocation> useReturnValue(){
		return this.returnValueBinding;
	}
	
	public Binding<ReturnValueMemoryLocation> defReturnValue(Value value, BytecodeEvent provider){
		Binding<ReturnValueMemoryLocation> binding = new Binding<>(this.returnValueLocation, value, provider);
		this.returnValueBinding = binding;
		return binding;
	}
	
	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @return the ownerClass
	 */
	public String getOwnerClass() {
		return ownerClass;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Upon catching an exception, all the values are popped off the stack and the exception object is the only value that
	 * is pushed on the stack. This method is used for that purpose.
	 * 
	 */
	public void clear()
	{
		this.stackBindings.clear();
		this.currentAvailabeSlotIndex = 0;
	}
	
	/**
	 * @return the lastInvokationFrame
	 */
	public Activation getLastInvokationFrame() {
		return lastInvokationFrame;
	}

	/**
	 * @param lastInvokationFrame the lastInvokationFrame to set
	 */
	public void setLastInvokationFrame(Activation lastInvokationFrame) {
		this.lastInvokationFrame = lastInvokationFrame;
	}


	/**
	 * @return the argsCount
	 */
	public int getArgsCount() {
		return argsCount;
	}

	@Override
	public String toString(){
		return this.ownerClass + "." + this.methodName + this.desc;
	}

	public String toStringVerbose(){
		if (!initialized)
			return "Trying to print details for the activation:\t"+ this.ownerClass + "." + this.methodName + this.desc +", though not initialized yet!";
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("Activation for :\t").append(this.ownerClass).append(".").append(this.methodName).append(this.desc).append(",\tLocals array size:\t").append(this.localLocations.length).append(",\tLocal names:\t");
		for (int i = 0; i < this.localLocations.length; i++) {
			if (this.localLocations[i] != null){
				for(int j = 0; j < this.localLocations[i].length; j++){
					buffer.append("Local[").append(i).append("]:\t").append(localLocations[i][j]).append("\n");
				}
			}
		}
		
		buffer.append("\nStack:\n");
		for (Binding<StackSlotMemoryLocation> stackSlot : stackBindings){
			buffer.append(stackSlot).append("\n");
		}
		buffer.append("***********End of Activation Details***********");
		return buffer.toString();
	}
	
	public boolean isStatic(){
		return isStatic;
	}
	
	public String getMethodSignature(){	
		StringBuffer buffer = new StringBuffer();
		int start = 0;
		if (!isStatic()){
			buffer.append(getLocalLocation(0, -1).getName()).append(".");
			start = 1;
		}
		buffer.append(getMethodName()).append("(");
		for (int i = start; i<argsCount; i++){
			LocalMemoryLocation local = getLocalLocation(i, -1);
			buffer.append(local.getType().getName()).append(" ").append(local.getName());
			if (i < argsCount -1)
				buffer.append(", ");
		}
		buffer.append(")");
		return buffer.toString();
	}


	/**
	 * @return the nextTemporaryId
	 */
	public int getNextTemporaryId() {
		return nextTemporaryId++;
	}
	
	public StackSnapshot takeSnapshot(){
		return StackSnapshot.createSnapshot(this.stackBindings.toArray(new Binding[0]));
	}
	
	public int getAndIncrementNextLocalColumnId(){
		return nextLocalColumnId ++;
	}

	/**
	 * The maximum size of the stack while this activation was alive
	 * @return
	 */
	public int getMaxStackHeight(){
		return maxStackHeight;
	}
	
	/**
	 * Note: this method should be called after the execution of this activation is over, because
	 * during the execution some local variables might be added to the array of locals which were
	 * not intialliy found after reading the class file.
	 * 	 
	 * @return
	 */
	public int getLocalCount(){
		int localCount = 0;
		for (int i = 0; i < localLocations.length; i++) {
			if (localLocations[i] != null){
				for (LocalMemoryLocation local : localLocations[i]) {
					localCount ++;
				}
			}
		}
		return localCount;
	}
	
	/**
	 * 
	 * @return the local memory locations ordered as they are used in the program
	 */
	public List<LocalMemoryLocation> getLocals(){
		List<LocalMemoryLocation> localList = new ArrayList<>();
		for (int i = 0; i < localLocations.length; i++) {
			if (localLocations[i] != null){
				for (LocalMemoryLocation local : localLocations[i]) {
					localList.add(local);
				}
			}
		}
		
		Collections.sort(localList, new Comparator<LocalMemoryLocation>() {

			@Override
			public int compare(LocalMemoryLocation l1, LocalMemoryLocation l2) {
				if (l1.getOffset() < l2.getOffset())
					return -1;
				if (l1.getOffset() > l2.getOffset())
					return 1;
				
				return 0;
			}
		});
		
		return localList;
	}
	
	
	/**
	 * activation size indicates the number of columns required for presenting an activation in a tabular form 
	 * @return
	 */
	public int getSize(){
		return getMaxStackHeight() + getLocalCount() + (returnValueBinding != null ? 1 : 0); 
	}
	
	public int getOffset(){
		return this.offset;
	}
	
	public void setOffset(final int offset){
		this.offset = offset;
	}
	
	public boolean hasReturnValue(){
		return returnValueBinding != null;
	}

	@Override
	public int getElementCount() {
		return getSize();
	}

	@Override
	public List<? extends MemoryLocation> getElements() {
		return null;
	}

	/**
	 * The earliest time (in terms of bytecode index) which this activation record was alive
	 * 
	 * @param startTime the startTime to set
	 */
	public void updateStartTime(int startTime) {
		if (this.startTime < 0)
			this.startTime = startTime;
	}

	/**
	 * The latest time (in terms of bytecode index) which this activation record was still alive and died afterwards
	 * 
	 * @param endTime the endTime to set
	 */
	public void updateEndTime(int endTime) {
		if (this.endTime < endTime)
			this.endTime = endTime;
	}

	@Override
	public int getStartTime() {
		return startTime;
	}

	@Override
	public int getEndTime() {
		return endTime;
	}
	
	public void addPseudoVariable(int psedoVariable){
		this.pseudoVariables.add(psedoVariable);
	}

	public int getPseudoVariableIndex(int pseudoVariable){
		return this.pseudoVariables.indexOf(pseudoVariables);
	}

	/**
	 * @return the invokationId
	 */
	public int getInvokationId() {
		return invokationId;
	}

	/**
	 * @param invokationId the invokationId to set
	 */
	public void setInvokationId(int invokationId) {
		this.invokationId = invokationId;
	}

	/**
	 * @return the provider
	 */
	public INVOKEARGS_Operation getProvider() {
		return provider;
	}
	
	public static class UnTracedActivation extends Activation{

		public UnTracedActivation() {
			super("UNTRACED", "UNTRACED", "UNTRACED", 0, false, null);
			this.sourceFile = "UNTRACED";
			this.sourceFileWithFullAddress = "UNTRACED";
		}

		@Override
		public INVOKEARGS_Operation getProvider() {
			return INVOKEARGS_Operation.UNTRACED_INVOKEARGS_OPERATION;
		}
		
		@Override
		public void accept(TraceElementVisitor visitor) throws VisitorException {
			visitor.visit(this);
		}

	}

	/**
	 * @return the name of the source File to which the method represented
	 * by this activation record was defined
	 */
	public String getSourceFile() {
		return sourceFile;
	}

	/**
	 * @param sourceFile the sourceFile to set
	 */
	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}
	
	/**
	 * Each bytecode is contained in a class file. Several class files can
	 * belong to a single source file. This method returns the name of the
	 * source file to which this event was stemmed from. 
	 * 
	 * The format of the name is as follows:
	 * Full address of the package + / + <file name>.java
	 * 
	 * E.g.:
	 * java/lang/Object.java
	 */
	public String getSourceFileWithFullAdress(){
		String pckg = getOwnerClass().substring(0, getOwnerClass().lastIndexOf('/') + 1);
		return pckg + getSourceFile();
	}
	
	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}

}