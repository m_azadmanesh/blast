package ch.usi.inf.sape.tracer.analyzer;

import ch.usi.inf.sape.tracer.analyzer.locations.InstanceMetaData;
import ch.usi.inf.sape.tracer.analyzer.locations.MemoryLocationInfo.Type;

public class InstanceReferenceValue extends Value implements Pointer{

	private final InstanceMetaData instance;

	public InstanceReferenceValue(InstanceMetaData instance) {
		super(instance.toString(), Type.REFERENCE);
		this.instance = instance;
	}

	/**
	 * Used for values whose string representation is meaningful
	 * such as instances of Integer, Float, String, ...
	 * 
	 * @param metaData
	 * @param stringValue
	 */
	public InstanceReferenceValue(InstanceMetaData metaData, String stringValue){
		super(stringValue, Type.REFERENCE);
		this.instance = metaData;
	}
	
	@Override
	public InstanceMetaData getReferencedObject(){
		return instance;
	}

	@Override
	public void accept(TraceElementVisitor visitor) throws VisitorException {
		visitor.visit(this);
	}
	
}
