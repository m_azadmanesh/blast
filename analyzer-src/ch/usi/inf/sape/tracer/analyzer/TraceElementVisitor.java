package ch.usi.inf.sape.tracer.analyzer;

import ch.usi.inf.sape.tracer.analyzer.Activation.UnTracedActivation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.ANEWARRAY_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.ARRAYLENGTH_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.ATHROW_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BIPUSH_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BasicBlockEntry_PseudoOperation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.Binary_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.CHECKCAST_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.CatchBlock_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUMMY_RETURN_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_1_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_2_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_X1_1_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_X1_2_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_X2_1_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_X2_2_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_X2_3_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP2_X2_4_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP_X1_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP_X2_1_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.DUP_X2_2_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.Division_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.GETFIELD_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.GETSTATIC_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.GOTO_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.IF_ACMP_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.IF_ICMP_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.IF_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.IINC_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.INSTANCEOF_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.INVOKEARGS_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.INVOKEARGS_Operation.UnTracedInvokeArgs_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.INVOKECALLING_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.INVOKERETURNING_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.LOOKUPSWITCH_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.MONITORENTER_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.MONITOREXIT_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.MULTIANEWARRAY_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.NEWARRAY_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.NEW_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.PUTFIELD_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.PUTSTATIC_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.Rem_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.SIPUSH_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.SWAP_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.TABLESWITCH_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.Unary_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.UntracedBytecode_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XALOAD_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XASTORE_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XCONST_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XLDC_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XLOAD_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XPOP_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XRETURN_Operation;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.XSTORE_Operation;
import ch.usi.inf.sape.tracer.analyzer.locations.ArrayElementMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.ArrayLengthMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.ArrayMetaData;
import ch.usi.inf.sape.tracer.analyzer.locations.ClassFieldMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.ClassMetaData;
import ch.usi.inf.sape.tracer.analyzer.locations.InstanceFieldMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.InstanceMetaData;
import ch.usi.inf.sape.tracer.analyzer.locations.LocalMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.PseudoVariableMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.ReturnValueMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.StackSlotMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.TemporaryMemoryLocation;
import ch.usi.inf.sape.tracer.analyzer.locations.UndefinedMemoryLocation;

public interface TraceElementVisitor {

	//Events
	public Object visit(ANEWARRAY_Operation anewarray_Operation) throws VisitorException;

	public Object visit(ARRAYLENGTH_Operation arraylength_Operation) throws VisitorException;

	public Object visit(ATHROW_Operation athrow_Operation) throws VisitorException;

	public Object visit(BasicBlockEntry_PseudoOperation basicBlockEntry_PseudoOperation) throws VisitorException;

	public Object visit(Binary_Operation binary_Operation) throws VisitorException;
	
	public Object visit(Division_Operation division_Operation) throws VisitorException;
	
	public Object visit(Rem_Operation rem_Operation) throws VisitorException;
	
	public Object visit(BIPUSH_Operation bipush_Operation) throws VisitorException;

	public Object visit(CatchBlock_Operation catchBlock_Operation) throws VisitorException;

	public Object visit(CHECKCAST_Operation checkcast_Operation) throws VisitorException;

	public Object visit(DUMMY_RETURN_Operation dummy_RETURN_Operation) throws VisitorException;

	public Object visit(DUP_Operation dup_Operation) throws VisitorException;

	public Object visit(DUP_X1_Operation dup_X1_Operation) throws VisitorException;

	public Object visit(DUP_X2_1_Operation dup_X2_1_Operation) throws VisitorException;

	public Object visit(DUP_X2_2_Operation dup_X2_2_Operation) throws VisitorException;

	public Object visit(DUP2_1_Operation dup2_1_Operation) throws VisitorException;

	public Object visit(DUP2_2_Operation dup2_2_Operation) throws VisitorException;

	public Object visit(DUP2_X1_1_Operation dup2_X1_1_Operation) throws VisitorException;

	public Object visit(DUP2_X1_2_Operation dup2_X1_2_Operation) throws VisitorException;

	public Object visit(DUP2_X2_1_Operation dup2_X2_1_Operation) throws VisitorException;

	public Object visit(DUP2_X2_2_Operation dup2_X2_2_Operation) throws VisitorException;

	public Object visit(DUP2_X2_3_Operation dup2_X2_3_Operation) throws VisitorException;

	public Object visit(DUP2_X2_4_Operation dup2_X2_4_Operation) throws VisitorException;

	public Object visit(GETFIELD_Operation getfield_Operation) throws VisitorException;

	public Object visit(GETSTATIC_Operation getstatic_Operation) throws VisitorException;

	public Object visit(GOTO_Operation goto_Operation) throws VisitorException;

	public Object visit(IF_ACMP_Operation if_ACMP_Operation) throws VisitorException;

	public Object visit(IF_ICMP_Operation if_ICMP_Operation) throws VisitorException;

	public Object visit(IF_Operation if_Operation) throws VisitorException;

	public Object visit(IINC_Operation iinc_Operation) throws VisitorException;

	public Object visit(INSTANCEOF_Operation instanceof_Operation) throws VisitorException;

	public Object visit(INVOKEARGS_Operation o) throws VisitorException;
	
	public Object visit(UnTracedInvokeArgs_Operation o) throws VisitorException;
	
	public Object visit(INVOKECALLING_Operation invokecalling_Operation) throws VisitorException;

	public Object visit(INVOKERETURNING_Operation invokereturning_Operation) throws VisitorException;

	public Object visit(LOOKUPSWITCH_Operation lookupswitch_Operation) throws VisitorException;

	public Object visit(MONITORENTER_Operation monitorenter_Operation) throws VisitorException;

	public Object visit(MONITOREXIT_Operation monitorexit_Operation) throws VisitorException;

	public Object visit(MULTIANEWARRAY_Operation multianewarray_Operation) throws VisitorException;

	public Object visit(NEW_Operation new_Operation) throws VisitorException;

	public Object visit(NEWARRAY_Operation newarray_Operation) throws VisitorException;

	public Object visit(PUTFIELD_Operation putfield_Operation) throws VisitorException;

	public Object visit(PUTSTATIC_Operation putstatic_Operation) throws VisitorException;

	public Object visit(SIPUSH_Operation sipush_Operation) throws VisitorException;

	public Object visit(SWAP_Operation swap_Operation) throws VisitorException;

	public Object visit(TABLESWITCH_Operation tableswitch_Operation) throws VisitorException;

	public Object visit(Unary_Operation unary_Operation) throws VisitorException;

	public Object visit(UntracedBytecode_Operation untracedBytecode_Operation) throws VisitorException;

	public Object visit(XALOAD_Operation xaload_Operation) throws VisitorException;

	public Object visit(XASTORE_Operation xastore_Operation) throws VisitorException;

	public Object visit(XCONST_Operation xconst_Operation) throws VisitorException;

	public Object visit(XLDC_Operation xldc_Operation) throws VisitorException;

	public Object visit(XLOAD_Operation xload_Operation) throws VisitorException;

	public Object visit(XPOP_Operation xpop_Operation) throws VisitorException;

	public Object visit(XRETURN_Operation xreturn_Operation) throws VisitorException;

	public Object visit(XSTORE_Operation xstore_Operation) throws VisitorException;
	
	
	//Memory locations
	public Object visit(ArrayElementMemoryLocation arrayElementMemoryLocation) throws VisitorException;

	public Object visit(ArrayLengthMemoryLocation arrayLengthMemoryLocation) throws VisitorException;

	public Object visit(ClassFieldMemoryLocation classFieldMemoryLocation) throws VisitorException;

	public Object visit(InstanceFieldMemoryLocation instanceFieldMemoryLocation) throws VisitorException;

	public Object visit(LocalMemoryLocation localMemoryLocation) throws VisitorException;

	public Object visit(UndefinedMemoryLocation undefinedMemoryLocation) throws VisitorException;

	public Object visit(PseudoVariableMemoryLocation pseudoVariableMemoryLocation) throws VisitorException;

	public Object visit(ReturnValueMemoryLocation returnValueMemoryLocation) throws VisitorException;

	public Object visit(StackSlotMemoryLocation stackSlotMemoryLocation) throws VisitorException;

	public Object visit(TemporaryMemoryLocation temporaryMemoryLocation) throws VisitorException;


	//Containers
	public Object visit(Activation a) throws VisitorException;
	
	public Object visit(UnTracedActivation a) throws VisitorException;
	
	public Object visit(ArrayMetaData arrayMetaData) throws VisitorException;
	
	public Object visit(ClassMetaData classMetaData) throws VisitorException;
	
	public Object visit(InstanceMetaData instanceMetaData) throws VisitorException;
	
	public Object visit(ThreadCallStack threadCallStack) throws VisitorException;
	

	//Value
	public Object visit(Value v) throws VisitorException;
	
	public Object visit(ArrayReferenceValue av) throws VisitorException;
	
	public Object visit(InstanceReferenceValue iv) throws VisitorException;

	
	//Binding
	public Object visit(Binding<?> b) throws VisitorException;
	
}
