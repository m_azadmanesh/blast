
public class InvokationTest {

	public static void run(){
		instrumentedInvokeInstrumented();
		instrumentedInvokeUninstrumented();
		uninstrumentedInvokeInstrumented();
		uninstrumentedInvokeUninstrumented();
	}
	
	public static int instrumentedInvokeInstrumented(){
		GeneralClassTest instrumentableClass = new InstrumentableClassTest();
		return (int)instrumentableClass.invokeRecursiveVirtualMethod(2, null, 0);
	}
	
	public static int instrumentedInvokeUninstrumented(){
		GeneralClassTest instrumentedClass = new InstrumentableClassTest();
		GeneralClassTest uninstrumentedClass = new UninstrumentableClassTest();
		return (int)instrumentedClass.invokeRecursiveVirtualMethod(2, uninstrumentedClass, 2);
	}
	
	public static int uninstrumentedInvokeInstrumented(){
		GeneralClassTest uninstrumentedClass = new UninstrumentableClassTest();
		GeneralClassTest instrumentedClass = new InstrumentableClassTest();
		return (int)uninstrumentedClass.invokeRecursiveVirtualMethod(2, instrumentedClass, 2);
	}
	
	public static int uninstrumentedInvokeUninstrumented(){
		GeneralClassTest uninstrumentedClass = new UninstrumentableClassTest();
		return (int)uninstrumentedClass.invokeRecursiveVirtualMethod(2, null, 0);
	}
}
