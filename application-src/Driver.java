import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import ch.usi.inf.sape.tracer.agent.UninstrumentableClassLoader;
import ch.usi.inf.sape.tracer.analyzer.BytecodeAnalyzer;
import ch.usi.inf.sape.tracer.analyzer.bytecodeops.BytecodeSessionManager;


public class Driver {

	public static void main(String[] args) {
		if (args.length == 0){
			System.out.println("Usage: java Driver -Dapp=<appClass> -Danalyzer=<analyzer class>");
			return;
		}
		
		String appClassName = "";
		String analyzerClassName = "";
		for(String arg:args){
			if (arg.startsWith("-Dapp=")){
				appClassName = arg.substring(6);
			} else if (arg.startsWith("-Danalyzer=")){
				analyzerClassName = arg.substring(11);
			}
		}
		
		try {
			Class<?> appClass = Class.forName(appClassName);
			Method mainMethod = appClass.getMethod("main", new Class[]{String[].class});
			mainMethod.invoke(null, new Object[]{new String[0]});
		}catch(Exception e){
			e.printStackTrace();
		}
			
		try{	
			BytecodeSessionManager session = BytecodeSessionManager.getInstance();
			URLClassLoader cl = new UninstrumentableClassLoader(((URLClassLoader)Thread.currentThread().getContextClassLoader()).getURLs(), null);
			Class<?> analyzerClass = cl.loadClass(analyzerClassName);
			Method analyzerMethod = analyzerClass.getMethod("analyze", new Class[]{BytecodeSessionManager.class});
			BytecodeAnalyzer bytecodeAnalyzer = (BytecodeAnalyzer) analyzerClass.newInstance();
			analyzerMethod.invoke(bytecodeAnalyzer, session);
			cl.close();
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException | IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
