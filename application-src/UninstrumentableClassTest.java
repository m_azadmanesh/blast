import java.io.IOException;


public class UninstrumentableClassTest extends GeneralClassTest{
	
	private GeneralClassTest delegate;
	
	public UninstrumentableClassTest(GeneralClassTest delegate){
		this.delegate = delegate;
	}
	
	public UninstrumentableClassTest() {
	}
	
	public int invokeExceptionThrowingMethod() throws IOException{
		if (delegate == null){
			int ret = Math.abs(56);
			float floatVal = throwException();
			return ret / (int) floatVal;
		}else {
			return delegate.invokeExceptionThrowingMethod();
		}
	}
	
	public double invokeRuntimeExceptionThrowingMethod(){
		double ret = Math.sin(2);
		return ret / throwRuntimeException();
	}
	
	public double catchException(GeneralClassTest klass){
		double doubleValBeforeException = Math.cos(1.5);
		int intValBeforeException = Math.max(34, 56);
		try{
			klass.invokeExceptionThrowingMethod();
		}catch(IOException e){
			e.printStackTrace();
		}
		double doubleValAfterException = doubleValBeforeException / intValBeforeException;
		return doubleValAfterException;
	}
	
	private float throwException() throws IOException{
		IOException e = new IOException("A dummy Exception thrown for testing purposes!");
		throw e;
	}
	
	@SuppressWarnings("null")
	private double throwRuntimeException(){
		String nullString = null;
		return nullString.length();
	}
	
	@Override
	public long invokeRecursiveVirtualMethod(int depth1, GeneralClassTest klass2, int depth2) {
		char id = 1;
		if (depth1 > 0){
			return invokeRecursiveVirtualMethod(depth1 - 1, klass2, depth2) + id;
		}else if (depth2 > 0){
			return klass2.invokeRecursiveVirtualMethod(depth1, klass2, depth2-1) + id;
		}else
			return id;
	}
	
}
