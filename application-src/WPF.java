/**
 * Code taken from "Why Programs Fail", Chapter 9 "Tracking Origins", Example 9.1 "Static and Dynamic Slices"
 */
public class WPF {

	public static void run() {
		int n = read(); // n: LV#0
		int a = read(); // a: LV#1
		int x = 1;      // x: LV#2
		int b = a + x;  // b: LV#3
		a = a + 1;
		int i = 1;      // i: LV#4
		int s = 0;      // s: LV#5
		while (i<=n) {
			if (b>0) {
				if (a>1) {
					x = 2;
				}
			}
			s = s + x;
			i = i + 1;
		}
		write(s);
	}
	
	private static int[] inputs = new int[] {2, 0};
	private static int inputIndex = 0;
	private static int read() {
		return inputs[inputIndex++];
	}
	
	public static void write(int value) {
		System.out.println(value);
	}
	
}
