public class StaticTest {

	private static boolean booleanField = true;
	private static byte byteField = 2;
	private static char charField = 'A';
	private static double doubleField = 3.14;
	private static float floatField = 0.2f;
	private static int intField = 10;
	private static long longField = 100;
	private static short shortField = 19;
	private static Object objectField = "hello";
	private static int[] arrayField = new int[3];
	
	public static void run() {
		boolean booleanLocal = booleanField;
		byte byteLocal = byteField;
		char charLocal = charField;
		double doubleLocal = doubleField;
		float floatLocal = floatField;
		int intLocal = intField;
		long longLocal = longField;
		short shortLocal = shortField;
		Object objectLocal = objectField;
		int[] arrayLocal = arrayField;
		
		booleanField = booleanLocal;
		byteField = byteLocal;
		charField = charLocal;
		doubleField = doubleLocal;
		floatField = floatLocal;
		intField = intLocal;
		longField = longLocal;
		shortField = shortLocal;
		objectField = objectLocal;
		arrayField = arrayLocal;
	}

}
