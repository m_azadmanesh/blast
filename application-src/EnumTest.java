
public class EnumTest {

	public EnumTest(MyEnum tm){
		switch (tm){
		case SINGLE:
			System.out.println("single");
			break;
		case FIXED:
			System.out.println("fixed");
			break;
			
		}
	}
	public enum MyEnum {
	    SINGLE("single threaded"), FIXED("fixed threads"), PER_CPU("scaled to available CPUs");

	    private String description;

	    private MyEnum(String description) {
	      this.description = description;
	    }

	    public String describe() {
	      return description;
	    }
	  };
}
