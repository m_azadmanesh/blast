import static org.junit.Assert.*;

import org.junit.Test;


public class ObjectTest {

	private boolean booleanField;
	private byte byteField;
	private char charField;
	private double doubleField;
	private float floatField;
	private int intField = 5;
	private long longField;
	private short shortField;
	private Object objectField;
	private int[] arrayField;
	private static int staticIntField = 5;
	private static double staticDoubleField;
	
	public ObjectTest() {	
	}
	
	@Test
	public static void run() {
		ObjectTest o = new ObjectTest();
		o.invokeVirtual(2.0, o);
		
		new Object();
		o.booleanField = true;
		boolean booleanLocal = o.booleanField;
		o.byteField = 3;
		byte byteLocal = o.byteField;
		o.charField = 'A';
		char charLocal = o.charField;
		o.doubleField = 3d;
		double doubleLocal = o.doubleField;
		o.intField = (int)doubleLocal + o.byteField + staticIntField;
		o.floatField = 3f;
		float floatLocal = o.floatField;
		o.intField = 3;
		int intLocal = o.intField;
		o.longField = 3l;
		long longLocal = o.longField;
		o.shortField = 3;
		short shortLocal = o.shortField;
		o.objectField = new Object();
		Object objectLocal = o.objectField;
		o.arrayField = new int[10];
		o.arrayField [3] = 5;
		o.arrayField [4] = o.arrayField[4] + o.arrayField.length+o.intField;
		assertNotEquals(1,o.arrayField[4]);
		int intLocal2 = 5;
		intLocal = intLocal2 = ++intLocal;
		
		int[] arrayLocal = o.arrayField;
		if (o != null && o.arrayField.length > 4)
			o = new ObjectTest();  
		assertNotEquals(null, o);
		Object o2 = make();
		take(o2);
	}
	
	private static Object make() {
		return "oo";
	}
	
	private static void take(Object o) {
		Object l = o;
	}
	
	public int invokeVirtual(double arg, Object o){
//		double local = arg;
//		Object localObj = o;
//		this.doubleField = arg;
		return 1;
	}

}
