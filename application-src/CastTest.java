public class CastTest {

	public static void run() {
		int i = 300;
		
		long l = i; // I2L
		float f = i; // I2F
		double d = i; // I2D
		
		int i2 = (int)l; // L2I
		float f2 = (float)l; // L2F
		double d2 = (double)l; // L2D
		
		int i3 = (int)f; // F2I
		long l2 = (long)f; // F2L
		double d3 = (double)f; // F2D

		int i4 = (int)d; // D2I
		long l3 = (long)d; // D2L
		float f3 = (float)d; // D2F
		
		byte b1 = (byte)i; // I2B
		char c1 = (char)i; // I2C
		short s1 = (short)i; // I2S

		Object o = "Hello";
		String s = (String)o; // CHECKCAST
		
		boolean result1 = o instanceof String;
		boolean result2 = o instanceof Object;
		boolean result3 = o instanceof Integer;
	}
	
}
