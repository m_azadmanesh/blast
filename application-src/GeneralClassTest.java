import java.io.IOException;


public abstract class GeneralClassTest {

	public abstract int invokeExceptionThrowingMethod() throws IOException;
	
	public abstract double invokeRuntimeExceptionThrowingMethod();
	
	public abstract double catchException(GeneralClassTest klass);
	
	public abstract long invokeRecursiveVirtualMethod(int depth1, GeneralClassTest klass2, int depth2);

}
