
public class ExceptionTest {
	
	
	/**
	 * Runs tests for all possible scenarios (excluding RuntimeException throwing scenarios)
	 * 
	 */
	public static void run(){
		thrownInInstrumentedCaughtInInstrumented();
		thrownInInstrumentedCaughtInUninstrumented();
		thrownInUninstrumentedCaughtinInstrumented();
		thrownInUninstrumentedCaughtInUninstrumented();
	}
	
	public static void thrownInInstrumentedCaughtInInstrumented(){
		GeneralClassTest instrumented = new InstrumentableClassTest();
		instrumented.catchException(instrumented);
	}
	
	public static void thrownInInstrumentedCaughtInUninstrumented(){
		GeneralClassTest instrumented = new InstrumentableClassTest();
		GeneralClassTest unInstrumented = new UninstrumentableClassTest();
		unInstrumented.catchException(instrumented);
	}

	public static void thrownInInstrumentedNotCaught(){
		GeneralClassTest instrumented = new InstrumentableClassTest();
		instrumented.invokeRuntimeExceptionThrowingMethod();
	}
	
	public static void thrownInUninstrumentedCaughtinInstrumented(){
		GeneralClassTest uninstrumented = new UninstrumentableClassTest();
		GeneralClassTest instrumented = new InstrumentableClassTest();
		instrumented.catchException(uninstrumented);
	}
	
	public static void thrownInUninstrumentedCaughtInUninstrumented(){
		GeneralClassTest uninstrumented = new UninstrumentableClassTest();
		uninstrumented.catchException(uninstrumented);
	}
	
	public static void thrownInUninstrumentedNotCaught(){
		GeneralClassTest uninstrumented = new UninstrumentableClassTest();
		uninstrumented.invokeRuntimeExceptionThrowingMethod();
	}
	
	public static void tIcIinterleavedByU(){
		InstrumentableClassTest i1 = new InstrumentableClassTest();
		UninstrumentableClassTest u = new UninstrumentableClassTest(i1);
		InstrumentableClassTest i2 = new InstrumentableClassTest(u);
		i2.catchException(u);
	}
}
