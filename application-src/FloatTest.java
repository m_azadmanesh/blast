public class FloatTest {

	public static void run() {
		float f1 = 1;
		float f2 = 99;
		float f3 = f1+f2;
		float f4 = f1-f2;
		float f5 = make();
		if (f1 < f2)
			f1 = f3;
		take(f5);
	}
	
	private static float make() {
		return 5;
	}
	
	private static void take(float f) {
		float l = f;
	}
}
