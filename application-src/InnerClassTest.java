import java.util.List;
import java.util.Vector;

public abstract class InnerClassTest{
	private String args;
	private String desc;
	
	public InnerClassTest(String args, String desc){
		args = args;
		this.desc = desc;
	}
	
	static class InnerClass extends InnerClassTest{
		private String val;
		private int id ;
		
		public InnerClass(String args, String val, String desc){
			super(args,desc);
			int a = 3;
		}
	}
}