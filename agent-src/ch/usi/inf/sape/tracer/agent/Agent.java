package ch.usi.inf.sape.tracer.agent;

import java.lang.instrument.Instrumentation;


public class Agent {

	public static void premain(String agentArgs, Instrumentation inst) {
		System.out.println("Agent:\tstarting (arguments: '"+agentArgs+"')");
		inst.addTransformer(new Transformer());
	}
	
}