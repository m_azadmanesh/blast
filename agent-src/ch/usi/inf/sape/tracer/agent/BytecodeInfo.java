package ch.usi.inf.sape.tracer.agent;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;

public class BytecodeInfo {
	
	public static boolean isPEI(AbstractInsnNode insn){
		switch(insn.getOpcode()){
			case Opcodes.AALOAD: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.AASTORE: // NullPointerException, ArrayIndexOutOfBoundsException, ArrayStoreException
	        case Opcodes.ANEWARRAY: // NegativeArraySizeException, (linking)
	        case Opcodes.ARETURN: // IllegalMonitorStateException (if synchronized)
	        case Opcodes.ARRAYLENGTH: // NullPointerException
	        case Opcodes.ATHROW: // NullPointerException, IllegalMonitorStateException (if synchronized), 
	        case Opcodes.BALOAD: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.BASTORE: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.CALOAD: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.CASTORE: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.CHECKCAST: // ClassCastException, (linking)
	        case Opcodes.DALOAD: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.DASTORE: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.DRETURN: // IllegalMonitorStateException (if synchronized)
	        case Opcodes.FALOAD: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.FASTORE: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.FRETURN: // IllegalMonitorStateException (if synchronized)
	        case Opcodes.GETFIELD: // NullPointerException, (linking)
	        case Opcodes.GETSTATIC: // Error*, (linking)
	        case Opcodes.IALOAD: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.IASTORE: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.IDIV: // ArithmeticException
	        case Opcodes.INSTANCEOF: // (linking)
	        case Opcodes.INVOKEDYNAMIC: // what's this??
	        case Opcodes.INVOKEINTERFACE: // NullPointerException, IncompatibleClassChangeError, AbstractMethodError, IllegalAccessError, AbstractMethodError, UnsatisfiedLinkError, (linking)
	        case Opcodes.INVOKESPECIAL: // NullPointerException, UnsatisfiedLinkError, (linking)
	        case Opcodes.INVOKESTATIC: // UnsatisfiedLinkError, Error*, (linking)
	        case Opcodes.INVOKEVIRTUAL: // NullPointerException, AbstractMethodError, UnsatisfiedLinkError, (linking)
	        case Opcodes.IREM: // ArithmeticException
	        case Opcodes.IRETURN: // IllegalMonitorStateException (if synchronized)
	        case Opcodes.LALOAD: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.LASTORE: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.LDIV: // ArithmeticException
	        case Opcodes.LREM: // ArithmeticException
	        case Opcodes.LRETURN: // IllegalMonitorStateException (if synchronized)
	        case Opcodes.MONITORENTER: // NullPointerException
	        case Opcodes.MONITOREXIT: // NullPointerException, IllegalMonitorStateException
	        case Opcodes.MULTIANEWARRAY: // NegativeArraySizeException, (linking)
	        case Opcodes.NEW: // Error*, (linking)
	        case Opcodes.NEWARRAY: // NegativeArraySizeException
	        case Opcodes.PUTFIELD: // NullPointerException, (linking)
	        case Opcodes.PUTSTATIC: // Error*, (linking)
	        case Opcodes.RETURN: // IllegalMonitorStateException (if synchronized)
	        case Opcodes.SALOAD: // NullPointerException, ArrayIndexOutOfBoundsException
	        case Opcodes.SASTORE: // NullPointerException, ArrayIndexOutOfBoundsException
	        	return true;
	        default:
	        	return false;
		}
	}
}
