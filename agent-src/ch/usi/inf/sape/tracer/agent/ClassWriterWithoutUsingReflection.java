package ch.usi.inf.sape.tracer.agent;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;

public final class ClassWriterWithoutUsingReflection extends ClassWriter{

	private final ClassLoader cl;
	
	public ClassWriterWithoutUsingReflection(ClassLoader cl, int flags) {
		super(flags);
		this.cl = cl;
	}

	@Override
	protected String getCommonSuperClass(String type1, String type2) {
		log("GetCommonSuperClass for "+ type1 + " and " + type2 + " with loader "+ cl, false);

		ClassNode c, d;
		ClassLoader classLoader = getClass().getClassLoader();
		try {
			c = getClassNodeWithoutLoading(type1);
			d = getClassNodeWithoutLoading(type2);

			if (isAssignableFrom(c, d)) {
				return type1;
			}

			if (isAssignableFrom(d, c)) {
				return type2;
			}

			if (isInterface(c) || isInterface(d)) {
				return "java/lang/Object";
			} else {
				do {
					c = getClassNodeWithoutLoading(c.superName);
				} while (!isAssignableFrom(c, d));

				return c.name;

			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e.toString());
		}
	}
	
	private ClassNode getClassNodeWithoutLoading(final String className) throws IOException{
		if (className == null)
			return null;
		
		log("Agent: Trying to read the class " + className, false);
				
		final String classFileName = className.concat(".class");

		final InputStream is = cl.getResourceAsStream(classFileName);
		final ClassReader reader = new ClassReader(is);
		final ClassNode node = new ClassNode();
		reader.accept(node, ClassReader.SKIP_FRAMES);
		return node;
	}
	
	
	@SuppressWarnings("unchecked")
	private boolean isAssignableFrom(ClassNode klass, ClassNode c) throws IOException{
		if (klass.name.equals(c.name))
			return true;

		String superName = c.superName;
		while(superName != null){
			if (superName.equals(klass.name))
				return true;
			superName = getClassNodeWithoutLoading(superName).superName;
		}

		final Stack<String> stackOfSuperInterfaceNames = new Stack<>();
		stackOfSuperInterfaceNames.push(c.name);			
		
		while(stackOfSuperInterfaceNames.size() != 0){
			List<String> interfaceNames = getClassNodeWithoutLoading(stackOfSuperInterfaceNames.pop()).interfaces;
			for (String interfaceName : interfaceNames) {
				if (klass.name.equals(interfaceName))
					return true;
				
				stackOfSuperInterfaceNames.add(interfaceName);
			}
		}
		
		return false;
	}
	
	private boolean isInterface(ClassNode klass){
		if ((klass.access & Opcodes.ACC_INTERFACE) != 0)
			return true;
		
		return false;
	}
	
	private static void log(String message, boolean log){
		if (log)
			System.out.println("ClassWriterNoReflection:\t"+ message);
	}
}
