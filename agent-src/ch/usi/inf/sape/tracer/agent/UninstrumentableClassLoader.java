package ch.usi.inf.sape.tracer.agent;

import java.net.URL;
import java.net.URLClassLoader;

public class UninstrumentableClassLoader extends URLClassLoader{

	public UninstrumentableClassLoader(URL[] urls, ClassLoader parent) {
		super(urls, parent);
	}
	
}
