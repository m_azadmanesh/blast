package ch.usi.inf.sape.tracer.agent;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.AdviceAdapter;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.IincInsnNode;
import org.objectweb.asm.tree.InnerClassNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.LineNumberNode;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.LookupSwitchInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.MultiANewArrayInsnNode;
import org.objectweb.asm.tree.TableSwitchInsnNode;
import org.objectweb.asm.tree.TryCatchBlockNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.VarInsnNode;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.AnalyzerException;
import org.objectweb.asm.tree.analysis.BasicInterpreter;
import org.objectweb.asm.tree.analysis.BasicValue;
import org.objectweb.asm.tree.analysis.Frame;
import org.objectweb.asm.util.CheckClassAdapter;
import org.objectweb.asm.util.Printer;

import ch.usi.inf.sape.tracer.analyzer.cdg.BasicBlock;
import ch.usi.inf.sape.tracer.analyzer.cdg.CDGUsingPostDom;
import ch.usi.inf.sape.tracer.analyzer.cdg.ControlDependencyGraph;
import ch.usi.inf.sape.tracer.analyzer.cdg.ControlFlowGraph;
import ch.usi.inf.sape.tracer.analyzer.cdg.Graph;



/**
 * This Transformer instruments a class so it calls the Profiler for each bytecode instruction,
 * passing along the relevant information to trace information flow and the values produced by each instruction.
 * 
 * This Transformer is not complete.
 * TODO: instrument remaining bytecode instructions
 * TODO: complete instrumentation where it needs operand stack unwinding
 * TODO: distinguish between different stack slots (maybe simply in the Profiler?)
 * TODO: fully instrument calls and returns ("arg" and "rv" pseudo-locations)
 */
public final class Transformer implements ClassFileTransformer {
	
	
	@Override
	public byte[] transform(final ClassLoader loader,
			final String className,
			final Class<?> classBeingRedefined,
			final ProtectionDomain protectionDomain,
			final byte[] classfileBuffer)
					throws IllegalClassFormatException {

		logln("About to transform class <"+loader+", "+className+">", false);
		
		if (className.startsWith("java/") ||
				className.startsWith("sun/") ||
				className.startsWith("javax/") ||
				className.startsWith("com/sun/") ||
				className.startsWith("com/apple/") ||
				className.startsWith("ch/usi/inf/sape/tracer/agent/") || 
				className.startsWith("ch/usi/inf/sape/tracer/profiler/") ||
				className.startsWith("ch/usi/inf/sape/tracer/visualizer/") ||
				className.startsWith("ch/usi/inf/sape/tracer/analyzer/") ||
				className.startsWith("ch/usi/inf/sape/tracer/utilities/") ||
				className.startsWith("ch/usi/inf/sape/tracer/rmi/") ||
//				className.startsWith("org/apache/") ||
				className.startsWith("org/hamcrest/") ||
				className.startsWith("org/omg/") ||
				className.startsWith("org/dom4j/") ||
				className.startsWith("org/openxmlformats/") ||
				className.startsWith("org/xml/") ||
				className.startsWith("schemaorg_apache_xmlbeans") ||
				className.startsWith("org/junit/") ||
				className.startsWith("junit/") ||
				className.startsWith("$Proxy") ||
				className.startsWith("JUnitTestWrapper") ||
				className.startsWith("TestHarness") ||
				className.startsWith("ch/usi/inf/sape/tracer/htmlizer") ||
				className.startsWith("com/google/common/base") ||
				className.startsWith("org/rendersnake") ||
				className.startsWith("Driver") ||
				className.startsWith("UninstrumentableClassTest") ||
				className.startsWith("com/mysql") ||
				className.startsWith("org/postgresql") ||
				isUninstrumentable(className)
				){

			logln("AGENT: skipping.", false);
			return classfileBuffer;

		} else if (loader instanceof UninstrumentableClassLoader){
			logln("AGENT: skipping.", false);
			return classfileBuffer;
		}else {
			try {
				final byte[] instrumentedBuffer = instrument(loader, classfileBuffer);
				logln("Transformed class <"+loader+", "+className+">", false);
				return instrumentedBuffer;
				
			} catch (final Error | RuntimeException ex) {
				logln("Failed transforming class <"+loader+", "+className+">: "+ex, true);
				ex.printStackTrace(System.out);
				return classfileBuffer;
			} 
		}

	}

	private byte[] instrument(ClassLoader cl, byte[] bytes) {
		ClassReader cr = new ClassReader(bytes);
		ClassNode cn = new ClassNode();
		cr.accept(new CheckClassAdapter(cn), ClassReader.SKIP_FRAMES );  ////ClassReader.EXPAND_FRAMES

		instrument(cn);
		final ClassWriter cw = new ClassWriterWithoutUsingReflection(cl, ClassWriter.COMPUTE_FRAMES);  //ClassWriter.COMPUTE_FRAMES
		FilterAdapter finallyClassAdapter = new FilterAdapter(Opcodes.ASM4, cw);		
		cn.accept(finallyClassAdapter);
		byte[] bs = cw.toByteArray();
		
//		if (cn.name.equals("org/joda/time/DateTimeZone")){  //org/apache/commons/math3/fraction/BigFraction
//			try{
//				FileOutputStream out = new FileOutputStream("/Users/reza/temp/DateTimeZone"+".class");
//				out.write(bs);
//				out.close();
//			}catch(IOException e){
//				e.printStackTrace(System.out);
//			}
//		}
		return bs;  
	}

	@SuppressWarnings("unchecked")
	private void instrument(ClassNode cn) {
		logln("AGENT: Class "+cn.name, false);
		for (MethodNode mn : (List<MethodNode>)cn.methods) {
			instrument(cn, mn);
		}
		
		logln("AGENT: Class "+cn.name+" [done]", false);
	}
	
	
	private void instrument(ClassNode cn, MethodNode mn) {
		logln("Method "+cn.name+"." +mn.name+mn.desc, false);
		//An abstract method should not be instrumented, otherwise there will be an exception thrown at runtime by the verifier
		if ((mn.access & Opcodes.ACC_ABSTRACT) != 0){
			return;
		}
		
		//Avoid instrumenting methods declared in Object because of recursion issues
		if (mn.name.equals("toString") ||
				 mn.name.equals("hashCode") ||
				 mn.name.equals("equals")
				){
			return;
		}
		
		// get the list of all instructions in that method
		final InsnList instructions = mn.instructions;
		LocalVariableTable localVariableTable = new LocalVariableTable(mn.localVariables == null ? 0 : mn.maxLocals + 1);  //1 is added because an implicit local variable added for the inner classes
		
		//We first find the cfg before instrumentation
		ControlFlowGraph<AbstractInsnNode , String> cfg = findControlDependencies(cn, mn, localVariableTable);
		Graph<AbstractInsnNode, String> postDom = findPostDomTree(cfg);
		Graph<AbstractInsnNode, String> cdg = findControlDependenceGraph(cfg, postDom);
		
//		System.out.println("CDG for "+mn.name+":\t"+ cdg);
//		try {
//			if (cdg.getVertices().size()> 0 ){
//				View<AbstractInsnNode,String> view = new DotGraphView<>(cn.name.replace('/',  '.')+"."+mn.name+"CDG.dot", cdg);
//				view.update();
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		
		Analyzer analyzer = new Analyzer(new BasicInterpreter());
		Frame[] frames;
		try {
			analyzer.analyze(cn.name, mn);
			frames = analyzer.getFrames();
		}catch(AnalyzerException e){
			e.printStackTrace();
			throw new RuntimeException();
		}
		
		final String localDetails = findLocalDetails(cn, mn); 		//The information about locals should be fetched here because the indices of operations change after the following loop			
		
		AbstractInsnNode instruction = instructions.getFirst();
		int nextSourceLineNo = -1;
		int i = 0;
		while (instruction!=null) {
			final AbstractInsnNode next = instruction.getNext();
			nextSourceLineNo = instrument(instruction, i, nextSourceLineNo, mn, localVariableTable, frames[i]);
			instruction = next;
			i++;
		}
		
		List<TryCatchBlockNode> tryCatchBlocks = mn.tryCatchBlocks;
		for (TryCatchBlockNode block : tryCatchBlocks) {
			LabelNode handler = block.handler;
			
			InsnList patch = new InsnList();
			patch.add(new InsnNode(Opcodes.DUP));
			patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"CATCH_BLOCK", "(Ljava/lang/Throwable;)V"));

			mn.instructions.insert(handler, patch);
		}
		
		instrument(cdg, mn, localVariableTable);
		
		instrumentMethodEntry(cn, mn, localDetails, localVariableTable);
//		InsnList beforePatch = new InsnList();
//		LabelNode returnLabel = new LabelNode();
//		LabelNode startTry = new LabelNode();
//		LabelNode handlerLabel = new LabelNode();
//		beforePatch.add(startTry);
//		mn.instructions.insert(beforePatch);
//		
//		InsnList afterPatch = new InsnList();
//		LabelNode catchBlockLabel = new LabelNode();
//		afterPatch.add(new FrameNode(Opcodes.F_SAME1, 0, null, 1, new Object[]{"java/lang/Throwable"}));
//		afterPatch.add(catchBlockLabel);
//		mn.tryCatchBlocks.add(new TryCatchBlockNode(startTry, catchBlockLabel, catchBlockLabel, "java/lang/Throwable"));
//		
//		afterPatch.add(new FieldInsnNode(Opcodes.GETSTATIC, 
//				 				"java/lang/System", "err", 
//				 				"Ljava/io/PrintStream;"));	 
//		afterPatch.add(new LdcInsnNode("Exiting " + mn.name)); 
//		afterPatch.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, 
//				 "java/io/PrintStream", "println", 
//				 "(Ljava/lang/String;)V"));
//
//		afterPatch.add(returnLabel);
//     	mn.instructions.insert(mn.instructions.getLast(), afterPatch);
		logln("AGENT: Method "+mn.name+mn.desc+" [done]", false);
	}

	private int instrument(final AbstractInsnNode instruction, final int bcIndex, final int sourceLineNo, final MethodNode mn, final LocalVariableTable localVariableTable, Frame frame) {
		final int opcode = instruction.getOpcode();
		final String mnemonic = opcode==-1?"":Printer.OPCODES[instruction.getOpcode()];
		logln(bcIndex + ":\t"+mnemonic+" ", false);
		int newSourceLineNo = sourceLineNo;
		// There are different subclasses of AbstractInsnNode.
		// AbstractInsnNode.getType() represents the subclass as an int.
		// Note:
		// to check the subclass of an instruction node, we can either use:
		//   if (instruction.getType()==AbstractInsnNode.LABEL)
		// or we can use:
		//   if (instruction instanceof LabelNode)
		// They give the same result, but the first one can be used in a switch statement.
		switch (instruction.getType()) {
		case AbstractInsnNode.LABEL: 
			// pseudo-instruction (branch or exception target)
			logln("// label");
			break;
		case AbstractInsnNode.FRAME:
			// pseudo-instruction (stack frame map)
			logln("// stack frame map");
			break;
		case AbstractInsnNode.LINE:
			// pseudo-instruction (line number information)
			logln("// line number information");
			
			LineNumberNode lineNumberNode = (LineNumberNode) instruction;
			logln("Line: "+ lineNumberNode.line+", starting at bytecode: "+ mn.instructions.indexOf(lineNumberNode.start));
			newSourceLineNo = lineNumberNode.line;
			break;
		case AbstractInsnNode.INSN:
			// Opcodes: NOP, ACONST_NULL, ICONST_M1, ICONST_0, ICONST_1, ICONST_2,
			// ICONST_3, ICONST_4, ICONST_5, LCONST_0, LCONST_1, FCONST_0,
			// FCONST_1, FCONST_2, DCONST_0, DCONST_1, IALOAD, LALOAD, FALOAD,
			// DALOAD, AALOAD, BALOAD, CALOAD, SALOAD, IASTORE, LASTORE, FASTORE,
			// DASTORE, AASTORE, BASTORE, CASTORE, SASTORE, POP, POP2, DUP,
			// DUP_X1, DUP_X2, DUP2, DUP2_X1, DUP2_X2, SWAP, IADD, LADD, FADD,
			// DADD, ISUB, LSUB, FSUB, DSUB, IMUL, LMUL, FMUL, DMUL, IDIV, LDIV,
			// FDIV, DDIV, IREM, LREM, FREM, DREM, INEG, LNEG, FNEG, DNEG, ISHL,
			// LSHL, ISHR, LSHR, IUSHR, LUSHR, IAND, LAND, IOR, LOR, IXOR, LXOR,
			// I2L, I2F, I2D, L2I, L2F, L2D, F2I, F2L, F2D, D2I, D2L, D2F, I2B,
			// I2C, I2S, LCMP, FCMPL, FCMPG, DCMPL, DCMPG, IRETURN, LRETURN,
			// FRETURN, DRETURN, ARETURN, RETURN, ARRAYLENGTH, ATHROW,
			// MONITORENTER, or MONITOREXIT.
			// zero operands, nothing to print
			switch (instruction.getOpcode()) {
			case Opcodes.NOP:
				break;
			case Opcodes.ACONST_NULL:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction 
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ACONST_NULL", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.ICONST_M1:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ICONST_M1", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.ICONST_0:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ICONST_0", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.ICONST_1:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ICONST_1", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.ICONST_2:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ICONST_2", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.ICONST_3:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ICONST_3", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.ICONST_4:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ICONST_4", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.ICONST_5:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ICONST_5", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.LCONST_0:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LCONST_0", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.LCONST_1:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LCONST_1", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.FCONST_0:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FCONST_0", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.FCONST_1:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FCONST_1", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.FCONST_2:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FCONST_2", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.DCONST_0:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DCONST_0", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.DCONST_1:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DCONST_1", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IALOAD:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IALOAD", "([III)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.LALOAD:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LALOAD", "([JII)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.FALOAD:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FALOAD", "([FII)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.DALOAD:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DALOAD", "([DII)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.AALOAD:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"AALOAD", "([Ljava/lang/Object;II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.BALOAD:
			{
				// could operate on byte[] or boolean[]
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"BALOAD", "(Ljava/lang/Object;II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.CALOAD:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"CALOAD", "([CII)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.SALOAD:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"SALOAD", "([SII)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IASTORE:
			{
				InsnList patch = new InsnList();
				int newLocalIndex = localVariableTable.add(Type.INT);      //store value to be used later
				storeInsnList(Type.INT, newLocalIndex, patch);			
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IASTORE", "([III)V"));
				loadInsnList(Type.INT, newLocalIndex, patch);		//value
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.LASTORE:
			{
				InsnList patch = new InsnList(); 
				int newLocalIndex = localVariableTable.add(Type.LONG);      //store value to be used later
				storeInsnList(Type.LONG, newLocalIndex, patch);			
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LASTORE", "([JII)V"));
				loadInsnList(Type.LONG, newLocalIndex, patch);
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.FASTORE:
			{
				InsnList patch = new InsnList(); 
				int newLocalIndex = localVariableTable.add(Type.FLOAT);      //store value to be used later
				storeInsnList(Type.FLOAT, newLocalIndex, patch);			
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FASTORE", "([FII)V"));
				loadInsnList(Type.FLOAT, newLocalIndex, patch);
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.DASTORE:
			{
				InsnList patch = new InsnList(); 
				int newLocalIndex = localVariableTable.add(Type.DOUBLE);      //store value to be used later
				storeInsnList(Type.DOUBLE, newLocalIndex, patch);			
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DASTORE", "([DII)V"));
				loadInsnList(Type.DOUBLE, newLocalIndex, patch);
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.AASTORE:
			{
				InsnList patch = new InsnList(); 
				int newLocalIndex = localVariableTable.add(Type.OBJECT);      //store value to be used later
				storeInsnList(Type.OBJECT, newLocalIndex, patch);			
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"AASTORE", "([Ljava/lang/Object;II)V"));
				loadInsnList(Type.OBJECT, newLocalIndex, patch);
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.BASTORE:
			{
				// could operate on byte[] or boolean[]
				InsnList patch = new InsnList(); 
				int newLocalIndex = localVariableTable.add(Type.INT);    //computational type for boolean and byte is integer
				storeInsnList(Type.INT, newLocalIndex, patch);			
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"BASTORE", "(Ljava/lang/Object;II)V"));
				loadInsnList(Type.INT, newLocalIndex, patch);
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.CASTORE:
			{
				InsnList patch = new InsnList(); 
				int newLocalIndex = localVariableTable.add(Type.CHAR);      //store value to be used later 
				storeInsnList(Type.CHAR, newLocalIndex, patch);			
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"CASTORE", "([CII)V"));
				loadInsnList(Type.CHAR, newLocalIndex, patch);
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.SASTORE:
			{
				InsnList patch = new InsnList(); 
				int newLocalIndex = localVariableTable.add(Type.SHORT);      //store value to be used later
				storeInsnList(Type.SHORT, newLocalIndex, patch);			
				patch.add(new InsnNode(Opcodes.DUP2)); // array, index
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"SASTORE", "([SII)V"));
				loadInsnList(Type.SHORT, newLocalIndex, patch);
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.POP:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"POP", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.POP2:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"POP2", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.DUP:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DUP", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.DUP_X1:
			{
				// operand stack: value2, value1 -> value1, value2, value1     
				// value1 and value2 are values of category1 computational type
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DUP_X1", "(I)V"));

				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.DUP_X2:
			{
				//Form 1: value3, value2, value1 -> value1, value3, value2, value1    (all values are of category 1 computational type) 
				//Form 2: value2, value1  ->  value1, value2, value1				  (value1 is a value of a category 1 computational type and value2 is a value of a category 2 computational type)
				InsnList patch = new InsnList();
				int typeOfValue2 = ((BasicValue)frame.getStack(frame.getStackSize() - 2)).getType().getSort();
				
				boolean isForm2= false;
				if (typeOfValue2 == Type.LONG || typeOfValue2 == Type.DOUBLE)               // category 2 computational types
					isForm2 = true;
					
				if (isForm2){
					logln("DUP_X2<Form 2>", false);
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"DUP_X2_2", "(I)V"));
				}else{
					logln("DUP_X2<Form 1>", false);
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"DUP_X2_1", "(I)V"));
				}
				mn.instructions.insertBefore(instruction, patch);
				break; 
			}
			case Opcodes.DUP2:
			{
				// Form1: value2, value1   ->   value2, value1, value2, value1       where both value1 and value2 are values of a category 1 computational type
				// Form2: value  -> value, value        where value is a value of a category 2 computational type
				
				InsnList patch = new InsnList();
				
				int typeOfValue1 = ((BasicValue) frame.getStack(frame.getStackSize() - 1)).getType().getSort();
				boolean isForm2 = false;
				if (typeOfValue1 == Type.LONG || typeOfValue1 == Type.DOUBLE)
					isForm2 = true;
				
				if (isForm2){
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"DUP2_2", "(I)V"));
				}else{
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"DUP2_1", "(I)V"));
				}
				
				mn.instructions.insertBefore(instruction, patch);
				break; 
			}
			case Opcodes.DUP2_X1:
			{
				//Form1: value3, value2, value1  ->  value2, value1, value3, value2, value1          where value1, value2, and value3 are all values of a category 1 computational type
				//Form2: value2, value1    ->    value1, value2, value1     	where value1 is a value of a category 2 computational type and value2 is a value of a category 1 computational type
				InsnList patch = new InsnList();
				int typeOfValue1 = ((BasicValue)frame.getStack(frame.getStackSize() - 1)).getType().getSort();
				boolean isForm2 = false;
				if (typeOfValue1 == Type.DOUBLE  || typeOfValue1 == Type.LONG)
					isForm2 = true;
				
				if (isForm2){
					logln("DUP2_X1<Form 2>", false);
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"DUP2_X1_2", "(I)V"));
				}else{
					logln("DUP2_X1<Form 1>", false);
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"DUP2_X1_1", "(I)V"));
					
				}
				
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.DUP2_X2:
			{
				// Form1: value4, value3, value2, value1  ->   value2, value1, value4, value3, value2, value1      where value1, value2, value3, and value4 are all values of a category 1 computational type
				// Form2: value3, value2, value1   ->    value1, value3, value2, value1    	where value1 is a value of a category 2 computational type and value2 and value3 are both values of a category 1 computational type
				// Form3: value3, value2, value1   ->    value2, value1, value3, value2, value1       where value1 and value2 are both values of a category 1 computational type and value3 is a value of a category 2 computational type
				// Form4: value2, value1    ->    value1, value2, value1		where value1 and value2 are both values of a category 2 computational type
				
				int form = 1;
				int typeOfValue1 = ((BasicValue)frame.getStack(frame.getStackSize() - 1)).getType().getSort(); 
				int typeOfValue2 = ((BasicValue)frame.getStack(frame.getStackSize() - 2)).getType().getSort();
				
				if (typeOfValue1 == Type.DOUBLE || typeOfValue1 == Type.LONG){			//value1 is category two  ->  either form 4 or form 2
					if (typeOfValue2 == Type.DOUBLE || typeOfValue2 == Type.LONG)
						form = 4;
					else
						form = 2;
				}else{																// Either form 1 or form 3
					int typeOfValue3 = ((BasicValue)frame.getStack(frame.getStackSize() - 3)).getType().getSort();
					if (typeOfValue3 == Type.DOUBLE || typeOfValue3 == Type.LONG)
						form = 3;
				}
				
				InsnList patch = new InsnList();
				
				switch (form){
				case 1:
				{
					logln("DUP2_X2<Form 1>");
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"DUP2_X2_1", "(I)V"));
					break;
				}
				case 2:
				{
					logln("DUP2_X2<Form 2>");
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"DUP2_X2_2", "(I)V"));
					break;
				}
				case 3:
				{
					logln("DUP2_X2<Form 3>");
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"DUP2_X2_3", "(I)V"));
					break;
				}
				case 4:
				{
					logln("DUP2_X2<Form 4>");
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"DUP2_X2_4", "(I)V"));
					break;
				}
				}
				
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.SWAP:
			{
				//  Form:  value2, value1  ->  value1, value2				value1 and value2 are both values of a category 1 computational type
				
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"SWAP", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break; 
			}
			case Opcodes.IADD:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IADD", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.LADD:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LADD", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.FADD:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FADD", "(FI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.DADD:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DADD", "(DI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.ISUB:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ISUB", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.LSUB:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LSUB", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.FSUB:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FSUB", "(FI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.DSUB:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DSUB", "(DI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.IMUL:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IMUL", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.LMUL:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LMUL", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.FMUL:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FMUL", "(FI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.DMUL:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DMUL", "(DI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.IDIV:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IDIV", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.LDIV:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LDIV", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.FDIV:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FDIV", "(FI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.DDIV:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DDIV", "(DI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.IREM:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IREM", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.LREM:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LREM", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.FREM:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FREM", "(FI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.DREM:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DREM", "(DI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.INEG:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"INEG", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.LNEG:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LNEG", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.FNEG:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FNEG", "(FI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.DNEG:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DNEG", "(DI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.ISHL:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ISHL", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.LSHL:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LSHL", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.ISHR:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ISHR", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.LSHR:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LSHR", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.IUSHR:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IUSHR", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.LUSHR:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LUSHR", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.IAND:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IAND", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.LAND:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LAND", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.IOR:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IOR", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.LOR:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LOR", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.IXOR:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IXOR", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.LXOR:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LXOR", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.I2L:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"I2L", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.I2F:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"I2F", "(FI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.I2D:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"I2D", "(DI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.L2I:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"L2I", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.L2F:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"L2F", "(FI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.L2D:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"L2D", "(DI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.F2I:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"F2I", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.F2L:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"F2L", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.F2D:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"F2D", "(DI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.D2I:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"D2I", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.D2L:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP2)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"D2L", "(JI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.D2F:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"D2F", "(FI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.I2B:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"I2B", "(BI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.I2C:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"I2C", "(CI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.I2S:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"I2S", "(SI)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.LCMP:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LCMP", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.FCMPL:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FCMPL", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.FCMPG:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FCMPG", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.DCMPL:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DCMPL", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.DCMPG:
			{
				final InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DCMPG", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.IRETURN:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IRETURN", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.LRETURN:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LRETURN", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.FRETURN:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FRETURN", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.DRETURN:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DRETURN", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.ARETURN:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ARETURN", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.RETURN:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"RETURN", "(I)V"));
				
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.ARRAYLENGTH:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // array
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction 
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ARRAYLENGTH", "(Ljava/lang/Object;I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.ATHROW:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ATHROW", "(Ljava/lang/Object;I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.MONITORENTER:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"MONITORENTER", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.MONITOREXIT:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"MONITOREXIT", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			}
			break;
		case AbstractInsnNode.INT_INSN:
			// Opcodes: NEWARRAY, BIPUSH, SIPUSH.
			if (instruction.getOpcode()==Opcodes.NEWARRAY) {
				// NEWARRAY
				//{@link Opcodes#T_BOOLEAN}, {@link Opcodes#T_CHAR},
				//{@link Opcodes#T_FLOAT}, {@link Opcodes#T_DOUBLE},
				//{@link Opcodes#T_BYTE}, {@link Opcodes#T_SHORT},
				//{@link Opcodes#T_INT} or {@link Opcodes#T_LONG}.
				final int elementType = ((IntInsnNode)instruction).operand;
				logln(Printer.TYPES[elementType]);
				switch (elementType) {
				case Opcodes.T_BOOLEAN:
				{
					InsnList beforePatch = new InsnList();
					beforePatch.add(new InsnNode(Opcodes.DUP));	       //array size
					beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_pre", "(II)V"));
					mn.instructions.insertBefore(instruction, beforePatch);
					
					InsnList afterPatch = new InsnList();
					afterPatch.add(new InsnNode(Opcodes.DUP));   //array 
					afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_post", "([Z)V"));
					mn.instructions.insert(instruction, afterPatch);
					break;
				}
				case Opcodes.T_CHAR:
				{
					InsnList beforePatch = new InsnList();
					beforePatch.add(new InsnNode(Opcodes.DUP));	       //array size
					beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_pre", "(II)V"));
					mn.instructions.insertBefore(instruction, beforePatch);
					
					InsnList afterPatch = new InsnList();
					afterPatch.add(new InsnNode(Opcodes.DUP));   //array 
					afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_post", "([C)V"));
					mn.instructions.insert(instruction, afterPatch);
					break;
				}
				case Opcodes.T_FLOAT:
				{
					InsnList beforePatch = new InsnList();
					beforePatch.add(new InsnNode(Opcodes.DUP));	       //array size
					beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_pre", "(II)V"));
					mn.instructions.insertBefore(instruction, beforePatch);
					
					InsnList afterPatch = new InsnList();
					afterPatch.add(new InsnNode(Opcodes.DUP));   //array 
					afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_post", "([F)V"));
					mn.instructions.insert(instruction, afterPatch);
					break;
				}
				case Opcodes.T_DOUBLE:
				{
					InsnList beforePatch = new InsnList();
					beforePatch.add(new InsnNode(Opcodes.DUP));	       //array size
					beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_pre", "(II)V"));
					mn.instructions.insertBefore(instruction, beforePatch);
					
					InsnList afterPatch = new InsnList();
					afterPatch.add(new InsnNode(Opcodes.DUP));   //array 
					afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_post", "([D)V"));
					mn.instructions.insert(instruction, afterPatch);
					break;
				}
				case Opcodes.T_BYTE:
				{
					InsnList beforePatch = new InsnList();
					beforePatch.add(new InsnNode(Opcodes.DUP));	       //array size
					beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_pre", "(II)V"));
					mn.instructions.insertBefore(instruction, beforePatch);
					
					InsnList afterPatch = new InsnList();
					afterPatch.add(new InsnNode(Opcodes.DUP));   //array 
					afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_post", "([B)V"));
					mn.instructions.insert(instruction, afterPatch);
					break;
				}
				case Opcodes.T_SHORT:
				{
					InsnList beforePatch = new InsnList();
					beforePatch.add(new InsnNode(Opcodes.DUP));	       //array size
					beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_pre", "(II)V"));
					mn.instructions.insertBefore(instruction, beforePatch);
					
					InsnList afterPatch = new InsnList();
					afterPatch.add(new InsnNode(Opcodes.DUP));   //array 
					afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_post", "([S)V"));
					mn.instructions.insert(instruction, afterPatch);
					break;
				}
				case Opcodes.T_INT:
				{
					InsnList beforePatch = new InsnList();
					beforePatch.add(new InsnNode(Opcodes.DUP));	       //array size
					beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_pre", "(II)V"));
					mn.instructions.insertBefore(instruction, beforePatch);
					
					InsnList afterPatch = new InsnList();
					afterPatch.add(new InsnNode(Opcodes.DUP));   //array 
					afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_post", "([I)V"));
					mn.instructions.insert(instruction, afterPatch);
					break;
				}
				case Opcodes.T_LONG:
				{
					InsnList beforePatch = new InsnList();
					beforePatch.add(new InsnNode(Opcodes.DUP));	       //array size
					beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_pre", "(II)V"));
					mn.instructions.insertBefore(instruction, beforePatch);
					
					InsnList afterPatch = new InsnList();
					afterPatch.add(new InsnNode(Opcodes.DUP));   //array 
					afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"NEWARRAY_post", "([J)V"));
					mn.instructions.insert(instruction, afterPatch);
					break;
				}
				}
			} else {
				// BIPUSH or SIPUSH
				switch (instruction.getOpcode()) {
				case Opcodes.BIPUSH:
				{
					InsnList patch = new InsnList();
					patch.add(new LdcInsnNode(((IntInsnNode)instruction).operand)); // value
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"BIPUSH", "(II)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Opcodes.SIPUSH:
				{
					InsnList patch = new InsnList();
					patch.add(new LdcInsnNode(((IntInsnNode)instruction).operand)); // value
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"SIPUSH", "(II)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				}
				logln(((IntInsnNode)instruction).operand);
			}
			break;
		case AbstractInsnNode.JUMP_INSN:
			// Opcodes: IFEQ, IFNE, IFLT, IFGE, IFGT, IFLE, IF_ICMPEQ,
			// IF_ICMPNE, IF_ICMPLT, IF_ICMPGE, IF_ICMPGT, IF_ICMPLE, IF_ACMPEQ,
			// IF_ACMPNE, GOTO, JSR, IFNULL or IFNONNULL.
		{
			final LabelNode targetInstruction = ((JumpInsnNode)instruction).label;
			final int targetId = mn.instructions.indexOf(targetInstruction);
			log(targetId);
			switch (instruction.getOpcode()) {
			case Opcodes.IFEQ:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IFEQ", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IFNE:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IFNE", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IFLT:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IFLT", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IFGE:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IFGE", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IFGT:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IFGT", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IFLE:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IFLE", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IF_ICMPEQ:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IF_ICMPEQ", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IF_ICMPNE:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IF_ICMPNE", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IF_ICMPLT:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IF_ICMPLT", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IF_ICMPGE:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IF_ICMPGE", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IF_ICMPGT:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IF_ICMPGT", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IF_ICMPLE:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IF_ICMPLE", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IF_ACMPEQ:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IF_ACMPEQ", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IF_ACMPNE:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IF_ACMPNE", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}

			case Opcodes.GOTO:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"GOTO", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.JSR:
				break; // ignore (won't occur)
			case Opcodes.IFNULL:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IFNULL", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.IFNONNULL:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, targetId));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IFNONNULL", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			}
			break;
		}
		case AbstractInsnNode.LDC_INSN:
			// Opcodes: LDC.
			final Object cst = ((LdcInsnNode)instruction).cst;
			log(cst);
			if (cst instanceof Integer) {
				InsnList patch = new InsnList();
				patch.add(new LdcInsnNode(((Integer)cst).intValue())); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LDC", "(II)V"));
				mn.instructions.insertBefore(instruction, patch);
			} else if (cst instanceof Float) {
				InsnList patch = new InsnList();
				patch.add(new LdcInsnNode(((Float)cst).floatValue())); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LDC", "(FI)V"));
				mn.instructions.insertBefore(instruction, patch);
			} else if (cst instanceof Long) {
				InsnList patch = new InsnList();
				patch.add(new LdcInsnNode(((Long)cst).longValue())); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LDC", "(JI)V"));
				mn.instructions.insertBefore(instruction, patch);
			} else if (cst instanceof Double) {
				InsnList patch = new InsnList();
				patch.add(new LdcInsnNode(((Double)cst).doubleValue())); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LDC", "(DI)V"));
				mn.instructions.insertBefore(instruction, patch);
			} else if (cst instanceof String) {
				InsnList patch = new InsnList();
				patch.add(new LdcInsnNode(((String)cst))); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LDC", "(Ljava/lang/String;I)V"));
				mn.instructions.insertBefore(instruction, patch);				
			} else {   
				InsnList beforePatch = new InsnList();
				beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LDC_pre", "(I)V"));
				mn.instructions.insertBefore(instruction, beforePatch);
				
				InsnList afterPatch = new InsnList();
				afterPatch.add(new InsnNode(Opcodes.DUP)); // value
				afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LDC", "(Ljava/lang/Object;)V"));
				mn.instructions.insert(instruction, afterPatch);
			}
			break;
		case AbstractInsnNode.IINC_INSN:
			// Opcodes: IINC.
			log(((IincInsnNode)instruction).var);
			log(" ");
			logln(((IincInsnNode)instruction).incr);
			{
				InsnList patch = new InsnList();
				patch.add(new VarInsnNode(Opcodes.ILOAD, ((IincInsnNode)instruction).var));		//the new value
				patch.add(new IntInsnNode(Opcodes.BIPUSH, ((IincInsnNode)instruction).var)); // index of the local var to be incremented
				patch.add(new IntInsnNode(Opcodes.BIPUSH, ((IincInsnNode)instruction).incr)); // amount to increment the value by
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"IINC", "(IIII)V"));
				mn.instructions.insert(instruction, patch);
			}
			break;
		case AbstractInsnNode.TYPE_INSN:
			// Opcodes: NEW, ANEWARRAY, CHECKCAST or INSTANCEOF.
			log(((TypeInsnNode)instruction).desc);
			switch (instruction.getOpcode()) {
			case Opcodes.NEW:
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value   
				patch.add(new LdcInsnNode(((TypeInsnNode)instruction).desc));
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"NEW", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.ANEWARRAY:
			{
				InsnList beforePatch = new InsnList();
				beforePatch.add(new InsnNode(Opcodes.DUP));			//count 
				beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ANEWARRAY_pre", "(II)V"));
				mn.instructions.insertBefore(instruction, beforePatch);
				
				InsnList afterPatch = new InsnList();
				afterPatch.add(new InsnNode(Opcodes.DUP));   //array 
				afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ANEWARRAY_post", "([Ljava/lang/Object;)V"));
				mn.instructions.insert(instruction, afterPatch);
				break;
			}
			case Opcodes.CHECKCAST:
				// before: ..., object
				// after:  ..., object
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"CHECKCAST", "(I)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.INSTANCEOF:
				// before: ..., object
				// after:  ..., value
			{
				InsnList patch = new InsnList();
				patch.add(new InsnNode(Opcodes.DUP)); // value
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"INSTANCEOF", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			}
			break;
		case AbstractInsnNode.VAR_INSN:
			// Opcodes: ILOAD, LLOAD, FLOAD, DLOAD, ALOAD, ISTORE,
			// LSTORE, FSTORE, DSTORE, ASTORE or RET.
			log(((VarInsnNode)instruction).var);
			switch (instruction.getOpcode()) {
			case Opcodes.ILOAD:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, ((VarInsnNode)instruction).var)); // var
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ILOAD", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.LLOAD:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, ((VarInsnNode)instruction).var)); // var
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LLOAD", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.FLOAD:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, ((VarInsnNode)instruction).var)); // var
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FLOAD", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.DLOAD:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, ((VarInsnNode)instruction).var)); // var
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DLOAD", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.ALOAD:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, ((VarInsnNode)instruction).var)); // var
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ALOAD", "(II)V"));
				mn.instructions.insert(instruction, patch);
				break;
			}
			case Opcodes.ISTORE:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, ((VarInsnNode)instruction).var)); // var
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new IntInsnNode(Opcodes.SIPUSH, bcIndex));    //bytecode index only used for accessing local variables in order to retrieve the proper local variable location
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ISTORE", "(III)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.LSTORE:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, ((VarInsnNode)instruction).var)); // var
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new IntInsnNode(Opcodes.SIPUSH, bcIndex));    //bytecode index only used for accessing local variables in order to retrieve the proper local variable location
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"LSTORE", "(III)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.FSTORE:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, ((VarInsnNode)instruction).var)); // var
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new IntInsnNode(Opcodes.SIPUSH, bcIndex));    //bytecode index only used for accessing local variables in order to retrieve the proper local variable location
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"FSTORE", "(III)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.DSTORE:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, ((VarInsnNode)instruction).var)); // var
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new IntInsnNode(Opcodes.SIPUSH, bcIndex));    //bytecode index only used for accessing local variables in order to retrieve the proper local variable location
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"DSTORE", "(III)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.ASTORE:
			{
				InsnList patch = new InsnList();
				patch.add(new IntInsnNode(Opcodes.BIPUSH, ((VarInsnNode)instruction).var)); // var
				patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
				patch.add(new IntInsnNode(Opcodes.SIPUSH, bcIndex));    //bytecode index only used for accessing local variables in order to retrieve the proper local variable location
				patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
						"ch/usi/inf/sape/tracer/analyzer/Analyzer",
						"ASTORE", "(III)V"));
				mn.instructions.insertBefore(instruction, patch);
				break;
			}
			case Opcodes.RET:
				break; // ignore (won't occur)
			}
			break;
		case AbstractInsnNode.FIELD_INSN:
			// Opcodes: GETSTATIC, PUTSTATIC, GETFIELD or PUTFIELD.
			log(((FieldInsnNode)instruction).owner);
			log(".");
			log(((FieldInsnNode)instruction).name);
			log(" ");
			final String desc = ((FieldInsnNode)instruction).desc; 
			log(desc);
			final int sort = Type.getType(desc).getSort();
			switch (instruction.getOpcode()) {
			case Opcodes.GETSTATIC:
			{
				switch (sort) {
				case Type.BOOLEAN:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP));		//value
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));   //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETSTATIC_boolean", "(ZLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insert(instruction, patch);
					break;
				}
				case Type.BYTE:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP));		//value
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));   //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETSTATIC_byte", "(BLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insert(instruction, patch);
					break;
				}
				case Type.CHAR:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP));		//value
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));   //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETSTATIC_char", "(CLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insert(instruction, patch);
					break;
				}
				case Type.DOUBLE:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP2));		//value
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));   //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETSTATIC_double", "(DLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insert(instruction, patch);
					break;
				}
				case Type.FLOAT:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP));		//value
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));   //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETSTATIC_float", "(FLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insert(instruction, patch);
					break;
				}
				case Type.INT:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP));		//value
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));   //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETSTATIC_int", "(ILjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insert(instruction, patch);
					break;
				}
				case Type.LONG:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP2));		//value
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));   //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETSTATIC_long", "(JLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insert(instruction, patch);
					break;
				}
				case Type.SHORT:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP));		//value
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));   //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETSTATIC_short", "(SLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insert(instruction, patch);
					break;				
				}
				case Type.ARRAY:
				case Type.OBJECT:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP));		//value
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));   //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETSTATIC_object", "(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insert(instruction, patch);
					break;
				}
				}
				break;
			}
			case Opcodes.PUTSTATIC:
			{
				switch (sort) {
				case Type.BOOLEAN:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP)); // value
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));    //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"PUTSTATIC", "(ZLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.BYTE:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP)); // value
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));    //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"PUTSTATIC", "(BLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.CHAR:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP)); // value
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));    //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"PUTSTATIC", "(CLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.DOUBLE:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP2)); // value
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));    //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"PUTSTATIC", "(DLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.FLOAT:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP)); // value
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));    //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"PUTSTATIC", "(FLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.INT:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP)); // value
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));    //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"PUTSTATIC", "(ILjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.LONG:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP2)); // value
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));    //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"PUTSTATIC", "(JLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.SHORT:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP)); // value
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));    //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"PUTSTATIC", "(SLjava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.ARRAY:
				case Type.OBJECT:
				{
					InsnList patch = new InsnList();
					patch.add(new InsnNode(Opcodes.DUP)); // value
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).owner.replaceAll("/", ".")));
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,"java/lang/Class","forName", "(Ljava/lang/String;)Ljava/lang/Class;"));    //Class object
					
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"PUTSTATIC", "(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				}
				break;
			}
			case Opcodes.GETFIELD:
				// before: ..., object
				// after:  ..., value
			{
				switch (sort) {
				case Type.BOOLEAN:
				{
					final InsnList patch = new InsnList(); // stack: ..., object
					patch.add(new InsnNode(Opcodes.DUP)); // stack: ..., object, object
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name (stack: ..., value, object, value, fieldName)
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction (stack: ..., value, object, value, fieldName, instruction index)
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETFIELD_boolean", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.BYTE:
				{
					final InsnList patch = new InsnList(); // stack: ..., object
					patch.add(new InsnNode(Opcodes.DUP)); // stack: ..., object, object
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name (stack: ..., value, object, value, fieldName)
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction (stack: ..., value, object, value, fieldName, instruction index)
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETFIELD_byte", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.CHAR:
				{
					final InsnList patch = new InsnList(); // stack: ..., object
					patch.add(new InsnNode(Opcodes.DUP)); // stack: ..., object, object
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name (stack: ..., value, object, value, fieldName)
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction (stack: ..., value, object, value, fieldName, instruction index)
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETFIELD_char", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.DOUBLE:
				{
					final InsnList patch = new InsnList(); // stack: ..., object
					patch.add(new InsnNode(Opcodes.DUP)); // stack: ..., object, object
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name (stack: ..., value, object, value, fieldName)
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction (stack: ..., value, object, value, fieldName, instruction index)
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETFIELD_double", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.FLOAT:
				{
					final InsnList patch = new InsnList(); // stack: ..., object
					patch.add(new InsnNode(Opcodes.DUP)); // stack: ..., object, object
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name (stack: ..., value, object, value, fieldName)
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction (stack: ..., value, object, value, fieldName, instruction index)
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETFIELD_float", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.INT:
				{
					final InsnList patch = new InsnList(); // stack: ..., object
					patch.add(new InsnNode(Opcodes.DUP)); // stack: ..., object, object
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name (stack: ..., value, object, value, fieldName)
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction (stack: ..., value, object, value, fieldName, instruction index)
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETFIELD_int", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.LONG:
				{
					final InsnList patch = new InsnList(); // stack: ..., object
					patch.add(new InsnNode(Opcodes.DUP)); // stack: ..., object, object
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name (stack: ..., value, object, value, fieldName)
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction (stack: ..., value, object, value, fieldName, instruction index)
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETFIELD_long", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.SHORT:
				{
					final InsnList patch = new InsnList(); // stack: ..., object
					patch.add(new InsnNode(Opcodes.DUP)); // stack: ..., object, object
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name (stack: ..., value, object, value, fieldName)
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction (stack: ..., value, object, value, fieldName, instruction index)
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETFIELD_short", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				case Type.ARRAY:
				case Type.OBJECT:
				{
					final InsnList patch = new InsnList(); // stack: ..., object
					patch.add(new InsnNode(Opcodes.DUP)); // stack: ..., object, object
					patch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name (stack: ..., value, object, value, fieldName)
					patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction (stack: ..., value, object, value, fieldName, instruction index)
					patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"GETFIELD_object", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, patch);
					break;
				}
				}
				break;
			}
			case Opcodes.PUTFIELD:
				// before: ..., object, value
				// after: ...
			{
				switch (sort) {
				case Type.BOOLEAN:
				case Type.SHORT:
				case Type.ARRAY:
				case Type.OBJECT:
				case Type.BYTE:
				case Type.CHAR:
				case Type.FLOAT:
				case Type.INT:
				{
					InsnList beforePatch = new InsnList(); // stack: ..., object, value
					beforePatch.add(new InsnNode(Opcodes.DUP2)); // stack: ..., object, value, object, value
					beforePatch.add(new InsnNode(Opcodes.POP));  // stack: ..., object, value, object
					beforePatch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"PUTFIELD", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
					mn.instructions.insertBefore(instruction, beforePatch);
					break;
				}
				case Type.DOUBLE:
				{
					InsnList beforePatch = new InsnList(); // stack: ..., object, value
					int tempLocalForValue = localVariableTable.add(Type.DOUBLE);
					storeInsnList(Type.DOUBLE, tempLocalForValue, beforePatch);			
					
					beforePatch.add(new InsnNode(Opcodes.DUP));          		//stack:    ..., object, object 
					beforePatch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"PUTFIELD", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
					loadInsnList(Type.DOUBLE, tempLocalForValue, beforePatch);
					mn.instructions.insertBefore(instruction, beforePatch);
					break;
				}
				case Type.LONG:
				{
					InsnList beforePatch = new InsnList(); // stack: ..., object, value
					int tempLocalForValue = localVariableTable.add(Type.LONG);
					storeInsnList(Type.LONG, tempLocalForValue, beforePatch);			
					
					beforePatch.add(new InsnNode(Opcodes.DUP));          		//stack:    ..., object, object 
					beforePatch.add(new LdcInsnNode(((FieldInsnNode)instruction).name)); // field name
					beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
					beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
							"ch/usi/inf/sape/tracer/analyzer/Analyzer",
							"PUTFIELD", "(Ljava/lang/Object;Ljava/lang/String;I)V"));
					loadInsnList(Type.LONG, tempLocalForValue, beforePatch);
					mn.instructions.insertBefore(instruction, beforePatch);
					break;
				}
				}
				break;
			}
			}
			break;
		case AbstractInsnNode.METHOD_INSN:
			// Opcodes: INVOKEVIRTUAL, INVOKESPECIAL, INVOKESTATIC,
			// INVOKEINTERFACE or INVOKEDYNAMIC.
			log(((MethodInsnNode)instruction).owner);
			log(".");
			log(((MethodInsnNode)instruction).name);
			log(" ");
			log(((MethodInsnNode)instruction).desc);
			switch (instruction.getOpcode()) {
			case Opcodes.INVOKEVIRTUAL:
			{
				instrumentInvokeInstruction(mn, instruction, "INVOKEVIRTUAL", localVariableTable, sourceLineNo);
				break;
			}
			case Opcodes.INVOKESPECIAL:
			{
				instrumentInvokeInstruction(mn, instruction, "INVOKESPECIAL", localVariableTable, sourceLineNo);
				break;
			}
			case Opcodes.INVOKESTATIC:
			{
				instrumentInvokeInstruction(mn, instruction, "INVOKESTATIC", localVariableTable, sourceLineNo);
				break;
			}
			case Opcodes.INVOKEINTERFACE:
			{
				instrumentInvokeInstruction(mn, instruction, "INVOKEINTERFACE", localVariableTable, sourceLineNo);
				break;
			}
			case Opcodes.INVOKEDYNAMIC:
				break; //ignore
			}
			break;
		case AbstractInsnNode.MULTIANEWARRAY_INSN:
		{
			// Opcodes: MULTIANEWARRAY.
			log(((MultiANewArrayInsnNode)instruction).desc);
			log(" ");
			log(((MultiANewArrayInsnNode)instruction).dims);
			MultiANewArrayInsnNode multiANewArrayNode = (MultiANewArrayInsnNode) instruction;
			final int dims = multiANewArrayNode.dims;
			InsnList beforePatch = new InsnList();
			beforePatch.add(new IntInsnNode(Opcodes.BIPUSH, dims));   // array dimensions
			beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			beforePatch.add(new LdcInsnNode(((MultiANewArrayInsnNode)instruction).desc));
			beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"MULTIANEWARRAY_pre", "(IILjava/lang/String;)V"));
			mn.instructions.insertBefore(instruction, beforePatch);

			InsnList afterPatch = new InsnList();
			afterPatch.add(new InsnNode(Opcodes.DUP)); 			//array 
			afterPatch.add(new IntInsnNode(Opcodes.BIPUSH, dims));   // array dimensions
			afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"MULTIANEWARRAY_post", "([Ljava/lang/Object;I)V"));
			mn.instructions.insert(instruction, afterPatch);
			break;
		}
		case AbstractInsnNode.LOOKUPSWITCH_INSN:
			// Opcodes: LOOKUPSWITCH.
		{
			final List keys = ((LookupSwitchInsnNode)instruction).keys;
			final List labels = ((LookupSwitchInsnNode)instruction).labels;
			for (int t=0; t<keys.size(); t++) {
				final int key = (Integer)keys.get(t);
				final LabelNode targetInstruction = (LabelNode)labels.get(t);
				final int targetId = mn.instructions.indexOf(targetInstruction);
				log(key+": "+targetId+", ");
			}
			final LabelNode defaultTargetInstruction = ((LookupSwitchInsnNode)instruction).dflt;
			final int defaultTargetId = mn.instructions.indexOf(defaultTargetInstruction);
			log("default: "+defaultTargetId);
			InsnList patch = new InsnList();
			patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"LOOKUPSWITCH_INSN", "(I)V"));
			mn.instructions.insertBefore(instruction, patch);
			break;
		}
		case AbstractInsnNode.TABLESWITCH_INSN:
			// Opcodes: TABLESWITCH.
		{
			final int minKey = ((TableSwitchInsnNode)instruction).min;
			final List labels = ((TableSwitchInsnNode)instruction).labels;
			for (int t=0; t<labels.size(); t++) {
				final int key = minKey+t;
				final LabelNode targetInstruction = (LabelNode)labels.get(t);
				final int targetId = mn.instructions.indexOf(targetInstruction);
				log(key+": "+targetId+", ");
			}
			final LabelNode defaultTargetInstruction = ((TableSwitchInsnNode)instruction).dflt;
			final int defaultTargetId = mn.instructions.indexOf(defaultTargetInstruction);
			log("default: "+defaultTargetId);
			InsnList patch = new InsnList();
			patch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"TABLESWITCH_INSN", "(I)V"));
			mn.instructions.insertBefore(instruction, patch);
			break;
		}
		}		
		logln("");
		return newSourceLineNo;
	}
	
	private void instrumentMethodEntry(ClassNode cn, MethodNode mn, String localDetails, LocalVariableTable localVariableTable) {
		logln("Instrumenting entry of "+mn.name, false);
		InsnList patch = new InsnList();
		patch.add(new LdcInsnNode(cn.name+"."+mn.name+mn.desc));
		int isStatic = 0;
		if ((mn.access & Opcodes.ACC_STATIC) != 0)
			isStatic = 1;
		
		
		
		
		Type[] argTypes = Type.getArgumentTypes(mn.desc);
		
		int[] argTypeSorts;
		
		//for non-static method, we need a extra ref for the "this" variable
		int argCount;
		if (isStatic == 1){
			argCount = argTypes.length;
			argTypeSorts = new int[argCount];
			for (int i = 0; i < argTypes.length; i++) {
				argTypeSorts[i] = argTypes[i].getSort();
			}
		}else{
			argCount = argTypes.length + 1;
			argTypeSorts = new int[argCount];
			argTypeSorts[0] = Type.OBJECT;
			for (int i = 0; i < argTypes.length; i++) {
				argTypeSorts[i+1] = argTypes[i].getSort();
			}
		}
		
		patch.add(new LdcInsnNode(mn.maxLocals));
		patch.add(new LdcInsnNode(localDetails));
		patch.add(new LdcInsnNode(mn.maxStack));
		patch.add(new LdcInsnNode(argCount));

		if (argCount == 0){					//We need to pass a reference to an array that keeps track of args, so for the case when there is no args, we pass an array of length 0
			
			patch.add(new InsnNode(Opcodes.ICONST_0));
			patch.add(new TypeInsnNode(Opcodes.ANEWARRAY, "java/lang/Object"));
			
		}else{
			int localVarIndex[] = new int[argCount];
			localVarIndex[0] = 0;
			for (int j = 1; j < localVarIndex.length; j++) {
				localVarIndex[j] = isCategory2(argTypeSorts[j - 1]) ? localVarIndex[j - 1] + 2 : localVarIndex[j - 1] + 1; 
			}
			
			//create an array to store the arg values
			patch.add(new IntInsnNode(Opcodes.BIPUSH, argCount));
			patch.add(new TypeInsnNode(Opcodes.ANEWARRAY, "java/lang/Object"));
			int argArrayLocalIndex = localVariableTable.add(Type.OBJECT);
			patch.add(new VarInsnNode(Opcodes.ASTORE, argArrayLocalIndex));

			//Passing method args to the elements of an array to be passed to the profiler
			//Primitive types are wrapped in the corresponding wrapper for that type
			//We use a string to tell the analyzer which args are of primitive types
			//This is used for generating correct values based on the type
			for (int j = 0; j < argCount; j++) {
				patch.add(new VarInsnNode(Opcodes.ALOAD, argArrayLocalIndex));
				patch.add(new IntInsnNode(Opcodes.BIPUSH, j));
				loadInsnList(argTypeSorts[j], localVarIndex[j], patch);
				wrapPrimitiveInsnList(argTypeSorts[j], patch);
				patch.add(new InsnNode(Opcodes.AASTORE));
			}

			patch.add(new VarInsnNode(Opcodes.ALOAD, argArrayLocalIndex));    // load arg array
		}
		
		patch.add(new IntInsnNode(Opcodes.BIPUSH, isStatic));
		patch.add(new LdcInsnNode(cn.sourceFile));
		patch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
				"ch/usi/inf/sape/tracer/analyzer/Analyzer",
				"methodEntry", "(Ljava/lang/String;ILjava/lang/String;II[Ljava/lang/Object;ZLjava/lang/String;)V"));
		mn.instructions.insert(patch);
	}
	
	/**
	 * Adds instructions for loading a value from local variable table
	 * 
	 * @param type The TypeBase of the variable to be loaded from local variable table
	 * @param index The index in local variable table used for loading the value
	 * @param methodInsnList
	 */
	private void loadInsnList(int type, int index, InsnList methodInsnList){
		switch(type){
		case Type.BOOLEAN:
		case Type.CHAR:
		case Type.BYTE:
		case Type.SHORT:
		case Type.INT:
			methodInsnList.add(new VarInsnNode(Opcodes.ILOAD, index));
			break;
		case Type.FLOAT:
			methodInsnList.add(new VarInsnNode(Opcodes.FLOAD, index));
			break;
		case Type.LONG:
			methodInsnList.add(new VarInsnNode(Opcodes.LLOAD, index));
			break;
		case Type.DOUBLE:
			methodInsnList.add(new VarInsnNode(Opcodes.DLOAD, index));
			break;
		case Type.ARRAY:
		case Type.OBJECT:
			methodInsnList.add(new VarInsnNode(Opcodes.ALOAD, index));
			break;
		}
	}

	
	
	/**
	 * Adds instructions for storing a value in local variable table
	 * 
	 * @param type The TypeBase of the variable to be stored in local variable table
	 * @param index The index in local variable table used for storing the value
	 * @param methodInsnList
	 */
	private void storeInsnList(final int type, final int index, final InsnList methodInsnList){
		switch (type){
		case Type.BOOLEAN:
		case Type.CHAR:
		case Type.BYTE:
		case Type.SHORT:
		case Type.INT:
			methodInsnList.add(new VarInsnNode(Opcodes.ISTORE, index));
			break;
		case Type.FLOAT:
			methodInsnList.add(new VarInsnNode(Opcodes.FSTORE, index));
			break;
		case Type.LONG:
			methodInsnList.add(new VarInsnNode(Opcodes.LSTORE, index));
			break;
		case Type.DOUBLE:
			methodInsnList.add(new VarInsnNode(Opcodes.DSTORE, index));
			break;
		case Type.ARRAY:
		case Type.OBJECT:
			methodInsnList.add(new VarInsnNode(Opcodes.ASTORE, index));
			break;
		}
	}
	
	
	/**
	 * Add instructions for wrapping a primitive type value
	 * 
	 * @param type The TypeBase of the variable to be stored in local variable table
	 * @param methodInsnList
	 */
	private void wrapPrimitiveInsnList(final int type, final InsnList methodInsnList){
		switch (type){
		case Type.BOOLEAN:
			methodInsnList.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;"));
			break;
		case Type.CHAR:
			methodInsnList.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;"));
			break;
		case Type.BYTE:
			methodInsnList.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;"));
			break;
		case Type.SHORT:
			methodInsnList.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;"));
			break;
		case Type.INT:
			methodInsnList.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;"));
			break;
		case Type.FLOAT:
			methodInsnList.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;"));
			break;
		case Type.LONG:
			methodInsnList.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;"));
			break;
		case Type.DOUBLE:
			methodInsnList.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;"));
			break;
		case Type.VOID:
			methodInsnList.add(new InsnNode(Opcodes.ACONST_NULL));
		}
	}
	
	
	private boolean isPrimitive(final int type){
		if (type == Type.OBJECT || type == Type.ARRAY)
			return false;
		else 
			return true;
	}
	
	private boolean isCategory2(final int type){
		if (type == Type.DOUBLE || type == Type.LONG)
			return true;
		else
			return false;
	}

	/**
	 *  Values computed to be passed to the profiler need to be stored temporarily in local
	 *  variable table. This class keeps track of the available slots of local variable table
	 *  for a method.
	 */
    static class LocalVariableTable{
    	private int currentAvailableIndex;
    	
    	public LocalVariableTable(int initialSize){
    		currentAvailableIndex = initialSize;
    	}
    	
    	private int getCurrentAvailableIndex(){
    		return currentAvailableIndex;
    	}
    	
    	
    	/**
    	 * A new local variable slot gets assumed to be reserved and the index for the slot is returned 
    	 * 
    	 * @param type Type of the value to be stored in this new slot
    	 * @return Index of the next availabe slot in the local variable table of the current method
    	 */
    	public int add(int type){
    		int current = getCurrentAvailableIndex();
    		this.currentAvailableIndex += type == Type.LONG || type == Type.DOUBLE ? 2 : 1;
    		return current;
    	}

    }
    
    private void instrumentInvokeInstruction(final MethodNode mn, final AbstractInsnNode instruction, final String opName, final LocalVariableTable localVariableTable, final int sourceLineNo){
		InsnList beforePatch = new InsnList(); 
		MethodInsnNode methodInsnNode = (MethodInsnNode) instruction;
		int argsCount = getArgumentsCount(methodInsnNode.desc);
		
		if (!opName.equals("INVOKESTATIC"))
			argsCount ++;
		beforePatch.add(new LdcInsnNode(opName));
		beforePatch.add(new LdcInsnNode(methodInsnNode.owner+"."+methodInsnNode.name+methodInsnNode.desc));
		beforePatch.add(new IntInsnNode(Opcodes.BIPUSH, argsCount));
		beforePatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
		beforePatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
				"ch/usi/inf/sape/tracer/analyzer/Analyzer",
				"INVOKE_calling", "(Ljava/lang/String;Ljava/lang/String;II)V"));
		
				
		mn.instructions.insertBefore(instruction, beforePatch);
		
		InsnList afterPatch = new InsnList(); 
		int returnType = Type.getReturnType(methodInsnNode.desc).getSort();
		switch (returnType){
		case Type.BOOLEAN:
		{
			afterPatch.add(new InsnNode(Opcodes.DUP));     //return value
			afterPatch.add(new LdcInsnNode(opName));
			afterPatch.add(new LdcInsnNode(methodInsnNode.owner+"."+methodInsnNode.name+methodInsnNode.desc));
			afterPatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"INVOKE_returning", "(ZLjava/lang/String;Ljava/lang/String;I)V"));
			break;
		}
		case Type.BYTE:
		{
			afterPatch.add(new InsnNode(Opcodes.DUP));     //return value
			afterPatch.add(new LdcInsnNode(opName));
			afterPatch.add(new LdcInsnNode(methodInsnNode.owner+"."+methodInsnNode.name+methodInsnNode.desc));
			afterPatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"INVOKE_returning", "(BLjava/lang/String;Ljava/lang/String;I)V"));
			break;
		}
		case Type.CHAR:
		{
			afterPatch.add(new InsnNode(Opcodes.DUP));     //return value
			afterPatch.add(new LdcInsnNode(opName));
			afterPatch.add(new LdcInsnNode(methodInsnNode.owner+"."+methodInsnNode.name+methodInsnNode.desc));
			afterPatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"INVOKE_returning", "(CLjava/lang/String;Ljava/lang/String;I)V"));
			break;
		}
		case Type.DOUBLE:
		{
			afterPatch.add(new InsnNode(Opcodes.DUP2));     //return value
			afterPatch.add(new LdcInsnNode(opName));
			afterPatch.add(new LdcInsnNode(methodInsnNode.owner+"."+methodInsnNode.name+methodInsnNode.desc));
			afterPatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"INVOKE_returning", "(DLjava/lang/String;Ljava/lang/String;I)V"));
			break;
		}
		case Type.FLOAT:
		{
			afterPatch.add(new InsnNode(Opcodes.DUP));     //return value
			afterPatch.add(new LdcInsnNode(opName));
			afterPatch.add(new LdcInsnNode(methodInsnNode.owner+"."+methodInsnNode.name+methodInsnNode.desc));
			afterPatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"INVOKE_returning", "(FLjava/lang/String;Ljava/lang/String;I)V"));
			break;
		}
		case Type.INT:
		{
			afterPatch.add(new InsnNode(Opcodes.DUP));     //return value
			afterPatch.add(new LdcInsnNode(opName));
			afterPatch.add(new LdcInsnNode(methodInsnNode.owner+"."+methodInsnNode.name+methodInsnNode.desc));
			afterPatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"INVOKE_returning", "(ILjava/lang/String;Ljava/lang/String;I)V"));
			break;
		}
		case Type.LONG:
		{
			afterPatch.add(new InsnNode(Opcodes.DUP2));     //return value
			afterPatch.add(new LdcInsnNode(opName));
			afterPatch.add(new LdcInsnNode(methodInsnNode.owner+"."+methodInsnNode.name+methodInsnNode.desc));
			afterPatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"INVOKE_returning", "(JLjava/lang/String;Ljava/lang/String;I)V"));
			break;
		}
		case Type.SHORT:
		{
			afterPatch.add(new InsnNode(Opcodes.DUP));     //return value
			afterPatch.add(new LdcInsnNode(opName));
			afterPatch.add(new LdcInsnNode(methodInsnNode.owner+"."+methodInsnNode.name+methodInsnNode.desc));
			afterPatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"INVOKE_returning", "(SLjava/lang/String;Ljava/lang/String;I)V"));
			break;
		}
		case Type.OBJECT:
		case Type.ARRAY:
		{
			afterPatch.add(new InsnNode(Opcodes.DUP));     //return value
			afterPatch.add(new LdcInsnNode(opName));
			afterPatch.add(new LdcInsnNode(methodInsnNode.owner+"."+methodInsnNode.name+methodInsnNode.desc));
			afterPatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"INVOKE_returning", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;I)V"));
			break;
		}
		case Type.VOID:
			afterPatch.add(new LdcInsnNode(opName));
			afterPatch.add(new LdcInsnNode(methodInsnNode.owner+"."+methodInsnNode.name+methodInsnNode.desc));
			afterPatch.add(new IntInsnNode(Opcodes.SIPUSH, sourceLineNo)); //source code line no. corresponding to this instruction
			afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"INVOKE_returning", "(Ljava/lang/String;Ljava/lang/String;I)V"));
			break;
		}
		
//		afterPatch.add(new IntInsnNode(Opcodes.BIPUSH, insnIndex));	
//		afterPatch.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
//				"ch/usi/inf/sape/tracer/analyzer/Analyzer",
//				opName+"_returning", "(I)V"));
		
		mn.instructions.insert(instruction, afterPatch);
    }

    static class FilterAdapter extends ClassVisitor{

    	public FilterAdapter(int api, ClassVisitor cv) {
    		super(api, cv);
    	}

    	public MethodVisitor visitMethod(int acc, String name, String desc, String signature, String[] exceptions) { 
    		MethodVisitor mv = cv.visitMethod(acc, name, desc, signature, exceptions);
    		
    		//Avoid instrumenting methods declared in Object because of recursion issues
    		//Avoid instrumenting abstract methods
    		if (mv != null && 
    				((acc & Opcodes.ACC_ABSTRACT) == 0) &&
    				!name.equals("toString") &&
    				!name.equals("hashCode") &&
    				!name.equals("equals"))
    			mv = new FinallyAdapter(Opcodes.ASM4, mv, acc, name, desc);
    			
    		return mv; 
    	} 

    }
    

    static class FinallyAdapter extends AdviceAdapter{
    	private String name; 
    	private String desc;
    	private int access;
    	private Label startFinally = new Label();
    	private Label endFinally = new Label(); 
    	
    	protected FinallyAdapter(int api, MethodVisitor mv, int access,
    			String name, String desc) {
    		super(api, mv, access, name, desc);
    		logln("Finally visitor for method "+ name + desc, false);
    		this.name = name;
    		this.desc = desc;
    		this.access = access;
    	}
    	
    	public void visitCode() { 
    		super.visitCode(); 
    		mv.visitLabel(startFinally); 
    	} 

    	
    	public void visitMaxs(int maxStack, int maxLocals) { 
    		mv.visitTryCatchBlock(startFinally, 
    				endFinally, endFinally, null); 
    		mv.visitLabel(endFinally); 
    		onFinally(); 
    		mv.visitMaxs(maxStack, maxLocals); 
    	} 

    	protected void onMethodExit(int opcode) { 
    		if(opcode != ATHROW) { 
    			onExit();
    		}
    	}

    	private void onFinally() {
    		//Here we are sure that there is a throwable object on top of stack 
    		mv.visitInsn(Opcodes.DUP);
    		mv.visitMethodInsn(Opcodes.INVOKESTATIC, 
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"handleException", "(Ljava/lang/Throwable;)V");
    		
    		mv.visitInsn(ATHROW);
    	}
    	
    	public void onExit(){
    		mv.visitMethodInsn(Opcodes.INVOKESTATIC, 
					"ch/usi/inf/sape/tracer/analyzer/Analyzer",
					"onExit", "()V");
    		//We could pop stack simulator frames here, but currently it happens through instrumenting return statements
    		
//    		mv.visitMethodInsn(Opcodes.INVOKESTATIC, 
//    				"ch/usi/inf/sape/tracer/analyzer/Analyzer",
//    				"onExit", "()V");
    	}
    	
    }
    
    private static int getArgumentsCount(String desc){
    	char[] buf = desc.toCharArray();
        int off = 1;
        int count = 0;
        while (true) {
            char car = buf[off++];
            if (car == ')') {
                break;
            } else if (car == 'L') {
                while (buf[off++] != ';') {
                }
                ++count;
            } else if (car != '[') {
                ++count;
            }
        }
        
        return count;
    }

    public static void logln(Object message, boolean log){
		if (log)
			System.out.println("AGENT:\t"+ message+"\n");
    }
    
	public static void logln(Object message){
		logln(message, false);
	}
	
	public static void log(Object message){
		logln(message, false);
	}
	
	public static void log(Object message, boolean log){
		if (log)
			System.out.print(message);
	}

	private ControlFlowGraph<AbstractInsnNode , String> findControlDependencies(ClassNode cn, MethodNode mn, LocalVariableTable localTable){
		logln("AGENT: CDG for Method "+mn.name+mn.desc, false);
		final InsnList instructions = mn.instructions;
		
		//We first find the basic block boundaries
		List<AbstractInsnNode> bbLeaders= new ArrayList<>(findBasicBlockBoundaries(instructions));
		Collections.sort(bbLeaders, new Comparator<AbstractInsnNode>() {

			@Override
			public int compare(AbstractInsnNode o1, AbstractInsnNode o2) {
				if (instructions.indexOf(o1) < instructions.indexOf(o2))
					return -1;
				else if (instructions.indexOf(o1) == instructions.indexOf(o2))
					return 0;
				return 1;
			}
		});
		
		List<AbstractInsnNode> bbEnds = new ArrayList<>();
		
		for (int i = 0; i < bbLeaders.size() - 1; i++) {
			bbEnds.add(instructions.get(instructions.indexOf(bbLeaders.get(i+1)) - 1));
		}
		
		bbEnds.add(instructions.getLast());
		
		//In the second round, we create the graph nodes corresponding to basic blocks
		BasicBlock<AbstractInsnNode> [] bbs = new BasicBlock[bbLeaders.size() ];
		
		for (int i = 0; i < bbs.length ; i++) {
			bbs[i] = new BasicBlock<>(bbLeaders.get(i), bbEnds.get(i), bbLeaders.get(i), instructions.indexOf(bbLeaders.get(i)));
		}
		
		BasicBlock<AbstractInsnNode> end = bbs[bbs.length - 1];
		
		final ControlFlowGraph<AbstractInsnNode , String> cfg = new ControlFlowGraph(bbs.length + 2);  //two more rooms for a dummy entry and exit node
		for (int i = 0; i < bbs.length; i++) {
			cfg.addVertex(bbs[i].getLabel(), bbs[i]);
		}		
		cfg.setRoot(bbs[0].getLabel());
		
		//Augment the CFG with a dummy start and end node to represent the prolouge and epilouge of the method respectively
		BasicBlock<AbstractInsnNode> dummyStart = new BasicBlock(new InsnNode(Opcodes.NOP), new InsnNode(Opcodes.NOP), new InsnNode(Opcodes.NOP), -1);
		BasicBlock<AbstractInsnNode> dummyEnd = new BasicBlock(new InsnNode(Opcodes.NOP), new InsnNode(Opcodes.NOP), new InsnNode(Opcodes.NOP), -2);
		cfg.addVertex(dummyStart.getLabel(), dummyStart);
		cfg.addVertex(dummyEnd.getLabel(), dummyEnd);
		cfg.addEdge(dummyStart.getLabel(), cfg.getRoot(), "T");
		cfg.setRoot(dummyStart.getLabel());
		cfg.addEdge(dummyStart.getLabel(), dummyEnd.getLabel(), "F");
		cfg.addEdge(end.getLabel(), dummyEnd.getLabel(), "");
		cfg.setExit(dummyEnd.getLabel());
		
		//In the third round, we find the links between blocks based on the type of the final instruction in the block
		findBasicBlockLinks(cfg, mn, bbs, dummyEnd);
		
//		try {
//			
//				DotGraphView<AbstractInsnNode, String> view = new DotGraphView<>(cn.name.replace('/', '.')+"."+mn.name+"CFG.dot", cfg);
//				view.update();
//			
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
//		Iterator<AbstractInsnNode> it = instructions.iterator();
//		while (it.hasNext()){
//			AbstractInsnNode insn = it.next();
//			System.out.println("insn is "+ instructions.indexOf(insn) + ":" + Printer.OPCODES[insn.getOpcode() == -1 ? 0 : insn.getOpcode()] + " is in bb" + getBlockForInstruction(cfg, mn, insn).getID());
//		}
//		
		return cfg;
	}
	/**
	 * 
	 * @param instructions
	 * @return
	 */
	private Set<AbstractInsnNode> findBasicBlockBoundaries(InsnList instructions){
		Set<AbstractInsnNode> bbLeaders = new HashSet<>();
			
		//The first instruction is a leader.
		AbstractInsnNode instruction = instructions.getFirst();
		bbLeaders.add(instruction);
		
		// In the first round, we find the leader indices of basic blocks
		while (instruction != null){
			AbstractInsnNode next = instruction.getNext();

			switch(instruction.getType()){
			case AbstractInsnNode.JUMP_INSN:
			{
				JumpInsnNode jumpInsn = (JumpInsnNode) instruction;
				//The target of a conditional or an unconditional goto/jump instruction is a leader.
				bbLeaders.add(jumpInsn.label);
				
				//The instruction that immediately follows a conditional is a leader
//				if (jumpInsn.getOpcode() != Opcodes.GOTO && jumpInsn.getOpcode() != Opcodes.JSR){	//There is no fall-through for goto and jsr
				AbstractInsnNode target = instruction.getNext();
				if (target.getType() != AbstractInsnNode.LABEL && 
						target.getType() != AbstractInsnNode.LINE &&
						target.getType() != AbstractInsnNode.FRAME){	//we add a label node to separate the boundaries of the basic blocks. This is required to avoid the conflicts in instrumentation  
					target = new LabelNode();
					instructions.insertBefore(instruction.getNext(), target);
				}
				bbLeaders.add(target);
//				}
				break;
			}
			case AbstractInsnNode.TABLESWITCH_INSN:
			{
				TableSwitchInsnNode switchInsn = (TableSwitchInsnNode) instruction;
				List<LabelNode> labels = switchInsn.labels;
				for (LabelNode label : labels){
					bbLeaders.add(label);
				}
				bbLeaders.add(switchInsn.dflt);
				
				break;
			}
			case AbstractInsnNode.LOOKUPSWITCH_INSN:
			{
				LookupSwitchInsnNode switchInsn = (LookupSwitchInsnNode) instruction;
				List<LabelNode> labels = switchInsn.labels;
				for (LabelNode label : labels){
					bbLeaders.add(label);
				}
				bbLeaders.add(switchInsn.dflt);
				break;
			}
			}
			
			// We do a conservative analysis over PEIs such that every PEI introduces 
			// a new edge in CFG to any catch block whose scope is valid for this insn.
			if (BytecodeInfo.isPEI(instruction) && instruction.getNext() != null){
				AbstractInsnNode target = instruction.getNext();
				if (target.getType() != AbstractInsnNode.LABEL && 
						target.getType() != AbstractInsnNode.LINE &&
						target.getType() != AbstractInsnNode.FRAME){	//we add a label node to separate the boundaries of the basic blocks. This is required to avoid the conflicts in instrumentation  
					target = new LabelNode();
					instructions.insertBefore(instruction.getNext(), target);
				}
				bbLeaders.add(target);
			}
			
			instruction = next;
			
		}
		
		return bbLeaders;
	}
	
	/**
	 * Find and create all the links between the nodes in the CFG
	 * 
	 * @param cfg
	 * @param mn
	 * @param bbs
	 */
	private void findBasicBlockLinks(Graph<AbstractInsnNode, String> cfg, MethodNode mn, BasicBlock<AbstractInsnNode>[] bbs, ch.usi.inf.sape.tracer.analyzer.cdg.Label<AbstractInsnNode> end){
		InsnList instructions = mn.instructions;
		for (int i = 0; i < bbs.length; i++) {	//we should skip the dummy end node
			final AbstractInsnNode insn = bbs[i].getEnd();
			
			if (insn.getOpcode() == -1){
				continue;
			}
			
			switch(insn.getType()){
			case AbstractInsnNode.JUMP_INSN:
			{
				final JumpInsnNode jumpInsn = (JumpInsnNode) insn;
				
				if (jumpInsn.getOpcode() == Opcodes.GOTO || jumpInsn.getOpcode() == Opcodes.JSR){	//There is no fall-through for goto and jsr
					cfg.addEdge(bbs[i].getLabel(), jumpInsn.label, "");
					break;
				}
				
				cfg.addEdge(bbs[i].getLabel(), jumpInsn.label, "T");
				cfg.addEdge(bbs[i].getLabel(), insn.getNext(), "T");
				break;
			}
			case AbstractInsnNode.TABLESWITCH_INSN:
			{
				final TableSwitchInsnNode switchInsn = (TableSwitchInsnNode) insn; 
				List<LabelNode> labels = switchInsn.labels;
				for (LabelNode labelNode : labels) {
					cfg.addEdge(bbs[i].getLabel(), labelNode, null);
				}
				cfg.addEdge(bbs[i].getLabel(), switchInsn.dflt, null);
				break;
			}
			case AbstractInsnNode.LOOKUPSWITCH_INSN:
			{
				
				final LookupSwitchInsnNode switchInsn = (LookupSwitchInsnNode) insn; 
				List<LabelNode> labels = switchInsn.labels;
				for (LabelNode labelNode : labels) {
					cfg.addEdge(bbs[i].getLabel(), labelNode, null);
				}
				cfg.addEdge(bbs[i].getLabel(), switchInsn.dflt, null);
				break;
			}
			default:   //this should be a fall-through instruction that is followed by a label
			{
				if (insn.getNext() != null)			//a return statement could occur as the last bytecode without any follow up
					cfg.addEdge(bbs[i].getLabel(), insn.getNext(), "");
			}
			}
			
			// We do a conservative analysis over PEIs such that every PEI introduces 
			// a new edge in CFG to any catch block whose scope is valid for this insn.
			if (BytecodeInfo.isPEI(insn) && insn.getNext() != null){
				List<TryCatchBlockNode> nodes = mn.tryCatchBlocks;
				BasicBlock<AbstractInsnNode> container = getBlockForInstruction(cfg, mn, insn);
				for (TryCatchBlockNode tcbn : nodes){
					if (instructions.indexOf(tcbn.start) <= instructions.indexOf(insn) && instructions.indexOf(tcbn.end) > instructions.indexOf(insn)){
						cfg.addEdge(container.getLabel(), tcbn.handler, "EX");
						cfg.addEdge(container.getLabel(), insn.getNext(), null);
					}
				}
				cfg.addEdge(container.getLabel(), end.getLabel(), "EX");
				cfg.addEdge(container.getLabel(), insn.getNext(), null);
			}
			
		}
		
	}
	
	private Graph<AbstractInsnNode, String> findPostDomTree(ControlFlowGraph<AbstractInsnNode, String> cfg){
		Graph<AbstractInsnNode, String> revGraph = cfg.reverse();
		revGraph.setRoot(cfg.getExit());
//		System.out.println("The reversed graph is:");
//		System.out.println(revGraph);
		
		
		Graph<AbstractInsnNode, String> postDom = revGraph.findDominatorsGraph();
//		System.out.println("PostDom tree is:");
//		System.out.println(postDom);
//		try {
//			View<AbstractInsnNode,String> view = new DotGraphView<>("POSTDOM.dot", postDom);
//			view.update();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		return postDom;
	}
	
	public Graph<AbstractInsnNode, String> findControlDependenceGraph(Graph<AbstractInsnNode, String> cfg, Graph<AbstractInsnNode, String> postDom){
		ControlDependencyGraph<AbstractInsnNode, String> cdgGraph = new CDGUsingPostDom<>(cfg, postDom);
		Graph<AbstractInsnNode, String> cdg = cdgGraph.findCDG();
		return cdg;
	}
	
	
	private void instrument(final Graph<AbstractInsnNode,String> cdg, MethodNode mn, LocalVariableTable localTable){
	InsnList list = mn.instructions;
	
	for (AbstractInsnNode insn = list.getFirst(); insn != list.getLast() ; ){
		BasicBlock<AbstractInsnNode> bb = getBlockForInstruction(cdg, mn, insn);
		
		AbstractInsnNode next = insn.getNext();
		
		if (bb == null)	{	//This is an instruction that is caused by instrumentation and doesn't reside in any basic block of the original program
			insn = next;
			continue;
		}

		if (insn == bb.getStart()){
			InsnList newInsnList = new InsnList();
			newInsnList.add(new LdcInsnNode(bb.getID()));
			newInsnList.add(new LdcInsnNode(mn.name));
			addDominators(cdg, bb, newInsnList, localTable);
			newInsnList.add(new MethodInsnNode(Opcodes.INVOKESTATIC, 
					"ch/usi/inf/sape/tracer/analyzer/Analyzer", 
					"enter", "(ILjava/lang/String;[I)V", false));
			mn.instructions.insert(insn, newInsnList);
		}
		
		insn = next;
	}
	
}

	private BasicBlock<AbstractInsnNode> getBlockForInstruction(Graph<AbstractInsnNode,String> cfg, MethodNode mn, AbstractInsnNode insn){
		for (AbstractInsnNode v : cfg.getVertices()){
			BasicBlock<AbstractInsnNode> bb = (BasicBlock<AbstractInsnNode>)cfg.getVertex(v).getVertexContents();
			if (mn.instructions.indexOf(bb.getStart()) > mn.instructions.indexOf(insn)){
				continue;
			}else if (mn.instructions.indexOf(bb.getEnd()) < mn.instructions.indexOf(insn)){
				continue;
			}else
				return bb;
		}
		return null;
	}

	/**
	 * Make an array to store the id of the basic blocks to which this basic block
	 * is control dependent.
	 * 
	 * @param dominators
	 * @param patch
	 * @param localTable
	 * @return
	 */
	private void addDominators(Graph<AbstractInsnNode, String> cdg, BasicBlock<AbstractInsnNode> bb, InsnList patch, LocalVariableTable localTable){ 
		Set<AbstractInsnNode> dominators = cdg.getPredecessors(bb.getLabel());
		patch.add(new IntInsnNode(Opcodes.BIPUSH, dominators.size()));
		patch.add(new IntInsnNode(Opcodes.NEWARRAY, Opcodes.T_INT));
		int arrayLocalIndex = localTable.add(Type.OBJECT);
		patch.add(new VarInsnNode(Opcodes.ASTORE, arrayLocalIndex));

		int j = 0;
		for (AbstractInsnNode dominator : dominators) {
			patch.add(new VarInsnNode(Opcodes.ALOAD, arrayLocalIndex));
			patch.add(new IntInsnNode(Opcodes.BIPUSH, j));
			patch.add(new LdcInsnNode(cdg.getVertex(dominator).getVertexContents().getID()));
			patch.add(new InsnNode(Opcodes.IASTORE));
			j++;
		}

		patch.add(new VarInsnNode(Opcodes.ALOAD, arrayLocalIndex));    // load arg array
	}
	
	private String findLocalDetails(ClassNode cn, MethodNode mn){
		StringBuffer localDetails = new StringBuffer();
		List<LocalVariableNode> locals = (List<LocalVariableNode>) mn.localVariables;
		
		logln("Method:\t"+ cn.name+"."+mn.name+mn.desc+"\tmaxLocals:\t" + mn.maxLocals+"\tmaxStack:\t"+mn.maxStack, false);
		if (locals != null){
			for (LocalVariableNode localVariableNode : locals) {
				localDetails.append(localVariableNode.name).append(",");
				localDetails.append(localVariableNode.desc).append(",");
				localDetails.append(localVariableNode.index).append(",");
				localDetails.append(mn.instructions.indexOf(localVariableNode.start)).append(",");
				localDetails.append(mn.instructions.indexOf(localVariableNode.end)).append(",");

				logln("name:\t" +localVariableNode.name, false);
				logln("desc:\t" +localVariableNode.desc, false);
				logln("index:\t" +localVariableNode.index, false);
				logln("start:\t" +mn.instructions.indexOf(localVariableNode.start), false);
				logln("end:\t" +mn.instructions.indexOf(localVariableNode.end) , false);
				logln("****************", false);
			}
		}
		return localDetails.toString();
	}
	
	//TODO: remove this method. Used only for defects4j benchmarks
	private boolean isUninstrumentable(String className){
		if (className.equals("org/apache/commons/lang3/text/translate/EntityArrays") ||
				className.equals("org/apache/commons/math/util/FastMathLiteralArrays") ||
				className.equals("org/apache/commons/lang/Entities") ||
				className.equals("org/apache/commons/math3/exception/util/LocalizedFormats") ||
				className.equals("com/google/javascript/jscomp/mozilla/rhino/Interpreter") ||
				className.equals("org/apache/commons/math3/dfp/DfpTest") ||
				className.equals("com/google/javascript/rhino/head/Interpreter") ||
				className.equals("org/apache/commons/math3/util/FastMathLiteralArrays") ||
				className.equals("org/apache/commons/math/util/LocalizedFormats") ||
				className.equals("apple/laf/JRSUIControl") ||
				className.equals("org/json/JSONTokener") ||
				className.equals("org/apache/commons/math/exception/util/LocalizedFormats") ||
				className.equals("org/apache/commons/math/dfp/DfpTest") 
				)
			return true;
		
		return false;
	}
}
