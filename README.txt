The project is a detailed information flow tracer for Java programs. It traces all the execution steps at runtime
and the information manipulated by those steps. It instruments all Java bytecode instructions to track which memory 
locations they read from and write to. In order to make the trace more understandable for the developers, the system
can abstract the bytecode traces to make source-code-like traces. Having the full trace of the program, the framework 
can perform slicing and abstraction/deabstraction operations on each single bytecode/abstract operation. The framework
provides a query API for the programmer to query different aspect of an execution trace.   


This Eclipse project includes a Java agent (agent-src), an analyzer (analyzer-src), and an example application (Demo).
The agent instruments the application when it's loaded, and the instrumentation calls into the Analyzer. 

1- Import the "InformationFlowTracer" project into Eclipse and build it.
2- Use Ant with the build.xml to build the agent and run the tracer on the example application. For this, you need to use the
ant target "make-agent-jar" and "run-app" respectively. The example application class is called "Demo". If everything goes 
smoothly, the framework should run the application, generate the logs for bytecode operations and abstract operations and be
blocked for incoming RMI requests. 
3- Clone the TracerWebView application from the repository and import it into Eclipse. 
4- As for hosting the web application, we can use any servlet container, e.g.: Apache Tomcat. Download and unzip the Tomcat 
in your desired path. 

6- Open the build.properties file in the root folder of the web application and update the value for the key appserver.home 
to point to the address of the root folder of your Tomcat copy. Run Ant on the build.xml file defined for the web application 
with the ant target set to "deploy".
 
7- In this step, we need to create the stub/skeleton files for the remote object. We can use rmic for this purpose. Going back 
to the root folder of the InformationFlowTracer project, first export the path to the root folder of your tomcat as a new environment 
variable called CATALINA_HOME, i.e.:
> export CATALINA_HOME= <The path to the root folder of your Tomcat copy>

Then, within the same shell, run "stub-generator.sh" script to create the stub file. It also copies the stub file to the appropriate
path in the deployed application.

8- So far, we have fully deployed the application on the server and we can start the application server at this step. Run the 
script startup.sh in the following address to start tomcat: $CATALINA_HOME/bin/startup.sh

9- Go to this address to see the table: http://localhost:8080/tracer/bclist.jsp